! function(t) { var e = {};

    function i(n) { if (e[n]) return e[n].exports; var r = e[n] = { i: n, l: !1, exports: {} }; return t[n].call(r.exports, r, r.exports, i), r.l = !0, r.exports }
    i.m = t, i.c = e, i.d = function(t, e, n) { i.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: n }) }, i.r = function(t) { "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(t, "__esModule", { value: !0 }) }, i.t = function(t, e) { if (1 & e && (t = i(t)), 8 & e) return t; if (4 & e && "object" == typeof t && t && t.__esModule) return t; var n = Object.create(null); if (i.r(n), Object.defineProperty(n, "default", { enumerable: !0, value: t }), 2 & e && "string" != typeof t)
            for (var r in t) i.d(n, r, function(e) { return t[e] }.bind(null, r)); return n }, i.n = function(t) { var e = t && t.__esModule ? function() { return t.default } : function() { return t }; return i.d(e, "a", e), e }, i.o = function(t, e) { return Object.prototype.hasOwnProperty.call(t, e) }, i.p = "", i(i.s = 58) }([function(t, e, i) { "use strict"; var n = i(4).a.Symbol;
    e.a = n }, function(t, e, i) { "use strict";

    function n(t, e) { for (var i = 0; i < e.length; i++) { var n = e[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n) } } var r = function() {
        function t() {! function(t, e) { if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function") }(this, t), this.queries = {} } return function(t, e, i) { e && n(t.prototype, e), i && n(t, i) }(t, [{ key: "wrapCallback", value: function(t) { return function(e) { e.matches && t.call(window) } } }, { key: "add", value: function(t, e) { var i = !(arguments.length > 2 && void 0 !== arguments[2]) || arguments[2],
                    n = Math.random().toString(),
                    r = window.matchMedia(t),
                    s = this.wrapCallback(e); return r.addListener(s), i && s(r), this.queries[n] = { media: r, func: s }, n } }, { key: "remove", value: function(t) { var e = this.queries[t],
                    i = e.media,
                    n = e.func;
                i.removeListener(n) } }, { key: "handler", value: function() {} }]), t }();
    e.a = new r }, function(t, e, i) { "use strict";
    e.a = function(t) { return null != t && "object" == typeof t } }, function(t, e, i) { "use strict"; var n = i(0),
        r = Object.prototype,
        s = r.hasOwnProperty,
        a = r.toString,
        o = n.a ? n.a.toStringTag : void 0; var u = function(t) { var e = s.call(t, o),
                i = t[o]; try { t[o] = void 0; var n = !0 } catch (t) {} var r = a.call(t); return n && (e ? t[o] = i : delete t[o]), r },
        c = Object.prototype.toString; var l = function(t) { return c.call(t) },
        h = "[object Null]",
        f = "[object Undefined]",
        v = n.a ? n.a.toStringTag : void 0;
    e.a = function(t) { return null == t ? void 0 === t ? f : h : v && v in Object(t) ? u(t) : l(t) } }, function(t, e, i) { "use strict"; var n = i(11),
        r = "object" == typeof self && self && self.Object === Object && self,
        s = n.a || r || Function("return this")();
    e.a = s }, function(t, e, i) { "use strict";
    e.a = function(t) { var e = typeof t; return null != t && ("object" == e || "function" == e) } }, function(t, e, i) { "use strict"; var n = Array.isArray;
    e.a = n }, function(t, e, i) { "use strict"; var n = i(26),
        r = i(18);
    e.a = function(t) { return null != t && Object(r.a)(t.length) && !Object(n.a)(t) } }, function(t, e, i) { "use strict";
    e.a = function(t, e) { return t === e || t != t && e != e } }, function(t, e, i) { "use strict";
    Object.defineProperty(e, "__esModule", { value: !0 }), e.UIEventObserver = e.eventObserver = void 0; var n = function(t) { return t && t.__esModule ? t : { default: t } }(i(40)); var r = new n.default;
    e.eventObserver = r, e.UIEventObserver = n.default }, function(t, e, i) { "use strict"; var n = i(3),
        r = i(2),
        s = "[object Symbol]";
    e.a = function(t) { return "symbol" == typeof t || Object(r.a)(t) && Object(n.a)(t) == s } }, function(t, e, i) { "use strict";
    (function(t) { var i = "object" == typeof t && t && t.Object === Object && t;
        e.a = i }).call(this, i(22)) }, function(t, e, i) { "use strict"; var n = i(26),
        r = i(4).a["__core-js_shared__"],
        s = function() { var t = /[^.]+$/.exec(r && r.keys && r.keys.IE_PROTO || ""); return t ? "Symbol(src)_1." + t : "" }(); var a = function(t) { return !!s && s in t },
        o = i(5),
        u = i(23),
        c = /^\[object .+?Constructor\]$/,
        l = Function.prototype,
        h = Object.prototype,
        f = l.toString,
        v = h.hasOwnProperty,
        p = RegExp("^" + f.call(v).replace(/[\\^$.*+?()[\]{}|]/g, "\\$&").replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, "$1.*?") + "$"); var d = function(t) { return !(!Object(o.a)(t) || a(t)) && (Object(n.a)(t) ? p : c).test(Object(u.a)(t)) }; var m = function(t, e) { return null == t ? void 0 : t[e] };
    e.a = function(t, e) { var i = m(t, e); return d(i) ? i : void 0 } }, function(t, e, i) { "use strict"; var n = i(12),
        r = function() { try { var t = Object(n.a)(Object, "defineProperty"); return t({}, "", {}), t } catch (t) {} }();
    e.a = r }, , function(t, e, i) { "use strict";
    e.a = function(t) { return t } }, function(t, e, i) { "use strict";
    e.a = function(t, e) { for (var i = -1, n = null == t ? 0 : t.length, r = Array(n); ++i < n;) r[i] = e(t[i], i, t); return r } }, function(t, e, i) { "use strict"; var n = i(8),
        r = i(7),
        s = i(19),
        a = i(5);
    e.a = function(t, e, i) { if (!Object(a.a)(i)) return !1; var o = typeof e; return !!("number" == o ? Object(r.a)(i) && Object(s.a)(e, i.length) : "string" == o && e in i) && Object(n.a)(i[e], t) } }, function(t, e, i) { "use strict"; var n = 9007199254740991;
    e.a = function(t) { return "number" == typeof t && t > -1 && t % 1 == 0 && t <= n } }, function(t, e, i) { "use strict"; var n = 9007199254740991,
        r = /^(?:0|[1-9]\d*)$/;
    e.a = function(t, e) { var i = typeof t; return !!(e = null == e ? n : e) && ("number" == i || "symbol" != i && r.test(t)) && t > -1 && t % 1 == 0 && t < e } }, function(t, e, i) { "use strict"; var n = i(15),
        r = i(27),
        s = Math.max; var a = function(t, e, i) { return e = s(void 0 === e ? t.length - 1 : e, 0),
            function() { for (var n = arguments, a = -1, o = s(n.length - e, 0), u = Array(o); ++a < o;) u[a] = n[e + a];
                a = -1; for (var c = Array(e + 1); ++a < e;) c[a] = n[a]; return c[e] = i(u), Object(r.a)(t, this, c) } }; var o = function(t) { return function() { return t } },
        u = i(13),
        c = u.a ? function(t, e) { return Object(u.a)(t, "toString", { configurable: !0, enumerable: !1, value: o(e), writable: !0 }) } : n.a,
        l = 800,
        h = 16,
        f = Date.now; var v = function(t) { var e = 0,
            i = 0; return function() { var n = f(),
                r = h - (n - i); if (i = n, r > 0) { if (++e >= l) return arguments[0] } else e = 0; return t.apply(void 0, arguments) } }(c);
    e.a = function(t, e) { return v(a(t, e, n.a), t + "") } }, function(t, e, i) { "use strict"; var n = i(0),
        r = i(16),
        s = i(6),
        a = i(10),
        o = 1 / 0,
        u = n.a ? n.a.prototype : void 0,
        c = u ? u.toString : void 0; var l = function t(e) { if ("string" == typeof e) return e; if (Object(s.a)(e)) return Object(r.a)(e, t) + ""; if (Object(a.a)(e)) return c ? c.call(e) : ""; var i = e + ""; return "0" == i && 1 / e == -o ? "-0" : i };
    e.a = function(t) { return null == t ? "" : l(t) } }, function(t, e) { var i;
    i = function() { return this }(); try { i = i || Function("return this")() || (0, eval)("this") } catch (t) { "object" == typeof window && (i = window) }
    t.exports = i }, function(t, e, i) { "use strict"; var n = Function.prototype.toString;
    e.a = function(t) { if (null != t) { try { return n.call(t) } catch (t) {} try { return t + "" } catch (t) {} } return "" } }, function(t, e, i) { "use strict"; var n = function(t) { return function(e) { return null == t ? void 0 : t[e] } }({ "&": "&amp;", "<": "&lt;", ">": "&gt;", '"': "&quot;", "'": "&#39;" }),
        r = i(21),
        s = /[&<>"']/g,
        a = RegExp(s.source); var o = function(t) { return (t = Object(r.a)(t)) && a.test(t) ? t.replace(s, n) : t },
        u = { escape: /<%-([\s\S]+?)%>/g, evaluate: /<%([\s\S]+?)%>/g, interpolate: i(25).a, variable: "", imports: { _: { escape: o } } };
    e.a = u }, function(t, e, i) { "use strict";
    e.a = /<%=([\s\S]+?)%>/g }, function(t, e, i) { "use strict"; var n = i(3),
        r = i(5),
        s = "[object AsyncFunction]",
        a = "[object Function]",
        o = "[object GeneratorFunction]",
        u = "[object Proxy]";
    e.a = function(t) { if (!Object(r.a)(t)) return !1; var e = Object(n.a)(t); return e == a || e == o || e == s || e == u } }, function(t, e, i) { "use strict";
    e.a = function(t, e, i) { switch (i.length) {
            case 0:
                return t.call(e);
            case 1:
                return t.call(e, i[0]);
            case 2:
                return t.call(e, i[0], i[1]);
            case 3:
                return t.call(e, i[0], i[1], i[2]) } return t.apply(e, i) } }, function(t, e, i) { "use strict"; var n = Object.prototype;
    e.a = function(t) { var e = t && t.constructor; return t === ("function" == typeof e && e.prototype || n) } }, function(t, e, i) { "use strict";
    e.a = function(t, e) { return function(i) { return t(e(i)) } } }, function(t, e, i) { "use strict"; var n = i(31),
        r = i(28),
        s = i(29),
        a = Object(s.a)(Object.keys, Object),
        o = Object.prototype.hasOwnProperty; var u = function(t) { if (!Object(r.a)(t)) return a(t); var e = []; for (var i in Object(t)) o.call(t, i) && "constructor" != i && e.push(i); return e },
        c = i(7);
    e.a = function(t) { return Object(c.a)(t) ? Object(n.a)(t) : u(t) } }, function(t, e, i) { "use strict"; var n = function(t, e) { for (var i = -1, n = Array(t); ++i < t;) n[i] = e(i); return n },
        r = i(32),
        s = i(6),
        a = i(34),
        o = i(19),
        u = i(39),
        c = Object.prototype.hasOwnProperty;
    e.a = function(t, e) { var i = Object(s.a)(t),
            l = !i && Object(r.a)(t),
            h = !i && !l && Object(a.a)(t),
            f = !i && !l && !h && Object(u.a)(t),
            v = i || l || h || f,
            p = v ? n(t.length, String) : [],
            d = p.length; for (var m in t) !e && !c.call(t, m) || v && ("length" == m || h && ("offset" == m || "parent" == m) || f && ("buffer" == m || "byteLength" == m || "byteOffset" == m) || Object(o.a)(m, d)) || p.push(m); return p } }, function(t, e, i) { "use strict"; var n = i(3),
        r = i(2),
        s = "[object Arguments]"; var a = function(t) { return Object(r.a)(t) && Object(n.a)(t) == s },
        o = Object.prototype,
        u = o.hasOwnProperty,
        c = o.propertyIsEnumerable,
        l = a(function() { return arguments }()) ? a : function(t) { return Object(r.a)(t) && u.call(t, "callee") && !c.call(t, "callee") };
    e.a = l }, function(t, e, i) { "use strict";
    t.exports = i(45).polyfill() }, function(t, e, i) { "use strict";
    (function(t) { var n = i(4),
            r = i(49),
            s = "object" == typeof exports && exports && !exports.nodeType && exports,
            a = s && "object" == typeof t && t && !t.nodeType && t,
            o = a && a.exports === s ? n.a.Buffer : void 0,
            u = (o ? o.isBuffer : void 0) || r.a;
        e.a = u }).call(this, i(48)(t)) }, function(t, e, i) { "use strict";
    (function(t) { var n = i(11),
            r = "object" == typeof exports && exports && !exports.nodeType && exports,
            s = r && "object" == typeof t && t && !t.nodeType && t,
            a = s && s.exports === r && n.a.process,
            o = function() { try { var t = s && s.require && s.require("util").types; return t || a && a.binding && a.binding("util") } catch (t) {} }();
        e.a = o }).call(this, i(48)(t)) }, , function(t, e, i) { "use strict"; var n = i(13); var r = function(t, e, i) { "__proto__" == e && n.a ? Object(n.a)(t, e, { configurable: !0, enumerable: !0, value: i, writable: !0 }) : t[e] = i },
        s = i(8),
        a = Object.prototype.hasOwnProperty; var o = function(t, e, i) { var n = t[e];
        a.call(t, e) && Object(s.a)(n, i) && (void 0 !== i || e in t) || r(t, e, i) }; var u = function(t, e, i, n) { var s = !i;
            i || (i = {}); for (var a = -1, u = e.length; ++a < u;) { var c = e[a],
                    l = n ? n(i[c], t[c], c, i, t) : void 0;
                void 0 === l && (l = t[c]), s ? r(i, c, l) : o(i, c, l) } return i },
        c = i(20),
        l = i(17); var h = function(t) { return Object(c.a)(function(e, i) { var n = -1,
                    r = i.length,
                    s = r > 1 ? i[r - 1] : void 0,
                    a = r > 2 ? i[2] : void 0; for (s = t.length > 3 && "function" == typeof s ? (r--, s) : void 0, a && Object(l.a)(i[0], i[1], a) && (s = r < 3 ? void 0 : s, r = 1), e = Object(e); ++n < r;) { var o = i[n];
                    o && t(e, o, n, s) } return e }) },
        f = i(31),
        v = i(5),
        p = i(28); var d = function(t) { var e = []; if (null != t)
                for (var i in Object(t)) e.push(i); return e },
        m = Object.prototype.hasOwnProperty; var y = function(t) { if (!Object(v.a)(t)) return d(t); var e = Object(p.a)(t),
                i = []; for (var n in t)("constructor" != n || !e && m.call(t, n)) && i.push(n); return i },
        g = i(7); var b = function(t) { return Object(g.a)(t) ? Object(f.a)(t, !0) : y(t) },
        $ = h(function(t, e, i, n) { u(e, b(e), t, n) }),
        w = i(27),
        j = i(3),
        _ = i(2),
        O = i(29),
        k = Object(O.a)(Object.getPrototypeOf, Object),
        x = "[object Object]",
        T = Function.prototype,
        S = Object.prototype,
        I = T.toString,
        L = S.hasOwnProperty,
        C = I.call(Object); var M = function(t) { if (!Object(_.a)(t) || Object(j.a)(t) != x) return !1; var e = k(t); if (null === e) return !0; var i = L.call(e, "constructor") && e.constructor; return "function" == typeof i && i instanceof i && I.call(i) == C },
        A = "[object DOMException]",
        E = "[object Error]"; var P = function(t) { if (!Object(_.a)(t)) return !1; var e = Object(j.a)(t); return e == E || e == A || "string" == typeof t.message && "string" == typeof t.name && !M(t) },
        V = Object(c.a)(function(t, e) { try { return Object(w.a)(t, void 0, e) } catch (t) { return P(t) ? t : new Error(t) } }),
        z = i(16); var D = function(t, e) { return Object(z.a)(e, function(e) { return t[e] }) },
        W = Object.prototype,
        q = W.hasOwnProperty; var F = function(t, e, i, n) { return void 0 === t || Object(s.a)(t, W[i]) && !q.call(n, i) ? e : t },
        N = { "\\": "\\", "'": "'", "\n": "n", "\r": "r", "\u2028": "u2028", "\u2029": "u2029" }; var H = function(t) { return "\\" + N[t] },
        U = i(30),
        B = i(25),
        R = i(24),
        X = i(21),
        Y = /\b__p \+= '';/g,
        K = /\b(__p \+=) '' \+/g,
        G = /(__e\(.*?\)|\b__t\)) \+\n'';/g,
        J = /\$\{([^\\}]*(?:\\.[^\\}]*)*)\}/g,
        Q = /($^)/,
        Z = /['\n\r\u2028\u2029\\]/g;
    e.a = function(t, e, i) { var n = R.a.imports._.templateSettings || R.a;
        i && Object(l.a)(t, e, i) && (e = void 0), t = Object(X.a)(t), e = $({}, e, n, F); var r, s, a = $({}, e.imports, n.imports, F),
            o = Object(U.a)(a),
            u = D(a, o),
            c = 0,
            h = e.interpolate || Q,
            f = "__p += '",
            v = RegExp((e.escape || Q).source + "|" + h.source + "|" + (h === B.a ? J : Q).source + "|" + (e.evaluate || Q).source + "|$", "g"),
            p = "sourceURL" in e ? "//# sourceURL=" + e.sourceURL + "\n" : "";
        t.replace(v, function(e, i, n, a, o, u) { return n || (n = a), f += t.slice(c, u).replace(Z, H), i && (r = !0, f += "' +\n__e(" + i + ") +\n'"), o && (s = !0, f += "';\n" + o + ";\n__p += '"), n && (f += "' +\n((__t = (" + n + ")) == null ? '' : __t) +\n'"), c = u + e.length, e }), f += "';\n"; var d = e.variable;
        d || (f = "with (obj) {\n" + f + "\n}\n"), f = (s ? f.replace(Y, "") : f).replace(K, "$1").replace(G, "$1;"), f = "function(" + (d || "obj") + ") {\n" + (d ? "" : "obj || (obj = {});\n") + "var __t, __p = ''" + (r ? ", __e = _.escape" : "") + (s ? ", __j = Array.prototype.join;\nfunction print() { __p += __j.call(arguments, '') }\n" : ";\n") + f + "return __p\n}"; var m = V(function() { return Function(o, p + "return " + f).apply(void 0, u) }); if (m.source = f, P(m)) throw m; return m } }, function(t, e, i) { "use strict";
    e.a = function(t) { return function(e) { return t(e) } } }, function(t, e, i) { "use strict"; var n = i(3),
        r = i(18),
        s = i(2),
        a = {};
    a["[object Float32Array]"] = a["[object Float64Array]"] = a["[object Int8Array]"] = a["[object Int16Array]"] = a["[object Int32Array]"] = a["[object Uint8Array]"] = a["[object Uint8ClampedArray]"] = a["[object Uint16Array]"] = a["[object Uint32Array]"] = !0, a["[object Arguments]"] = a["[object Array]"] = a["[object ArrayBuffer]"] = a["[object Boolean]"] = a["[object DataView]"] = a["[object Date]"] = a["[object Error]"] = a["[object Function]"] = a["[object Map]"] = a["[object Number]"] = a["[object Object]"] = a["[object RegExp]"] = a["[object Set]"] = a["[object String]"] = a["[object WeakMap]"] = !1; var o = function(t) { return Object(s.a)(t) && Object(r.a)(t.length) && !!a[Object(n.a)(t)] },
        u = i(38),
        c = i(35),
        l = c.a && c.a.isTypedArray,
        h = l ? Object(u.a)(l) : o;
    e.a = h }, function(t, e, i) { "use strict";
    Object.defineProperty(e, "__esModule", { value: !0 }); var n = function() {
            function t(t, e) { for (var i = 0; i < e.length; i++) { var n = e[i];
                    n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n) } } return function(e, i, n) { return i && t(e.prototype, i), n && t(e, n), e } }(),
        r = a(i(41)),
        s = a(i(43));

    function a(t) { return t && t.__esModule ? t : { default: t } } var o = function() {
        function t() {! function(t, e) { if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function") }(this, t), this._eventTargetMap = new s.default, this._eventHandlerMap = new s.default } return n(t, [{ key: "subscribe", value: function(t, e, i) { var n = this,
                    r = this._eventTargetMap.get([e, t]); if (!r) { r = this._createEventEmitter(r), this._eventTargetMap.set([e, t], r); var s = function(t) { r.emit(t) };
                    this._eventHandlerMap.set([e, t], s), t.addEventListener(e, s) } return r.on(i),
                    function() { n.unsubscribe(t, e, i) } } }, { key: "subscribeOnce", value: function(t, e, i) { var n = this; return this.subscribe(t, e, function r(s) { i(s), n.unsubscribe(t, e, r) }) } }, { key: "unsubscribe", value: function(t, e, i) { var n = this._eventTargetMap.get([e, t]);
                n && (n.removeListener(i), t.removeEventListener(e, i), this._eventHandlerMap.delete([e, t])) } }, { key: "unsubscribeAll", value: function() { var t = this;
                this._eventTargetMap.forEach(function(e, i) { var n = t._eventHandlerMap.get([i, e]);
                    n && t.unsubscribe(e, i, n) }) } }, { key: "hasSubscriber", value: function(t, e) { return this._eventHandlerMap.has([e, t]) } }, { key: "_createEventEmitter", value: function(t) { return new r.default(t) } }]), t }();
    e.default = o }, function(t, e, i) { "use strict";
    Object.defineProperty(e, "__esModule", { value: !0 }); var n = function() {
            function t(t, e) { for (var i = 0; i < e.length; i++) { var n = e[i];
                    n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n) } } return function(e, i, n) { return i && t(e.prototype, i), n && t(e, n), e } }(),
        r = function t(e, i, n) { null === e && (e = Function.prototype); var r = Object.getOwnPropertyDescriptor(e, i); if (void 0 === r) { var s = Object.getPrototypeOf(e); return null === s ? void 0 : t(s, i, n) } if ("value" in r) return r.value; var a = r.get; return void 0 !== a ? a.call(n) : void 0 }; var s = i(42),
        a = function(t) {
            function e(t) {! function(t, e) { if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function") }(this, e); var i = function(t, e) { if (!t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); return !e || "object" != typeof e && "function" != typeof e ? t : e }(this, (e.__proto__ || Object.getPrototypeOf(e)).call(this)); return i.eventName = t, i.setMaxListeners(0), i } return function(t, e) { if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function, not " + typeof e);
                t.prototype = Object.create(e && e.prototype, { constructor: { value: t, enumerable: !1, writable: !0, configurable: !0 } }), e && (Object.setPrototypeOf ? Object.setPrototypeOf(t, e) : t.__proto__ = e) }(e, s), n(e, [{ key: "on", value: function(t) { r(e.prototype.__proto__ || Object.getPrototypeOf(e.prototype), "on", this).call(this, this.eventName, t) } }, { key: "emit", value: function(t) { r(e.prototype.__proto__ || Object.getPrototypeOf(e.prototype), "emit", this).call(this, this.eventName, t) } }, { key: "removeListener", value: function(t) { r(e.prototype.__proto__ || Object.getPrototypeOf(e.prototype), "removeListener", this).call(this, this.eventName, t) } }]), e }();
    e.default = a }, function(t, e) {
    function i() { this._events = this._events || {}, this._maxListeners = this._maxListeners || void 0 }

    function n(t) { return "function" == typeof t }

    function r(t) { return "object" == typeof t && null !== t }

    function s(t) { return void 0 === t }
    t.exports = i, i.EventEmitter = i, i.prototype._events = void 0, i.prototype._maxListeners = void 0, i.defaultMaxListeners = 10, i.prototype.setMaxListeners = function(t) { if (! function(t) { return "number" == typeof t }(t) || t < 0 || isNaN(t)) throw TypeError("n must be a positive number"); return this._maxListeners = t, this }, i.prototype.emit = function(t) { var e, i, a, o, u, c; if (this._events || (this._events = {}), "error" === t && (!this._events.error || r(this._events.error) && !this._events.error.length)) { if ((e = arguments[1]) instanceof Error) throw e; var l = new Error('Uncaught, unspecified "error" event. (' + e + ")"); throw l.context = e, l } if (s(i = this._events[t])) return !1; if (n(i)) switch (arguments.length) {
            case 1:
                i.call(this); break;
            case 2:
                i.call(this, arguments[1]); break;
            case 3:
                i.call(this, arguments[1], arguments[2]); break;
            default:
                o = Array.prototype.slice.call(arguments, 1), i.apply(this, o) } else if (r(i))
            for (o = Array.prototype.slice.call(arguments, 1), a = (c = i.slice()).length, u = 0; u < a; u++) c[u].apply(this, o);
        return !0 }, i.prototype.addListener = function(t, e) { var a; if (!n(e)) throw TypeError("listener must be a function"); return this._events || (this._events = {}), this._events.newListener && this.emit("newListener", t, n(e.listener) ? e.listener : e), this._events[t] ? r(this._events[t]) ? this._events[t].push(e) : this._events[t] = [this._events[t], e] : this._events[t] = e, r(this._events[t]) && !this._events[t].warned && (a = s(this._maxListeners) ? i.defaultMaxListeners : this._maxListeners) && a > 0 && this._events[t].length > a && (this._events[t].warned = !0, console.error("(node) warning: possible EventEmitter memory leak detected. %d listeners added. Use emitter.setMaxListeners() to increase limit.", this._events[t].length), "function" == typeof console.trace && console.trace()), this }, i.prototype.on = i.prototype.addListener, i.prototype.once = function(t, e) { if (!n(e)) throw TypeError("listener must be a function"); var i = !1;

        function r() { this.removeListener(t, r), i || (i = !0, e.apply(this, arguments)) } return r.listener = e, this.on(t, r), this }, i.prototype.removeListener = function(t, e) { var i, s, a, o; if (!n(e)) throw TypeError("listener must be a function"); if (!this._events || !this._events[t]) return this; if (a = (i = this._events[t]).length, s = -1, i === e || n(i.listener) && i.listener === e) delete this._events[t], this._events.removeListener && this.emit("removeListener", t, e);
        else if (r(i)) { for (o = a; o-- > 0;)
                if (i[o] === e || i[o].listener && i[o].listener === e) { s = o; break }
            if (s < 0) return this;
            1 === i.length ? (i.length = 0, delete this._events[t]) : i.splice(s, 1), this._events.removeListener && this.emit("removeListener", t, e) } return this }, i.prototype.removeAllListeners = function(t) { var e, i; if (!this._events) return this; if (!this._events.removeListener) return 0 === arguments.length ? this._events = {} : this._events[t] && delete this._events[t], this; if (0 === arguments.length) { for (e in this._events) "removeListener" !== e && this.removeAllListeners(e); return this.removeAllListeners("removeListener"), this._events = {}, this } if (n(i = this._events[t])) this.removeListener(t, i);
        else if (i)
            for (; i.length;) this.removeListener(t, i[i.length - 1]); return delete this._events[t], this }, i.prototype.listeners = function(t) { return this._events && this._events[t] ? n(this._events[t]) ? [this._events[t]] : this._events[t].slice() : [] }, i.prototype.listenerCount = function(t) { if (this._events) { var e = this._events[t]; if (n(e)) return 1; if (e) return e.length } return 0 }, i.listenerCount = function(t, e) { return t.listenerCount(e) } }, function(t, e, i) { "use strict";
    Object.defineProperty(e, "__esModule", { value: !0 }); var n = function() { return function(t, e) { if (Array.isArray(t)) return t; if (Symbol.iterator in Object(t)) return function(t, e) { var i = [],
                        n = !0,
                        r = !1,
                        s = void 0; try { for (var a, o = t[Symbol.iterator](); !(n = (a = o.next()).done) && (i.push(a.value), !e || i.length !== e); n = !0); } catch (t) { r = !0, s = t } finally { try {!n && o.return && o.return() } finally { if (r) throw s } } return i }(t, e); throw new TypeError("Invalid attempt to destructure non-iterable instance") } }(),
        r = function() {
            function t(t, e) { for (var i = 0; i < e.length; i++) { var n = e[i];
                    n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n) } } return function(e, i, n) { return i && t(e.prototype, i), n && t(e, n), e } }(); var s = i(44).MapLike,
        a = function() {
            function t() {! function(t, e) { if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function") }(this, t), this._eventMap = new s } return r(t, [{ key: "set", value: function(t, e) { var i = n(t, 2),
                        r = i[0],
                        s = i[1];
                    this._getTargetMap(r).set(s, e) } }, { key: "get", value: function(t) { var e = n(t, 2),
                        i = e[0],
                        r = e[1]; return this._getTargetMap(i).get(r) } }, { key: "has", value: function(t) { var e = n(t, 2),
                        i = e[0],
                        r = e[1]; return this._getTargetMap(i).has(r) } }, { key: "forEach", value: function(t) { this._eventMap.forEach(function(e, i) { e.forEach(function(e, n) { t(n, i) }) }) } }, { key: "delete", value: function(t) { var e = n(t, 2),
                        i = e[0],
                        r = e[1],
                        s = this._getTargetMap(i);
                    s && s.delete(r) } }, { key: "_getTargetMap", value: function(t) { var e = this._eventMap.get(t); return e || (e = new s, this._eventMap.set(t, e)), e } }]), t }();
    e.default = a }, function(t, e, i) { "use strict";
    Object.defineProperty(e, "__esModule", { value: !0 }); var n = function() {
        function t(t, e) { for (var i = 0; i < e.length; i++) { var n = e[i];
                n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n) } } return function(e, i, n) { return i && t(e.prototype, i), n && t(e, n), e } }(); var r = {};

    function s(t) { return "number" == typeof t && t != t ? r : t }

    function a(t) { return t === r ? NaN : t }
    e.MapLike = function() {
        function t() { var e = this,
                i = arguments.length <= 0 || void 0 === arguments[0] ? [] : arguments[0];! function(t, e) { if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function") }(this, t), this._keys = [], this._values = [], i.forEach(function(t) { if (!Array.isArray(t)) throw new Error("should be `new MapLike([ [key, value] ])`"); if (2 !== t.length) throw new Error("should be `new MapLike([ [key, value] ])`");
                e.set(t[0], t[1]) }) } return n(t, [{ key: "entries", value: function() { var t = this; return this.keys().map(function(e) { var i = t.get(e); return [a(e), i] }) } }, { key: "keys", value: function() { return this._keys.filter(function(t) { return void 0 !== t }).map(a) } }, { key: "values", value: function() { return this._values.slice() } }, { key: "get", value: function(t) { var e = this._keys.indexOf(s(t)); return -1 !== e ? this._values[e] : void 0 } }, { key: "has", value: function(t) { return -1 !== this._keys.indexOf(s(t)) } }, { key: "set", value: function(t, e) { var i = this._keys.indexOf(s(t)); return -1 !== i ? this._values[i] = e : (this._keys.push(s(t)), this._values.push(e)), this } }, { key: "delete", value: function(t) { var e = this._keys.indexOf(s(t)); return -1 !== e && (this._keys.splice(e, 1), this._values.splice(e, 1), !0) } }, { key: "clear", value: function() { return this._keys = [], this._values = [], this } }, { key: "forEach", value: function(t, e) { var i = this;
                this.keys().forEach(function(n) { t(i.get(n), n, e || i) }) } }, { key: "size", get: function() { return this._values.filter(function(t) { return void 0 !== t }).length } }]), t }() }, function(t, e, i) {
    (function(e, i) {
        /*!
         * @overview es6-promise - a tiny implementation of Promises/A+.
         * @copyright Copyright (c) 2014 Yehuda Katz, Tom Dale, Stefan Penner and contributors (Conversion to ES6 API by Jake Archibald)
         * @license   Licensed under MIT license
         *            See https://raw.githubusercontent.com/stefanpenner/es6-promise/master/LICENSE
         * @version   v4.2.5+7f2b526d
         */
        ! function(e, i) { t.exports = i() }(0, function() { "use strict";

            function t(t) { return "function" == typeof t } var n = Array.isArray ? Array.isArray : function(t) { return "[object Array]" === Object.prototype.toString.call(t) },
                r = 0,
                s = void 0,
                a = void 0,
                o = function(t, e) { p[r] = t, p[r + 1] = e, 2 === (r += 2) && (a ? a(d) : m()) }; var u = "undefined" != typeof window ? window : void 0,
                c = u || {},
                l = c.MutationObserver || c.WebKitMutationObserver,
                h = "undefined" == typeof self && void 0 !== e && "[object process]" === {}.toString.call(e),
                f = "undefined" != typeof Uint8ClampedArray && "undefined" != typeof importScripts && "undefined" != typeof MessageChannel;

            function v() { var t = setTimeout; return function() { return t(d, 1) } } var p = new Array(1e3);

            function d() { for (var t = 0; t < r; t += 2) {
                    (0, p[t])(p[t + 1]), p[t] = void 0, p[t + 1] = void 0 }
                r = 0 } var m = void 0;

            function y(t, e) { var i = this,
                    n = new this.constructor($);
                void 0 === n[b] && P(n); var r = i._state; if (r) { var s = arguments[r - 1];
                    o(function() { return A(r, n, s, i._result) }) } else C(i, n, t, e); return n }

            function g(t) { if (t && "object" == typeof t && t.constructor === this) return t; var e = new this($); return T(e, t), e }
            m = h ? function() { return e.nextTick(d) } : l ? function() { var t = 0,
                    e = new l(d),
                    i = document.createTextNode(""); return e.observe(i, { characterData: !0 }),
                    function() { i.data = t = ++t % 2 } }() : f ? function() { var t = new MessageChannel; return t.port1.onmessage = d,
                    function() { return t.port2.postMessage(0) } }() : void 0 === u ? function() { try { var t = Function("return this")().require("vertx"); return void 0 !== (s = t.runOnLoop || t.runOnContext) ? function() { s(d) } : v() } catch (t) { return v() } }() : v(); var b = Math.random().toString(36).substring(2);

            function $() {} var w = void 0,
                j = 1,
                _ = 2,
                O = { error: null };

            function k(t) { try { return t.then } catch (t) { return O.error = t, O } }

            function x(e, i, n) { i.constructor === e.constructor && n === y && i.constructor.resolve === g ? function(t, e) { e._state === j ? I(t, e._result) : e._state === _ ? L(t, e._result) : C(e, void 0, function(e) { return T(t, e) }, function(e) { return L(t, e) }) }(e, i) : n === O ? (L(e, O.error), O.error = null) : void 0 === n ? I(e, i) : t(n) ? function(t, e, i) { o(function(t) { var n = !1,
                            r = function(t, e, i, n) { try { t.call(e, i, n) } catch (t) { return t } }(i, e, function(i) { n || (n = !0, e !== i ? T(t, i) : I(t, i)) }, function(e) { n || (n = !0, L(t, e)) }, t._label);!n && r && (n = !0, L(t, r)) }, t) }(e, i, n) : I(e, i) }

            function T(t, e) { t === e ? L(t, new TypeError("You cannot resolve a promise with itself")) : ! function(t) { var e = typeof t; return null !== t && ("object" === e || "function" === e) }(e) ? I(t, e) : x(t, e, k(e)) }

            function S(t) { t._onerror && t._onerror(t._result), M(t) }

            function I(t, e) { t._state === w && (t._result = e, t._state = j, 0 !== t._subscribers.length && o(M, t)) }

            function L(t, e) { t._state === w && (t._state = _, t._result = e, o(S, t)) }

            function C(t, e, i, n) { var r = t._subscribers,
                    s = r.length;
                t._onerror = null, r[s] = e, r[s + j] = i, r[s + _] = n, 0 === s && t._state && o(M, t) }

            function M(t) { var e = t._subscribers,
                    i = t._state; if (0 !== e.length) { for (var n = void 0, r = void 0, s = t._result, a = 0; a < e.length; a += 3) n = e[a], r = e[a + i], n ? A(i, n, r, s) : r(s);
                    t._subscribers.length = 0 } }

            function A(e, i, n, r) { var s = t(n),
                    a = void 0,
                    o = void 0,
                    u = void 0,
                    c = void 0; if (s) { if ((a = function(t, e) { try { return t(e) } catch (t) { return O.error = t, O } }(n, r)) === O ? (c = !0, o = a.error, a.error = null) : u = !0, i === a) return void L(i, new TypeError("A promises callback cannot return that same promise.")) } else a = r, u = !0;
                i._state !== w || (s && u ? T(i, a) : c ? L(i, o) : e === j ? I(i, a) : e === _ && L(i, a)) } var E = 0;

            function P(t) { t[b] = E++, t._state = void 0, t._result = void 0, t._subscribers = [] } var V = function() {
                function t(t, e) { this._instanceConstructor = t, this.promise = new t($), this.promise[b] || P(this.promise), n(e) ? (this.length = e.length, this._remaining = e.length, this._result = new Array(this.length), 0 === this.length ? I(this.promise, this._result) : (this.length = this.length || 0, this._enumerate(e), 0 === this._remaining && I(this.promise, this._result))) : L(this.promise, new Error("Array Methods must be provided an Array")) } return t.prototype._enumerate = function(t) { for (var e = 0; this._state === w && e < t.length; e++) this._eachEntry(t[e], e) }, t.prototype._eachEntry = function(t, e) { var i = this._instanceConstructor,
                        n = i.resolve; if (n === g) { var r = k(t); if (r === y && t._state !== w) this._settledAt(t._state, e, t._result);
                        else if ("function" != typeof r) this._remaining--, this._result[e] = t;
                        else if (i === z) { var s = new i($);
                            x(s, t, r), this._willSettleAt(s, e) } else this._willSettleAt(new i(function(e) { return e(t) }), e) } else this._willSettleAt(n(t), e) }, t.prototype._settledAt = function(t, e, i) { var n = this.promise;
                    n._state === w && (this._remaining--, t === _ ? L(n, i) : this._result[e] = i), 0 === this._remaining && I(n, this._result) }, t.prototype._willSettleAt = function(t, e) { var i = this;
                    C(t, void 0, function(t) { return i._settledAt(j, e, t) }, function(t) { return i._settledAt(_, e, t) }) }, t }(); var z = function() {
                function e(t) { this[b] = E++, this._result = this._state = void 0, this._subscribers = [], $ !== t && ("function" != typeof t && function() { throw new TypeError("You must pass a resolver function as the first argument to the promise constructor") }(), this instanceof e ? function(t, e) { try { e(function(e) { T(t, e) }, function(e) { L(t, e) }) } catch (e) { L(t, e) } }(this, t) : function() { throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.") }()) } return e.prototype.catch = function(t) { return this.then(null, t) }, e.prototype.finally = function(e) { var i = this.constructor; return t(e) ? this.then(function(t) { return i.resolve(e()).then(function() { return t }) }, function(t) { return i.resolve(e()).then(function() { throw t }) }) : this.then(e, e) }, e }(); return z.prototype.then = y, z.all = function(t) { return new V(this, t).promise }, z.race = function(t) { var e = this; return n(t) ? new e(function(i, n) { for (var r = t.length, s = 0; s < r; s++) e.resolve(t[s]).then(i, n) }) : new e(function(t, e) { return e(new TypeError("You must pass an array to race.")) }) }, z.resolve = g, z.reject = function(t) { var e = new this($); return L(e, t), e }, z._setScheduler = function(t) { a = t }, z._setAsap = function(t) { o = t }, z._asap = o, z.polyfill = function() { var t = void 0; if (void 0 !== i) t = i;
                else if ("undefined" != typeof self) t = self;
                else try { t = Function("return this")() } catch (t) { throw new Error("polyfill failed because global object is unavailable in this environment") }
                var e = t.Promise; if (e) { var n = null; try { n = Object.prototype.toString.call(e.resolve()) } catch (t) {} if ("[object Promise]" === n && !e.cast) return }
                t.Promise = z }, z.Promise = z, z })
    }).call(this, i(46), i(22))
}, function(t, e) { var i, n, r = t.exports = {};

    function s() { throw new Error("setTimeout has not been defined") }

    function a() { throw new Error("clearTimeout has not been defined") }

    function o(t) { if (i === setTimeout) return setTimeout(t, 0); if ((i === s || !i) && setTimeout) return i = setTimeout, setTimeout(t, 0); try { return i(t, 0) } catch (e) { try { return i.call(null, t, 0) } catch (e) { return i.call(this, t, 0) } } }! function() { try { i = "function" == typeof setTimeout ? setTimeout : s } catch (t) { i = s } try { n = "function" == typeof clearTimeout ? clearTimeout : a } catch (t) { n = a } }(); var u, c = [],
        l = !1,
        h = -1;

    function f() { l && u && (l = !1, u.length ? c = u.concat(c) : h = -1, c.length && v()) }

    function v() { if (!l) { var t = o(f);
            l = !0; for (var e = c.length; e;) { for (u = c, c = []; ++h < e;) u && u[h].run();
                h = -1, e = c.length }
            u = null, l = !1,
                function(t) { if (n === clearTimeout) return clearTimeout(t); if ((n === a || !n) && clearTimeout) return n = clearTimeout, clearTimeout(t); try { n(t) } catch (e) { try { return n.call(null, t) } catch (e) { return n.call(this, t) } } }(t) } }

    function p(t, e) { this.fun = t, this.array = e }

    function d() {}
    r.nextTick = function(t) { var e = new Array(arguments.length - 1); if (arguments.length > 1)
            for (var i = 1; i < arguments.length; i++) e[i - 1] = arguments[i];
        c.push(new p(t, e)), 1 !== c.length || l || o(v) }, p.prototype.run = function() { this.fun.apply(null, this.array) }, r.title = "browser", r.browser = !0, r.env = {}, r.argv = [], r.version = "", r.versions = {}, r.on = d, r.addListener = d, r.once = d, r.off = d, r.removeListener = d, r.removeAllListeners = d, r.emit = d, r.prependListener = d, r.prependOnceListener = d, r.listeners = function(t) { return [] }, r.binding = function(t) { throw new Error("process.binding is not supported") }, r.cwd = function() { return "/" }, r.chdir = function(t) { throw new Error("process.chdir is not supported") }, r.umask = function() { return 0 } }, , function(t, e) { t.exports = function(t) { if (!t.webpackPolyfill) { var e = Object.create(t);
            e.children || (e.children = []), Object.defineProperty(e, "loaded", { enumerable: !0, get: function() { return e.l } }), Object.defineProperty(e, "id", { enumerable: !0, get: function() { return e.i } }), Object.defineProperty(e, "exports", { enumerable: !0 }), e.webpackPolyfill = 1 } return e } }, function(t, e, i) { "use strict";
    e.a = function() { return !1 } }, function(t, e, i) { "use strict"; var n = i(1),
        r = (i(33), i(9)),
        s = i(37);

    function a(t, e) { for (var i = 0; i < e.length; i++) { var n = e[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n) } } var o = function() {
        function t(e, i, r, a) { var o = this;! function(t, e) { if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function") }(this, t), this.$window = $(window), this.options = r, this.$slider = $(e), this.$sliderInner = this.$slider.find(".js-slider-inner"), this.$sliderList = this.$slider.find(".js-slider-list"), this.$slides = this.$sliderList.find("li"), this.$slideLinks = this.$slides.find(".js-slider-link"), this.$prev = this.$slider.find(".js-slider-prev"), this.$next = this.$slider.find(".js-slider-next"), this.$controller = this.$slider.find(".js-slider-controller"), this.$controllerIcn = this.$controller.find(".js-slider-controller-icn"), this.$paginationContainer = this.$slider.find(".js-slider-pagination-container"), this.$pagination = null, this.$paginationItem = null; var u = this.$slider.find(".js-slider-pagination-template").html(),
                c = this.$slider.find(".js-slider-pagination-item-template").html();
            this.paginationCompiled = Object(s.a)(u), this.paginationItemComplied = Object(s.a)(c), this.slideSize = this.$slides.length, this.slideWidth = 0, this.direction = { next: 0, prev: 0 }, this.swipeThreshold = 40, this.windowWidth = this.$window.width(), this.offsetLeft = null, this.currentIndex = 0, this.timer = null, this.isTimer = !0, void 0 === a ? this.isTimer = !0 : a || (this.isTimer = !1), this.status = { pause: { alt: "再生", src: this.$controllerIcn[0].src.replace("pause", "play") }, play: { alt: "停止", src: this.$controllerIcn[0].src } }, n.a.add("(min-width:".concat(i + 1, "px)"), function() { o.initLargeView() }), n.a.add("(max-width:".concat(i, "px)"), function() { o.initSmallView() }), this.initStart(), this.bind() } return function(t, e, i) { e && a(t.prototype, e), i && a(t, i) }(t, [{ key: "initStart", value: function() { this.preLoad(), this.calculateWidth(), this.isTimer ? this.startTimer() : this.stopTimer() } }, { key: "preLoad", value: function() {
                (new Image).src = this.status.play.src } }, { key: "initLargeView", value: function() { this.displaySize = this.options.displaySizeLg; var t = this.normalizeIndex(this.slideSize / this.displaySize);
                this.currentIndex = isNaN(t) ? 0 : t, this.pageSize = this.slideSize / this.displaySize, this.createPagination(), this.calculateWidth(), this.margin = Math.abs(parseInt(this.$sliderList.css("margin-left"), 10)) } }, { key: "initSmallView", value: function() { this.displaySize = this.options.displaySizeSm; var t = this.normalizeIndex(this.slideSize / this.displaySize);
                this.currentIndex = isNaN(t) ? 0 : t, this.pageSize = this.slideSize / this.displaySize, this.createPagination(), this.calculateWidth(), this.margin = Math.abs(parseInt(this.$sliderList.css("margin-left"), 10)) } }, { key: "calculateWidth", value: function() { this.windowWidth = this.$window.width(), this.$slides.add(this.$sliderList).removeAttr("style"), this.$sliderList.css("width", "auto"), this.slideWidth = this.$slides.outerWidth(!0), this.$slides.width(this.$slides.width()), this.$sliderList.width(this.slideWidth * this.slideSize) } }, { key: "normalizeIndex", value: function(t) { return Math.floor(this.currentIndex * t / this.pageSize) } }, { key: "createPagination", value: function() { this.$paginationContainer.empty().append(this.paginationCompiled), this.$pagination = this.$paginationContainer.find(".js-slider-pagination"); for (var t = 0; t <= this.pageSize - 1; t++) this.$pagination.append(this.paginationItemComplied);
                this.$paginationItem = this.$slider.find(".js-slider-pagination-item"), this.$paginationItem.eq(this.currentIndex).addClass("is-current") } }, { key: "update", value: function() { var t = this;
                this.isTimer && (this.timer = setTimeout(function() { var e = (t.currentIndex + 1) % t.pageSize;
                    t.slideStart(e) }, 5e3)) } }, { key: "paginationUpdate", value: function(t) { this.$paginationItem.removeClass("is-current").eq(t).addClass("is-current") } }, { key: "getDirection", value: function(t) { return $.map(this.direction, function(e, i) { if (e === t) return i })[0] } }, { key: "slideMove", value: function(t, e) { var i = this; return new Promise(function(n) { e ? (TweenMax.set(i.$sliderList, { marginLeft: -t }), n(!0)) : TweenMax.to(i.$sliderList, .4, { marginLeft: -t, ease: Power3.easeOut, onComplete: function() { n(!0) } }) }) } }, { key: "slideSet", value: function(t) { var e = Math.min(this.direction.prev, this.direction.next),
                    i = this.getDirection(e); "prev" === String(i) ? this.slidePrev(t, e) : "next" === String(i) && this.slideNext(t, e) } }, { key: "slidePrev", value: function(t, e) { var i = this;
                this.$prev.addClass("is-disabled"); for (var n = 0; n < this.slideSize; n++) { var r = t * this.displaySize;
                    r = (r + n) % this.slideSize, this.$sliderList.append(this.$slides.eq(r)) } var s = this.slideWidth * e * this.displaySize + this.margin;
                this.$sliderList.css("margin-left", -s), s = this.margin, this.slideMove(s).then(function() { i.$prev.removeClass("is-disabled"), i.currentIndex = t }) } }, { key: "slideNext", value: function(t, e) { var i = this;
                this.$next.addClass("is-disabled"); var n = this.slideWidth * this.displaySize * e + this.margin;
                this.slideMove(n).then(function() { for (var e = 0; e < i.slideSize; e++) { var n = t * i.displaySize;
                        n = (n + e) % i.slideSize, i.$sliderList.append(i.$slides.eq(n)) }
                    i.$sliderList.css("margin-left", -i.margin), i.$next.removeClass("is-disabled"), i.currentIndex = t }) } }, { key: "slideStart", value: function(t) { var e = Math.max(t, this.currentIndex),
                    i = Math.min(t, this.currentIndex);
                t > this.currentIndex ? (this.direction.next = e - i, this.direction.prev = this.pageSize - this.direction.next) : (this.direction.prev = e - i, this.direction.next = this.pageSize - this.direction.prev), clearTimeout(this.timer), this.update(), this.paginationUpdate(t), this.slideSet(t) } }, { key: "focusStart", value: function(t, e) { var i = this,
                    n = this.slideWidth * this.displaySize * e + this.margin;
                this.offsetLeft && this.$sliderList.offset({ left: this.offsetLeft }), this.paginationUpdate(t), this.slideMove(n, !0).then(function() { i.currentIndex = t }) } }, { key: "stopTimer", value: function() { this.isTimer = !1, clearTimeout(this.timer), this.$controller.removeClass("is-play").addClass("is-pause"), this.$controllerIcn.attr({ alt: this.status.pause.alt, title: this.status.pause.alt, src: this.status.pause.src }) } }, { key: "startTimer", value: function() { this.isTimer = !0, this.update(), this.$controller.removeClass("is-pause").addClass("is-play"), this.$controllerIcn.attr({ alt: this.status.play.alt, title: this.status.play.alt, src: this.status.play.src }) } }, { key: "toggleTimer", value: function() { this.$controller.hasClass("is-pause") ? this.startTimer() : this.stopTimer() } }, { key: "handleEvent", value: function(t) { switch (t.type) {
                    case "touchstart":
                        this.isMove = !1, this.positionX = t.touches[0].clientX, this.offset = parseInt(this.$sliderList.css("margin-left"), 10); break;
                    case "touchmove":
                        this.isMove = !0, this.moveX = t.touches[0].clientX - this.positionX; break;
                    case "touchend":
                        this.moveX < -this.swipeThreshold ? this.$next.triggerHandler("click") : this.moveX > this.swipeThreshold && this.$prev.triggerHandler("click") } } }, { key: "bind", value: function() { var t = this;
                this.$slideLinks.on("focus", function(e) { var i = $(e.target),
                        n = t.$sliderList.find("li").find(".js-slider-link"),
                        r = Math.floor(t.$slideLinks.index(i) / t.displaySize),
                        s = Math.floor(n.index(i) / t.displaySize);
                    t.stopTimer(), t.focusStart(r, s) }), this.$slideLinks.on("blur", function(e) { var i = $(e.target),
                        n = t.$sliderList.find("li").find(".js-slider-link"),
                        r = Math.floor(n.index(i) / t.displaySize),
                        s = t.slideWidth * t.displaySize * r + t.margin;
                    t.offsetLeft = (t.windowWidth - t.$sliderInner.width()) / 2 - s, t.isTimer && t.startTimer() }), this.$slider.on("click", ".js-slider-pagination-item", function(e) { var i = t.$paginationItem.index($(e.target));
                    t.slideStart(i) }), this.$prev.on("click", function() { var e = t.currentIndex - 1;
                    e < 0 && (e = t.pageSize - 1), t.slideStart(e) }), this.$next.on("click", function() { var e = (t.currentIndex + 1) % t.pageSize;
                    t.slideStart(e) }), this.$sliderList.get(0).addEventListener("touchstart", this, !1), this.$sliderList.get(0).addEventListener("touchmove", this, !1), this.$sliderList.get(0).addEventListener("touchend", this, !1), this.$controller.on("click", function() { t.toggleTimer() }), r.eventObserver.subscribe(window, "resize", function() { t.resize() }) } }, { key: "resize", value: function() { this.calculateWidth() } }]), t }();
    e.a = o }, , , , , , , , function(t, e, i) { "use strict";
    i.r(e); var n = i(1),
        r = i(9);

    function s(t, e) { for (var i = 0; i < e.length; i++) { var n = e[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n) } } var a = function() {
            function t(e) { var i = this;! function(t, e) { if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function") }(this, t), this.$window = $(window), this.$winWidth = this.$window.width(), this.$winHeight = this.$window.height(), this.maxWidth = 1920, this.maxHeight = 700, this.$mainVisual = $(".js-main-visual"), this.skipNum = this.$mainVisual.data("skip"), this.$mainVisualImgList = this.$mainVisual.find(".js-main-visual-img-list"), this.$mainVisualImg = this.$mainVisualImgList.find(".js-main-visual-img"), this.$mainVisualBlurList = this.$mainVisual.find(".js-main-visual-blur-list"), this.$mainVisualBlur = this.$mainVisualBlurList.find(".js-main-visual-blur"), this.$mainVisualCopyList = this.$mainVisual.find(".js-main-visual-copy-list"), this.$mainVisualCopy = this.$mainVisualCopyList.find(".js-main-visual-copy"), this.$mainVisualIllustrationList = this.$mainVisual.find(".js-main-visual-illustration-list"), this.$mainVisualSvg = this.$mainVisual.find(".js-main-visual-svg"), this.currentItem = { img: null, blur: null }, this.nextItem = { img: null, blur: null }, this.tweens = [], this.$controller = this.$mainVisual.find(".js-main-visual-controller"), this.$controllerIcn = this.$controller.find(".js-main-visual-controller-icn"), this.currentIndex = 0, this.currentScene = "s0", this.totalItem = this.$mainVisualImg.length, this.imageAspectRatio = null, this.timer = null, this.isTimer = !0, this.status = { pause: { alt: "再生", src: this.$controllerIcn[0].src.replace("pause", "play") }, play: { alt: "停止", src: this.$controllerIcn[0].src } }, n.a.add("(min-width:".concat(e + 1, "px)"), function() { i.initLargeView() }), n.a.add("(max-width:".concat(e, "px)"), function() { i.initSmallView() }), this.initStart(), this.bind(), this.skip(this.skipNum) } return function(t, e, i) { e && s(t.prototype, e), i && s(t, i) }(t, [{ key: "skip", value: function(t) { var e = this;
                    $.each(t, function(t, i) { e.$mainVisualImg.eq(i - 1).remove(), e.$mainVisualBlur.eq(i - 1).remove() }), this.$mainVisualImg = this.$mainVisualImgList.find(".js-main-visual-img"), this.$mainVisualBlur = this.$mainVisualBlurList.find(".js-main-visual-blur"), this.totalItem = this.$mainVisualImg.length, this.setCurrent(this.currentIndex), this.setNext(this.currentIndex + 1), $.each(this.currentItem, function(t, e) { e.addClass("is-active") }), $.each(this.nextItem, function(t, e) { e.addClass("is-next") }); var i = $(this.getCopy(this.currentScene));
                    $.each(i, function(t, e) { e.addClass("is-active") }), this.$mainVisualCopy.each(function(t, i) { e.objectFit($(i).find("img")) }), this.$mainVisualSvg.each(function(t, i) { e.objectFit($(i)) }), this.$mainVisualImgList.css("opacity", 1), this.$mainVisualCopyList.css("opacity", 1), this.$mainVisualIllustrationList.css("opacity", 1), this.$mainVisualBlurList.css("opacity", 1) } }, { key: "getHeight", value: function() { var t = this.$winWidth / this.imageAspectRatio; return t > this.maxHeight ? this.maxHeight : t } }, { key: "setCurrent", value: function(t) { this.currentItem.img = this.$mainVisualImg.eq(t), this.currentItem.blur = this.$mainVisualBlur.eq(t) } }, { key: "setNext", value: function(t) { this.nextItem.img = this.$mainVisualImg.eq(t), this.nextItem.blur = this.$mainVisualBlur.eq(t) } }, { key: "setInitTweens", value: function() { this.tweens = [] } }, { key: "getCopy", value: function(t) { return this.$mainVisualCopy.map(function(e, i) { var n = $(i); if (n.data("scene") === t) return n }) } }, { key: "getIllustration", value: function(t, e) { return t.find(".js-main-visual-illustration").map(function(t, i) { var n = $(i); if (n.data("svg") === e) return n })[0] } }, { key: "getStart", value: function() { return this.$mainVisualImg.eq(0).data("svg") } }, { key: "initStart", value: function() { this.preLoad(), this.$mainVisual.height(this.getHeight()) } }, { key: "svgAnim", value: function() { var t = this; return { s0: function(e) { var i = []; return t.$mainVisualIllustrationList.each(function(n, r) { var s = $.Deferred(),
                                    a = t.getIllustration($(r), e);
                                a.css("opacity", 1); var o = a.find(".js-main-visual-svg-path");
                                t.tweens[n] = TweenMax.staggerFromTo(o, 2, { opacity: 0, y: -30 }, { y: 0, opacity: 1, delay: .5, ease: Elastic.easeOut }, .2, function() { s.resolve() }), i.push(s) }), t.isTimer || t.stopSvgAnim(), $.when.apply(void 0, i).promise() }, s1: function(e) { var i = []; return t.$mainVisualIllustrationList.each(function(n, r) { var s = $.Deferred(),
                                    a = t.getIllustration($(r), e);
                                a.css("opacity", 1); var o = a.find(".js-main-visual-svg-path");
                                t.tweens[n] = TweenMax.staggerFromTo(o, .5, { opacity: 0 }, { opacity: 1 }, .2, function() { s.resolve() }), i.push(s) }), t.isTimer || t.stopSvgAnim(), $.when.apply(void 0, i).promise() }, s2: function(e) { var i = []; return t.$mainVisualIllustrationList.each(function(n, r) { var s = $.Deferred(),
                                    a = t.getIllustration($(r), e);
                                a.css("opacity", 1); var o = a.find(".js-main-visual-svg-path");
                                t.tweens[n] = TweenMax.staggerFromTo(o, 1, { opacity: 0 }, { opacity: 1, ease: Power1.easeInOut }, .4, function() { s.resolve() }), i.push(s) }), t.isTimer || t.stopSvgAnim(), $.when.apply(void 0, i).promise() }, s3: function(e) { var i = t,
                                n = []; return t.$mainVisualIllustrationList.each(function(r, s) { var a = $.Deferred(),
                                    o = t.getIllustration($(s), e);
                                o.css("opacity", 1); var u = o.find(".js-main-visual-svg-path");
                                t.tweens[r] = TweenMax.staggerFromTo(u, 1.4, { scale: .6, opacity: 0, transformOrigin: "50% 50%" }, { scale: 1, opacity: 1, ease: Power1.easeInOut, onComplete: function() { var t = TweenMax.to(this.target, 3, { y: function(t, e) { return Math.floor(Math.random() * (e - t + 1) + t) }(-20, 20), repeat: 3, yoyo: !0, ease: Power0.easeNone, onUpdate: function() { i.isTimer || t.kill() } }) } }, .04, function() { setTimeout(function() { t.isTimer && a.resolve() }, 3e3) }), n.push(a) }), t.isTimer || t.stopSvgAnim(), $.when.apply(void 0, n).promise() }, s4: function(e) { var i = []; return t.$mainVisualIllustrationList.each(function(n, r) { var s = $.Deferred(),
                                    a = t.getIllustration($(r), e);
                                a.css("opacity", 1); var o = a.find(".js-main-visual-svg-path");
                                t.tweens[n] = TweenMax.fromTo(o, .4, { opacity: 1, scale: 0, transformOrigin: "50% 50%" }, { scale: 1, onComplete: function() { setTimeout(function() { s.resolve() }, 1e3) } }), i.push(s) }), t.isTimer || t.stopSvgAnim(), $.when.apply(void 0, i).promise() }, s5: function(e) { var i = []; return t.$mainVisualIllustrationList.each(function(n, r) { var s = $.Deferred(),
                                    a = t.getIllustration($(r), e);
                                a.css("opacity", 1); var o = a.find(".js-main-visual-svg-path");
                                t.tweens[n] = TweenMax.fromTo(o, 1, { opacity: 1, strokeDasharray: 500, strokeDashoffset: 500 }, { strokeDashoffset: 0, ease: Power1.easeOut, onComplete: function() { s.resolve() } }), i.push(s) }), t.isTimer || t.stopSvgAnim(), $.when.apply(void 0, i).promise() }, s6: function(e) { var i = []; return t.$mainVisualIllustrationList.each(function(n, r) { var s = $.Deferred(),
                                    a = t.getIllustration($(r), e);
                                a.css("opacity", 1); var o = a.find(".js-main-visual-svg-path");
                                t.tweens[n] = TweenMax.fromTo(o, 1, { opacity: 1, strokeDasharray: 500, strokeDashoffset: 500 }, { strokeDashoffset: 0, ease: Power1.easeOut, onComplete: function() { s.resolve() } }), i.push(s) }), t.isTimer || t.stopSvgAnim(), $.when.apply(void 0, i).promise() } } } }, { key: "startSvgAnim", value: function() { $.each(this.tweens, function(t, e) { Array.isArray(e) ? $.each(e, function(t, e) { e.play() }) : e.play() }) } }, { key: "stopSvgAnim", value: function() { $.each(this.tweens, function(t, e) { Array.isArray(e) ? $.each(e, function(t, e) { e.pause() }) : e.pause() }) } }, { key: "slideStart", value: function(t) { var e = this;
                    clearTimeout(this.timer); var i = this.$mainVisualImg.eq((this.currentIndex + 1) % this.totalItem).data("scene"); if (this.currentScene !== i) { var n = $(this.getCopy(i)),
                            r = $(this.getCopy(this.currentScene));
                        $.each(n, function(t, e) { e.addClass("is-next") }), TweenMax.to(r, 1, { opacity: 0, ease: Power3.easeOut, onComplete: function() { $.each(r, function(t, e) { e.removeClass("is-active").css("opacity", 1) }), $.each(n, function(t, e) { e.removeClass("is-next").addClass("is-active") }), e.currentScene = i, e.currentScene = i } }) } var s = TweenMax.to(this.$mainVisualImg.eq(this.currentIndex), 1, { opacity: 0, ease: Power3.easeOut, onUpdate: function() { e.isTimer || s.kill() }, onComplete: function() { e.currentItem.img.removeClass("is-active").css("opacity", 1), e.currentItem.img = e.$mainVisualImg.eq(t), e.nextItem.img.removeClass("is-next"), e.nextItem.img = e.$mainVisualImg.eq((t + 1) % e.totalItem), e.currentItem.img.addClass("is-active"), e.nextItem.img.addClass("is-next"), e.currentIndex = t; var i = e.currentItem.img.data("svg");
                                e.svgAnim()[i](i).then(function() { e.setInitTweens(), e.update() }) } }),
                        a = TweenMax.to(this.$mainVisualBlur.eq(this.currentIndex), 1, { opacity: 0, ease: Power3.easeOut, onUpdate: function() { e.isTimer || a.kill() }, onComplete: function() { e.currentItem.blur.removeClass("is-active").css("opacity", 1), e.currentItem.blur = e.$mainVisualBlur.eq(t), e.nextItem.blur.removeClass("is-next"), e.nextItem.blur = e.$mainVisualBlur.eq((t + 1) % e.totalItem), e.currentItem.blur.addClass("is-active"), e.nextItem.blur.addClass("is-next") } }),
                        o = this.$mainVisualImg.eq(this.currentIndex).data("svg");
                    this.$mainVisualIllustrationList.each(function(t, i) { var n = e.getIllustration($(i), o);
                        TweenMax.to(n, 1, { opacity: 0, ease: Power3.easeOut }) }) } }, { key: "preLoad", value: function() {
                    (new Image).src = this.status.play.src } }, { key: "getTimer", value: function() { return this.isTimer } }, { key: "update", value: function() { var t = this;
                    this.isTimer && (this.timer = setTimeout(function() { var e = (t.currentIndex + 1) % t.totalItem;
                        t.slideStart(e) }, 2e3)) } }, { key: "startTimer", value: function() { this.isTimer = !0, this.tweens.length ? this.startSvgAnim() : this.update(), this.$controller.removeClass("is-pause").addClass("is-play"), this.$controllerIcn.attr({ alt: this.status.play.alt, title: this.status.play.alt, src: this.status.play.src }) } }, { key: "stopTimer", value: function() { this.isTimer = !1, clearTimeout(this.timer), this.$controller.removeClass("is-play").addClass("is-pause"), this.$controllerIcn.attr({ alt: this.status.pause.alt, title: this.status.pause.alt, src: this.status.pause.src }), this.stopSvgAnim() } }, { key: "initLargeView", value: function() { this.imageAspectRatio = 1920 / 700 } }, { key: "initSmallView", value: function() { this.imageAspectRatio = 750 / 580 } }, { key: "objectFit", value: function(t) { var e = this.$winWidth > this.maxWidth ? this.maxWidth : this.$winWidth,
                        i = this.$winWidth / this.imageAspectRatio > this.maxHeight ? this.maxHeight : this.$winWidth / this.imageAspectRatio,
                        n = e / 1920,
                        r = i / 700,
                        s = Math.max(n, r),
                        a = 1920 * s,
                        o = 700 * s;
                    t.css({ width: a, height: o, marginLeft: -(a - e) / 2, marginTop: -(o - i) / 2 }) } }, { key: "toggleTimer", value: function() { this.$controller.hasClass("is-pause") ? this.startTimer() : this.stopTimer() } }, { key: "resize", value: function() { var t = this;
                    this.$winWidth = this.$window.width(), this.$winHeight = this.$window.height(), this.$mainVisual.height(this.getHeight()), this.$mainVisualCopy.each(function(e, i) { t.objectFit($(i).find("img")) }), this.$mainVisualSvg.each(function(e, i) { t.objectFit($(i)) }) } }, { key: "bind", value: function() { var t = this;
                    this.$controller.on("click", function() { t.toggleTimer() }), r.eventObserver.subscribe(window, "resize", function() { t.resize() }) } }]), t }(),
        o = i(50);

    function u(t, e) { for (var i = 0; i < e.length; i++) { var n = e[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n) } } var c = function() {
        function t() {! function(t, e) { if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function") }(this, t), this.$window = $(window), this.$body = $("body"), this.$modal = $(".js-olympic"), this.$modalBody = this.$modal.find(".js-olympic-body"), this.$modalDialog = this.$modal.find(".js-olympic-dialog"), this.$modalContent = this.$modal.find(".js-olympic-content"), this.$modalClose = this.$modal.find(".js-olympic-close"), this.focusableElementsString = 'a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, [tabindex="0"], [contenteditable]', this.bind() } return function(t, e, i) { e && u(t.prototype, e), i && u(t, i) }(t, [{ key: "open", value: function() { this.$body.addClass("is-modal-open").addClass("is-fixed"), this.lastFocusedElement = document.activeElement, this.focusableElements = this.$modal[0].querySelectorAll(this.focusableElementsString), this.focusableElements = Array.prototype.slice.call(this.focusableElements), this.firstTabStop = this.focusableElements[0], this.lastTabStop = this.focusableElements[this.focusableElements.length - 1], this.$modal.addClass("is-open"), this.$modalDialog.attr({ tabindex: "0", "aria-modal": !0, "aria-label": "POWER! ひとくちの力" }).focus(), this.resize() } }, { key: "close", value: function() { this.$window.triggerHandler("olympicClose"), this.$modal.attr("aria-hidden", !0).fadeOut(), this.$body.removeClass("is-modal-open").removeClass("is-fixed"), this.lastFocusedElement.focus() } }, { key: "focus", value: function(t) { 9 === t.keyCode && (t.shiftKey ? document.activeElement === this.firstTabStop && (t.preventDefault(), this.lastTabStop.focus()) : document.activeElement === this.lastTabStop && (t.preventDefault(), this.firstTabStop.focus())), 27 === t.keyCode && this.close() } }, { key: "resize", value: function() { this.$modalDialog.removeAttr("style"); var t = this.$modalBody.outerHeight();
                t < this.$modalDialog.outerHeight() && this.$modalDialog.height(t) } }, { key: "bind", value: function() { var t = this;
                this.$modalClose.on("click", function() { t.close() }), this.$modal.on("click", function(e) { var i = $(e.target);
                    (i.hasClass("js-olympic") || i.hasClass("js-olympic-mv") || i.hasClass("js-olympic-body") || i.hasClass("js-olympic-dialog") || i.hasClass("js-olympic-inner")) && t.close() }), this.$modal.on("keydown", function(e) { t.focus(e) }), r.eventObserver.subscribe(window, "resize", function() { t.resize() }) } }]), t }();! function() { var t = $(window),
            e = $(".js-olympic").length,
            i = null,
            n = null,
            r = function() { var t, e = $(window),
                    i = $(".js-scroll-item"),
                    n = [],
                    r = [],
                    s = e.height(),
                    a = -1,
                    o = [];

                function u(t, e) { return { check: function(i, n) { if ((e - n) / i < .85) return t.forEach(function(t, e) { var i = $(t).data("delay");
                                i = i ? e * i : 170 * e, t.style.transitionDelay = "".concat(i, "ms"), $(t).addClass("enter") }), !0; return !1 } } }

                function c() { for (var i = e.scrollTop(), n = e.scrollTop() + s - i, r = t.length; r--;) { t[r].check(n, i) && t.splice(r, 1) } }
                i.each(function(t, e) { var i = $(e);
                    n.push(e), r.push(i.offset().top) }), t = n.reduce(function(t, e, i) { var s = r[i]; return s === a ? (o.push(e), i === n.length - 1 && t.push(u(o, s)), t) : (o.length && t.push(u(o, a)), o = [e], a = s, i === n.length - 1 && 1 === o.length && t.push(u(o, s)), t) }, []), e.on("scroll", function() { c() }), c(), e.on("resize", function() { s = e.height() }) }; if (n = new a(767), $(".js-txt-motion").each(function(t, e) { var i = $(e),
                    n = i.text(),
                    r = i.data("delay");
                i.empty().removeClass("js-txt-motion"); for (var s = n.split(""), a = [], o = 0; o < s.length; o++) a[o] = $('<span class="js-scroll-item" data-delay="'.concat(r, '">').concat(s[o], "</span>")), i.append(a[o]) }), $.when(function() { var t = $.Deferred(),
                    e = $(".js-campaign-area"); return $.get("./campaign/", function(i) { var n = $(i).find(".js-campaign");
                    n.find("li").each(function(t, e) { t >= 6 && $(e).remove() }), e.append(n), t.resolve() }).fail(function() { t.resolve() }), t.promise() }(), function() { var t = $.Deferred(); return $(window).on("load", function() { t.resolve() }), t.promise() }()).then(function() { r() }), e) $(".js-slider").each(function() { this.options = { displaySizeLg: 2, displaySizeSm: 1 }, i = new o.a(this, 767, this.options, !1) }), $(".js-olympic").each(function() {
            (new c).open() }), t.on("olympicClose", function() { var t = n.getStart();
            n.svgAnim()[t](t).then(function() { n.getTimer() && (n.setInitTweens(), n.startTimer()) }), i.startTimer() });
        else { var s = n.getStart();
            n.svgAnim()[s](s).then(function() { n.getTimer() && (n.setInitTweens(), n.startTimer()) }), $(".js-slider").each(function() { this.options = { displaySizeLg: 2, displaySizeSm: 1 }, i = new o.a(this, 767, this.options) }) } }() }]);