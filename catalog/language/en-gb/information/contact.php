<?php
// Heading
$_['heading_title']  = 'CONTACT US';

// Text
$_['text_location']  = 'Our Location';
$_['text_store']     = 'Our Stores';
$_['text_contact']   = 'Contact Form';
$_['text_address']   = 'Address: 3104 Wintersmith Lane, Arlington, Texas, UNITED STATES';
$_['text_sendusamessage']       = 'Send Us A Message';
$_['text_telephone'] = 'Telephone';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Opening Times';
$_['text_comment']   = 'Comments';
$_['text_success']   = '<p>Your enquiry has been successfully sent to the store owner!</p>';
$_['text_googlemap']   = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3357.707034928314!2d-97.07305208444193!3d32.693835180997155!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x864e89e72c3c9ee3%3A0x3f9b83b47135ed80!2sFAROSON!5e0!3m2!1svi!2s!4v1609404758116!5m2!1svi!2s';
$_['text_home'] = 'HOME';
// Entry
$_['entry_name']     = 'Your Name';
$_['entry_email']    = 'E-Mail Address';
$_['entry_enquiry']  = 'Enquiry';

// Email
$_['email_subject']  = 'Enquiry %s';

// Errors
$_['error_name']     = 'Name must be between 3 and 32 characters!';
$_['error_email']    = 'E-Mail Address does not appear to be valid!';
$_['error_enquiry']  = 'Enquiry must be between 10 and 3000 characters!';