<?php
// Heading
$_['heading_title'] = 'オールインワンブログ';

$_['text_trending'] = 'トレンド';
$_['text_latest'] = '最新';
$_['text_featured'] = '注目';
$_['text_popular'] = '人気';

$_['text_empty_trending'] = 'トレンドブログなし';
$_['text_empty_latest'] = '最新のブログはありません';
$_['text_empty_featured'] = '注目のブログはありません';
$_['text_empty_popular'] = '人気のあるブログはありません';

$_['text_readmore'] = '続きを読む';
$_['text_comment'] = 'コメント';