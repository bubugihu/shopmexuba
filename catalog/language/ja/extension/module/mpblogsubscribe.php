<?php
// Text
$_['text_subscribe'] = 'ブログ購読';
$_['text_email'] = 'Eメール';
$_['text_msubscrieb_success'] = '成功：正常にサブスクライブしました！';
$_['text_msubscrieb_verification'] = '確認リンクをクリックしてメールを確認してください。 ％sで確認メールが届きます！ ';
$_['text_msubscrieb_adminapproval'] = '管理者によって承認されます！';
$_['text_munsubscrieb_success'] = '成功：購読解除に成功しました。 購読解除プロセスを完了するには、メールを確認してください！ ';

//ボタン
$_['button_subscribe'] = '購読';
$_['button_unsubscribe'] = '購読解除';

//エラー
$_['error_request_invalid'] = '無効なリクエスト！ ページを更新してください。 ';
$_['error_msubscrieber_exists'] = '購読用の電子メールは既に存在します。';
$_['error_msubscrieber_nonexists'] = '購読解除の電子メールが存在しません。';
$_['error_email'] = '電子メールアドレスが有効ではないようです！';