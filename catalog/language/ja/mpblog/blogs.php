<?php
// Text
$_['heading_title'] = 'ブログ';

$_['text_blog'] = 'ブログ';
$_['text_comment'] = 'コメント';
$_['text_readmore'] = '続きを読む';
$_['text_empty'] = 'ブログが見つかりません！';