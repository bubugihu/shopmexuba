<?php
// Text
$_['heading_title'] = 'サブスクライバーの検証';

$_['text_blog'] = 'ブログ';

$_['text_verification_success'] = 'メールを確認していただきありがとうございます！';
$_['text_url_expire'] = 'URLの有効期限が切れています！';