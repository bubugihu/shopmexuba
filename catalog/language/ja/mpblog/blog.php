<?php
//テキスト
$_['heading_title'] = 'ブログを表示';
$_['heading_give_rating'] = '評価を与える';

$_['text_rating_success'] = '評価ありがとうございます。';
$_['text_comment_success'] = 'コメントありがとうございます。';
$_['text_no_comments'] = 'このブログへのコメントはありません。';

$_['text_comments_default'] = 'コメント';
$_['text_comments_facebook'] = 'フェイスブック';
$_['text_comments_google'] = 'グーグル';
$_['text_comments_disqus'] = 'Disqus';
$_['text_loading'] = '読み込み中...お待ちください！';

$_['text_home'] = 'HOME';
$_['text_readmore'] = '続きを読む';
$_['text_blog'] = 'ブログ';
$_['text_rating'] = '評価';
$_['text_follow'] = 'フォローする';
$_['text_share'] = '共有オン';
$_['text_tags'] = 'タグ';
$_['text_give_rating'] = '評価を与える';
$_['text_not_good'] = 'まったく良くない';
$_['text_good'] = 'とても良い';
$_['text_comments'] = 'コメント';
$_['text_write_comment'] = 'コメントを書く';
$_['text_comment'] = 'コメント';
$_['text_related'] = '関連ブログ';
$_['text_related_products'] = '関連製品';
$_['text_tax'] = '税金';
$_['text_related_categories'] = '関連カテゴリ';
$_['text_login_rating']         = '<a href="%s">ログイン</a> または <a href="%s">登録</a> して評価を付けてください。 ';
$_['text_login_comment']        = 'コメントを追加するには、<a href="%s">ログイン</a> または <a href="%s">登録</a> してください。 ';
$_['text_note']          	    = ' ';
$_['text_error'] = 'ブログが見つかりません！';


//エントリ
$_['entry_name'] = '名前';
$_['entry_rating'] = '評価';
$_['entry_comment'] = 'コメント';

//ボタン

$_['button_give_rating'] = '評価を与える';
$_['button_add_comment'] = 'コメントを追加';

//エラー
$_['error_name'] = '警告：名前は3〜25文字でなければなりません！';
$_['error_text'] = '警告：コメントテキストは25〜1000文字である必要があります！';
$_['error_rating'] = '警告：評価を選択してください！';