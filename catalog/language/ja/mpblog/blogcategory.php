<?php
// Text
$_['text_mpblogpost'] = 'ブログ';
$_['text_error'] = 'ブログカテゴリが見つかりません！';
$_['text_empty'] = 'このブログカテゴリにリストするブログはありません。';
$_['text_readmore'] = '続きを読む';
$_['text_comment'] = 'コメント';
$_['text_categories'] = 'サブカテゴリ';
$_['text_blog'] = 'ブログ';
$_['text_home'] = 'ホームページ';