<?php
//made by bestshop24h,support email:support@bestshop24h.com  or 95672639@qq.com
// Heading
$_['heading_title']  = 'お問合せ';

// Text
$_['text_home']  = 'ホーム';
$_['text_location']  = '所在地';
$_['text_store']     = 'ストア情報';
$_['text_contact']   = 'お問合せフォーム';
$_['text_sendusamessage']       = '私達にメッセージを送ります';
$_['text_address']   = '住所: 4-4-18 Nishitenumi, Kita-ku, Osaka-shi, Osaka, JAPAN.';
$_['text_telephone'] = '電話番号:';
$_['text_fax']       = 'FAX番号:';
$_['text_open']      = '営業時間';
$_['text_comment']   = 'コメント';
$_['text_success']   = '<p>お客様のお問合せメールは当ストア宛に送信されました！</p>';
$_['text_googlemap']   = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1379.1965152701969!2d135.50439818578303!3d34.69759689712607!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6000e6e9c8410249%3A0xc5f684b0842d4bb7!2sNUCOS!5e0!3m2!1svi!2s!4v1609405001817!5m2!1svi!2s';

// Entry
$_['entry_name']     = 'お名前:';
$_['entry_email']    = 'メールアドレス:';
$_['entry_enquiry']  = 'お問合せ内容:';
$_['entry_captcha']  = 'ボックスに表示されている確認コードを入力してください:';

// Email
$_['email_subject']  = 'お問合せ %s';

// Errors
$_['error_name']     = 'お名前は3文字以上32文字以下で入力してください!';
$_['error_email']    = '無効なメールアドレスです!';
$_['error_enquiry']  = 'お問合せ内容は10文字以上3000文字以下で入力してください!';
$_['error_captcha']  = '確認コードが表示されていたものと一致しませんでした。!';
