<?php
//made by bestshop24h,support email:support@bestshop24h.com  or 95672639@qq.com
// Text
$_['text_search']                             = '検索';
$_['text_brand']                              = 'ブランド';
$_['text_manufacturer']                       = 'ブランド:';
$_['text_model']                              = '品番:';
$_['text_reward']                             = 'ポイント:';
$_['text_points']                             = '使用可能ポイント:';
$_['text_stock']                              = '在庫:';
$_['text_instock']                            = '在庫あり';
$_['text_tax']                                = '税別:';
$_['text_discount']                           = ' 点以上ご注文の場合 ';
$_['text_option']                             = 'オプション:';
$_['text_minimum']                            = '(最小ご注文単位 %s)';
$_['text_reviews']                            = '%s レビュー';
$_['text_write']                              = 'レビューを書く';
$_['text_login']                              = 'レビューを投稿するには <a href="%s">ログイン</a> または <a href="%s">ご登録</a> をお願いします';
$_['text_no_reviews']                         = 'この商品のレビューはまだありません';
$_['text_note']                               = ' ';
$_['text_success']                            = 'ご記入頂きましたレビューが送信されました';
$_['text_related']                            = '関連商品';
$_['text_tags']                               = 'タグ:';
$_['text_error']                              = '商品が見つかりません!';
$_['text_payment_recurring']                  = '支払いプロファイル';
$_['text_trial_description']                  = '%sを %d %s毎に%d 回お支払の後、';
$_['text_payment_description']                = '%sを %d %s毎に%d 回のお支払い';
$_['text_payment_cancel'] = 'キャンセルされるまで%sを %d %s毎にお支払い';
$_['text_day']                                = '日';
$_['text_week']                               = '週';
$_['text_semi_month']                         = '半月';
$_['text_month']                              = 'ヶ月';
$_['text_year']                               = '年';
$_['text_location']                = '購入エリアの選択：';
$_['text_location_vn']                = 'ベトナム';
$_['text_location_jp']                = '日本';
$_['text_location_us']                = 'アメリカ';
$_['text_contact_buy']                = '注文のお問い合わせ';
$_['text_contact_form']                = '連絡先情報';
$_['text_contact_name'] = 'フルネーム';
$_['text_contact_mail'] = 'Eメール';
$_['text_contact_content'] = 'コンテンツ';
$_['text_contact_buyproduct'] = 'この製品を注文する必要があります。';
$_['text_contact_send'] = '送信';
$_['text_contact_close'] = '閉じる';
$_['text_caution'] = '同意する';
$_['text_yes'] = '同意する';
$_['text_no'] = '同意しない';
// Entry
$_['entry_qty']                               = '数量';
$_['entry_name']                              = 'お名前 (こちらに入力されたお名前が表示されます) :';
$_['entry_review']                            = 'レビュー内容:';
$_['entry_rating']                            = '評価:';
$_['entry_good']                              = '良い';
$_['entry_bad']                               = '悪い';
$_['entry_captcha']                           = '以下の文字コードを入力してください:';

// Tabs
$_['tab_description']                         = '説明';
$_['tab_attribute']                           = '詳細';
$_['tab_review']                              = 'レビュー (%s)';
$_['code']                                    = 'jp';
// Error
$_['error_name']                              = 'Warning: レビューのお名前は3文字以上25文字以下で入力してください!';
$_['error_text']                              = 'Warning: レビューの内容は25文字以上1000文字以下で入力してください!';
$_['error_rating']                            = 'Warning: 評価を選んでください!';
$_['error_captcha']                           = 'Warning: 入力された文字コードが違います!';
$_['error_location'] = 'ショッピングカートには別のエリアに商品があります。 ローカル製品を削除してこの製品を注文することに同意しますか？ ';