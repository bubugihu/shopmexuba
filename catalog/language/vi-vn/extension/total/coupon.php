<?php
// Heading
$_['heading_title'] = 'Sử dụng mã giảm giá';

// Text
$_['text_coupon']   = 'Giảm giá (%s)';
$_['text_success'] = 'Thành công: Phiếu giảm giá của bạn đã được áp dụng!';

// Mục nhập
$_['entry_coupon'] = 'Nhập mã giảm giá của bạn tại đây';

// Lỗi
$_['error_coupon'] = 'Cảnh báo: Phiếu thưởng không hợp lệ, đã hết hạn hoặc đã đạt đến giới hạn sử dụng!';
$_['error_empty'] = 'Cảnh báo: Vui lòng nhập mã phiếu giảm giá!';