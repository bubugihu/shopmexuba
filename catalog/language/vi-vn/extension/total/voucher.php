<?php
// Heading
$_['heading_title'] = 'Sử dụng phiếu quà tặng';

// Text
$_['text_voucher']  = 'Phiếu quà tặng giá trị (%s)';
$_['text_success'] = 'Thành công: Giảm giá phiếu quà tặng của bạn đã được áp dụng!';

// Mục nhập
$_['entry_voucher'] = 'Nhập mã quà tặng của bạn tại đây';

// Lỗi
$_['error_voucher'] = 'Cảnh báo: Phiếu Quà tặng không hợp lệ hoặc số dư đã được sử dụng hết!';
$_['error_empty'] = 'Cảnh báo: Vui lòng nhập mã chứng nhận quà tặng!';