<?php
// Phần mở đầu
$_['header_title'] = 'Ước tính phí vận chuyển &amp; Thuế ';

// Bản văn
$_['text_success'] = 'Thành công: Ước tính vận chuyển của bạn đã được áp dụng!';
$_['text_shipping'] = 'Nhập điểm đến của bạn để nhận ước tính vận chuyển.';
$_['text_shipping_method'] = 'Vui lòng chọn phương thức giao hàng ưu tiên để sử dụng cho đơn hàng này.';

// Mục nhập
$_['entry_country'] = 'Quốc gia';
$_['entry_zone'] = 'Vùng / Thành phố';
$_['entry_postcode'] = 'Mã bưu điện';

// Lỗi
$_['error_postcode'] = 'Mã bưu điện phải từ 2 đến 10 ký tự!';
$_['error_country'] = 'Vui lòng chọn quốc gia!';
$_['error_zone'] = 'Vui lòng chọn vùng / thành phố!';
$_['error_shipping'] = 'Cảnh báo: Yêu cầu phương thức vận chuyển!';
$_['error_no_shipping'] = 'Cảnh báo: Không có tùy chọn Vận chuyển. Vui lòng <a href="%s"> liên hệ với chúng tôi </a> để được hỗ trợ! ';