<?php
// Text
$_['terms']                               = '';
$_['text_title']                          = 'Thanh toán trực tuyến ATM / VISA / MASTER / JCB qua VNPAY';
$_['heading_title']				= 'Cảm ơn bạn đã mua với %s .... ';
$_['text_response']				= 'Phản hồi từ VNPAY:';
$_['text_success']				= '... thanh toán của bạn đã được nhận thành công.';
$_['text_success_wait']			= '<b><span style="color: #FF0000">Vui lòng đợi ...</span></b> trong khi chúng tôi xử lý xong đơn đặt hàng của bạn.<br>Nếu bạn không được chuyển hướng tự động sau 10 giây, vui lòng nhấp vào <a href="%s">đây</a>.';
$_['text_failure']				= '... Thanh toán của bạn đã bị hủy!';
$_['text_failure_wait']			= '<b><span style="color: #FF0000">Vui lòng đợi ...</span></b><br>Nếu bạn không được chuyển hướng tự động sau 10 giây, vui lòng nhấp vào<a href="%s">đây</a>.';