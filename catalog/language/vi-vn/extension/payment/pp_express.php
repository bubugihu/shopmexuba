<?php
// Phần mở đầu
$_['express_text_title'] = 'Xác nhận đơn hàng';

// Bản văn
$_['text_title'] = 'Thanh toán bằng PayPal Express';
$_['text_cart'] = 'Giỏ hàng';
$_['text_shipping_updated'] = 'Đã cập nhật dịch vụ vận chuyển';
$_['text_trial']              = '%s mỗi %s %s cho %s thanh toán sau đó ';
$_['text_recurring']          = '%s mỗi %s %s';
$_['text_recurring_item']     = 'Mục định kỳ';
$_['text_length']             = ' cho %s thanh toán';

// Mục nhập
$_['express_entry_coupon'] = 'Nhập phiếu giảm giá của bạn tại đây:';

// Cái nút
$_['button_express_coupon'] = 'Thêm';
$_['button_express_confirm'] = 'Xác nhận';
$_['button_express_login'] = 'Tiếp tục đến PayPal';
$_['button_express_shipping'] = 'Cập nhật giao hàng';

// Lỗi
$_['error_heading_title'] = 'Đã xảy ra lỗi';
$_['error_too_many_failures'] = 'Thanh toán của bạn đã thất bại quá nhiều lần';
$_['error_unavailable'] = 'Vui lòng sử dụng toàn bộ thanh toán với đơn đặt hàng này';
$_['error_no_shipping']    	  = 'Cảnh báo: Không có tùy chọn Vận chuyển. Xin vui lòng <a href="%s">liên hệ chúng tôi</a> để được hỗ trợ!';
