<?php
// Bản văn
$_['text_subscribe'] = 'Đăng ký Blog';
$_['text_email'] = 'E-mail';
$_['text_msubscrieb_success'] = 'Thành công: Bạn đã đăng ký thành công!';
$_['text_msubscrieb_verification'] = 'Vui lòng xác minh email của bạn bằng cách nhấp vào liên kết xác minh. Bạn nhận được một email xác minh tại% s! ';
$_['text_msubscrieb_adminapproval'] = 'Bạn sẽ được quản trị viên phê duyệt!';
$_['text_munsubscrieb_success'] = 'Thành công: Bạn đã Hủy đăng ký thành công. Vui lòng kiểm tra email của bạn để hoàn tất quá trình hủy đăng ký! ';

//Cái nút
$_['button_subscribe'] = 'Đăng ký';
$_['button_unsubscribe'] = 'Bỏ đăng ký';

//Lỗi
$_['error_request_invalid'] = 'Yêu cầu không hợp lệ! Vui lòng làm mới trang. ';
$_['error_msubscrieber_exists'] = 'E-mail cho Đăng ký đã tồn tại.';
$_['error_msubscrieber_nonexists'] = 'Email cho Hủy đăng ký không tồn tại.';
$_['error_email'] = 'Địa chỉ E-mail dường như không hợp lệ!';