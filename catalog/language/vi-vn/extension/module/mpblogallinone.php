<?php
// Phần mở đầu
$_['header_title'] = 'Tất cả trong một blog';

$_['text_trending'] = 'Xu hướng';
$_['text_latest'] = 'Mới nhất';
$_['text_featured'] = 'Nổi bật';
$_['text_popular'] = 'Phổ biến';

$_['text_empty_trending'] = 'Không có blog thịnh hành';
$_['text_empty_latest'] = 'Không có blog mới nhất';
$_['text_empty_featured'] = 'Không có blog nổi bật';
$_['text_empty_popular'] = 'Không có blog phổ biến';

$_['text_readmore'] = 'Đọc thêm';
$_['text_comment'] = 'Nhận xét';