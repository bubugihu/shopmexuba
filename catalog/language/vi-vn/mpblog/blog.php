<?php
// Text
$_['header_title'] = 'Xem Blog';
$_['header_give_rating'] = 'Xếp hạng';

$_['text_rating_success'] = 'Cảm ơn đánh giá của bạn.';
$_['text_comment_success'] = 'Cảm ơn bạn đã nhận xét.';
$_['text_no_comments'] = 'Không có bình luận nào cho blog này.';

$_['text_comments_default'] = 'Nhận xét';
$_['text_comments_facebook'] = 'Facebook';
$_['text_comments_google'] = 'Google';
$_['text_comments_disqus'] = 'Disqus';
$_['text_loading'] = 'Đang tải ... Vui lòng đợi!';


$_['text_readmore'] = 'Đọc thêm';
$_['text_blog'] = 'Blog';
$_['text_rating'] = 'Xếp hạng';
$_['text_follow'] = 'Theo dõi chúng tôi';
$_['text_share'] = 'Chia sẻ trên';
$_['text_tags'] = 'Thẻ';
$_['text_give_rating'] = 'Xếp hạng';
$_['text_not_good'] = 'Không tốt chút nào';
$_['text_good'] = 'Rất tốt';
$_['text_comments'] = 'Nhận xét';
$_['text_related'] = 'Blog liên quan';
$_['text_write_comment'] = 'Viết bình luận';
$_['text_comment'] = 'Nhận xét';
$_['text_osystem'] = 'Các Blog có Liên quan';
$_['text_inity_products'] = 'Sản phẩm liên quan';
$_['text_tax'] = 'Thuế';
$_['text_inity_categories'] = 'Danh mục liên quan';
$_['text_login_rating']         = 'Vui lòng <a href="%s">đăng nhập</a> hoặc <a href="%s">đăng ký</a> để đánh giá';
$_['text_login_comment']        = 'Vui lòng <a href="%s">đăng nhập</a> hoặc <a href="%s">đăng ký</a> để bình luận';
$_['text_note']          	    = ' ';
$_['text_error']          	    = 'Không tìm thấy blog!';


// Entry
$_['entry_name'] = 'Tên';
$_['entry_rating'] = 'Xếp hạng';
$_['entry_comment'] = 'Bình luận';

// Cái nút

$_['button_give_rating'] = 'Xếp hạng';
$_['button_add_comment'] = 'Thêm bình luận';

// Lỗi
$_['error_name'] = 'Cảnh báo: Tên phải từ 3 đến 25 ký tự!';
$_['error_text'] = 'Cảnh báo: Văn bản Nhận xét phải từ 25 đến 1000 ký tự!';
$_['error_rating'] = 'Cảnh báo: Vui lòng chọn xếp hạng!';