<?php
// Bản văn
$_['text_mpblogpost'] = 'Blog';
$_['text_error'] = 'Không tìm thấy danh mục blog!';
$_['text_empty'] = 'Không có blog nào để liệt kê trong danh mục blog này.';
$_['text_readmore'] = 'Đọc thêm';
$_['text_comment'] = 'Nhận xét';
$_['text_categories'] = 'Danh mục phụ';
$_['text_blog'] = 'Blog';