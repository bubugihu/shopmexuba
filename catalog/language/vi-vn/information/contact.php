<?php
// Heading
$_['heading_title']      = 'Liên hệ với chúng tôi';
$_['text_home'] = 'Trang Chủ';
// Text 
$_['text_location']      = 'Thông tin của chúng tôi';
$_['text_store']     = 'Gian hàng của chúng tôi';
$_['text_contact']       = 'Liên hệ';
$_['text_sendusamessage']  = 'Gửi tin nhắn cho chúng tôi';
$_['text_address']       = 'Địa chỉ: 163 Trần Huy Liệu, F8, Phú Nhuận, TP.HCM.';
$_['text_telephone'] = 'Điện thoại';
$_['text_fax']       = 'Số Fax';
$_['text_open']      = 'Giờ mở cửa';
$_['text_comment']   = 'Ý kiến đóng góp';
$_['text_success']   = '<p>Yêu cầu của bạn đã được gửi tới chủ cửa hàng!</p>';
$_['text_googlemap']   = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.190565534432!2d106.67538131524243!3d10.796711992307783!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317528d613abd4a5%3A0x709fe766c5f93992!2zMTYzIFRy4bqnbiBIdXkgTGnhu4d1LCBQaMaw4budbmcgOCwgUGjDuiBOaHXhuq1uLCBUaMOgbmggcGjhu5EgSOG7kyBDaMOtIE1pbmgsIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1609404570075!5m2!1svi!2s';

// Entry
$_['entry_name']     = 'Tên của bạn';
$_['entry_email']    = 'Địa chỉ E-Mail';
$_['entry_enquiry']  = 'Nội dung';

// Email
$_['email_subject']  = 'Tiêu đề %s';

// Errors
$_['error_name']         = 'Tên của bạn phải nhiều hơn 3 và nhỏ hơn 32 ký tự!';
$_['error_email']        = 'Bạn phải điền chính xác Email!';
$_['error_enquiry']      = 'Nội dung liên hệ phải nhiều hơn 10 và nhỏ hơn 3000 ký tự!';