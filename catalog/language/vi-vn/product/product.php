<?php
// Text
$_['text_search']       = 'Tìm kiếm';
$_['text_brand']        = 'Thương hiệu';
$_['text_manufacturer'] = 'Hãng sản xuất:';
$_['text_model']        = 'Mã sản phẩm:';
$_['text_reward']       = 'Điểm thưởng:'; 
$_['text_points']       = 'Giá điểm thưởng:';
$_['text_stock']        = 'Tình trạng:';
$_['text_instock']      = 'Còn hàng';
$_['text_tax']          = 'Giá chưa có VAT:'; 
$_['text_discount']     = '<b>Giá đặc biệt khi mua với số lượng từ %s: %s</b>';
$_['text_option']       = 'Tùy chọn đang có';
$_['text_minimum']      = 'Sản phẩm dịch vụ này có số lượng đặt tối thiểu %s';
$_['text_reviews']      = '%s đánh giá'; 
$_['text_write']        = 'Viết đánh giá';
$_['text_login']        = 'Bạn hãy <a href="%s">đăng nhập</a> hoặc <a href="%s">đăng ký</a> để đăng/viết ý kiến';
$_['text_no_reviews']   = 'Không có đánh giá cho sản phẩm này.';
$_['text_note']         = ' ';
$_['text_success']      = 'Cám ơn bạn đã đánh giá. Đánh giá đã được gửi đến quản trị trước khi được chấp nhận.';
$_['text_related']             = 'Sản phẩm cùng loại';
$_['text_tags']                = 'Thẻ từ khóa:';
$_['text_error']               = 'không tìm thấy sản phẩm nào!';
$_['text_payment_recurring']   = 'Hồ sơ chi trả định kỳ';
$_['text_trial_description']   = '%s every %d %s(s) for %d payment(s) then';
$_['text_payment_description'] = '%s every %d %s(s) for %d payment(s)';
$_['text_payment_cancel']      = '%s every %d %s(s) until canceled';
$_['text_day']                 = 'Ngày';
$_['text_week']                = 'Tuần';
$_['text_semi_month']          = 'Nửa tháng';
$_['text_month']               = 'Tháng';
$_['text_year']                = 'Năm';
$_['text_location']                = 'Chọn khu vực mua hàng:';
$_['text_location_vn']                = 'Việt Nam';
$_['text_location_jp']                = 'Nhật Bản';
$_['text_location_us']                = 'Hoa Kỳ';
$_['text_contact_buy']                = 'Liên hệ để đặt hàng';
$_['text_contact_form']                = 'Thông tin liên hệ';
$_['text_contact_name']                = 'Họ tên';
$_['text_contact_mail']                = 'Email';
$_['text_contact_content']                = 'Nội dung';
$_['text_contact_buyproduct']                = 'Tôi cần đặt mua sản phẩm này.';
$_['text_contact_send']                = 'Gửi';
$_['text_contact_close']                = 'Đóng';
$_['text_caution']                = 'Đồng ý';
$_['text_yes']                = 'Đồng ý';
$_['text_no']                = 'Không đồng ý';

// Entry
$_['entry_qty']                = 'Số lượng';
$_['entry_name']               = 'Tên của bạn';
$_['entry_review']             = 'Đánh giá của bạn';
$_['entry_rating']             = 'Xếp hạng';
$_['entry_good']               = 'Tốt';
$_['entry_bad']                = 'Không tốt';

// Tabs
$_['tab_description']          = 'Mô tả';
$_['tab_attribute']     = 'Thành phần';
$_['tab_review']        = 'Đánh giá (%s)';
$_['code']              = 'vn';
// Error
$_['error_name']        = 'Lỗi: Tên đánh giá phải từ 3 đến 25 kí tự!';
$_['error_text']        = 'Lỗi: Đánh giá phải từ 25 đến 1000 kí tự!';
$_['error_rating']      = 'Lỗi: Vui lòng mức bình chọn!';
$_['error_location']      = 'Giỏ hàng đang có sản phẩm ở khu vực khác. Bạn có đồng ý xóa bỏ sản phẩm khu vực đó và đặt mua sản phẩm này ?';
