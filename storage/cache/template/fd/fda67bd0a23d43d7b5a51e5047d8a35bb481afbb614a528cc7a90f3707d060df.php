<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/information/sitemap.twig */
class __TwigTemplate_4cac4c2aae6aedb0707e6838310959fff1b2afc2ef0e414539c74be2a2cf73bc extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div style=\"background-color: #DBF9FB\">
  <div class=\"l-breadcrumb\" style=\"margin-bottom: 20px;\"> 
    <div class=\"l-breadcrumb-container\"> 
      <div class=\"l-breadcrumb-inner\"> 
        <nav role=\"navigation\" aria-label=\"現在位置\"> 
          <ol class=\"m-breadcrumb\" itemscope=\"itemscope\" itemtype=\"http://schema.org/BreadcrumbList\"> 
            ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 9
            echo "            <li itemprop=\"itemListElement\" itemscope=\"itemscope\" itemtype=\"http://schema.org/ListItem\"><a style=\"text-transform: uppercase;\" href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 9);
            echo "\" class=\"m-breadcrumb-item\" itemprop=\"item\"><span style=\"color: #fff; font-size: 18px;\" itemprop=\"name\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 9);
            echo "</span></a>
              <meta itemprop=\"position\" content=\"1\" style=\"color: #fff; font-size: 18px;\"> </li> 
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "          </ol> 
        </nav> 
      </div> 
    </div> 
  </div>
  <div id=\"information-sitemap\" class=\"container\">
    <div class=\"row\">";
        // line 18
        echo ($context["column_left"] ?? null);
        echo "
      ";
        // line 19
        if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
            // line 20
            echo "      ";
            $context["class"] = "col-sm-6";
            // line 21
            echo "      ";
        } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 22
            echo "      ";
            $context["class"] = "col-sm-9";
            // line 23
            echo "      ";
        } else {
            // line 24
            echo "      ";
            $context["class"] = "col-sm-12";
            // line 25
            echo "      ";
        }
        // line 26
        echo "      <div id=\"content\" class=\"";
        echo ($context["class"] ?? null);
        echo "\">";
        echo ($context["content_top"] ?? null);
        echo "
        <h1>";
        // line 27
        echo ($context["heading_title"] ?? null);
        echo "</h1>
        <div class=\"row\">
          <div class=\"col-sm-6\" style=\"line-height: 2.5;\">
            <ul>
              ";
        // line 31
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["category_1"]) {
            // line 32
            echo "              <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["category_1"], "href", [], "any", false, false, false, 32);
            echo "\" style=\"color: black;\">";
            echo twig_get_attribute($this->env, $this->source, $context["category_1"], "name", [], "any", false, false, false, 32);
            echo "</a>
                ";
            // line 33
            if (twig_get_attribute($this->env, $this->source, $context["category_1"], "children", [], "any", false, false, false, 33)) {
                // line 34
                echo "                <ul>
                  ";
                // line 35
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["category_1"], "children", [], "any", false, false, false, 35));
                foreach ($context['_seq'] as $context["_key"] => $context["category_2"]) {
                    // line 36
                    echo "                  <li><a href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["category_2"], "href", [], "any", false, false, false, 36);
                    echo "\" style=\"color: black;\">";
                    echo twig_get_attribute($this->env, $this->source, $context["category_2"], "name", [], "any", false, false, false, 36);
                    echo "</a>
                    ";
                    // line 37
                    if (twig_get_attribute($this->env, $this->source, $context["category_2"], "children", [], "any", false, false, false, 37)) {
                        // line 38
                        echo "                    <ul>
                      ";
                        // line 39
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["category_2"], "children", [], "any", false, false, false, 39));
                        foreach ($context['_seq'] as $context["_key"] => $context["category_3"]) {
                            // line 40
                            echo "                      <li><a href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["category_3"], "href", [], "any", false, false, false, 40);
                            echo "\" style=\"color: black;\">";
                            echo twig_get_attribute($this->env, $this->source, $context["category_3"], "name", [], "any", false, false, false, 40);
                            echo "</a></li>
                      ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category_3'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 42
                        echo "                    </ul>
                    ";
                    }
                    // line 44
                    echo "                  </li>
                  ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category_2'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 46
                echo "                </ul>
                ";
            }
            // line 48
            echo "              </li>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category_1'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "            </ul>
          </div>
          <div class=\"col-sm-6\" style=\"line-height: 2.5;\">
            <ul>
              <li><a href=\"";
        // line 54
        echo ($context["special"] ?? null);
        echo "\" style=\"color: black;\">";
        echo ($context["text_special"] ?? null);
        echo "</a></li>
              <li><a href=\"";
        // line 55
        echo ($context["account"] ?? null);
        echo "\" style=\"color: black;\">";
        echo ($context["text_account"] ?? null);
        echo "</a>
                <ul>
                  <li><a href=\"";
        // line 57
        echo ($context["edit"] ?? null);
        echo "\" style=\"color: black;\">";
        echo ($context["text_edit"] ?? null);
        echo "</a></li>
                  <li><a href=\"";
        // line 58
        echo ($context["password"] ?? null);
        echo "\" style=\"color: black;\">";
        echo ($context["text_password"] ?? null);
        echo "</a></li>
                  <li><a href=\"";
        // line 59
        echo ($context["address"] ?? null);
        echo "\" style=\"color: black;\">";
        echo ($context["text_address"] ?? null);
        echo "</a></li>
                  <li><a href=\"";
        // line 60
        echo ($context["history"] ?? null);
        echo "\" style=\"color: black;\">";
        echo ($context["text_history"] ?? null);
        echo "</a></li>
                  <li><a href=\"";
        // line 61
        echo ($context["download"] ?? null);
        echo "\" style=\"color: black;\">";
        echo ($context["text_download"] ?? null);
        echo "</a></li>
                </ul>
              </li>
              <li><a href=\"";
        // line 64
        echo ($context["history"] ?? null);
        echo "\" style=\"color: black;\">";
        echo ($context["text_cart"] ?? null);
        echo "</a></li>
              <li><a href=\"";
        // line 65
        echo ($context["checkout"] ?? null);
        echo "\" style=\"color: black;\">";
        echo ($context["text_checkout"] ?? null);
        echo "</a></li>
              <li><a href=\"";
        // line 66
        echo ($context["search"] ?? null);
        echo "\" style=\"color: black;\">";
        echo ($context["text_search"] ?? null);
        echo "</a></li>
              <li>";
        // line 67
        echo ($context["text_information"] ?? null);
        echo "
                <ul>
                  ";
        // line 69
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["informations"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
            // line 70
            echo "                  <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["information"], "href", [], "any", false, false, false, 70);
            echo "\" style=\"color: black;\">";
            echo twig_get_attribute($this->env, $this->source, $context["information"], "title", [], "any", false, false, false, 70);
            echo "</a></li>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 72
        echo "                  <li><a href=\"";
        echo ($context["contact"] ?? null);
        echo "\" style=\"color: black;\">";
        echo ($context["text_contact"] ?? null);
        echo "</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
        ";
        // line 78
        echo ($context["content_bottom"] ?? null);
        echo "</div>
      ";
        // line 79
        echo ($context["column_right"] ?? null);
        echo "</div>
  </div>
</div>
";
        // line 82
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "default/template/information/sitemap.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  289 => 82,  283 => 79,  279 => 78,  267 => 72,  256 => 70,  252 => 69,  247 => 67,  241 => 66,  235 => 65,  229 => 64,  221 => 61,  215 => 60,  209 => 59,  203 => 58,  197 => 57,  190 => 55,  184 => 54,  178 => 50,  171 => 48,  167 => 46,  160 => 44,  156 => 42,  145 => 40,  141 => 39,  138 => 38,  136 => 37,  129 => 36,  125 => 35,  122 => 34,  120 => 33,  113 => 32,  109 => 31,  102 => 27,  95 => 26,  92 => 25,  89 => 24,  86 => 23,  83 => 22,  80 => 21,  77 => 20,  75 => 19,  71 => 18,  63 => 12,  51 => 9,  47 => 8,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/information/sitemap.twig", "");
    }
}
