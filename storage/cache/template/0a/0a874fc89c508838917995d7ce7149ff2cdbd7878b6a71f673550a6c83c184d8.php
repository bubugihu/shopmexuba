<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/information/contact.twig */
class __TwigTemplate_24da25cd1aa3d73a0397ad9d8325483069c6bf8235748d8b1dcb5cf4feb518ce extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<style>
  .contact .box-heading {
  text-align: center;
  }
  .relative {
  position: relative !important;
  }
  .contact .box-heading h2 {
  background: #DBF9FB;
  display: inline-block;
  position: relative;
  z-index: 999;
  padding: 0 40px;
  font-size: 32px;
  text-transform: none;
  font-weight: 400;
  }
  .title-head {
  font-size: 18px;
  text-transform: uppercase;
  margin-top: 9px;
  color: #252525;
  text-decoration: none;
  }
  .box-heading .border-bottom-3line-blog {
  width: 100%;
  display: block;
  z-index: 9;
  position: absolute;
  top: 47%;
  right: 0;
  }
  .border-bottom-1px {
  border-bottom: 1px solid #ffb3ab;
  margin-bottom: 2px;
  }
</style>
<div style=\"background-color: #DBF9FB\">
  <div class=\"l-breadcrumb\" style=\"margin-bottom: 20px;\"> 
    <div class=\"l-breadcrumb-container\"> 
      <div class=\"l-breadcrumb-inner\"> 
        <nav role=\"navigation\" aria-label=\"現在位置\"> 
          <ol class=\"m-breadcrumb\" itemscope=\"itemscope\" itemtype=\"http://schema.org/BreadcrumbList\"> 
            ";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 46
            echo "            <li itemprop=\"itemListElement\" itemscope=\"itemscope\" itemtype=\"http://schema.org/ListItem\"><a style=\"text-transform: uppercase;\" href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 46);
            echo "\" class=\"m-breadcrumb-item\" itemprop=\"item\"><span style=\"color: #fff; font-size: 18px;\" itemprop=\"name\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 46);
            echo "</span></a>
              <meta itemprop=\"position\" content=\"1\" style=\"color: #fff; font-size: 18px;\"> </li> 
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "          </ol> 
        </nav> 
      </div> 
    </div> 
  </div>
  <div id=\"information-contact\" class=\"container\">
    <div class=\"container contact\">
      <div class=\"box-heading box-heading-line relative\">
        <h2 style=\"text-align: center;\">";
        // line 57
        echo ($context["heading_title_contact"] ?? null);
        echo "</h2>
      </div>

      <div class=\"row margin-top-30\">
        <div class=\"col-sm-6 contact_left\">
          <div class=\"contact-box-info clearfix margin-bottom-20\" style=\"line-height: 2.5; margin-top: 64px\">

            <ul class=\"contact\">
              <h3 class=\"title-head-contact\"><span> ヌコス日本 </span></h3>
              <li> 住所: 4-4-18 Nishitenumi, Kita-ku, Osaka-shi, Osaka, JAPAN.</li>
              <li>
                インスタグラム: <a href=\"https://www.instagram.com/nucos.japan/\"
                              target=\"_blank\">https://www.instagram.com/nucos.japan</a>
              </li>
              <li>
                ウェブサイト <a href=\"https://www.nucos.jp/\" target=\"_blank\">https://www.nucos.jp</a>
              </li>
              <li>
                アマゾン日本: <a href=\"https://www.amazon.co.jp/\"
                                  target=\"_blank\">https://www.amazon.co.jp</a>
              </li>
            </ul>

          </div>
          <div id=\"login\">

          </div>
        </div>
        <div class=\"col-sm-6\">
          <div class=\"box-maps margin-bottom-60\">
            <iframe
                    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1379.1965152701969!2d135.50439818578303!3d34.69759689712607!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6000e6e9c8410249%3A0xc5f684b0842d4bb7!2sNUCOS!5e0!3m2!1svi!2s!4v1609405001817!5m2!1sja!2s\"
                    width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"
                    aria-hidden=\"false\" tabindex=\"0\"></iframe>
          </div>
        </div>
      </div>
      <br />
      <div class=\"row margin-top-30\" style=\"line-height: 2.5;\">
        <div class=\"col-sm-6 contact_left\">
          <div class=\"contact-box-info clearfix margin-bottom-20\" style=\"line-height: 2.5; margin-top: 64px\">
            <ul class=\"contact\">
              <h3 class=\"title-head-contact\"><span> ヌコスベトナム </span></h3>
              <li> 住所: 163 Trần Huy Liệu, F8, Phú Nhuận, TP.HCM</li>
              <li>
                ウェブサイト: <a href=\"https://www.nucos.jp/\" target=\"_blank\">https://www.nucos.vn</a>
                <link rel=\"https://www.nucos.jp/\" href=\"<? php echo DEFAULT_CANONICAL; ?>\" >
              </li>
              <li>
                チキ: <a href=\"https://tiki.vn/\" target=\"_blank\">https://tiki.vn/cua-hang/nucos-nhat-ban</a>
              </li>
              <li>
               ソッピー： <a href=\"https://shopee.vn/\" target=\"_blank\">https://shopee.vn</a>
              </li>
              <li>
                ラザダ : <a href=\"https://www.lazada.vn/\" target=\"_blank\">https://lazada.vn/shop/nucos-nhat-ban</a>
              </li>
            </ul>

          </div>
          <div id=\"login\">

          </div>
        </div>
        <div class=\"col-sm-6\">
          

          <div class=\"box-maps margin-bottom-60\">
            <iframe
                    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.190565534432!2d106.67538131524243!3d10.796711992307783!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317528d613abd4a5%3A0x709fe766c5f93992!2zMTYzIFRy4bqnbiBIdXkgTGnhu4d1LCBQaMaw4budbmcgOCwgUGjDuiBOaHXhuq1uLCBUaMOgbmggcGjhu5EgSOG7kyBDaMOtIE1pbmgsIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1609404570075!5m2!1svi!2s\"
                    width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"
                    aria-hidden=\"false\" tabindex=\"0\"></iframe>
          </div>
        </div>
      </div>
      <br />
      <div class=\"row margin-top-30\">
        <div class=\"col-sm-6 contact_left\">
          <div class=\"contact-box-info clearfix margin-bottom-20\" style=\"line-height: 2.5; margin-top: 64px\">

            <ul class=\"contact\">
              <h3 class=\"title-head-contact\"><span> ヌコスアメリカ合衆国 </span></h3>
              <li> 住所: 3104 Wintersmith Lane, Arlington, Texas, UNITED STATES</li>
              <li>
                ウェブサイト: <a href=\"https://www.nucos.jp/\" target=\"_blank\">https://www.nucos.jp/</a>
              </li>
              <li>
                アマゾン: <a href=\"https://www.amazon.com/\" target=\"_blank\">https://www.amazon.com/</a>
              </li>
            </ul>

          </div>
          <div id=\"login\">

          </div>
        </div>
        <div class=\"col-sm-6\">
          <div class=\"box-maps margin-bottom-60\">
            <iframe
                    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3357.707034928314!2d-97.07305208444193!3d32.693835180997155!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x864e89e72c3c9ee3%3A0x3f9b83b47135ed80!2sFAROSON!5e0!3m2!1svi!2s!4v1609404758116!5m2!1sen!2s\"
                    width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"
                    aria-hidden=\"false\" tabindex=\"0\"></iframe>
          </div>
        </div>
      </div>
      <br><br />
      <div class=\"box-heading box-heading-line relative\">
        <h2 class=\"title-head page_title margin-bottom-5\">";
        // line 164
        echo ($context["heading_title"] ?? null);
        echo "</h2>
        <div class=\"border-bottom-3line-blog\">
          <div class=\"border-bottom-1px\"></div>
          <div class=\"border-bottom-1px\"></div>
          <div class=\"border-bottom-1px\"></div>
        </div>
      </div>
      <div class=\"row\" style=\"margin-top: 21px;\">
        <div class=\"col-md-3\"></div>
        <div class=\"col-md-6\">
          <form form action=\"";
        // line 174
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal\">
            <div class=\"row row-8Gutter\">
              <div class=\"col-md-12\">
                <fieldset class=\"form-group\">
                  <input type=\"text\" placeholder=\"";
        // line 178
        echo ($context["entry_name"] ?? null);
        echo "\" value=\"";
        echo ($context["name"] ?? null);
        echo "\" name=\"name\"
                         id=\"input-name\" class=\"form-control  form-control-lg\" required=\"\" style=\"border-radius: 20px\">
                </fieldset>
              </div>
              <div class=\"col-md-12\">
                <fieldset class=\"form-group\">
                  <input type=\"text\" placeholder=\"";
        // line 184
        echo ($context["entry_email"] ?? null);
        echo "\" value=\"";
        echo ($context["email"] ?? null);
        echo "\" name=\"email\"
                         pattern=\"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,63}\$\" data-validation=\"email\"
                         id=\"input-email\" class=\"form-control form-control-lg\" required=\"\" style=\"border-radius: 20px\">
                </fieldset>
              </div>
            </div>
            <fieldset class=\"form-group\">
              <textarea name=\"enquiry\" placeholder=\"";
        // line 191
        echo ($context["entry_enquiry"] ?? null);
        echo "\" id=\"input-enquiry\"
                        class=\"form-control form-control-lg\" rows=\"6\" minlength=\"2\" required=\"\" style=\"border-radius: 20px\"></textarea>
            </fieldset>
            <div class=\"pull-right\" style=\" margin-left: auto; margin-right: auto; width: 63%;}\">
              <input class=\"btn btn-primary\" type=\"submit\" style=\"border-radius: 20px; width: 54%; background: #378EA0; font-size: 16px; font-weight: bold;\" value=\"";
        // line 195
        echo ($context["button_submit"] ?? null);
        echo "\" />
            </div>
          </form>
        </div>
        <div class=\"col-sm-4\"></div>
      </div>
    </div>
  </div>
  <br />
  ";
        // line 204
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "default/template/information/contact.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  282 => 204,  270 => 195,  263 => 191,  251 => 184,  240 => 178,  233 => 174,  220 => 164,  110 => 57,  100 => 49,  88 => 46,  84 => 45,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/information/contact.twig", "");
    }
}
