<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/module/isenselabs_seo/sd_company_info.twig */
class __TwigTemplate_95f88ea10dd2106d4c5d8816ec281b3caca5e652b5d2296cd044fca36db9146d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["richsnippets_company_info"] ?? null)) {
            // line 2
            echo "<script type=\"application/ld+json\">            
{
 \"@context\":\"http://schema.org\",
 \"@type\":\"Organization\",
 \"url\":\"";
            // line 6
            echo ($context["seo_url_site"] ?? null);
            echo "\",
 \"logo\":\"";
            // line 7
            echo ($context["seo_logo"] ?? null);
            echo "\",
 \"potentialAction\":[{
   \"@type\":\"SearchAction\",
   \"target\":\"";
            // line 10
            echo ($context["seo_search"] ?? null);
            echo "\",
   \"query-input\":\"required name=search_term_string\"
 }],
 \"contactPoint\":[{
    \"@type\":\"ContactPoint\",
    \"telephone\":\"";
            // line 15
            echo ($context["richsnippet_phone"] ?? null);
            echo "\",
    \"contactType\":\"customer service\"
  }]
}
</script>
";
        }
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/isenselabs_seo/sd_company_info.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 15,  55 => 10,  49 => 7,  45 => 6,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/extension/module/isenselabs_seo/sd_company_info.twig", "");
    }
}
