<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/common/header.twig */
class __TwigTemplate_22c267896b77491aa94f4b8f787cca80c839620d0bf05b9f823a63492d818368 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir=\"";
        // line 3
        echo ($context["direction"] ?? null);
        echo "\" lang=\"";
        echo ($context["lang"] ?? null);
        echo "\" class=\"ie8\"><![endif]-->
<!--[if IE 9 ]><html dir=\"";
        // line 4
        echo ($context["direction"] ?? null);
        echo "\" lang=\"";
        echo ($context["lang"] ?? null);
        echo "\" class=\"ie9\"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir=\"";
        // line 6
        echo ($context["direction"] ?? null);
        echo "\" lang=\"";
        echo ($context["lang"] ?? null);
        echo "\">
  <!--<![endif]-->
  <head>
    <meta charset=\"UTF-8\" />
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <title>";
        // line 12
        echo ($context["title"] ?? null);
        echo "</title>
    <base href=\"";
        // line 13
        echo ($context["base"] ?? null);
        echo "\" />
    ";
        // line 14
        if (($context["description"] ?? null)) {
            // line 15
            echo "    <meta name=\"description\" content=\"";
            echo ($context["description"] ?? null);
            echo "\" />
    ";
        }
        // line 17
        echo "    ";
        if (($context["keywords"] ?? null)) {
            // line 18
            echo "    <meta name=\"keywords\" content=\"";
            echo ($context["keywords"] ?? null);
            echo "\" />
    ";
        }
        // line 20
        echo "    <!-- Style layout same meiji.jp -->
    <link rel=\"stylesheet\" href=\"catalog/view/theme/default/stylesheet/css/common.css\" type=\"text/css\" data-tg-desktop_or_tablet_or_phone=\"show\"/>
    <link rel=\"stylesheet\" href=\"catalog/view/theme/default/stylesheet/css/custom.css\" type=\"text/css\" data-tg-desktop_or_tablet_or_phone=\"show\"/>
    <link rel=\"stylesheet\" href=\"catalog/view/theme/default/stylesheet/css/index.css\" type=\"text/css\"/>
    <link href=\"https://fonts.googleapis.com/css?family=Oswald:600\" rel=\"stylesheet\">
    <!-- Style layout origin -->
    <script src=\"catalog/view/theme/default/javascript/js/vendor.min.js\"></script>
    <link href=\"catalog/view/javascript/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\" />
    <script src=\"catalog/view/javascript/bootstrap/js/bootstrap.min.js\" type=\"text/javascript\"></script>
    <link href=\"catalog/view/javascript/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />
    <link href=\"//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700\" rel=\"stylesheet\" type=\"text/css\" />
    <link href=\"catalog/view/theme/default/stylesheet/stylesheet.css\" rel=\"stylesheet\">
    ";
        // line 32
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["styles"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
            // line 33
            echo "    <link href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["style"], "href", [], "any", false, false, false, 33);
            echo "\" type=\"text/css\" rel=\"";
            echo twig_get_attribute($this->env, $this->source, $context["style"], "rel", [], "any", false, false, false, 33);
            echo "\" media=\"";
            echo twig_get_attribute($this->env, $this->source, $context["style"], "media", [], "any", false, false, false, 33);
            echo "\" />
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["scripts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 36
            echo "    <script src=\"";
            echo $context["script"];
            echo "\" type=\"text/javascript\"></script>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "    <script src=\"catalog/view/javascript/common.js\" type=\"text/javascript\"></script>
    ";
        // line 39
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["links"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
            // line 40
            echo "    <link href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["link"], "href", [], "any", false, false, false, 40);
            echo "\" rel=\"";
            echo twig_get_attribute($this->env, $this->source, $context["link"], "rel", [], "any", false, false, false, 40);
            echo "\" />
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["analytics"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["analytic"]) {
            // line 43
            echo "    ";
            echo $context["analytic"];
            echo "
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['analytic'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "    <style>
      button.dropdown-toggle:hover + ul.dropdown-menu{\t
      display:block 
      }
      p{
      word-break: break-word;
      }
      .top-acc{
      width: 100%
      }
      .logo-link{
      margin-right: 3px;
      padding-top:2px;
      }
      .input-readonly[readonly]{
      background-color: #fff;
      }
      button:focus, label:focus {
      outline: blue;
      }
      #button-comment{
      background-color: #378EA0;
      border-radius:20px; 
      font-size: 15px;
      }
      #button-rating{
      background-color: #378EA0;
      border-radius:20px; 
      font-size: 15px;
      }
      #button-confirm{
      background-color: #378EA0;
      border-radius:20px; 
      font-size: 15px;
      }
      #button-guest{
      background-color: #378EA0;
      border-radius:20px; 
      font-size: 15px;
      }
      .btn-primary:hover{
      border-color: #378EA0;
      }
    </style>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src=\"https://www.googletagmanager.com/gtag/js?id=G-SNTG5Y26H4\"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-SNTG5Y26H4');
</script>
    <script>
      \$(document).ready(function() {
      \t\$(\".paypal-button.paypal-button-color-gold\").css(\"background\", \"#378EA0\")
      })
      \$(document).scroll(function() {
          var y = \$(this).scrollTop();
          if (y > 800) {
            \$('.js-pagetop').removeClass(\"is-hidden\");
      \t\t\$('.js-pagetop').addClass(\"is-fixed\");
          } else {
            \$('.js-pagetop').removeClass(\"is-fixed\");
      \t\t\$('.js-pagetop').addClass(\"is-hidden\");
        }
      });
      \$(function () {
      \t \t\$('#pagetotop').click(function () {
              \$(\"html,body\").animate({
                  scrollTop: 0
              }, \"slow\");
            });
      }); 
    </script>
  </head>
  <body class=\"t-top\">
    <nav id=\"top\">
      <div class=\"top-header\">";
        // line 123
        echo ($context["currency"] ?? null);
        echo "
        ";
        // line 124
        echo ($context["language"] ?? null);
        echo "
        <div id=\"top-links\" class=\"nav pull-right\">
          <ul class=\"list-inline\">
            <li><a href=\"/contact\"><i class=\"fa fa-phone\"></i></a> <span class=\"hidden-xs hidden-sm hidden-md\">";
        // line 127
        echo ($context["telephone"] ?? null);
        echo "</span></li>
            <li><a href=\"/checkout\" title=\"";
        // line 128
        echo ($context["text_shopping_cart"] ?? null);
        echo "\"><i class=\"fa fa-shopping-cart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
        echo ($context["text_shopping_cart"] ?? null);
        echo "</span></a></li>
            <li class=\"dropdown\"><a href=\"";
        // line 129
        echo ($context["account"] ?? null);
        echo "\" title=\"";
        echo ($context["text_account"] ?? null);
        echo "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"fa fa-user\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
        echo ($context["text_account"] ?? null);
        echo "</span> <span class=\"caret\"></span></a>
              <ul class=\"dropdown-menu dropdown-menu-right\">
                ";
        // line 131
        if (($context["logged"] ?? null)) {
            // line 132
            echo "                <li class=\"top-acc\"><a href=\"/account\">";
            echo ($context["text_account"] ?? null);
            echo "</a></li>
                <li class=\"top-acc\"><a href=\"/order\">";
            // line 133
            echo ($context["text_order"] ?? null);
            echo "</a></li>
                <li class=\"top-acc\"><a href=\"/logout\">";
            // line 134
            echo ($context["text_logout"] ?? null);
            echo "</a></li>
                ";
        } else {
            // line 136
            echo "                <li class=\"top-acc\"><a href=\"/register\">";
            echo ($context["text_register"] ?? null);
            echo "</a></li>
                <li class=\"top-acc\"><a href=\"/login\">";
            // line 137
            echo ($context["text_login"] ?? null);
            echo "</a></li>
                ";
        }
        // line 139
        echo "      
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <header id=\"js-header\" role=\"banner\">
      <div class=\"l-header-container\">
        <div class=\"l-header-inner\">
          <h1 class=\"l-header-heading\">
            <a href=\"index.html\" class=\"l-header-logo\">
              ";
        // line 151
        if (($context["logo"] ?? null)) {
            echo "<a href=\"https://nucos.jp/\"><img src=\"";
            echo ($context["logo"] ?? null);
            echo "\" title=\"";
            echo ($context["name"] ?? null);
            echo "\" alt=\"";
            echo ($context["name"] ?? null);
            echo "\" class=\"m-header-logo\" /></a>";
        } else {
            // line 152
            echo "              <h1><a href=\"";
            echo ($context["home"] ?? null);
            echo "\">";
            echo ($context["name"] ?? null);
            echo "</a></h1>
              ";
        }
        // line 154
        echo "            </a>
          </h1>
          <div class=\"l-header-content\">
            <button class=\"m-header-btn js-header-btn\" aria-controls=\"header-menu\" aria-expanded=\"false\" style=\"width: 40px;\">
              <span class=\"m-header-btn-wrap js-header-btn-wrap\">
                <span class=\"m-header-btn-icon js-header-btn-icon\" style=\"transform: matrix(1, 0, 0, 1, 0, 0); background-color: rgb(102, 102, 102); top: 0px;\"></span>
                <span class=\"m-header-btn-icon js-header-btn-icon\" style=\"background-color: rgb(102, 102, 102);\"></span>
                <span class=\"m-header-btn-icon js-header-btn-icon\" style=\"transform: matrix(1, 0, 0, 1, 0, 0); background-color: rgb(102, 102, 102); bottom: 0px;\"></span>
              </span>
            </button>
            <div id=\"header-menu\" class=\"l-header-nav js-header-nav\" aria-hidden=\"false\" style=\"background-color: #fff; margin-top: 50px;\">
              <div class=\"l-header-nav-grid\">
                <div class=\"l-header-nav-item-1\">
                  ";
        // line 167
        echo ($context["menu"] ?? null);
        echo "
                </div>
                <div class=\"l-header-nav-item-2\">
                  <div class=\"l-header-link-list\">
                    <ul class=\"m-header-link-list\">
                      <li>";
        // line 172
        echo ($context["cart"] ?? null);
        echo "</li>
                      <li>";
        // line 173
        echo ($context["search"] ?? null);
        echo "</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>";
    }

    public function getTemplateName()
    {
        return "default/template/common/header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  361 => 173,  357 => 172,  349 => 167,  334 => 154,  326 => 152,  316 => 151,  302 => 139,  297 => 137,  292 => 136,  287 => 134,  283 => 133,  278 => 132,  276 => 131,  267 => 129,  261 => 128,  257 => 127,  251 => 124,  247 => 123,  167 => 45,  158 => 43,  153 => 42,  142 => 40,  138 => 39,  135 => 38,  126 => 36,  121 => 35,  108 => 33,  104 => 32,  90 => 20,  84 => 18,  81 => 17,  75 => 15,  73 => 14,  69 => 13,  65 => 12,  54 => 6,  47 => 4,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/common/header.twig", "");
    }
}
