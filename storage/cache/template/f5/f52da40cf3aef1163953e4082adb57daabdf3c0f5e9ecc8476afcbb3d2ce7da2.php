<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/mpblog/blogcategory.twig */
class __TwigTemplate_ce50d182a7c5c4c06e2707b15fb0e2eb304187a491273c7a84c5d7b70adadaf4 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div style=\"background-color: #DBF9FB\">
  <div class=\"l-breadcrumb\" style=\"margin-bottom: 30px;\"> 
    <div class=\"l-breadcrumb-container\"> 
      <div class=\"l-breadcrumb-inner\"> 
        <nav role=\"navigation\" aria-label=\"現在位置\"> 
          <ol class=\"m-breadcrumb\" itemscope=\"itemscope\" itemtype=\"http://schema.org/BreadcrumbList\"> 
            ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 9
            echo "            <li itemprop=\"itemListElement\" itemscope=\"itemscope\" itemtype=\"http://schema.org/ListItem\"><a style=\"text-transform: uppercase;\" href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 9);
            echo "\" class=\"m-breadcrumb-item\" itemprop=\"item\"><span style=\"color: #fff; font-size: 18px;\" itemprop=\"name\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 9);
            echo "</span></a>
              <meta itemprop=\"position\" content=\"1\" style=\"color: #fff; font-size: 18px;\"> </li> 
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "          </ol> 
        </nav> 
      </div> 
    </div> 
  </div>
  <div id=\"container\" class=\"container mp-blog j-container ";
        // line 17
        echo ($context["themeclass"] ?? null);
        echo "\">
    ";
        // line 18
        if (( !($context["mpblogcategories"] ?? null) &&  !($context["mpblogposts"] ?? null))) {
            // line 19
            echo "        <div class=\"row\">
          <div class=\"col-sm-12 xl-100\">
            <p>";
            // line 21
            echo ($context["text_empty"] ?? null);
            echo "</p>
            <div class=\"buttons text-right\">
              <div><a href=\"";
            // line 23
            echo ($context["continue"] ?? null);
            echo "\" class=\"btn btn-pink\" style=\"font-size: 15px; border-radius: 20px;\">";
            echo ($context["button_continue"] ?? null);
            echo "</a></div>
            </div>
          </div>
        </div>
        ";
        }
        // line 28
        echo "    <div class=\"row\">";
        echo ($context["column_left"] ?? null);
        echo "
      ";
        // line 29
        if ((($context["themename"] ?? null) == "journal2")) {
            // line 30
            echo "      ";
            echo ($context["column_right"] ?? null);
            echo "
      ";
        }
        // line 32
        echo "      ";
        if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
            // line 33
            echo "      ";
            $context["class"] = "col-sm-6";
            // line 34
            echo "      ";
        } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 35
            echo "      ";
            $context["class"] = "col-sm-9";
            // line 36
            echo "      ";
        }
        // line 37
        echo "      <div id=\"content\" class=\"";
        echo ($context["class"] ?? null);
        echo "\">";
        echo ($context["content_top"] ?? null);
        echo "\t
        
          ";
        // line 39
        if ((($context["thumb"] ?? null) || ($context["description"] ?? null))) {
            // line 40
            echo "          <div class=\"";
            echo (((($context["themename"] ?? null) != "journal2")) ? ("row") : ("journal-box"));
            echo "\">
            ";
            // line 41
            $context["rowno"] = 12;
            // line 42
            echo "            ";
            if (($context["thumb"] ?? null)) {
                echo " 
            ";
                // line 43
                $context["rowno"] = 8;
                // line 44
                echo "            <div class=\"col-sm-4 xl-33 sm-100\"><img src=\"";
                echo ($context["thumb"] ?? null);
                echo "\" alt=\"";
                echo ($context["heading_title"] ?? null);
                echo "\" title=\"";
                echo ($context["heading_title"] ?? null);
                echo "\" class=\"img-thumbnail\" /></div>
            ";
            }
            // line 46
            echo "            ";
            if (($context["description"] ?? null)) {
                // line 47
                echo "            <div class=\"col-sm-";
                echo ($context["rowno"] ?? null);
                echo " xl-66 sm-100\">";
                echo ($context["description"] ?? null);
                echo "</div>
            ";
            }
            // line 49
            echo "          </div>
          ";
        }
        // line 51
        echo "        </div>
        ";
        // line 52
        if (($context["mpblogcategories"] ?? null)) {
            // line 53
            echo "        <div class=\"mpblog-subcate\">
          <div class=\"";
            // line 54
            echo (((($context["themename"] ?? null) != "journal2")) ? ("row") : (""));
            echo "\">
            <div class=\"col-sm-12 xl-100\">
              <div class=\"swiper-viewport\">
                <div id=\"mpblogsubcates\" class=\"swiper-container\">
                  <ul class=\"list list-inline swiper-wrapper\" >
                    ";
            // line 59
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["mpblogcategories"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["mpblogcategory"]) {
                // line 60
                echo "                    <li class=\"item swiper-slide\"><a href=\"";
                echo twig_get_attribute($this->env, $this->source, $context["mpblogcategory"], "href", [], "any", false, false, false, 60);
                echo "\">
                      ";
                // line 61
                if (twig_get_attribute($this->env, $this->source, $context["mpblogcategory"], "thumb", [], "any", false, false, false, 61)) {
                    // line 62
                    echo "                      <img class=\"img-responsive\" src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogcategory"], "thumb", [], "any", false, false, false, 62);
                    echo "\" alt=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogcategory"], "name", [], "any", false, false, false, 62);
                    echo "\" title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogcategory"], "name", [], "any", false, false, false, 62);
                    echo "\" /> ";
                }
                // line 63
                echo "                      <h4>";
                echo twig_get_attribute($this->env, $this->source, $context["mpblogcategory"], "name", [], "any", false, false, false, 63);
                echo " </h4>
                      </a></li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mpblogcategory'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 66
            echo "                  </ul>
                </div>
                <div class=\"swiper-pagination blogcategory\"></div>
                <div class=\"swiper-pager\">
                  <div class=\"swiper-button-next\"></div>
                  <div class=\"swiper-button-prev\"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        ";
        }
        // line 78
        echo "        ";
        echo ($context["view_posts"] ?? null);
        echo "
        
        ";
        // line 80
        echo ($context["content_bottom"] ?? null);
        echo "</div>
      ";
        // line 81
        if ((($context["themename"] ?? null) != "journal2")) {
            // line 82
            echo "      ";
            echo ($context["column_right"] ?? null);
            echo "
      ";
        }
        // line 84
        echo "    </div>
  </div>
<script>
  var num_blog = \$('.list li').length
  if(num_blog == 0) {
    }
</script>
  <script type=\"text/javascript\"><!--
\$('#mpblogsubcates').swiper({
 mode: 'horizontal',
 slidesPerView: 6,
 //pagination: '.blogcategory',
 paginationClickable: true,
 nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
 autoplay: 3000,
 loop: true
});
--></script>
</div>
";
        // line 104
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "default/template/mpblog/blogcategory.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  272 => 104,  250 => 84,  244 => 82,  242 => 81,  238 => 80,  232 => 78,  218 => 66,  208 => 63,  199 => 62,  197 => 61,  192 => 60,  188 => 59,  180 => 54,  177 => 53,  175 => 52,  172 => 51,  168 => 49,  160 => 47,  157 => 46,  147 => 44,  145 => 43,  140 => 42,  138 => 41,  133 => 40,  131 => 39,  123 => 37,  120 => 36,  117 => 35,  114 => 34,  111 => 33,  108 => 32,  102 => 30,  100 => 29,  95 => 28,  85 => 23,  80 => 21,  76 => 19,  74 => 18,  70 => 17,  63 => 12,  51 => 9,  47 => 8,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/mpblog/blogcategory.twig", "");
    }
}
