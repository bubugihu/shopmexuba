<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/mpblog/blog.twig */
class __TwigTemplate_d5ce12b229aec8c5aebd9325046910ccd262e5a1a82b03f2b4daafa87ac905ea extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<!-- Add Next and Previous Blog Link stats-->
<div class=\"mpblognavs ";
        // line 3
        echo ($context["themeclass"] ?? null);
        echo "\">
  ";
        // line 4
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["nextprev"] ?? null), "prev", [], "any", false, false, false, 4), "href", [], "any", false, false, false, 4)) {
            // line 5
            echo "  <div class=\"mpblognav mpblognavs-nextlink\">
    <a href=\"";
            // line 6
            echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["nextprev"] ?? null), "prev", [], "any", false, false, false, 6), "href", [], "any", false, false, false, 6);
            echo "\">
      <span class=\"mpblognavs-image\">
        <img style=\"";
            // line 8
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["nextprev"] ?? null), "prev", [], "any", false, false, false, 8), "posttype", [], "any", false, false, false, 8)) {
                echo " width: ";
                echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["nextprev"] ?? null), "prev", [], "any", false, false, false, 8), "width", [], "any", false, false, false, 8);
                echo "px; height: ";
                echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["nextprev"] ?? null), "prev", [], "any", false, false, false, 8), "height", [], "any", false, false, false, 8);
                echo "px; ";
            }
            echo "\" src=\"";
            echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["nextprev"] ?? null), "prev", [], "any", false, false, false, 8), "image", [], "any", false, false, false, 8);
            echo "\" alt=\"";
            echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["nextprev"] ?? null), "prev", [], "any", false, false, false, 8), "name", [], "any", false, false, false, 8);
            echo "\"/>
      </span>
      <span class=\"mpblognavs-icon\" style=\"border-top-right-radius: 50px; border-bottom-right-radius: 50px;\">
        <i class=\"fa fa-arrow-left\"></i>
      </span>
      <div class=\"clearfix\"></div>
      ";
            // line 14
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["nextprev"] ?? null), "prev", [], "any", false, false, false, 14), "name", [], "any", false, false, false, 14)) {
                // line 15
                echo "      <span class=\"mpblognavs-title\">
        <h4>";
                // line 16
                echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["nextprev"] ?? null), "prev", [], "any", false, false, false, 16), "name", [], "any", false, false, false, 16);
                echo "</h4>
      </span>
      ";
            }
            // line 19
            echo "    </a>
  </div>
  ";
        }
        // line 22
        echo "  ";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["nextprev"] ?? null), "next", [], "any", false, false, false, 22), "href", [], "any", false, false, false, 22)) {
            // line 23
            echo "  <div class=\"mpblognav mpblognavs-prevlink\">
    <a href=\"";
            // line 24
            echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["nextprev"] ?? null), "next", [], "any", false, false, false, 24), "href", [], "any", false, false, false, 24);
            echo "\">
      <span class=\"mpblognavs-icon\" style=\"border-top-left-radius: 50px; border-bottom-left-radius: 50px;\">
        <i class=\"fa fa-arrow-right\"></i>
      </span>
      <span class=\"mpblognavs-image\">
        <img style=\"";
            // line 29
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["nextprev"] ?? null), "next", [], "any", false, false, false, 29), "posttype", [], "any", false, false, false, 29)) {
                echo " width: ";
                echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["nextprev"] ?? null), "next", [], "any", false, false, false, 29), "width", [], "any", false, false, false, 29);
                echo "px; height: ";
                echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["nextprev"] ?? null), "next", [], "any", false, false, false, 29), "height", [], "any", false, false, false, 29);
                echo "px; ";
            }
            echo "\" src=\"";
            echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["nextprev"] ?? null), "next", [], "any", false, false, false, 29), "image", [], "any", false, false, false, 29);
            echo "\" alt=\"";
            echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["nextprev"] ?? null), "next", [], "any", false, false, false, 29), "name", [], "any", false, false, false, 29);
            echo "\"/>
      </span>
      <div class=\"clearfix\"></div>
      ";
            // line 32
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["nextprev"] ?? null), "next", [], "any", false, false, false, 32), "name", [], "any", false, false, false, 32)) {
                // line 33
                echo "      <span class=\"mpblognavs-title\">
        <h4>";
                // line 34
                echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["nextprev"] ?? null), "next", [], "any", false, false, false, 34), "name", [], "any", false, false, false, 34);
                echo "</h4>
      </span>
      ";
            }
            // line 37
            echo "    </a>
  </div>
  ";
        }
        // line 40
        echo "</div>
<div class=\"breadcrumb-width\">
  <div class=\"l-breadcrumb\" style=\"margin-bottom: 20px;\"> 
    <div class=\"l-breadcrumb-container\"> 
      <div class=\"l-breadcrumb-inner\"> 
        <nav role=\"navigation\" aria-label=\"現在位置\"> 
          <ol class=\"m-breadcrumb\" itemscope=\"itemscope\" itemtype=\"http://schema.org/BreadcrumbList\"> 
            ";
        // line 47
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 48
            echo "            <li itemprop=\"itemListElement\" itemscope=\"itemscope\" itemtype=\"http://schema.org/ListItem\"><a style=\"text-transform: uppercase;\" href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 48);
            echo "\" class=\"m-breadcrumb-item\" itemprop=\"item\"><span style=\"color: #fff; font-size: 18px;\" itemprop=\"name\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 48);
            echo "</span></a>
              <meta itemprop=\"position\" content=\"1\" style=\"color: #fff; font-size: 18px;\"> </li> 
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "          </ol> 
        </nav> 
      </div> 
    </div> 
  </div>
</div>
<!-- Add Next and Previous Blog Link ends-->
<div id=\"container\" class=\"container mp-blog j-container ";
        // line 58
        echo ($context["themeclass"] ?? null);
        echo "\">
  ";
        // line 59
        echo ($context["content_top"] ?? null);
        echo "
  <div class=\"row\">";
        // line 60
        echo ($context["column_left"] ?? null);
        echo "
    ";
        // line 61
        if ((($context["themename"] ?? null) == "journal2")) {
            // line 62
            echo "    ";
            echo ($context["column_right"] ?? null);
            echo "
    ";
        }
        // line 64
        echo "    ";
        if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
            // line 65
            echo "    ";
            $context["class"] = "col-sm-6";
            // line 66
            echo "    ";
        } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 67
            echo "    ";
            $context["class"] = "col-sm-9";
            // line 68
            echo "    ";
        } else {
            // line 69
            echo "    ";
            $context["class"] = "col-sm-12";
            // line 70
            echo "    ";
        }
        // line 71
        echo "    <div id=\"content\" class=\"";
        echo ($context["class"] ?? null);
        echo "\">
      <h1 style=\"font-size: 40px;\">";
        // line 72
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <div class=\"";
        // line 73
        if ((($context["themename"] ?? null) != "journal2")) {
            echo " row ";
        }
        echo "\">
        <div class=\"col-sm-12 xl-100 sm-100\">
          ";
        // line 75
        if (($context["show_rating"] ?? null)) {
            // line 76
            echo "          <ul class=\"list-inline text-center rating-wrap\">
            <li title=\"";
            // line 77
            echo ($context["rating"] ?? null);
            echo "\" class=\"small-rating\">
              <div class=\"rating text-center\">
                <div class=\"rating-icons-container text-center\">
                  <div class=\"rating-icons-wrap\">
                    ";
            // line 81
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, 5));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 82
                echo "                    ";
                if ((($context["rating"] ?? null) < $context["i"])) {
                    // line 83
                    echo "                    ";
                    $context["rdecimal"] = "EMPTY_STAR";
                    // line 84
                    echo "                    ";
                    if ((twig_round(($context["rating"] ?? null), 1, "ceil") == $context["i"])) {
                        // line 85
                        echo "                    ";
                        $context["rates"] = twig_split_filter($this->env, ($context["rating"] ?? null), ".");
                        // line 86
                        echo "                    ";
                        if (twig_get_attribute($this->env, $this->source, ($context["rates"] ?? null), 1, [], "any", false, false, false, 86)) {
                            // line 87
                            echo "                    ";
                            if ((("0" . twig_round(twig_get_attribute($this->env, $this->source, ($context["rates"] ?? null), 1, [], "any", false, false, false, 87), 1, "floor")) < 0.6)) {
                                // line 88
                                echo "                    ";
                                $context["rdecimal"] = "HALF_STAR";
                                // line 89
                                echo "                    ";
                            }
                            // line 90
                            echo "                    ";
                            if ((("0" . twig_round(twig_get_attribute($this->env, $this->source, ($context["rates"] ?? null), 1, [], "any", false, false, false, 90), 1, "floor")) > 0.6)) {
                                // line 91
                                echo "                    ";
                                $context["rdecimal"] = "FULL_STAR";
                                // line 92
                                echo "                    ";
                            }
                            // line 93
                            echo "                    ";
                        }
                        // line 94
                        echo "                    ";
                    }
                    // line 95
                    echo "                    ";
                    if ((($context["rdecimal"] ?? null) == "HALF_STAR")) {
                        // line 96
                        echo "                    <div class=\"rating-icons half\">
                      <span class=\"red\"></span>
                      <span class=\"grey\"></span>
                      <i class=\"fa fa-star\"></i>
                    </div>
                    ";
                    }
                    // line 101
                    echo " ";
                    if ((($context["rdecimal"] ?? null) == "FULL_STAR")) {
                        // line 102
                        echo "                    <div class=\"rating-icons full\">
                      <span class=\"red\"></span>
                      <span class=\"grey\"></span>
                      <i class=\"fa fa-star\"></i>
                    </div>
                    ";
                    }
                    // line 107
                    echo " ";
                    if ((($context["rdecimal"] ?? null) == "EMPTY_STAR")) {
                        // line 108
                        echo "                    <div class=\"rating-icons\">
                      <span class=\"red\"></span>
                      <span class=\"grey\"></span>
                      <i class=\"fa fa-star\"></i>
                    </div>
                    ";
                    }
                    // line 113
                    echo " ";
                } else {
                    // line 114
                    echo "                    <div class=\"rating-icons full\">
                      <span class=\"red\"></span>
                      <span class=\"grey\"></span>
                      <i class=\"fa fa-star\"></i>
                    </div>
                    ";
                }
                // line 120
                echo "                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 121
            echo "                  </div>
                </div>
              </div>
            </li>
          </ul>
          ";
        }
        // line 127
        echo "        </div>
        <div class=\"col-md-8 col-sm-12 col-xs-12 xl-70 sm-100\">
          <ul class=\"blog-info list-inline mpblogpost-id-";
        // line 129
        echo ($context["mpblogpost_id"] ?? null);
        echo "\">
            ";
        // line 130
        if (($context["show_date"] ?? null)) {
            // line 131
            echo "            <li  title=\"";
            echo ($context["date_available"] ?? null);
            echo "\"><i class=\"fa fa-calendar\"></i> ";
            if ( !twig_test_empty(($context["date_availableurl"] ?? null))) {
                echo " <a href=\"";
                echo ($context["date_availableurl"] ?? null);
                echo "\"> ";
                echo ($context["date_available"] ?? null);
                echo "</a> ";
            } else {
                echo " ";
                echo ($context["date_available"] ?? null);
                echo " ";
            }
            echo " </li> <span>|</span>
            ";
        }
        // line 133
        echo "            ";
        if (($context["show_author"] ?? null)) {
            // line 134
            echo "            <li title=\"";
            echo ($context["author"] ?? null);
            echo "\"><i class=\"fa fa-user\"></i> ";
            if ( !twig_test_empty(($context["authorurl"] ?? null))) {
                echo " <a href=\"";
                echo ($context["authorurl"] ?? null);
                echo "\"> ";
                echo ($context["author"] ?? null);
                echo " </a> ";
            } else {
                echo " ";
                echo ($context["author"] ?? null);
                echo " ";
            }
            echo "</li> <span class=\"hidden-xs\">|</span>
            ";
        }
        // line 136
        echo "            ";
        if (($context["show_comments"] ?? null)) {
            // line 137
            echo "            <li class=\"hidden-xs\" title=\"";
            echo ($context["comments"] ?? null);
            echo "\"><i class=\"fa ";
            if (($context["comments"] ?? null)) {
                echo " fa-comments ";
            } else {
                echo " fa-comments-o ";
            }
            echo " \"></i> ";
            echo ((($context["comments"] ?? null) . " ") . ($context["text_comment"] ?? null));
            echo "</li> <span class=\"hidden-xs\">|</span>
            ";
        }
        // line 139
        echo "            ";
        if (($context["show_wishlist"] ?? null)) {
            // line 140
            echo "            <li class=\"hidden-xs\" title=\"";
            echo ($context["wishlists"] ?? null);
            echo "\"><span title=\"";
            echo ($context["wishlists"] ?? null);
            echo "\" class=\"mpbloglike ";
            if (($context["isLikeByMe"] ?? null)) {
                echo " liked ";
            }
            echo "\" data-id=\"";
            echo ($context["mpblogpost_id"] ?? null);
            echo "\"><i class=\"fa fa-heart ";
            if ( !($context["isLikeByMe"] ?? null)) {
                echo " fa-heart-o ";
            }
            echo "\"></i> <span>";
            echo ($context["wishlists"] ?? null);
            echo "</span></span></li>
            ";
        }
        // line 142
        echo "
          </ul>
        </div>
        <div class=\"col-md-4 col-sm-12 col-xs-12 xl-30 sm-100\">
          ";
        // line 146
        if (($context["socialtop"] ?? null)) {
            // line 147
            echo "          <!-- Social Media Icons Starts -->
          <ul class=\"list-inline social-media\">
            <li><h3 class=\"visible-md visible-lg\">";
            // line 149
            echo ($context["text_follow"] ?? null);
            echo "</h3></li>
            ";
            // line 150
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["socials"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["social"]) {
                // line 151
                echo "            <li> <a target=\"_BLANK\" href=\"";
                echo twig_get_attribute($this->env, $this->source, $context["social"], "href", [], "any", false, false, false, 151);
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo twig_get_attribute($this->env, $this->source, $context["social"], "sname", [], "any", false, false, false, 151);
                echo "\"> <i class=\"";
                echo twig_get_attribute($this->env, $this->source, $context["social"], "icon", [], "any", false, false, false, 151);
                echo "\"></i>  </a> </li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['social'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 153
            echo "          </ul>
          <!-- Social Media Icons Ends -->
          ";
        }
        // line 156
        echo "        </div>
      </div>
      <div class=\"row\">
        <div class=\"col-sm-12 xl-100 sm-100\">
          <div class=\"main-image\">
            ";
        // line 161
        if (($context["showImage"] ?? null)) {
            // line 162
            echo "            ";
            if ((($context["thumb"] ?? null) && ($context["blog_image"] ?? null))) {
                // line 163
                echo "            <ul class=\"thumbnails\">
              ";
                // line 164
                if (($context["blog_image_popup"] ?? null)) {
                    // line 165
                    echo "              <li><a class=\"thumbnail\" href=\"";
                    echo ($context["popup"] ?? null);
                    echo "\" title=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\"><img src=\"";
                    echo ($context["thumb"] ?? null);
                    echo "\" title=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" alt=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" class=\"img-responsive\" /></a></li>
              ";
                } else {
                    // line 167
                    echo "              <li><img src=\"";
                    echo ($context["thumb"] ?? null);
                    echo "\" title=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" alt=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" class=\"img-responsive\" /></li>
              ";
                }
                // line 169
                echo "            </ul>
            ";
            }
            // line 171
            echo "            ";
        } else {
            // line 172
            echo "            <div class=\"video-container\">
              <iframe width=\"";
            // line 173
            echo ($context["width"] ?? null);
            echo "\" height=\"";
            echo ($context["height"] ?? null);
            echo "\" src=\"";
            echo ($context["iframeVideo"] ?? null);
            echo "\" frameborder=\"0\" allowfullscreen></iframe>
            </div>
            ";
        }
        // line 176
        echo "            ";
        if (($context["sharethis"] ?? null)) {
            // line 177
            echo "            <!-- AddThis Button BEGIN -->
            <div class=\"hidden-xs addthis_toolbox addthis_default_style addthis_20x20_style\" data-url=\"";
            // line 178
            echo ($context["share"] ?? null);
            echo "\">
              <div class=\"share\"><i class=\"fa fa-share-alt-square\" aria-hidden=\"true\"></i> <span>";
            // line 179
            echo ($context["text_share"] ?? null);
            echo "</span></div>
              <!-- <a class=\"addthis_button_facebook_like addthis_button_preferred_1\" fb:like:layout=\"button_count\"></a>  -->
              <a class=\"addthis_button_print addthis_button_preferred_1\"></a>
              <a class=\"addthis_button_facebook addthis_button_preferred_1\" fb:like:layout=\"button_count\"></a>
              <!-- <a class=\"addthis_button_tweet addthis_button_preferred_2\"></a>  -->
              <a class=\"addthis_button_twitter addthis_button_preferred_2\"></a>
              <a class=\"addthis_button_whatsapp addthis_button_preferred_3\"></a>
              <a class=\"addthis_button_email addthis_button_preferred_4\"></a>
              <a class=\"addthis_button_linkedin addthis_button_preferred_5\"></a>
              <!-- <a class=\"addthis_button_pinterest_pinit addthis_button_preferred_5\"></a>  -->
              <!-- <a class=\"addthis_counter addthis_pill_style addthis_button_preferred_7\"></a> -->
              <a class=\"addthis_button_compact addthis_button_preferred_6\"></a>
            </div>
            <script type=\"text/javascript\" src=\"//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e\"></script>
            <!-- AddThis Button END -->
            ";
        }
        // line 195
        echo "          </div>
          ";
        // line 196
        if (($context["images"] ?? null)) {
            // line 197
            echo "          <ul class=\"thumbnails list-inline\">
            ";
            // line 198
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["images"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 199
                echo "            ";
                if (($context["blog_image_popup"] ?? null)) {
                    // line 200
                    echo "            <li class=\"mpblogimage-additional\"><a class=\"thumbnail\" href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "popup", [], "any", false, false, false, 200);
                    echo "\" title=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\"> <img src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "thumb", [], "any", false, false, false, 200);
                    echo "\" title=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" alt=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" class=\"img-responsive\" /></a></li>
            ";
                } else {
                    // line 202
                    echo "            <li class=\"mpblogimage-additional\"><img src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "thumb", [], "any", false, false, false, 202);
                    echo "\" title=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" alt=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" class=\"img-responsive\" /></li>
            ";
                }
                // line 204
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 205
            echo "          </ul>
          ";
        }
        // line 207
        echo "          <div class=\"blog-details\">
            <div class=\"description\"><p>";
        // line 208
        echo ($context["description"] ?? null);
        echo "</p></div>
            <div class=\"row\">
              ";
        // line 210
        if ((($context["show_tag"] ?? null) && ($context["tags"] ?? null))) {
            // line 211
            echo "              <div class=\"col-sm-8 col-xs-12 xl-75 sm-100 tags\">
                <ul class=\"list-unstyled\">
                  <li>
                    <div class=\"icon-tag\"><i class=\"fa fa-tags\"></i>  ";
            // line 214
            echo ($context["text_tags"] ?? null);
            echo "</div>
                    ";
            // line 215
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["tags"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["rtag"]) {
                // line 216
                echo "                    ";
                if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, $context["rtag"], "href", [], "any", false, false, false, 216))) {
                    // line 217
                    echo "                    <a href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["rtag"], "href", [], "any", false, false, false, 217);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["rtag"], "tag", [], "any", false, false, false, 217);
                    echo "</a>
                    ";
                } else {
                    // line 219
                    echo "                    <span>";
                    echo twig_get_attribute($this->env, $this->source, $context["rtag"], "tag", [], "any", false, false, false, 219);
                    echo "</span>
                    ";
                }
                // line 221
                echo "                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rtag'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 222
            echo "                  </li>
                </ul>
              </div>
              ";
        }
        // line 226
        echo "              ";
        if (($context["socialbottom"] ?? null)) {
            // line 227
            echo "              <div class=\"col-sm-4 col-xs-12 xl-25 sm-100 hidden-xs pull-right\">
                <!-- Social Media Icons Starts -->
                <ul class=\"list-inline social-media \">
                  <li><h3 class=\"visible-md visible-lg\">";
            // line 230
            echo ($context["text_follow"] ?? null);
            echo "</h3></li>
                  ";
            // line 231
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["socials"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["social"]) {
                // line 232
                echo "                  <li> <a target=\"_BLANK\" href=\"";
                echo twig_get_attribute($this->env, $this->source, $context["social"], "href", [], "any", false, false, false, 232);
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo twig_get_attribute($this->env, $this->source, $context["social"], "sname", [], "any", false, false, false, 232);
                echo "\"> <i class=\"";
                echo twig_get_attribute($this->env, $this->source, $context["social"], "icon", [], "any", false, false, false, 232);
                echo "\"></i>  </a> </li>
                  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['social'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 234
            echo "                </ul>
                <!-- Social Media Icons Ends -->
              </div>
              ";
        }
        // line 238
        echo "            </div>

            ";
        // line 240
        if (($context["comment_tabs"] ?? null)) {
            echo " ";
            $context["total_comment_tabs"] = twig_length_filter($this->env, ($context["comment_tabs"] ?? null));
            // line 241
            echo "            <hr/>
            ";
            // line 242
            if ((($context["total_comment_tabs"] ?? null) > 1)) {
                // line 243
                echo "            <ul class=\"nav nav-tabs htabs clearfix\" id=\"comments\">
              ";
                // line 244
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["comment_tabs"] ?? null));
                foreach ($context['_seq'] as $context["key"] => $context["comment_tab"]) {
                    // line 245
                    echo "              <li><a href=\"#tab-";
                    echo $context["key"];
                    echo "\" data-toggle=\"tab\">";
                    echo $context["comment_tab"];
                    echo "</a></li>
              ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['comment_tab'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 247
                echo "            </ul>
            ";
            }
            // line 249
            echo "            ";
            if ((($context["total_comment_tabs"] ?? null) > 1)) {
                // line 250
                echo "            <div class=\"tab-content tabs-content\">
              ";
            }
            // line 252
            echo "              ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["comment_tabs"] ?? null));
            foreach ($context['_seq'] as $context["key"] => $context["comment_tab"]) {
                // line 253
                echo "              <div class=\"tab-pane\" id=\"tab-";
                echo $context["key"];
                echo "\">
                ";
                // line 254
                if (($context["key"] == "default")) {
                    // line 255
                    echo "                <div class=\"row\">
                  ";
                    // line 256
                    if (($context["show_comments"] ?? null)) {
                        // line 257
                        echo "                  <div class=\"col-sm-12 xl-100 sm-100\">
                    <h1 class=\"text-center\"><i class=\"fa fa-comments mp-fa\"></i> ";
                        // line 258
                        echo ($context["text_comments"] ?? null);
                        echo "</h1>
                    <div id=\"comment\"></div>
                  </div>
                  ";
                    }
                    // line 262
                    echo "                  ";
                    $context["class"] = "col-sm-12 xl-100";
                    echo " ";
                    if ((($context["allow_comment"] ?? null) && ($context["allow_rating"] ?? null))) {
                        echo " ";
                        $context["class"] = "col-sm-6 xl-50 sm-100";
                        echo " ";
                    }
                    // line 263
                    echo "                  ";
                    if (($context["allow_comment"] ?? null)) {
                        // line 264
                        echo "                  <div class=\"";
                        echo ($context["class"] ?? null);
                        echo " capcha\">
                    <form class=\"form-horizontal\" id=\"form-comment\">
                      <h1><i class=\"fa fa-pencil\"></i> ";
                        // line 266
                        echo ($context["text_write_comment"] ?? null);
                        echo "</h1>
                      ";
                        // line 267
                        if (($context["comment_guest"] ?? null)) {
                            // line 268
                            echo "                      <div class=\"form-group required\">
                        <div class=\"col-sm-12 xl-100 sm-100\">
                          <input type=\"text\" name=\"name\" value=\"";
                            // line 270
                            echo ($context["customer_name"] ?? null);
                            echo "\" placeholder=\"";
                            echo ($context["entry_name"] ?? null);
                            echo "\" id=\"input-name\" class=\"form-control\" />
                        </div>
                      </div>
                      <div class=\"form-group required\">
                        <div class=\"col-sm-12 xl-100 sm-100\">
                          <textarea name=\"text\" placeholder=\"";
                            // line 275
                            echo ($context["entry_comment"] ?? null);
                            echo "\" rows=\"5\" id=\"input-comment\" class=\"form-control\"></textarea>
                          <div class=\"help-block\">";
                            // line 276
                            echo ($context["text_note"] ?? null);
                            echo "</div>
                        </div>
                      </div>
                      ";
                            // line 279
                            echo ($context["captcha"] ?? null);
                            echo "
                      <div class=\"buttons clearfix\">
                        <div class=\"pull-left\">
                          <button type=\"button\" id=\"button-comment\" data-loading-text=\"";
                            // line 282
                            echo ($context["text_loading"] ?? null);
                            echo "\" class=\"btn btn-primary\">";
                            echo ($context["button_add_comment"] ?? null);
                            echo "</button>
                        </div>
                      </div>
                      ";
                        } else {
                            // line 286
                            echo "                      <div class=\"inner-rating\">
                        <div class=\"login-text\">";
                            // line 287
                            echo ($context["text_login_comment"] ?? null);
                            echo "</div>
                      </div>
                      ";
                        }
                        // line 290
                        echo "                    </form>
                  </div>
                  ";
                    }
                    // line 293
                    echo "                  ";
                    if (($context["allow_rating"] ?? null)) {
                        // line 294
                        echo "                  <div class=\"";
                        echo ($context["class"] ?? null);
                        echo " rating-wrap\">
                    <h1><i class=\"fa fa-thumbs-up\"></i> ";
                        // line 295
                        echo ($context["heading_give_rating"] ?? null);
                        echo "</h1>
                    <div class=\"inner-rating\">
                      <form class=\"form-horizontal\" id=\"form-rating\">
                        ";
                        // line 298
                        if (($context["rating_guest"] ?? null)) {
                            // line 299
                            echo "                        <div id=\"rating\"></div>
                        <div class=\"form-group required\">
                          <div class=\"col-sm-12 xl-100 sm-100\">

                            <div class=\"rating-container\">
                              <input type=\"hidden\" name=\"rating\" id=\"input-rating\" class=\"rating hide\" value=\"\" data-error=\"";
                            // line 304
                            echo ($context["error_rating"] ?? null);
                            echo "\" />
                              <div class=\"rating-icons-wrap\">
                                ";
                            // line 306
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(range(1, 5));
                            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                                // line 307
                                echo "                                <div class=\"rating-icons\" data-value=\"";
                                echo $context["i"];
                                echo "\">
                                  <span class=\"red\"></span>
                                  <span class=\"grey\"></span>
                                  <i class=\"fa fa-star\"></i>
                                </div>
                                ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 313
                            echo "                              </div>
                            </div>

                            <button class=\"btn btn-primary\" type=\"button\" id=\"button-rating\">";
                            // line 316
                            echo ($context["button_give_rating"] ?? null);
                            echo "</button>
                          </div>
                        </div>
                        ";
                        } else {
                            // line 320
                            echo "                        ";
                            echo ($context["text_login_rating"] ?? null);
                            echo "
                        ";
                        }
                        // line 322
                        echo "                      </form>
                    </div>
                  </div>
                  ";
                    }
                    // line 326
                    echo "                </div>
                ";
                }
                // line 328
                echo "                ";
                if (($context["key"] == "facebook")) {
                    // line 329
                    echo "                ";
                    if (($context["allow_comment"] ?? null)) {
                        // line 330
                        echo "                <!-- Include the JavaScript SDK  -->
                <div id=\"fb-root\"></div>
                <script>(function(d, s, id) {
                  var js, fjs = d.getElementsByTagName(s)[0];
                  if (d.getElementById(id)) return;
                  js = d.createElement(s); js.id = id;
                  js.src = \"//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.9&appId=";
                        // line 336
                        echo ($context["mpblog_facebook_appid"] ?? null);
                        echo "\";
                  fjs.parentNode.insertBefore(js, fjs);
                  }(document, 'script', 'facebook-jssdk'));
                </script>
                <!--  Place this code wherever you want the plugin to appear on your page. -->

                <div class=\"fb-comments\" data-href=\"";
                        // line 342
                        echo ($context["action"] ?? null);
                        echo "\" data-numposts=\"";
                        echo ($context["mpblog_facebook_nocomment"] ?? null);
                        echo "\"  data-order-by=\"";
                        echo ($context["mpblog_facebook_order"] ?? null);
                        echo "\" data-colorscheme=\"";
                        echo ($context["mpblog_facebook_color"] ?? null);
                        echo "\" data-width=\"";
                        echo ($context["mpblog_facebook_width"] ?? null);
                        echo "%\"></div>
                ";
                    }
                    // line 344
                    echo "                ";
                }
                // line 345
                echo "                ";
                if (($context["key"] == "google")) {
                    // line 346
                    echo "                ";
                    if (($context["allow_comment"] ?? null)) {
                        // line 347
                        echo "                <script src=\"https://apis.google.com/js/plusone.js\"></script>
                <div id=\"g-comments\">";
                        // line 348
                        echo ($context["text_loading"] ?? null);
                        echo "</div>
                <script>
                  gapi.comments.render('g-comments', {
                  href: '";
                        // line 351
                        echo ($context["action"] ?? null);
                        echo "',
                  width: '800',
                  height : '200',
                  first_party_property: 'BLOGGER',
                  view_type: 'FILTERED_POSTMOD',
                  });
                </script>
                ";
                    }
                    // line 359
                    echo "                ";
                }
                // line 360
                echo "                ";
                if (($context["key"] == "disqus")) {
                    // line 361
                    echo "                ";
                    if (($context["allow_comment"] ?? null)) {
                        // line 362
                        echo "                <div id=\"disqus_thread\"></div>
                <script>
                  /**
                  *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                  *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                  var disqus_config = function () {
                  this.page.url = '";
                        // line 368
                        echo ($context["action"] ?? null);
                        echo "';  // Replace PAGE_URL with your page's canonical URL variable
                  this.page.identifier = 'mpblogpost_id=";
                        // line 369
                        echo ($context["mpblogpost_id"] ?? null);
                        echo "'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                  };
                </script>
                ";
                        // line 372
                        echo twig_replace_filter(($context["comment_disqus_code"] ?? null), "<div id=\"disqus_thread\"></div>", "");
                        echo "
                ";
                        // line 373
                        echo ($context["comment_disqus_count"] ?? null);
                        echo "
                ";
                    }
                    // line 375
                    echo "                ";
                }
                // line 376
                echo "              </div>
              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['comment_tab'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 378
            echo "              ";
            if ((($context["total_comment_tabs"] ?? null) > 1)) {
                // line 379
                echo "            </div>
            ";
            }
            // line 381
            echo "            ";
        }
        // line 382
        echo "          </div>
        </div>
      </div>
      ";
        // line 385
        if (($context["mpblogposts"] ?? null)) {
            // line 386
            echo "      <h1>";
            echo ($context["text_related"] ?? null);
            echo "</h1>
      <hr/>
      <div class=\"row mp-grid-layout\">
        ";
            // line 389
            $context["i"] = 0;
            // line 390
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["mpblogposts"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["mpblogpost"]) {
                // line 391
                echo "        ";
                if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
                    // line 392
                    echo "        ";
                    $context["class"] = "col-xs-12 col-sm-6 col-md-6 col-lg-6 xl-50 md-50 sm-100";
                    // line 393
                    echo "        ";
                } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
                    // line 394
                    echo "        ";
                    $context["class"] = "col-xs-12 col-md-6 col-lg-6 col-sm-6 xl-33 md-50 sm-100";
                    // line 395
                    echo "        ";
                } else {
                    // line 396
                    echo "        ";
                    $context["class"] = "col-xs-12 col-sm-6 col-md-4 col-lg-4 xl-33 md-50 sm-100";
                    // line 397
                    echo "        ";
                }
                // line 398
                echo "        <div class=\"";
                echo ($context["class"] ?? null);
                echo " mpblog-layout \">
          <div class=\"inner-layout\">
            <div class=\"image\">
              ";
                // line 401
                if (twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "showImage", [], "any", false, false, false, 401)) {
                    // line 402
                    echo "              <div class=\"video-container\"><a href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "href", [], "any", false, false, false, 402);
                    echo "\"><img src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "thumb", [], "any", false, false, false, 402);
                    echo "\" alt=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "name", [], "any", false, false, false, 402);
                    echo "\" title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "name", [], "any", false, false, false, 402);
                    echo "\" class=\"img-responsive\" /></a></div>
              ";
                } else {
                    // line 404
                    echo "              <div class=\"video-container\">
                <iframe width=\"";
                    // line 405
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "width", [], "any", false, false, false, 405);
                    echo "\" height=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "height", [], "any", false, false, false, 405);
                    echo "\" src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "iframeVideo", [], "any", false, false, false, 405);
                    echo "\" frameborder=\"0\" allowfullscreen></iframe>
              </div>
              ";
                }
                // line 408
                echo "              ";
                if (($context["show_viewed"] ?? null)) {
                    // line 409
                    echo "              <div class=\"view\" title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "viewed", [], "any", false, false, false, 409);
                    echo "\">
                <i class=\"fa fa-eye\"></i>
                ";
                    // line 411
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "viewed", [], "any", false, false, false, 411);
                    echo "
              </div>
              ";
                }
                // line 414
                echo "            </div>
            <div class=\"caption\">
              <h4><a href=\"";
                // line 416
                echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "href", [], "any", false, false, false, 416);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "name", [], "any", false, false, false, 416);
                echo "</a></h4>
              <ul class=\"dar clearfix list-inline mpblogpost-id-";
                // line 417
                echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "mpblogpost_id", [], "any", false, false, false, 417);
                echo "\">
                <li class=\"col-sm-6 col-xs-6 xl-50\" title=\"";
                // line 418
                echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "date_available", [], "any", false, false, false, 418);
                echo "\">
                  ";
                // line 419
                if (($context["show_date"] ?? null)) {
                    // line 420
                    echo "                  <i class=\"fa fa-calendar\"></i>
                  ";
                    // line 421
                    if (twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "date_availableurl", [], "any", false, false, false, 421)) {
                        // line 422
                        echo "                  <a href=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "date_availableurl", [], "any", false, false, false, 422);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "date_available", [], "any", false, false, false, 422);
                        echo "</a>
                  ";
                    } else {
                        // line 424
                        echo "                  ";
                        echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "date_available", [], "any", false, false, false, 424);
                        echo "
                  ";
                    }
                    // line 426
                    echo "                  ";
                }
                // line 427
                echo "                </li>
                ";
                // line 428
                if (($context["show_author"] ?? null)) {
                    // line 429
                    echo "                <li title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "author", [], "any", false, false, false, 429);
                    echo "\" class=\"author col-xs-6 col-sm-6 xl-50\">
                  <i class=\"fa fa-user\"></i>
                  ";
                    // line 431
                    if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "authorurl", [], "any", false, false, false, 431))) {
                        // line 432
                        echo "                  <a href=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "authorurl", [], "any", false, false, false, 432);
                        echo "\">
                    ";
                        // line 433
                        echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "author", [], "any", false, false, false, 433);
                        echo "
                  </a>
                  ";
                    } else {
                        // line 436
                        echo "                  ";
                        echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "author", [], "any", false, false, false, 436);
                        echo "
                  ";
                    }
                    // line 438
                    echo "                </li>
                ";
                }
                // line 440
                echo "                ";
                if (($context["show_rating"] ?? null)) {
                    // line 441
                    echo "                <li class=\"col-sm-12 col-xs-12 xl-100\" title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "rating", [], "any", false, false, false, 441);
                    echo "\">
                  <div class=\"rating small-rating text-center\">
                    <div class=\"rating-icons-container text-center\">
                      <div class=\"rating-icons-wrap\">
                        ";
                    // line 445
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(range(1, 5));
                    foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                        // line 446
                        echo "                        ";
                        if ((twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "rating", [], "any", false, false, false, 446) < $context["i"])) {
                            // line 447
                            echo "                        ";
                            $context["rdecimal"] = "EMPTY_STAR";
                            // line 448
                            echo "
                        ";
                            // line 449
                            if ((twig_round(twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "rating", [], "any", false, false, false, 449), 1, "ceil") == $context["i"])) {
                                // line 450
                                echo "                        ";
                                $context["rates"] = twig_split_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "rating", [], "any", false, false, false, 450), ".");
                                // line 451
                                echo "                        ";
                                if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["rates"] ?? null), 1, [], "any", false, false, false, 451))) {
                                    // line 452
                                    echo "                        ";
                                    if ((("0" . twig_round(twig_get_attribute($this->env, $this->source, ($context["rates"] ?? null), 1, [], "any", false, false, false, 452), 1, "floor")) < 0.6)) {
                                        // line 453
                                        echo "                        ";
                                        $context["rdecimal"] = "HALF_STAR";
                                        // line 454
                                        echo "                        ";
                                    }
                                    // line 455
                                    echo "                        ";
                                    if ((("0" . twig_round(twig_get_attribute($this->env, $this->source, ($context["rates"] ?? null), 1, [], "any", false, false, false, 455), 1, "floor")) > 0.6)) {
                                        // line 456
                                        echo "                        ";
                                        $context["rdecimal"] = "FULL_STAR";
                                        // line 457
                                        echo "                        ";
                                    }
                                    // line 458
                                    echo "                        ";
                                }
                                // line 459
                                echo "                        ";
                            }
                            // line 460
                            echo "                        ";
                            if ((($context["rdecimal"] ?? null) == "HALF_STAR")) {
                                // line 461
                                echo "                        <div class=\"rating-icons half\">
                          <span class=\"red\"></span>
                          <span class=\"grey\"></span>
                          <i class=\"fa fa-star\"></i>
                        </div>
                        ";
                            }
                            // line 466
                            echo " ";
                            if ((($context["rdecimal"] ?? null) == "FULL_STAR")) {
                                // line 467
                                echo "                        <div class=\"rating-icons full\">
                          <span class=\"red\"></span>
                          <span class=\"grey\"></span>
                          <i class=\"fa fa-star\"></i>
                        </div>
                        ";
                            }
                            // line 472
                            echo " ";
                            if ((($context["rdecimal"] ?? null) == "EMPTY_STAR")) {
                                // line 473
                                echo "                        <div class=\"rating-icons\">
                          <span class=\"red\"></span>
                          <span class=\"grey\"></span>
                          <i class=\"fa fa-star\"></i>
                        </div>
                        ";
                            }
                            // line 478
                            echo " ";
                        } else {
                            // line 479
                            echo "                        <div class=\"rating-icons full\">
                          <span class=\"red\"></span>
                          <span class=\"grey\"></span>
                          <i class=\"fa fa-star\"></i>
                        </div>
                        ";
                        }
                        // line 485
                        echo "                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 486
                    echo "                      </div>
                    </div>
                  </div>
                </li>
                ";
                }
                // line 491
                echo "              </ul>
              ";
                // line 492
                if (($context["show_sdescription"] ?? null)) {
                    echo "<p class=\"desc\">";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "sdescription", [], "any", false, false, false, 492);
                    if ((twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "sdescription", [], "any", false, false, false, 492) && ($context["show_readmore"] ?? null))) {
                        echo " <a href=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "href", [], "any", false, false, false, 492);
                        echo "\">  ";
                        echo ($context["text_readmore"] ?? null);
                        echo " </a> ";
                    }
                    echo "</p>";
                }
                // line 493
                echo "              <ul class=\"list-inline dar cmt-wsh clearfix\">
                ";
                // line 494
                if (($context["show_wishlist"] ?? null)) {
                    // line 495
                    echo "                <li title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "wishlist", [], "any", false, false, false, 495);
                    echo "\">
                  <span class=\"mpbloglike ";
                    // line 496
                    if (twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "isLikeByMe", [], "any", false, false, false, 496)) {
                        echo " liked ";
                    }
                    echo "\" data-id=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "mpblogpost_id", [], "any", false, false, false, 496);
                    echo "\">
                    <i class=\"fa fa-heart ";
                    // line 497
                    if ( !twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "isLikeByMe", [], "any", false, false, false, 497)) {
                        echo " fa-heart-o ";
                    }
                    echo " \"></i>
                    <span>";
                    // line 498
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "wishlist", [], "any", false, false, false, 498);
                    echo "</span>
                  </span>
                </li>
                ";
                }
                // line 502
                echo "                ";
                if (($context["show_comments"] ?? null)) {
                    // line 503
                    echo "                <li class=\"pull-right\" title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "comments", [], "any", false, false, false, 503);
                    echo "\">
                  <i class=\"fa ";
                    // line 504
                    if (twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "comments", [], "any", false, false, false, 504)) {
                        echo " fa-comments ";
                    } else {
                        echo " fa-comments-o ";
                    }
                    echo "\"></i>
                  ";
                    // line 505
                    echo ((twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "comments", [], "any", false, false, false, 505) . " ") . ($context["text_comment"] ?? null));
                    echo "
                </li>
                ";
                }
                // line 508
                echo "              </ul>
              ";
                // line 509
                if ((($context["show_tag"] ?? null) && twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "tag", [], "any", false, false, false, 509))) {
                    // line 510
                    echo "              <ul class=\"list-inline blog-tags\">
                <li>
                  <i class=\"fa fa-tags\"></i>
                  ";
                    // line 513
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "tag", [], "any", false, false, false, 513));
                    foreach ($context['_seq'] as $context["_key"] => $context["rtag"]) {
                        // line 514
                        echo "                  ";
                        if (twig_get_attribute($this->env, $this->source, $context["rtag"], "href", [], "any", false, false, false, 514)) {
                            // line 515
                            echo "                  <a href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["rtag"], "href", [], "any", false, false, false, 515);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["rtag"], "tag", [], "any", false, false, false, 515);
                            echo "</a>
                  ";
                        } else {
                            // line 517
                            echo "                  ";
                            echo twig_get_attribute($this->env, $this->source, $context["rtag"], "tag", [], "any", false, false, false, 517);
                            echo "
                  ";
                        }
                        // line 519
                        echo "                  ,
                  ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rtag'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 521
                    echo "                </li>
              </ul>
              ";
                }
                // line 524
                echo "            </div>
          </div>
        </div>
        ";
                // line 527
                if (((($context["column_left"] ?? null) && ($context["column_right"] ?? null)) && (((($context["i"] ?? null) + 1) % 2) == 0))) {
                    // line 528
                    echo "        <div class=\"clearfix visible-md visible-sm\"></div>
        ";
                } elseif (((                // line 529
($context["column_left"] ?? null) || ($context["column_right"] ?? null)) && (((($context["i"] ?? null) + 1) % 3) == 0))) {
                    // line 530
                    echo "        <div class=\"clearfix visible-md\"></div>
        ";
                } elseif ((((                // line 531
($context["i"] ?? null) + 1) % 4) == 0)) {
                    // line 532
                    echo "        <div class=\"clearfix visible-md\"></div>
        ";
                }
                // line 534
                echo "        ";
                $context["i"] = (($context["i"] ?? null) + 1);
                // line 535
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mpblogpost'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 536
            echo "      </div>
      ";
        }
        // line 538
        echo "
      ";
        // line 539
        if (($context["products"] ?? null)) {
            // line 540
            echo "      <h3 class=\"box-heading\">";
            echo ($context["text_related_products"] ?? null);
            echo "</h3>
      <hr/>
      <div class=\"row\" style=\"margin-bottom: 3%\">
         <ul class=\"product-list\" style=\"justify-content: start; width: 100%\">
            ";
            // line 544
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 545
                echo "            <li id=\"product";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 545);
                echo "\">
              <a class=\"product-link\" href=\"";
                // line 546
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 546);
                echo "\">
                <div class=\"product-img\">
                  <img alt=\"";
                // line 548
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 548);
                echo "\" src=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb", [], "any", false, false, false, 548);
                echo "\" data-size=\"lg\">
                </div>
                <div class=\"product-body\">
                  <p class=\"product-name\">";
                // line 551
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 551);
                echo "</p>
                </div>
              </a>
            </li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 556
            echo "          </ul>
      </div>
      ";
        }
        // line 559
        echo "
      ";
        // line 560
        if (($context["categories"] ?? null)) {
            // line 561
            echo "      <h3 class=\"box-heading\">";
            echo ($context["text_inity_products"] ?? null);
            echo "</h3>
      <hr/>
      <div class=\"row\">
        ";
            // line 564
            $context["i"] = 0;
            // line 565
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 566
                echo "        ";
                if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
                    // line 567
                    echo "        ";
                    $context["class"] = "col-xs-12 col-sm-6 xl-50 sm-100";
                    // line 568
                    echo "        ";
                } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
                    // line 569
                    echo "        ";
                    $context["class"] = "col-xs-12 col-md-4 xl-33 sm-100";
                    // line 570
                    echo "        ";
                } else {
                    // line 571
                    echo "        ";
                    $context["class"] = "col-xs-12 col-sm-3 xl-25 sm-100";
                    // line 572
                    echo "        ";
                }
                // line 573
                echo "        <div class=\"product-grid-item ";
                echo ($context["class"] ?? null);
                echo "\">
          <div class=\"product-thumb transition product-wrapper\">
            <div class=\"image\"><a href=\"";
                // line 575
                echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 575);
                echo "\"><img src=\"";
                echo twig_get_attribute($this->env, $this->source, $context["category"], "thumb", [], "any", false, false, false, 575);
                echo "\" alt=\"";
                echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 575);
                echo "\" title=\"";
                echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 575);
                echo "\" class=\"img-responsive\" /></a></div>
            <div class=\"product-details\">
              <div class=\"caption\">
                <h4 class=\"name\"><a href=\"";
                // line 578
                echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 578);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 578);
                echo "</a></h4>
                <p>";
                // line 579
                echo twig_get_attribute($this->env, $this->source, $context["category"], "description", [], "any", false, false, false, 579);
                echo "</p>
              </div>
            </div>
          </div>
        </div>
        ";
                // line 584
                if (((($context["column_left"] ?? null) && ($context["column_right"] ?? null)) && (((($context["i"] ?? null) + 1) % 2) == 0))) {
                    // line 585
                    echo "        <div class=\"clearfix visible-md visible-sm\"></div>
        ";
                } elseif (((                // line 586
($context["column_left"] ?? null) || ($context["column_right"] ?? null)) && (((($context["i"] ?? null) + 1) % 3) == 0))) {
                    // line 587
                    echo "        <div class=\"clearfix visible-md\"></div>
        ";
                } elseif ((((                // line 588
($context["i"] ?? null) + 1) % 4) == 0)) {
                    // line 589
                    echo "        <div class=\"clearfix visible-md\"></div>
        ";
                }
                // line 591
                echo "        ";
                $context["i"] = (($context["i"] ?? null) + 1);
                // line 592
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 593
            echo "      </div>
      ";
        }
        // line 595
        echo "
      ";
        // line 596
        echo ($context["content_bottom"] ?? null);
        echo "</div>
    ";
        // line 597
        if ((($context["themename"] ?? null) != "journal2")) {
            // line 598
            echo "    ";
            echo ($context["column_right"] ?? null);
            echo "
    ";
        }
        // line 600
        echo "  </div>
</div>
<script type=\"text/javascript\"><!--
\$('#comment').delegate('.pagination a', 'click', function(e) {
  e.preventDefault();

  \$('#comment').fadeOut('slow');

  \$('#comment').load(this.href);

  \$('#comment').fadeIn('slow');
});

\$('#comment').load('index.php?route=mpblog/blog/comment&mpblogpost_id=";
        // line 613
        echo ($context["mpblogpost_id"] ?? null);
        echo "');

\$('#button-comment').on('click', function() {
 \$.ajax({
  url: 'index.php?route=mpblog/blog/writeComment&mpblogpost_id=";
        // line 617
        echo ($context["mpblogpost_id"] ?? null);
        echo "',
  type: 'post',
  dataType: 'json',
  data: \$(\"#form-comment\").serialize(),
  beforeSend: function() {
   \$('#button-comment').button('loading');
  },
  complete: function() {
   \$('#button-comment').button('reset');
  },
  success: function(json) {
   \$('.alert-success, .alert-danger').remove();

   if (json['error']) {
    \$('#form-comment').after('<div class=\"alert alert-danger warning\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error'] + '</div>');
   }

   if (json['success']) {
    \$('#form-comment').after('<div class=\"alert alert-success success\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + '</div>');
    \$('input[name=\\'name\\']').val('');
    \$('#input-comment').val('');
    \$('#form-comment input[type=\"text\"]').val('');

    // load latest comments
    \$('#comment').fadeOut('slow');
    \$('#comment').load('index.php?route=mpblog/blog/comment&mpblogpost_id=";
        // line 642
        echo ($context["mpblogpost_id"] ?? null);
        echo "');
    \$('#comment').fadeIn('slow');

   }
  }
 });
});

\$('#button-rating').on('click', function() {
 \$.ajax({
  url: 'index.php?route=mpblog/blog/addRating&mpblogpost_id=";
        // line 652
        echo ($context["mpblogpost_id"] ?? null);
        echo "',
  type: 'post',
  dataType: 'json',
  data: \$(\"#form-rating\").serialize(),
  beforeSend: function() {
   \$('#button-rating').button('loading');
  },
  complete: function() {
   \$('#button-rating').button('reset');
  },
  success: function(json) {
   \$('.alert-success, .alert-danger').remove();

   if (json['error']) {
    \$('#rating').after('<div class=\"alert alert-danger warning\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error'] + '</div>');
   }

   if (json['success']) {
    \$('#rating').after('<div class=\"alert alert-success success\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + '</div>');
    \$('input[name=\\'rating\\']:checked').prop('checked', false);
   }
  }
 });
});

\$(document).ready(function() {

 ";
        // line 679
        if (($context["blog_image_popup"] ?? null)) {
            // line 680
            echo " \$.each(\$('.thumbnails'), function() {
  \$(this).magnificPopup({
   type:'image',
   delegate: 'a',
   gallery: {
    enabled:true
   }
  });
 });
 ";
        }
        // line 690
        echo "\$('#comments a:first').tab('show');

});
//--></script>
";
        // line 694
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "default/template/mpblog/blog.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1740 => 694,  1734 => 690,  1722 => 680,  1720 => 679,  1690 => 652,  1677 => 642,  1649 => 617,  1642 => 613,  1627 => 600,  1621 => 598,  1619 => 597,  1615 => 596,  1612 => 595,  1608 => 593,  1602 => 592,  1599 => 591,  1595 => 589,  1593 => 588,  1590 => 587,  1588 => 586,  1585 => 585,  1583 => 584,  1575 => 579,  1569 => 578,  1557 => 575,  1551 => 573,  1548 => 572,  1545 => 571,  1542 => 570,  1539 => 569,  1536 => 568,  1533 => 567,  1530 => 566,  1525 => 565,  1523 => 564,  1516 => 561,  1514 => 560,  1511 => 559,  1506 => 556,  1495 => 551,  1487 => 548,  1482 => 546,  1477 => 545,  1473 => 544,  1465 => 540,  1463 => 539,  1460 => 538,  1456 => 536,  1450 => 535,  1447 => 534,  1443 => 532,  1441 => 531,  1438 => 530,  1436 => 529,  1433 => 528,  1431 => 527,  1426 => 524,  1421 => 521,  1414 => 519,  1408 => 517,  1400 => 515,  1397 => 514,  1393 => 513,  1388 => 510,  1386 => 509,  1383 => 508,  1377 => 505,  1369 => 504,  1364 => 503,  1361 => 502,  1354 => 498,  1348 => 497,  1340 => 496,  1335 => 495,  1333 => 494,  1330 => 493,  1317 => 492,  1314 => 491,  1307 => 486,  1301 => 485,  1293 => 479,  1290 => 478,  1282 => 473,  1279 => 472,  1271 => 467,  1268 => 466,  1260 => 461,  1257 => 460,  1254 => 459,  1251 => 458,  1248 => 457,  1245 => 456,  1242 => 455,  1239 => 454,  1236 => 453,  1233 => 452,  1230 => 451,  1227 => 450,  1225 => 449,  1222 => 448,  1219 => 447,  1216 => 446,  1212 => 445,  1204 => 441,  1201 => 440,  1197 => 438,  1191 => 436,  1185 => 433,  1180 => 432,  1178 => 431,  1172 => 429,  1170 => 428,  1167 => 427,  1164 => 426,  1158 => 424,  1150 => 422,  1148 => 421,  1145 => 420,  1143 => 419,  1139 => 418,  1135 => 417,  1129 => 416,  1125 => 414,  1119 => 411,  1113 => 409,  1110 => 408,  1100 => 405,  1097 => 404,  1085 => 402,  1083 => 401,  1076 => 398,  1073 => 397,  1070 => 396,  1067 => 395,  1064 => 394,  1061 => 393,  1058 => 392,  1055 => 391,  1050 => 390,  1048 => 389,  1041 => 386,  1039 => 385,  1034 => 382,  1031 => 381,  1027 => 379,  1024 => 378,  1017 => 376,  1014 => 375,  1009 => 373,  1005 => 372,  999 => 369,  995 => 368,  987 => 362,  984 => 361,  981 => 360,  978 => 359,  967 => 351,  961 => 348,  958 => 347,  955 => 346,  952 => 345,  949 => 344,  936 => 342,  927 => 336,  919 => 330,  916 => 329,  913 => 328,  909 => 326,  903 => 322,  897 => 320,  890 => 316,  885 => 313,  872 => 307,  868 => 306,  863 => 304,  856 => 299,  854 => 298,  848 => 295,  843 => 294,  840 => 293,  835 => 290,  829 => 287,  826 => 286,  817 => 282,  811 => 279,  805 => 276,  801 => 275,  791 => 270,  787 => 268,  785 => 267,  781 => 266,  775 => 264,  772 => 263,  763 => 262,  756 => 258,  753 => 257,  751 => 256,  748 => 255,  746 => 254,  741 => 253,  736 => 252,  732 => 250,  729 => 249,  725 => 247,  714 => 245,  710 => 244,  707 => 243,  705 => 242,  702 => 241,  698 => 240,  694 => 238,  688 => 234,  675 => 232,  671 => 231,  667 => 230,  662 => 227,  659 => 226,  653 => 222,  647 => 221,  641 => 219,  633 => 217,  630 => 216,  626 => 215,  622 => 214,  617 => 211,  615 => 210,  610 => 208,  607 => 207,  603 => 205,  597 => 204,  587 => 202,  573 => 200,  570 => 199,  566 => 198,  563 => 197,  561 => 196,  558 => 195,  539 => 179,  535 => 178,  532 => 177,  529 => 176,  519 => 173,  516 => 172,  513 => 171,  509 => 169,  499 => 167,  485 => 165,  483 => 164,  480 => 163,  477 => 162,  475 => 161,  468 => 156,  463 => 153,  450 => 151,  446 => 150,  442 => 149,  438 => 147,  436 => 146,  430 => 142,  410 => 140,  407 => 139,  393 => 137,  390 => 136,  372 => 134,  369 => 133,  351 => 131,  349 => 130,  345 => 129,  341 => 127,  333 => 121,  327 => 120,  319 => 114,  316 => 113,  308 => 108,  305 => 107,  297 => 102,  294 => 101,  286 => 96,  283 => 95,  280 => 94,  277 => 93,  274 => 92,  271 => 91,  268 => 90,  265 => 89,  262 => 88,  259 => 87,  256 => 86,  253 => 85,  250 => 84,  247 => 83,  244 => 82,  240 => 81,  233 => 77,  230 => 76,  228 => 75,  221 => 73,  217 => 72,  212 => 71,  209 => 70,  206 => 69,  203 => 68,  200 => 67,  197 => 66,  194 => 65,  191 => 64,  185 => 62,  183 => 61,  179 => 60,  175 => 59,  171 => 58,  162 => 51,  150 => 48,  146 => 47,  137 => 40,  132 => 37,  126 => 34,  123 => 33,  121 => 32,  105 => 29,  97 => 24,  94 => 23,  91 => 22,  86 => 19,  80 => 16,  77 => 15,  75 => 14,  56 => 8,  51 => 6,  48 => 5,  46 => 4,  42 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/mpblog/blog.twig", "");
    }
}
