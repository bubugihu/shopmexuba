<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/module/sgfullwidthslider.twig */
class __TwigTemplate_06efc2ad25e5080436b965a25d70747d92bf1443bf3992417d2b81df067e097e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
</div>
</div>
</div>
<main class=\"l-main\" role=\"main\">
  <div class=\"l-container\">
    <div class=\"wrapper\">
      <div class=\"main-visual js-main-visual main_response\" style=\"height: 154px\">
        <div class=\"main-visual-images\">
          <ul class=\"main-visual-image-list js-main-visual-img-list\" role=\"presentation\">
            ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["banners"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
            // line 12
            echo "            ";
            if (twig_get_attribute($this->env, $this->source, $context["banner"], "link", [], "any", false, false, false, 12)) {
                // line 13
                echo "            <li class=\"js-main-visual-img\" data-num=\"";
                echo twig_get_attribute($this->env, $this->source, $context["banner"], "sort_order", [], "any", false, false, false, 13);
                echo "\" data-scene=\"s0\" data-svg=\"s1\" role=\"presentation\">
              <a href=\"";
                // line 14
                echo twig_get_attribute($this->env, $this->source, $context["banner"], "link", [], "any", false, false, false, 14);
                echo "\"><img width=\"100%\" height=\"100%\" style=\"margin-top: 0px; margin-left: 0px; background-size: cover;\" src=\"";
                echo twig_get_attribute($this->env, $this->source, $context["banner"], "image", [], "any", false, false, false, 14);
                echo "\" alt=\"";
                echo twig_get_attribute($this->env, $this->source, $context["banner"], "title", [], "any", false, false, false, 14);
                echo "\" class=\"js-main-visual-illustration img-fullwidth\" /></a>
            </li>
            ";
            } else {
                // line 17
                echo "            <li class=\"js-main-visual-img\" data-num=\"";
                echo twig_get_attribute($this->env, $this->source, $context["banner"], "sort_order", [], "any", false, false, false, 17);
                echo "\" data-scene=\"s0\" data-svg=\"s2\" role=\"presentation\">
              <img width=\"100%\" height=\"100%\" style=\"margin-top: 0px; margin-left: 0px; background-size: cover;\" src=\"";
                // line 18
                echo twig_get_attribute($this->env, $this->source, $context["banner"], "image", [], "any", false, false, false, 18);
                echo "\" alt=\"";
                echo twig_get_attribute($this->env, $this->source, $context["banner"], "title", [], "any", false, false, false, 18);
                echo "\" class=\"js-main-visual-illustration img-fullwidth\" />
            </li>
            ";
            }
            // line 21
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo " 
          </ul>
        </div>
        <div class=\"main-visual-blurs\">
          <ul class=\"main-visual-blur-list js-main-visual-blur-list\" role=\"presentation\">
            <li class=\"js-main-visual-blur\" style=\"background-size: cover;\" data-num=\"1\" role=\"presentation\"></li>
            <li class=\"js-main-visual-blur\" style=\"background-size: cover;\" data-num=\"2\" role=\"presentation\"></li>
          </ul>
        </div>
        <div class=\"main-visual-inner js-main-visual-inner\">
          <div class=\"main-visual-controller\">
            <a class=\"main-visual-controller-btn js-main-visual-controller\" style=\"z-index: 88\">
              <img src=\"catalog/view/theme/default/stylesheet/img/icons/pause.svg\" style=\"width: 43px;\" class=\"js-main-visual-controller-icn\" alt=\"停止\" title=\"停止\">
            </a>
          </div>
        </div>
      </div>
      ";
        // line 46
        echo "
<script type=\"text/javascript\">
  if(\$(window).width() < 800)
  {
    \$('.main_response').css('height','154px important');
  }
</script>";
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/sgfullwidthslider.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 46,  84 => 21,  76 => 18,  71 => 17,  61 => 14,  56 => 13,  53 => 12,  49 => 11,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/extension/module/sgfullwidthslider.twig", "");
    }
}
