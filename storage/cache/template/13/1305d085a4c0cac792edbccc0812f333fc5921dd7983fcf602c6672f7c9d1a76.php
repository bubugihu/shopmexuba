<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/mpblog/blogcategory_post_list.twig */
class __TwigTemplate_7c4c1f20b878ba65299c1d61e445c12a50946be0ab317e701091410c9c180d49 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["mpblogposts"] ?? null)) {
            // line 2
            echo "<hr/>
<div class=\"\">
\t";
            // line 4
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["mpblogposts"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["mpblogpost"]) {
                // line 5
                echo "\t<div class=\"mpblogpost-layout mpblogpost-list\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-sm-4 xl-33 sm-100\">
\t\t\t\t<div class=\"image\">
\t\t\t\t";
                // line 9
                if (twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "showImage", [], "any", false, false, false, 9)) {
                    // line 10
                    echo "\t\t\t\t<a href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "href", [], "any", false, false, false, 10);
                    echo "\"><img src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "thumb", [], "any", false, false, false, 10);
                    echo "\" alt=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "name", [], "any", false, false, false, 10);
                    echo "\" title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "name", [], "any", false, false, false, 10);
                    echo "\" class=\"img-responsive\" /></a>
\t\t\t\t";
                } else {
                    // line 12
                    echo "\t\t\t\t<div class=\"video-container\"><iframe width=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "width", [], "any", false, false, false, 12);
                    echo "\" height=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "height", [], "any", false, false, false, 12);
                    echo "\" src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "iframeVideo", [], "any", false, false, false, 12);
                    echo "\" frameborder=\"0\" allowfullscreen></iframe></div>
\t\t\t\t";
                }
                // line 14
                echo "\t\t\t\t<ul class=\"list-inline dar hidden-xs\">
\t\t\t\t\t";
                // line 15
                if (($context["show_author"] ?? null)) {
                    // line 16
                    echo "\t\t\t\t\t<li title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "author", [], "any", false, false, false, 16);
                    echo "\">
\t\t\t\t\t\t<i class=\"fa fa-user\"></i>                    
\t\t\t\t\t\t";
                    // line 18
                    if (twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "authorurl", [], "any", false, false, false, 18)) {
                        // line 19
                        echo "\t\t\t\t\t\t<a href=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "authorurl", [], "any", false, false, false, 19);
                        echo "\">
\t\t\t\t\t\t\t";
                        // line 20
                        echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "author", [], "any", false, false, false, 20);
                        echo "
\t\t\t\t\t\t</a>
\t\t\t\t\t\t";
                    } else {
                        // line 23
                        echo "\t\t\t\t\t\t";
                        echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "author", [], "any", false, false, false, 23);
                        echo "
\t\t\t\t\t\t";
                    }
                    // line 25
                    echo "\t\t\t\t\t</li>
\t\t\t\t\t";
                }
                // line 27
                echo "\t\t\t\t</ul>  
\t\t\t\t";
                // line 28
                if (($context["show_viewed"] ?? null)) {
                    // line 29
                    echo "\t\t\t\t\t<div class=\"view\" title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "viewed", [], "any", false, false, false, 29);
                    echo "\">
\t\t\t\t\t\t<i class=\"fa fa-eye\"></i>
\t\t\t\t\t\t";
                    // line 31
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "viewed", [], "any", false, false, false, 31);
                    echo "
\t\t\t\t\t</div>
\t\t\t\t";
                }
                // line 34
                echo "\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-sm-8 xl-66 sm-100\">
\t\t\t\t<div class=\"caption\">
\t\t\t\t\t<h4><a href=\"";
                // line 38
                echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "href", [], "any", false, false, false, 38);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "name", [], "any", false, false, false, 38);
                echo "</a></h4>
\t\t\t\t\t<ul class=\"list-inline dar mpblogpost-id-";
                // line 39
                echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "mpblogpost_id", [], "any", false, false, false, 39);
                echo "\">
\t\t\t\t\t\t";
                // line 40
                if (($context["show_date"] ?? null)) {
                    // line 41
                    echo "\t\t\t\t\t\t<li title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "date_available", [], "any", false, false, false, 41);
                    echo "\">
\t\t\t\t\t\t\t<i class=\"fa fa-calendar\"></i>
\t\t\t\t\t\t\t";
                    // line 43
                    if (twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "date_availableurl", [], "any", false, false, false, 43)) {
                        // line 44
                        echo "\t\t\t\t\t\t\t<a href=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "date_availableurl", [], "any", false, false, false, 44);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "date_available", [], "any", false, false, false, 44);
                        echo "</a>
\t\t\t\t\t\t\t";
                    } else {
                        // line 46
                        echo "\t\t\t\t\t\t\t";
                        echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "date_available", [], "any", false, false, false, 46);
                        echo "
\t\t\t\t\t\t\t";
                    }
                    // line 48
                    echo "\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
                }
                // line 50
                echo "\t\t\t\t\t\t";
                if (($context["show_rating"] ?? null)) {
                    // line 51
                    echo "\t\t\t\t\t\t\t<li title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "rating", [], "any", false, false, false, 51);
                    echo "\">
\t\t\t\t\t\t\t\t<div class=\"rating small-rating\">
\t\t\t\t\t\t\t\t\t<div class=\"rating-icons-container text-center\">
\t\t\t\t\t\t\t\t\t<div class=\"rating-icons-wrap\">
\t\t\t\t\t\t\t\t\t ";
                    // line 55
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(range(1, 5));
                    foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                        // line 56
                        echo "\t\t\t\t\t\t\t\t\t\t";
                        if ((twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "rating", [], "any", false, false, false, 56) < $context["i"])) {
                            // line 57
                            echo "\t\t\t\t\t\t\t\t\t\t";
                            $context["rdecimal"] = "EMPTY_STAR";
                            // line 58
                            echo "\t\t\t\t\t\t\t\t\t\t";
                            if ((twig_round(twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "rating", [], "any", false, false, false, 58), 1, "ceil") == $context["i"])) {
                                // line 59
                                echo "\t\t\t\t\t\t\t\t\t\t";
                                $context["rates"] = twig_split_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "rating", [], "any", false, false, false, 59), ".");
                                // line 60
                                echo "\t\t\t\t\t\t\t\t\t\t\t";
                                if ((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["rates"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[1] ?? null) : null)) {
                                    // line 61
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                                    if ((("0" . twig_round((($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["rates"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[1] ?? null) : null), 1, "floor")) < 0.6)) {
                                        // line 62
                                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                        $context["rdecimal"] = "HALF_STAR";
                                        // line 63
                                        echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                                    }
                                    // line 64
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                                    if ((("0" . twig_round((($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["rates"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b[1] ?? null) : null), 1, "floor")) > 0.6)) {
                                        // line 65
                                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                        $context["rdecimal"] = "FULL_STAR";
                                        // line 66
                                        echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                                    }
                                    // line 67
                                    echo "                   \t\t";
                                }
                                // line 68
                                echo "                 \t\t ";
                            }
                            // line 69
                            echo "\t\t\t\t\t\t\t\t\t";
                            if ((($context["rdecimal"] ?? null) == "HALF_STAR")) {
                                // line 70
                                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"rating-icons half\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"red\"></span>  
\t\t\t\t\t\t\t\t\t\t\t<span class=\"grey\"></span>  
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
                            }
                            // line 75
                            echo " ";
                            if ((($context["rdecimal"] ?? null) == "FULL_STAR")) {
                                // line 76
                                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"rating-icons full\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"red\"></span>  
\t\t\t\t\t\t\t\t\t\t\t<span class=\"grey\"></span>  
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
                            }
                            // line 81
                            echo " ";
                            if ((($context["rdecimal"] ?? null) == "EMPTY_STAR")) {
                                // line 82
                                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"rating-icons\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"red\"></span>  
\t\t\t\t\t\t\t\t\t\t\t<span class=\"grey\"></span>  
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
                            }
                            // line 87
                            echo " ";
                        } else {
                            // line 88
                            echo "                  <div class = \"rating-icons full\">
                    <span class = \"red\"></span>  
                    <span class = \"grey\"></span>  
                    <i class = \"fa fa-star\"></i>
                  </div>
\t\t\t\t\t\t\t\t\t";
                        }
                        // line 94
                        echo "            \t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 95
                    echo "\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t";
                }
                // line 100
                echo "\t\t\t\t\t</ul>
\t\t\t\t\t";
                // line 101
                if (($context["show_sdescription"] ?? null)) {
                    echo "<p class=\"desc\">";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "sdescription1", [], "any", false, false, false, 101);
                    // line 102
                    echo " ";
                    if ((twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "sdescription1", [], "any", false, false, false, 102) && ($context["show_readmore"] ?? null))) {
                        echo " <a href=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "href", [], "any", false, false, false, 102);
                        echo "\">  ";
                        echo ($context["text_readmore"] ?? null);
                        echo " </a> ";
                    }
                    echo "</p>";
                }
                echo " 
\t\t\t\t\t<ul class=\"list-inline dar\">
\t\t\t\t\t\t";
                // line 104
                if (($context["show_wishlist"] ?? null)) {
                    // line 105
                    echo "\t\t\t\t\t\t<li title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "wishlist", [], "any", false, false, false, 105);
                    echo "\">
\t\t\t\t\t\t\t<span class=\"mpbloglike ";
                    // line 106
                    if (twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "isLikeByMe", [], "any", false, false, false, 106)) {
                        echo " liked ";
                    }
                    echo "\" data-id=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "mpblogpost_id", [], "any", false, false, false, 106);
                    echo "\">
\t\t\t\t\t\t\t<i class=\"fa fa-heart ";
                    // line 107
                    if ( !twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "isLikeByMe", [], "any", false, false, false, 107)) {
                        echo " fa-heart-o ";
                    }
                    echo "\"></i>
\t\t\t\t\t\t\t<span>";
                    // line 108
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "wishlist", [], "any", false, false, false, 108);
                    echo "</span>
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
                }
                // line 111
                echo "\t\t\t 
\t\t\t\t\t\t";
                // line 112
                if (($context["show_comments"] ?? null)) {
                    // line 113
                    echo "\t\t\t\t\t\t<li title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "comments", [], "any", false, false, false, 113);
                    echo "\">
\t\t\t\t\t\t\t<i class=\"fa ";
                    // line 114
                    if (twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "comments", [], "any", false, false, false, 114)) {
                        echo " fa-comments ";
                    } else {
                        echo " fa-comments-o ";
                    }
                    echo "\"></i>
\t\t\t\t\t\t\t";
                    // line 115
                    echo ((twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "comments", [], "any", false, false, false, 115) . " ") . ($context["text_comment"] ?? null));
                    echo "
\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
                }
                // line 118
                echo "\t\t\t\t\t</ul>
\t\t\t\t\t";
                // line 119
                if ((($context["show_tag"] ?? null) && twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "tag", [], "any", false, false, false, 119))) {
                    // line 120
                    echo "\t\t\t\t\t<ul class=\"list-inline blog-tags\">
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<i class=\"fa fa-tags\"></i>
\t\t\t\t\t\t\t";
                    // line 123
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "tag", [], "any", false, false, false, 123));
                    foreach ($context['_seq'] as $context["_key"] => $context["rtag"]) {
                        // line 124
                        echo "\t\t\t\t\t\t\t  ";
                        if (twig_get_attribute($this->env, $this->source, $context["rtag"], "href", [], "any", false, false, false, 124)) {
                            // line 125
                            echo "\t              \t<a href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["rtag"], "href", [], "any", false, false, false, 125);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["rtag"], "tag", [], "any", false, false, false, 125);
                            echo "</a>
\t              ";
                        } else {
                            // line 127
                            echo "\t              \t";
                            echo twig_get_attribute($this->env, $this->source, $context["rtag"], "tag", [], "any", false, false, false, 127);
                            echo "
\t              ";
                        }
                        // line 129
                        echo "                  ,
             \t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rtag'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 131
                    echo "\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t\t";
                }
                // line 134
                echo "\t\t\t\t</div>
\t\t\t</div>
\t\t</div>  
\t</div>
\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mpblogpost'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 139
            echo "</div>
<div class=\"row\">
\t<div class=\"col-sm-6 text-left\">";
            // line 141
            echo ($context["pagination"] ?? null);
            echo "</div>
\t<div class=\"col-sm-6 text-right\">";
            // line 142
            echo ($context["results"] ?? null);
            echo "</div>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "default/template/mpblog/blogcategory_post_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  417 => 142,  413 => 141,  409 => 139,  399 => 134,  394 => 131,  387 => 129,  381 => 127,  373 => 125,  370 => 124,  366 => 123,  361 => 120,  359 => 119,  356 => 118,  350 => 115,  342 => 114,  337 => 113,  335 => 112,  332 => 111,  325 => 108,  319 => 107,  311 => 106,  306 => 105,  304 => 104,  290 => 102,  286 => 101,  283 => 100,  276 => 95,  270 => 94,  262 => 88,  259 => 87,  251 => 82,  248 => 81,  240 => 76,  237 => 75,  229 => 70,  226 => 69,  223 => 68,  220 => 67,  217 => 66,  214 => 65,  211 => 64,  208 => 63,  205 => 62,  202 => 61,  199 => 60,  196 => 59,  193 => 58,  190 => 57,  187 => 56,  183 => 55,  175 => 51,  172 => 50,  168 => 48,  162 => 46,  154 => 44,  152 => 43,  146 => 41,  144 => 40,  140 => 39,  134 => 38,  128 => 34,  122 => 31,  116 => 29,  114 => 28,  111 => 27,  107 => 25,  101 => 23,  95 => 20,  90 => 19,  88 => 18,  82 => 16,  80 => 15,  77 => 14,  67 => 12,  55 => 10,  53 => 9,  47 => 5,  43 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/mpblog/blogcategory_post_list.twig", "");
    }
}
