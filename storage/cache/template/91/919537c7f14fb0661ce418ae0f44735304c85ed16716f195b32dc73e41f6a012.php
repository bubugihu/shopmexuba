<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/module/slideshow.twig */
class __TwigTemplate_2dab5e53ca5d71fea89f72449d3f3a40df50d2a6fbbbe0b79a5575b811471f63 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"pickup\" data-color=\"beige\" style=\"padding-top: 70px;\"> 
  <section class=\"pickup-section\">
    <div class=\"\">
      <div class=\"banner js-slider\">
        <div class=\"banner-container\">
          <div id=\"slideshow2";
        // line 6
        echo ($context["module"] ?? null);
        echo "\" class=\"swiper-container banner-inner\">
            <div class=\"swiper-wrapper\"> 
              ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["banners"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
            // line 9
            echo "              <div class=\"swiper-slide text-center\">
                ";
            // line 10
            if (twig_get_attribute($this->env, $this->source, $context["banner"], "link", [], "any", false, false, false, 10)) {
                // line 11
                echo "                <a href=\"";
                echo twig_get_attribute($this->env, $this->source, $context["banner"], "link", [], "any", false, false, false, 11);
                echo "\"><img class=\"img_slide img_mobile\" style=\"border-radius: 15px;\" src=\"";
                echo twig_get_attribute($this->env, $this->source, $context["banner"], "image", [], "any", false, false, false, 11);
                echo "\" alt=\"";
                echo twig_get_attribute($this->env, $this->source, $context["banner"], "title", [], "any", false, false, false, 11);
                echo "\" /></a>
                ";
            } else {
                // line 13
                echo "                <img class=\"img_slide img_mobile\" style=\"border-radius: 15px;\" src=\"";
                echo twig_get_attribute($this->env, $this->source, $context["banner"], "image", [], "any", false, false, false, 13);
                echo "\" alt=\"";
                echo twig_get_attribute($this->env, $this->source, $context["banner"], "title", [], "any", false, false, false, 13);
                echo "\" />
                ";
            }
            // line 15
            echo "              </div>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo " 
            </div>
          </div>
          <button class=\"m-banner-prev\" tabindex=\"-1\" aria-hidden=\"true\"><img src=\"catalog/view/theme/default/stylesheet/img/icons/prev.svg\" alt=\"前へ\"></button>
          <button class=\"m-banner-next\" tabindex=\"-1\" aria-hidden=\"true\"><img src=\"catalog/view/theme/default/stylesheet/img/icons/next.svg\" alt=\"次へ\"></button>
          <div class=\"swiper-pagination slideshow2";
        // line 21
        echo ($context["module"] ?? null);
        echo "\"></div>
        </div>
        <script type=\"text/template\" class=\"js-slider-pagination-template\">
          <ul class=\"pagination js-slider-pagination\"></ul>
        </script>
        <script type=\"text/template\" class=\"js-slider-pagination-item-template\">
          <li><button class=\"pagination-item js-slider-pagination-item is-pause\" tabindex=\"-1\" aria-hidden=\"true\"></button></li>
        </script>
      </div>
    </div>
  </section>
  <div class=\"l-content-decoration\"> 
    <svg xmlns=\"http://www.w3.org/2000/svg\" class=\"m-content-decoration-lg\" width=\"1600\" height=\"80\" viewBox=\"0 0 1600 80\" focusable=\"false\"> 
      <path d=\"M1046,70.5c0,0-149,2.4-445.3-40.4c0,0-270.8-36.8-451-29c0,0-67.4,2.5-149.7,17.3V80h1600V27.6C1409.2,28.3,1264,71,1046,70.5z\"></path> 
    </svg> 
    <svg xmlns=\"http://www.w3.org/2000/svg\" class=\"m-content-decoration-sm\" width=\"750\" height=\"70\" viewBox=\"0 0 750 70\" focusable=\"false\"> 
      <path d=\"M750,54.9c-38.5,7.7-143.3,20.7-314-15c0,0-225.5-47.4-374.5-38.9c0,0-24.7,1.3-61.5,7.4V80h750V54.9z\"></path> 
    </svg> 
  </div>
</div>

<script type=\"text/javascript\">
  // slideshow 2
  var mq = window.matchMedia(\"(max-width: 1200px)\")
  var mq1 = window.matchMedia(\"(min-width: 1200px)\")
   if(\$(window).width() < 770)
  {\t\t
  \t\t\$('.img_mobile').removeClass( \"img_slide\" );
  \t\t\$('.img_mobile').css('width','210px');
  \t\t\$('.img_mobile').css('height','72px');
     \$('#slideshow2";
        // line 51
        echo ($context["module"] ?? null);
        echo "').swiper({
      mode: 'horizontal',
      slidesPerView: 1,
      pagination: '.slideshow2";
        // line 54
        echo ($context["module"] ?? null);
        echo "',
      paginationClickable: true,
      nextButton: '.m-banner-next',
      prevButton: '.m-banner-prev',
      spaceBetween: 100,
      autoplay: 3000,
      autoplayDisableOnInteraction: false,
      loop: true,
    });
  }
  else if (mq.matches == false) {
    \$('#slideshow2";
        // line 65
        echo ($context["module"] ?? null);
        echo "').swiper({
      mode: 'horizontal',
      slidesPerView: 2,
      pagination: '.slideshow2";
        // line 68
        echo ($context["module"] ?? null);
        echo "',
      paginationClickable: true,
      nextButton: '.m-banner-next',
      prevButton: '.m-banner-prev',
      spaceBetween: 100,
      autoplay: 5000,
      autoplayDisableOnInteraction: false,
      loop: true,
    });
  }
  else if (mq.matches == true) {
    console.log(\"a\")
    \$('#slideshow2";
        // line 80
        echo ($context["module"] ?? null);
        echo "').swiper({
      mode: 'horizontal',
      slidesPerView: 2,
      pagination: '.slideshow2";
        // line 83
        echo ($context["module"] ?? null);
        echo "',
      paginationClickable: true,
      nextButton: '.m-banner-next',
      prevButton: '.m-banner-prev',
      spaceBetween: 100,
      autoplay: 5000,
      autoplayDisableOnInteraction: false,
      loop: true,
    });
  }
  else if (mq1.matches == true) {
    
      \$('.img_mobile').css('width','210px');
  \t\t\$('.img_mobile').css('height','72px');
    \$('#slideshow2";
        // line 97
        echo ($context["module"] ?? null);
        echo "').swiper({
      mode: 'horizontal',
      slidesPerView: 2,
      pagination: '.slideshow2";
        // line 100
        echo ($context["module"] ?? null);
        echo "',
      paginationClickable: true,
      nextButton: '.m-banner-next',
      prevButton: '.m-banner-prev',
      spaceBetween: 200,
      autoplay: 5000,
      autoplayDisableOnInteraction: false,
      loop: true,
  \t  width: 800
    });
  }
</script>";
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/slideshow.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  193 => 100,  187 => 97,  170 => 83,  164 => 80,  149 => 68,  143 => 65,  129 => 54,  123 => 51,  90 => 21,  83 => 16,  76 => 15,  68 => 13,  58 => 11,  56 => 10,  53 => 9,  49 => 8,  44 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/extension/module/slideshow.twig", "");
    }
}
