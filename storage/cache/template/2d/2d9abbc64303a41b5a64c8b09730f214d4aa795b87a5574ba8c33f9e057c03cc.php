<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/account/login.twig */
class __TwigTemplate_32a783a1928c0bf5bf773e748cf31e4d9de8d2277e853410c4b586fd93126616 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<style>
  .fotgot-pwd {
  \tcolor: black;
  \tline-height: 1.5;
  }
  .fotgot-pwd:hover {
  \tcolor: #3399ff;
  }
</style>
<div style=\"background-color: #DBF9FB\">
  <div class=\"l-breadcrumb\" style=\"margin-bottom: 30px;\"> 
    <div class=\"l-breadcrumb-container\"> 
      <div class=\"l-breadcrumb-inner\"> 
        <nav role=\"navigation\" aria-label=\"現在位置\"> 
          <ol class=\"m-breadcrumb\" itemscope=\"itemscope\" itemtype=\"http://schema.org/BreadcrumbList\"> 
            ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 18
            echo "            <li itemprop=\"itemListElement\" itemscope=\"itemscope\" itemtype=\"http://schema.org/ListItem\"><a style=\"text-transform: uppercase;\" href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 18);
            echo "\" class=\"m-breadcrumb-item\" itemprop=\"item\"><span style=\"color: #fff; font-size: 18px;\" itemprop=\"name\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 18);
            echo "</span></a>
              <meta itemprop=\"position\" content=\"1\" style=\"color: #fff; font-size: 18px;\"> </li> 
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "          </ol> 
        </nav> 
      </div> 
    </div> 
  </div>
  <div id=\"account-login\" class=\"container\">
    ";
        // line 27
        if (($context["success"] ?? null)) {
            // line 28
            echo "    <div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ";
            echo ($context["success"] ?? null);
            echo "</div>
    ";
        }
        // line 30
        echo "    ";
        if (($context["error_warning"] ?? null)) {
            // line 31
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "</div>
    ";
        }
        // line 33
        echo "    <div class=\"row\">";
        echo ($context["column_left"] ?? null);
        echo "
      ";
        // line 34
        if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
            // line 35
            echo "      ";
            $context["class"] = "col-sm-6";
            // line 36
            echo "      ";
        } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 37
            echo "      ";
            $context["class"] = "col-sm-9";
            // line 38
            echo "      ";
        } else {
            // line 39
            echo "      ";
            $context["class"] = "col-sm-12";
            // line 40
            echo "      ";
        }
        // line 41
        echo "      <div id=\"content\" class=\"";
        echo ($context["class"] ?? null);
        echo "\">";
        echo ($context["content_top"] ?? null);
        echo "
        <div class=\"row\">
          <div class=\"col-sm-6\">
            <div class=\"well\">
              <h2>";
        // line 45
        echo ($context["text_new_customer"] ?? null);
        echo "</h2>
              <p><strong>";
        // line 46
        echo ($context["text_register"] ?? null);
        echo "</strong></p>
              <p>";
        // line 47
        echo ($context["text_register_account"] ?? null);
        echo "</p>
              <a href=\"";
        // line 48
        echo ($context["register"] ?? null);
        echo "\" class=\"btn btn-pink\" style=\"border-radius:20px; font-size: 15px\">";
        echo ($context["button_continue"] ?? null);
        echo "</a></div>
          </div>
          <div class=\"col-sm-6\">
            <div class=\"well\">
              <h2>";
        // line 52
        echo ($context["text_returning_customer"] ?? null);
        echo "</h2>
              <p><strong>";
        // line 53
        echo ($context["text_i_am_returning_customer"] ?? null);
        echo "</strong></p>
              <form action=\"";
        // line 54
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\">
                <div class=\"form-group\">
                  <label class=\"control-label\" for=\"input-email\">";
        // line 56
        echo ($context["entry_email"] ?? null);
        echo "</label>
                  <input type=\"text\" name=\"email\" value=\"";
        // line 57
        echo ($context["email"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_email"] ?? null);
        echo "\" id=\"input-email\" class=\"form-control\" />
                </div>
                <div class=\"form-group\">
                  <label class=\"control-label\" for=\"input-password\">";
        // line 60
        echo ($context["entry_password"] ?? null);
        echo "</label>
                  <input type=\"password\" name=\"password\" value=\"";
        // line 61
        echo ($context["password"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_password"] ?? null);
        echo "\" id=\"input-password\" class=\"form-control\" />
                  <a class=\"fotgot-pwd\" href=\"";
        // line 62
        echo ($context["forgotten"] ?? null);
        echo "\">";
        echo ($context["text_forgotten"] ?? null);
        echo "</a></div>
                <input type=\"submit\" value=\"";
        // line 63
        echo ($context["button_login"] ?? null);
        echo "\" class=\"btn btn-pink\" style=\"border-radius:20px; font-size: 15px\" />
                ";
        // line 64
        if (($context["redirect"] ?? null)) {
            // line 65
            echo "                <input type=\"hidden\" name=\"redirect\" value=\"";
            echo ($context["redirect"] ?? null);
            echo "\" />
                ";
        }
        // line 67
        echo "              </form>
            </div>
          </div>
        </div>
        ";
        // line 71
        echo ($context["content_bottom"] ?? null);
        echo "</div>
      ";
        // line 72
        echo ($context["column_right"] ?? null);
        echo "</div>
  </div>
</div>
";
        // line 75
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "default/template/account/login.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  222 => 75,  216 => 72,  212 => 71,  206 => 67,  200 => 65,  198 => 64,  194 => 63,  188 => 62,  182 => 61,  178 => 60,  170 => 57,  166 => 56,  161 => 54,  157 => 53,  153 => 52,  144 => 48,  140 => 47,  136 => 46,  132 => 45,  122 => 41,  119 => 40,  116 => 39,  113 => 38,  110 => 37,  107 => 36,  104 => 35,  102 => 34,  97 => 33,  91 => 31,  88 => 30,  82 => 28,  80 => 27,  72 => 21,  60 => 18,  56 => 17,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/account/login.twig", "");
    }
}
