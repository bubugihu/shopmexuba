<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/mpblog/mpblogcomment.twig */
class __TwigTemplate_73a62cf81b23a2cae3142b7b4d4c415e70faf61f730a5529a7b6e484ba2e2b65 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["comments"] ?? null)) {
            // line 2
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["comments"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
                // line 3
                echo "<div class=\"mp-comments\">
  \t<div class=\"img-circle mp-avatar\"><i class=\"fa fa-user\"></i></div>
  \t<div class=\"cmt-wrap\">
\t  \t<h4 class=\"mp-commentauthor\">";
                // line 6
                echo twig_get_attribute($this->env, $this->source, $context["comment"], "author", [], "any", false, false, false, 6);
                echo "</h4>
\t    <h5><i class=\"fa fa-calendar\"></i> ";
                // line 7
                echo twig_get_attribute($this->env, $this->source, $context["comment"], "date_added", [], "any", false, false, false, 7);
                echo "</h5>
\t    <p>";
                // line 8
                echo twig_get_attribute($this->env, $this->source, $context["comment"], "text", [], "any", false, false, false, 8);
                echo "</p>
    </div>
</div>
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "<div class=\"text-right\">";
            echo ($context["pagination"] ?? null);
            echo "</div>
";
        } else {
            // line 14
            echo "<p>";
            echo ($context["text_no_comments"] ?? null);
            echo "</p>
";
        }
    }

    public function getTemplateName()
    {
        return "default/template/mpblog/mpblogcomment.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 14,  66 => 12,  56 => 8,  52 => 7,  48 => 6,  43 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/mpblog/mpblogcomment.twig", "");
    }
}
