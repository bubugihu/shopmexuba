<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mpblog/mpblogpost_list.twig */
class __TwigTemplate_dbdda6b619b471d693b80567afc736e3d91725f980f882a24b5ff7e061e1a35f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\" class=\"mp-content\">
  ";
        // line 3
        echo ($context["mpblogmenu"] ?? null);
        echo "
  <div class=\"mpblog-body\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\"><a href=\"";
        // line 7
        echo ($context["add"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_add"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-plus\"></i></a>
        <button type=\"button\" data-toggle=\"tooltip\" title=\"";
        // line 8
        echo ($context["button_delete"] ?? null);
        echo "\" class=\"btn btn-danger\" onclick=\"confirm('";
        echo ($context["text_confirm"] ?? null);
        echo "') ? \$('#form-mpblogpost').submit() : false;\"><i class=\"fa fa-trash-o\"></i></button>
      </div>
      <h1>";
        // line 10
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 13
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 13);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 13);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 19
        if (($context["error_warning"] ?? null)) {
            // line 20
            echo "    <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 24
        echo "    ";
        if (($context["success"] ?? null)) {
            // line 25
            echo "    <div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i> ";
            echo ($context["success"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 29
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i> ";
        // line 31
        echo ($context["text_list"] ?? null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <div class=\"well\">
          <div class=\"row\">
            <div class=\"col-sm-4\">
              <div class=\"form-group\">
                <label class=\"control-label\" for=\"input-name\">";
        // line 38
        echo ($context["entry_name"] ?? null);
        echo "</label>
                <input type=\"text\" name=\"filter_name\" value=\"";
        // line 39
        echo ($context["filter_name"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_name"] ?? null);
        echo "\" id=\"input-name\" class=\"form-control\" />
              </div>
            </div>
            <div class=\"col-sm-4\">
              <div class=\"form-group\">
                <label class=\"control-label\" for=\"input-author\">";
        // line 44
        echo ($context["entry_author"] ?? null);
        echo "</label>
                <input type=\"text\" name=\"filter_author\" value=\"";
        // line 45
        echo ($context["filter_author"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_author"] ?? null);
        echo "\" id=\"input-author\" class=\"form-control\" value=\"";
        echo ($context["filter_author"] ?? null);
        echo "\"/>
              </div>
            </div>
            <div class=\"col-sm-4\">
              <div class=\"form-group\">
                <label class=\"control-label\" for=\"input-status\">";
        // line 50
        echo ($context["entry_status"] ?? null);
        echo "</label>
                <select name=\"filter_status\" id=\"input-status\" class=\"form-control\">
                  <option value=\"*\"></option>
                  ";
        // line 53
        if ((($context["filter_status"] ?? null) == "1")) {
            // line 54
            echo "                  <option value=\"1\" selected=\"selected\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                  ";
        } else {
            // line 56
            echo "                  <option value=\"1\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                  ";
        }
        // line 58
        echo "                  ";
        if ((($context["filter_status"] ?? null) == "0")) {
            // line 59
            echo "                  <option value=\"0\" selected=\"selected\">";
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                  ";
        } else {
            // line 61
            echo "                  <option value=\"0\">";
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                  ";
        }
        // line 63
        echo "                </select>
              </div>
              <button type=\"button\" id=\"button-filter\" class=\"btn btn-primary pull-right\"><i class=\"fa fa-filter\"></i> ";
        // line 65
        echo ($context["button_filter"] ?? null);
        echo "</button>
            </div>
          </div>
        </div>
        <form action=\"";
        // line 69
        echo ($context["delete"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-mpblogpost\">
          <div class=\"table-responsive\">
            <table class=\"table table-bordered table-hover\">
              <thead>
                <tr>
                  <td style=\"width: 1px;\" class=\"text-center\"><input type=\"checkbox\" onclick=\"\$('input[name*=\\'selected\\']').prop('checked', this.checked);\" /></td>
                  <td class=\"text-center\">";
        // line 75
        echo ($context["column_image"] ?? null);
        echo "</td>
                  <td class=\"text-left\">";
        // line 76
        if ((($context["sort"] ?? null) == "pd.name")) {
            // line 77
            echo "                    <a href=\"";
            echo ($context["sort_name"] ?? null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, ($context["order"] ?? null));
            echo "\">";
            echo ($context["column_name"] ?? null);
            echo "</a>
                    ";
        } else {
            // line 79
            echo "                    <a href=\"";
            echo ($context["sort_name"] ?? null);
            echo "\">";
            echo ($context["column_name"] ?? null);
            echo " </a>
                    ";
        }
        // line 80
        echo "</td>
                  <td class=\"text-left\">";
        // line 81
        if ((($context["sort"] ?? null) == "pd.name")) {
            // line 82
            echo "                    <a href=\"";
            echo ($context["sort_totalcomment"] ?? null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, ($context["order"] ?? null));
            echo "\">";
            echo ($context["column_totalcomment"] ?? null);
            echo "</a>
                    ";
        } else {
            // line 84
            echo "                    <a href=\"";
            echo ($context["sort_totalcomment"] ?? null);
            echo "\">";
            echo ($context["column_totalcomment"] ?? null);
            echo " </a>
                    ";
        }
        // line 85
        echo "</td>
                  <td class=\"text-left\">";
        // line 86
        if ((($context["sort"] ?? null) == "p.author")) {
            // line 87
            echo "                    <a href=\"";
            echo ($context["sort_author"] ?? null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, ($context["order"] ?? null));
            echo "\">";
            echo ($context["column_author"] ?? null);
            echo "</a>
                    ";
        } else {
            // line 89
            echo "                    <a href=\"";
            echo ($context["sort_author"] ?? null);
            echo "\">";
            echo ($context["column_author"] ?? null);
            echo "</a>
                    ";
        }
        // line 90
        echo "</td>
                  <td class=\"text-left\">";
        // line 91
        if ((($context["sort"] ?? null) == "p.status")) {
            // line 92
            echo "                    <a href=\"";
            echo ($context["sort_status"] ?? null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, ($context["order"] ?? null));
            echo "\">";
            echo ($context["column_status"] ?? null);
            echo "</a>
                    ";
        } else {
            // line 94
            echo "                    <a href=\"";
            echo ($context["sort_status"] ?? null);
            echo "\">";
            echo ($context["column_status"] ?? null);
            echo "</a>
                    ";
        }
        // line 95
        echo "</td>
                  <td class=\"text-right\">";
        // line 96
        echo ($context["column_action"] ?? null);
        echo "</td>
                </tr>
              </thead>
              <tbody>
                ";
        // line 100
        if (($context["mpblogposts"] ?? null)) {
            // line 101
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["mpblogposts"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["mpblogpost"]) {
                // line 102
                echo "                <tr>
                  <td class=\"text-center\">";
                // line 103
                if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "mpblogpost_id", [], "any", false, false, false, 103), ($context["selected"] ?? null))) {
                    // line 104
                    echo "                    <input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "mpblogpost_id", [], "any", false, false, false, 104);
                    echo "\" checked=\"checked\" />
                    ";
                } else {
                    // line 106
                    echo "                    <input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "mpblogpost_id", [], "any", false, false, false, 106);
                    echo "\" />
                    ";
                }
                // line 107
                echo "</td>
                  <td class=\"text-center\">";
                // line 108
                if (twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "image", [], "any", false, false, false, 108)) {
                    // line 109
                    echo "                    <img src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "image", [], "any", false, false, false, 109);
                    echo "\" alt=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "name", [], "any", false, false, false, 109);
                    echo "\" class=\"img-thumbnail\" />
                    ";
                } else {
                    // line 111
                    echo "                    <span class=\"img-thumbnail list\"><i class=\"fa fa-camera fa-2x\"></i></span>
                    ";
                }
                // line 112
                echo "</td>
                  <td class=\"text-left\">";
                // line 113
                echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "name", [], "any", false, false, false, 113);
                echo " <sup> <i class=\"fa fa-eye\"></i> (";
                echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "viewed", [], "any", false, false, false, 113);
                echo ")</sup> </td>
                  <td class=\"text-left\">";
                // line 114
                echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "totalcomments", [], "any", false, false, false, 114);
                echo "</td>
                  <td class=\"text-left\">";
                // line 115
                echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "author", [], "any", false, false, false, 115);
                echo "</td>
                  <td class=\"text-left\">";
                // line 116
                echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "status", [], "any", false, false, false, 116);
                echo "</td>
                  <td class=\"text-right\"><a href=\"";
                // line 117
                echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "edit", [], "any", false, false, false, 117);
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo ($context["button_edit"] ?? null);
                echo "\" class=\"btn btn-primary\"><i class=\"fa fa-pencil\"></i></a></td>
                </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mpblogpost'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 120
            echo "                ";
        } else {
            // line 121
            echo "                <tr>
                  <td class=\"text-center\" colspan=\"6\">";
            // line 122
            echo ($context["text_no_results"] ?? null);
            echo "</td>
                </tr>
                ";
        }
        // line 125
        echo "              </tbody>
            </table>
          </div>
        </form>
        <div class=\"row\">
          <div class=\"col-sm-6 text-left\">";
        // line 130
        echo ($context["pagination"] ?? null);
        echo "</div>
          <div class=\"col-sm-6 text-right\">";
        // line 131
        echo ($context["results"] ?? null);
        echo "</div>
        </div>
      </div>
    </div>
  </div>
  <script type=\"text/javascript\"><!--
\$('#button-filter').on('click', function() {
\tvar url = 'index.php?route=mpblog/mpblogpost&user_token=";
        // line 138
        echo ($context["user_token"] ?? null);
        echo "';

\tvar filter_name = \$('input[name=\\'filter_name\\']').val();

\tif (filter_name) {
\t\turl += '&filter_name=' + encodeURIComponent(filter_name);
\t}

\tvar filter_author = \$('input[name=\\'filter_author\\']').val();

\tif (filter_author) {
\t\turl += '&filter_author=' + encodeURIComponent(filter_author);
\t}

\tvar filter_status = \$('select[name=\\'filter_status\\']').val();

\tif (filter_status != '*') {
\t\turl += '&filter_status=' + encodeURIComponent(filter_status);
\t}

\tlocation = url;
});
//--></script>
  <script type=\"text/javascript\"><!--
\$('input[name=\\'filter_name\\']').autocomplete({
\t'source': function(request, response) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=mpblog/mpblogpost/autocomplete&user_token=";
        // line 165
        echo ($context["user_token"] ?? null);
        echo "&filter_name=' +  encodeURIComponent(request),
\t\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item['name'],
\t\t\t\t\t\tvalue: item['mpblogpost_id']
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t}
\t\t});
\t},
\t'select': function(item) {
\t\t\$('input[name=\\'filter_name\\']').val(item['label']);
\t}
});
//--></script></div></div>
";
        // line 182
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "mpblog/mpblogpost_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  468 => 182,  448 => 165,  418 => 138,  408 => 131,  404 => 130,  397 => 125,  391 => 122,  388 => 121,  385 => 120,  374 => 117,  370 => 116,  366 => 115,  362 => 114,  356 => 113,  353 => 112,  349 => 111,  341 => 109,  339 => 108,  336 => 107,  330 => 106,  324 => 104,  322 => 103,  319 => 102,  314 => 101,  312 => 100,  305 => 96,  302 => 95,  294 => 94,  284 => 92,  282 => 91,  279 => 90,  271 => 89,  261 => 87,  259 => 86,  256 => 85,  248 => 84,  238 => 82,  236 => 81,  233 => 80,  225 => 79,  215 => 77,  213 => 76,  209 => 75,  200 => 69,  193 => 65,  189 => 63,  183 => 61,  177 => 59,  174 => 58,  168 => 56,  162 => 54,  160 => 53,  154 => 50,  142 => 45,  138 => 44,  128 => 39,  124 => 38,  114 => 31,  110 => 29,  102 => 25,  99 => 24,  91 => 20,  89 => 19,  83 => 15,  72 => 13,  68 => 12,  63 => 10,  56 => 8,  50 => 7,  43 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "mpblog/mpblogpost_list.twig", "");
    }
}
