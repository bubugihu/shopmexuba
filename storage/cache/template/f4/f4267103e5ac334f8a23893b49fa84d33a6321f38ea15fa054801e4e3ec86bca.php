<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/common/footer.twig */
class __TwigTemplate_be6390b358890df65daa2d40b5afc84a45b451b7dc24e23aeb57f5f19ede61ee extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<style>
  .list-unstyled li a {
  color: black;
  }
  .list-unstyled li a:hover {
  color: #3399ff;
  }
</style>
<script type=\"text/javascript\">
\$('#carousel234').swiper({
\tmode: 'horizontal',
\tslidesPerView: 1,
\tpagination: '.carousel234',
\tpaginationClickable: true,
\tautoplay: 2000,
\tloop: true,
  autoplayDisableOnInteraction: false,
});
</script>
<script type=\"text/javascript\">
  
  if(\$(window).width() < 800)
  {
  \t\$('.pc-video').css('display','none');
  \t\$('.mobile-video').css('display','block');
    \$('.mobile-video').css('background-color','#DBF9FB');
  \t\$('.mobile-video').css('padding-bottom','30px');
  \tif(\$(window).width() >= 768)
    {
      \$('.carousel234').css('bottom', '5.2%');
    }
    else if(\$(window).width() >= 411)
    {
      \$('.carousel234').css('bottom', '4.3%');
    }
    else if(\$(window).width() < 411)
    { 
  \t
       \$('.carousel234').css('bottom', '5.2%');
    }
    else if(\$(window).width() < 330)
    {
      \$('.carousel234').css('bottom', '5.3%');
    }
  }
  else
  {
  \t\$('.pc-video').css('display','block');
  \t\$('.mobile-video').css('display','none');
    \$('.pc-video').css('background-color','#DBF9FB');
  \t\$('.pc-video').css('padding-bottom','30px');
  }
</script>
<div class=\"footer\">
  <div class=\"l-pagetop js-pagetop is-hidden\" id=\"pagetotop\" style=\"cursor: pointer;z-index: 999;\">
    <a  class=\"m-pagetop js-anchorLink\">
      <img src=\"catalog/view/theme/default/stylesheet/img/icons/pagetop.svg\" alt=\"ページの先頭へ\">
    </a>
  </div>
  <div class=\"l-footer-container\" style=\"padding-top: 30px;background: url(http://localhost:8085/nucos3/image/catalog/logo/footer2.png) !important;\">
    <div class=\"container\">
      <div class=\"l-footer-grid\">
        <div class=\"l-footer-item-1\">
          <div class=\"l-footer-sns\">
            <ul class=\"l-socialmedia\" data-content-md=\"center\">
              <li>
                <a href=\"https://www.facebook.com/nucos.japan\" target=\"_blank\" class=\"m-socialmedia-fb\">
                  <img src=\"catalog/view/theme/default/stylesheet/img/icons/facebook.svg\" alt=\"Facebook（新しいウィンドウ）\">
                </a>
              </li>
              <li>
                <a href=\"https://www.instagram.com/nucos.japan\" target=\"_blank\">
                  <img height=\"33\" src=\"https://cdn.iconscout.com/icon/free/png-256/instagram-1868978-1583142.png\" alt=\"Instagram（新しいウィンドウ）\">
                </a>
              </li>
              <li>
                <a href=\"https://www.youtube.com/channel/UCfpudVsvn4SISVGW8zXZfng\" target=\"_blank\" class=\"m-socialmedia-youtube\">
                  <img src=\"catalog/view/theme/default/stylesheet/img/icons/youtube.svg\" alt=\"Youtube（新しいウィンドウ）\">
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class=\"l-footer-item-2\">
          <div class=\"row content-footer\">
            <div class=\"col-md-4 col-xs-12\" style=\"text-align: center;\">
              <h5 style=\"font-size: 17px; line-height: 1.4;\"><b>";
        // line 87
        echo ($context["text_information"] ?? null);
        echo "</b></h5>
              <ul class=\"list-unstyled\" style=\"line-height: 1.6;\">
                ";
        // line 89
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["informations"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
            // line 90
            echo "                <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["information"], "href", [], "any", false, false, false, 90);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["information"], "title", [], "any", false, false, false, 90);
            echo "</a></li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 92
        echo "              </ul>
            </div>
            <div class=\"col-md-4 col-xs-12\" style=\"text-align: center;\">
              <h5 style=\"font-size: 17px; line-height: 1.4;\"><b>";
        // line 95
        echo ($context["text_service"] ?? null);
        echo "</b></h5>
              <ul class=\"list-unstyled\" style=\"line-height: 1.6;\">
                <li ><a href=\"";
        // line 97
        echo ($context["contact"] ?? null);
        echo "\">";
        echo ($context["text_contact"] ?? null);
        echo "</a></li>
                <li ><a href=\"";
        // line 98
        echo ($context["sitemap"] ?? null);
        echo "\">";
        echo ($context["text_sitemap"] ?? null);
        echo "</a></li>
              </ul>
            </div>
            <div class=\"col-md-4 col-xs-12\" style=\"text-align: center;\">
              <h5 style=\"font-size: 17px; line-height: 1.4;\"><b>";
        // line 102
        echo ($context["text_account"] ?? null);
        echo "</b></h5>
              <ul class=\"list-unstyled\" style=\"line-height: 1.6;\">
                <li ><a href=\"";
        // line 104
        echo ($context["account"] ?? null);
        echo "\">";
        echo ($context["text_account"] ?? null);
        echo "</a></li>
                <li ><a href=\"";
        // line 105
        echo ($context["order"] ?? null);
        echo "\">";
        echo ($context["text_order"] ?? null);
        echo "</a></li>
                <li ><a href=\"";
        // line 106
        echo ($context["newsletter"] ?? null);
        echo "\">";
        echo ($context["text_newsletter"] ?? null);
        echo "</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src=\"catalog/view/theme/default/javascript/js/common.js\" type=\"text/javascript\"></script>
<script src=\"catalog/view/theme/default/javascript/js/index.js\" type=\"text/javascript\"></script>
";
        // line 117
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["scripts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 118
            echo "<script src=\"";
            echo $context["script"];
            echo "\" type=\"text/javascript\"></script>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 120
        echo "</body></html>";
    }

    public function getTemplateName()
    {
        return "default/template/common/footer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  216 => 120,  207 => 118,  203 => 117,  187 => 106,  181 => 105,  175 => 104,  170 => 102,  161 => 98,  155 => 97,  150 => 95,  145 => 92,  134 => 90,  130 => 89,  125 => 87,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/common/footer.twig", "");
    }
}
