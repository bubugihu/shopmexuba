<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/product/category.twig */
class __TwigTemplate_1b34fab2cddfd1aceb304e5968962d8d3c942d39f793df86786f03c4c239c231 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div style=\"background-color: #DBF9FB\">
  <div class=\"l-breadcrumb\" style=\"margin-bottom: 20px;\"> 
    <div class=\"l-breadcrumb-container\"> 
      <div class=\"l-breadcrumb-inner\"> 
        <nav role=\"navigation\" aria-label=\"現在位置\"> 
          <ol class=\"m-breadcrumb\" itemscope=\"itemscope\" itemtype=\"http://schema.org/BreadcrumbList\"> 
            ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 9
            echo "            <li itemprop=\"itemListElement\" itemscope=\"itemscope\" itemtype=\"http://schema.org/ListItem\"><a style=\"text-transform: uppercase;\" href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 9);
            echo "\" class=\"m-breadcrumb-item\" itemprop=\"item\"><span style=\"color: #fff; font-size: 18px;\" itemprop=\"name\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 9);
            echo "</span></a>
              <meta itemprop=\"position\" content=\"1\" style=\"color: #fff; font-size: 18px;\"> </li> 
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "          </ol> 
        </nav> 
      </div> 
    </div> 
  </div>
  <div id=\"product-category\" class=\"container\">
    <div class=\"row\">";
        // line 18
        echo ($context["column_left"] ?? null);
        echo "
      ";
        // line 19
        if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
            // line 20
            echo "      ";
            $context["class"] = "col-sm-6";
            // line 21
            echo "      ";
        } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 22
            echo "      ";
            $context["class"] = "col-sm-9";
            // line 23
            echo "      ";
        } else {
            // line 24
            echo "      ";
            $context["class"] = "col-sm-12";
            // line 25
            echo "      ";
        }
        // line 26
        echo "      <div id=\"content\" class=\"";
        echo ($context["class"] ?? null);
        echo "\">";
        echo ($context["content_top"] ?? null);
        echo "
        ";
        // line 27
        if (($context["categories"] ?? null)) {
            // line 28
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 29
                echo "        <div class=\"l-heading3-container js-heading-accordion\" data-accordion=\"sm\">
          <div class=\"\">
            <div class=\"l-heading3-body\">
              <h3 id=\"dairies\" class=\"m-heading3\">
                <label class=\"m-heading-btn\">";
                // line 33
                echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 33);
                echo "</label>
              </h3>
            </div>
          </div>
        </div>
        <div class=\"js-heading-accordion-body\">
          <ul class=\"l-card-frame-list\">
            ";
                // line 40
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["category"], "child", [], "any", false, false, false, 40));
                foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                    // line 41
                    echo "            <li data-u-col=\"3\" data-u-col-sm=\"6\">
              <a href=\"";
                    // line 42
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 42);
                    echo "\" class=\"l-card-frame\">
                <div class=\"l-card-frame-img\">
                  <img href=\"";
                    // line 44
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 44);
                    echo "\" src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb", [], "any", false, false, false, 44);
                    echo "\" class=\"m-img-fluid\">
                </div>
                <div class=\"l-card-frame-body\">
                  <p class=\"m-txtLink-strong-block\">
                    <strong>";
                    // line 48
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 48);
                    echo "</strong>
                  </p>
                </div>
              </a>
            </li>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 54
                echo "          </ul>
        </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 57
            echo "        ";
        }
        // line 58
        echo "        ";
        if (($context["products"] ?? null)) {
            // line 59
            echo "        <div class=\"js-heading-accordion-body\">
          <ul class=\"l-card-frame-list\">
            ";
            // line 61
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 62
                echo "            <li data-u-col=\"3\" data-u-col-sm=\"6\">
              <a href=\"";
                // line 63
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 63);
                echo "\" class=\"l-card-frame\">
                <div class=\"l-card-frame-img\">
                  <img href=\"";
                // line 65
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 65);
                echo "\" src=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb", [], "any", false, false, false, 65);
                echo "\" class=\"m-img-fluid\">
                </div>
                <div class=\"l-card-frame-body\">
                  <p class=\"m-txtLink-strong-block\">
                    <strong>";
                // line 69
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 69);
                echo "</strong>
                  </p>
                </div>
              </a>
            </li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 75
            echo "          </ul>
        </div>
        ";
        }
        // line 78
        echo "        ";
        if (( !($context["categories"] ?? null) &&  !($context["products"] ?? null))) {
            // line 79
            echo "        <p>";
            echo ($context["text_empty"] ?? null);
            echo "</p>
        <div class=\"buttons\">
          <div class=\"pull-right\">
            <a href=\"";
            // line 82
            echo ($context["continue"] ?? null);
            echo "\" class=\"btn btn-primary\" style=\"font-size: 15px;\">";
            echo ($context["button_continue"] ?? null);
            echo "</a>
          </div>
        </div>
        ";
        }
        // line 86
        echo "        ";
        echo ($context["content_bottom"] ?? null);
        echo "</div>
      ";
        // line 87
        echo ($context["column_right"] ?? null);
        echo "</div>
  </div>
</div>
";
        // line 90
        echo ($context["footer"] ?? null);
        echo "
<script>
  var num_product = \$('.l-card-frame-list li').length
  if(num_product <= 4) {
  \t\$('#content').css(\"height\", \"595px\")
    \$('#content').css(\"padding-top\", \"20px\")
  }
</script>";
    }

    public function getTemplateName()
    {
        return "default/template/product/category.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  244 => 90,  238 => 87,  233 => 86,  224 => 82,  217 => 79,  214 => 78,  209 => 75,  197 => 69,  188 => 65,  183 => 63,  180 => 62,  176 => 61,  172 => 59,  169 => 58,  166 => 57,  158 => 54,  146 => 48,  137 => 44,  132 => 42,  129 => 41,  125 => 40,  115 => 33,  109 => 29,  104 => 28,  102 => 27,  95 => 26,  92 => 25,  89 => 24,  86 => 23,  83 => 22,  80 => 21,  77 => 20,  75 => 19,  71 => 18,  63 => 12,  51 => 9,  47 => 8,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/product/category.twig", "");
    }
}
