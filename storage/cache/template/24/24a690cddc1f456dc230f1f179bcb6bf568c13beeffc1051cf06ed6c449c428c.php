<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mpblog/mpblogmenu.twig */
class __TwigTemplate_6c4a4e729d36d21faab0cd92c9b695448869bec8cd6f6d54a989203b2a7e70a3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"mp-left-content clearfix\">
\t<div class=\"mpblog-title\">
\t\t<h3>";
        // line 3
        echo ($context["text_mpblog"] ?? null);
        echo "</h3>
\t</div>
\t<ul id=\"mpblog-menu\">
\t\t";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["menus"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["menu"]) {
            // line 7
            echo "\t\t\t<li id=\"";
            echo twig_get_attribute($this->env, $this->source, $context["menu"], "id", [], "any", false, false, false, 7);
            echo "\">
\t\t\t\t";
            // line 8
            if (twig_get_attribute($this->env, $this->source, $context["menu"], "href", [], "any", false, false, false, 8)) {
                // line 9
                echo "\t\t\t\t<a class=\"clearfix\" href=\"";
                echo twig_get_attribute($this->env, $this->source, $context["menu"], "href", [], "any", false, false, false, 9);
                echo "\"><i class=\"fa ";
                echo twig_get_attribute($this->env, $this->source, $context["menu"], "icon", [], "any", false, false, false, 9);
                echo " fw\"></i> <span>";
                echo twig_get_attribute($this->env, $this->source, $context["menu"], "name", [], "any", false, false, false, 9);
                echo "</span></a>
\t\t\t\t";
            } else {
                // line 11
                echo "\t\t\t\t<a class=\"parent\"><i class=\"fa ";
                echo twig_get_attribute($this->env, $this->source, $context["menu"], "icon", [], "any", false, false, false, 11);
                echo " fw\"></i> <span>";
                echo twig_get_attribute($this->env, $this->source, $context["menu"], "name", [], "any", false, false, false, 11);
                echo "</span></a>
\t\t\t\t";
            }
            // line 13
            echo "\t\t\t\t";
            if (twig_get_attribute($this->env, $this->source, $context["menu"], "children", [], "any", false, false, false, 13)) {
                // line 14
                echo "\t\t\t\t<ul>
\t\t\t\t\t";
                // line 15
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["menu"], "children", [], "any", false, false, false, 15));
                foreach ($context['_seq'] as $context["_key"] => $context["children_1"]) {
                    // line 16
                    echo "\t\t\t\t\t<li>
\t\t\t\t\t\t";
                    // line 17
                    if (twig_get_attribute($this->env, $this->source, $context["children_1"], "href", [], "any", false, false, false, 17)) {
                        // line 18
                        echo "\t\t\t\t\t\t<a href=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["children_1"], "href", [], "any", false, false, false, 18);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context["children_1"], "name", [], "any", false, false, false, 18);
                        echo "</a>
\t\t\t\t\t\t";
                    } else {
                        // line 20
                        echo "\t\t\t\t\t\t<a class=\"parent\">";
                        echo twig_get_attribute($this->env, $this->source, $context["children_1"], "name", [], "any", false, false, false, 20);
                        echo "</a>
\t\t\t\t\t\t";
                    }
                    // line 22
                    echo "\t\t\t\t\t\t";
                    if (twig_get_attribute($this->env, $this->source, $context["children_1"], "children", [], "any", false, false, false, 22)) {
                        // line 23
                        echo "\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t";
                        // line 24
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["children_1"], "children", [], "any", false, false, false, 24));
                        foreach ($context['_seq'] as $context["_key"] => $context["children_2"]) {
                            // line 25
                            echo "\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t";
                            // line 26
                            if (twig_get_attribute($this->env, $this->source, $context["children_2"], "href", [], "any", false, false, false, 26)) {
                                // line 27
                                echo "\t\t\t\t\t\t\t\t<a href=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["children_2"], "href", [], "any", false, false, false, 27);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["children_2"], "name", [], "any", false, false, false, 27);
                                echo "</a>
\t\t\t\t\t\t\t\t";
                            } else {
                                // line 29
                                echo "\t\t\t\t\t\t\t\t<a class=\"parent\">";
                                echo twig_get_attribute($this->env, $this->source, $context["children_2"], "name", [], "any", false, false, false, 29);
                                echo "</a>
\t\t\t\t\t\t\t\t";
                            }
                            // line 31
                            echo "\t\t\t\t\t\t\t\t";
                            if (twig_get_attribute($this->env, $this->source, $context["children_2"], "children", [], "any", false, false, false, 31)) {
                                // line 32
                                echo "\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t";
                                // line 33
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["children_2"], "children", [], "any", false, false, false, 33));
                                foreach ($context['_seq'] as $context["_key"] => $context["children_3"]) {
                                    // line 34
                                    echo "\t\t\t\t\t\t\t\t\t<li><a href=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["children_3"], "href", [], "any", false, false, false, 34);
                                    echo "\">";
                                    echo twig_get_attribute($this->env, $this->source, $context["children_3"], "name", [], "any", false, false, false, 34);
                                    echo "</a></li>
\t\t\t\t\t\t\t\t\t";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children_3'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 36
                                echo "\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t";
                            }
                            // line 38
                            echo "\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children_2'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 40
                        echo "\t\t\t\t\t\t</ul>
\t\t\t\t\t\t";
                    }
                    // line 42
                    echo "\t\t\t\t\t</li>
\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children_1'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 44
                echo "\t\t\t\t</ul>
\t\t\t\t";
            }
            // line 46
            echo "\t\t\t</li>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "\t</ul>                  
</div>";
    }

    public function getTemplateName()
    {
        return "mpblog/mpblogmenu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  188 => 48,  181 => 46,  177 => 44,  170 => 42,  166 => 40,  159 => 38,  155 => 36,  144 => 34,  140 => 33,  137 => 32,  134 => 31,  128 => 29,  120 => 27,  118 => 26,  115 => 25,  111 => 24,  108 => 23,  105 => 22,  99 => 20,  91 => 18,  89 => 17,  86 => 16,  82 => 15,  79 => 14,  76 => 13,  68 => 11,  58 => 9,  56 => 8,  51 => 7,  47 => 6,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "mpblog/mpblogmenu.twig", "");
    }
}
