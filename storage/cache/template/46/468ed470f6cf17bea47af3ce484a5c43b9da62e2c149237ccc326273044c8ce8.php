<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/contact/contact_list.twig */
class __TwigTemplate_2776d5d04e83422e6f24c93507bc26ccd29d13e48f2841a878ce5587187e8e16 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo " ";
        echo ($context["column_left"] ?? null);
        echo " 
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
      ";
        // line 7
        echo "        <button type=\"button\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_delete"] ?? null);
        echo " \" class=\"btn btn-danger\" onclick=\"confirm('";
        echo ($context["text_confirm"] ?? null);
        echo " ') ? \$('#form').submit() : false;\"><i class=\"fa fa-trash-o\"></i></button>
      </div>
      <h1>";
        // line 9
        echo ($context["heading_title"] ?? null);
        echo " </h1>
      <ul class=\"breadcrumb\">
        ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            echo " 
        <li><a href=\"";
            // line 12
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 12);
            echo " \">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 12);
            echo " </a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo " 
      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 18
        if (($context["error_warning"] ?? null)) {
            echo " 
    <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            // line 19
            echo ($context["error_warning"] ?? null);
            echo " 
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 22
        echo " 
    ";
        // line 23
        if (($context["success"] ?? null)) {
            echo " 
    <div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i> ";
            // line 24
            echo ($context["success"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 27
        echo " 
    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i> ";
        // line 30
        echo ($context["heading_title"] ?? null);
        echo " </h3>
      </div>
      <div class=\"panel-body\">
      <form action=\"";
        // line 33
        echo ($context["delete"] ?? null);
        echo " \" method=\"post\" enctype=\"multipart/form-data\" id=\"form\">
        <div class=\"table-responsive\">
    <table class=\"table table-bordered table-hover\">
          <thead>
            <tr>
              <td width=\"1\" style=\"text-align: center;\"><input type=\"checkbox\" onclick=\"\$('input[name*=\\'selected\\']').attr('checked', this.checked);\" /></td>
              <td class=\"left\">";
        // line 39
        if ((($context["sort"] ?? null) == "id.title")) {
            echo " 
                <a href=\"";
            // line 40
            echo ($context["sort_title"] ?? null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, ($context["order"] ?? null));
            echo "\">";
            echo ($context["column_id"] ?? null);
            echo " </a>
                ";
        } else {
            // line 41
            echo "   
                <a href=\"";
            // line 42
            echo ($context["sort_title"] ?? null);
            echo "\">";
            echo ($context["column_id"] ?? null);
            echo " </a>
                ";
        }
        // line 43
        echo " 
                 </td>
              ";
        // line 51
        echo "                <!-- add -->
                 <td class=\"right\">";
        // line 52
        if ((($context["sort"] ?? null) == "i.sort_order")) {
            echo " 
                <a href=\"";
            // line 53
            echo ($context["sort_sort_order"] ?? null);
            echo " \" class=\"";
            echo twig_lower_filter($this->env, ($context["order"] ?? null));
            echo "\">";
            echo ($context["column_email"] ?? null);
            echo " </a>
                ";
        } else {
            // line 54
            echo "   
                <a href=\"";
            // line 55
            echo ($context["sort_sort_order"] ?? null);
            echo " \">";
            echo ($context["column_email"] ?? null);
            echo " </a>
                ";
        }
        // line 56
        echo " 
                 </td>
                 <td class=\"right\">";
        // line 58
        if ((($context["sort"] ?? null) == "i.sort_order")) {
            echo " 
                <a href=\"";
            // line 59
            echo ($context["sort_sort_order"] ?? null);
            echo " \" class=\"";
            echo twig_lower_filter($this->env, ($context["order"] ?? null));
            echo "\">";
            echo ($context["column_date"] ?? null);
            echo " </a>
                ";
        } else {
            // line 60
            echo "   
                <a href=\"";
            // line 61
            echo ($context["sort_sort_order"] ?? null);
            echo " \">";
            echo ($context["column_date"] ?? null);
            echo " </a>
                ";
        }
        // line 62
        echo " 
                 </td>
                <!-- end add -->
              <td class=\"right\">";
        // line 65
        echo ($context["column_action"] ?? null);
        echo " </td>
            </tr>
          </thead>
          <tbody>
            ";
        // line 69
        if (($context["contacts"] ?? null)) {
            echo " 
            ";
            // line 70
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["contacts"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["contact"]) {
                echo " 
            <tr>
              <td style=\"text-align: center;\">";
                // line 72
                if (twig_get_attribute($this->env, $this->source, $context["contact"], "selected", [], "any", false, false, false, 72)) {
                    echo " 
                <input type=\"checkbox\" name=\"selected[]\" value=\"";
                    // line 73
                    echo twig_get_attribute($this->env, $this->source, $context["contact"], "contact_id", [], "any", false, false, false, 73);
                    echo "\" checked=\"checked\" />
                ";
                } else {
                    // line 74
                    echo "   
                <input type=\"checkbox\" name=\"selected[]\" value=\"";
                    // line 75
                    echo twig_get_attribute($this->env, $this->source, $context["contact"], "contact_id", [], "any", false, false, false, 75);
                    echo "\" />
                ";
                }
                // line 76
                echo " </td>
              <td class=\"left\">";
                // line 77
                echo twig_get_attribute($this->env, $this->source, $context["contact"], "contact_id", [], "any", false, false, false, 77);
                echo " </td>
              ";
                // line 79
                echo "              <td class=\"right\">
                ";
                // line 84
                echo "                ";
                echo twig_get_attribute($this->env, $this->source, $context["contact"], "email", [], "any", false, false, false, 84);
                echo "
              </td>
              <td class=\"right\">";
                // line 86
                echo twig_get_attribute($this->env, $this->source, $context["contact"], "contact_date", [], "any", false, false, false, 86);
                echo " </td>
             <td class=\"text-right\">
              <div style=\"min-width: 120px;\">
                  <div class=\"btn-group\"> <a href=\"";
                // line 89
                echo twig_get_attribute($this->env, $this->source, $context["contact"], "edit", [], "any", false, false, false, 89);
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo ($context["button_view"] ?? null);
                echo "\" class=\"btn btn-primary\"><i class=\"fa fa-eye\"></i></a>
                    <button type=\"button\" data-toggle=\"dropdown\" class=\"btn btn-primary dropdown-toggle\"><span class=\"caret\"></span></button>
                    <ul class=\"dropdown-menu dropdown-menu-right\">
                      ";
                // line 93
                echo "                      <li><a href=\"";
                echo twig_get_attribute($this->env, $this->source, $context["contact"], "delete", [], "any", false, false, false, 93);
                echo "\"><i class=\"fa fa-trash-o\"></i> ";
                echo ($context["button_delete"] ?? null);
                echo "</a></li>
                    </ul>
                  </div>
              </div>
              </td>
            </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['contact'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 100
            echo "            ";
        } else {
            echo "   
            <tr>
              <td class=\"center\" colspan=\"6\">";
            // line 102
            echo ($context["text_no_results"] ?? null);
            echo " </td>
            </tr>
            ";
        }
        // line 105
        echo "          </tbody>
        </table>
    </div>
      </form>
      <div class=\"row\">
          <div class=\"col-sm-6 text-left\">";
        // line 110
        echo ($context["pagination"] ?? null);
        echo " </div>
          <div class=\"col-sm-6 text-right\">";
        // line 111
        echo ($context["results"] ?? null);
        echo " </div>
        </div>
      </div>
    </div>
  </div>
</div>
";
        // line 117
        echo ($context["footer"] ?? null);
        echo " ";
    }

    public function getTemplateName()
    {
        return "extension/contact/contact_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  326 => 117,  317 => 111,  313 => 110,  306 => 105,  300 => 102,  294 => 100,  278 => 93,  270 => 89,  264 => 86,  258 => 84,  255 => 79,  251 => 77,  248 => 76,  243 => 75,  240 => 74,  235 => 73,  231 => 72,  224 => 70,  220 => 69,  213 => 65,  208 => 62,  201 => 61,  198 => 60,  189 => 59,  185 => 58,  181 => 56,  174 => 55,  171 => 54,  162 => 53,  158 => 52,  155 => 51,  151 => 43,  144 => 42,  141 => 41,  132 => 40,  128 => 39,  119 => 33,  113 => 30,  108 => 27,  101 => 24,  97 => 23,  94 => 22,  87 => 19,  83 => 18,  76 => 13,  66 => 12,  60 => 11,  55 => 9,  47 => 7,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/contact/contact_list.twig", "");
    }
}
