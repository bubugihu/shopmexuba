<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mpblog/mpblogdashboard.twig */
class __TwigTemplate_8e3949dd8018f179c6e16765162874fd12f802060c7066de3a626e45ecf9647b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\" class=\"mp-content\">
\t<div class=\"page-header\">
\t\t\t<div class=\"container-fluid\">
\t\t\t\t<h1>";
        // line 5
        echo ($context["heading_title"] ?? null);
        echo "</h1>
\t\t\t\t<ul class=\"breadcrumb\">
\t\t\t\t\t";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumb"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumbs"]) {
            // line 8
            echo "\t\t\t\t\t<li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, ($context["breadcrumb"] ?? null), "href", [], "any", false, false, false, 8);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, ($context["breadcrumb"] ?? null), "text", [], "any", false, false, false, 8);
            echo "</a></li>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumbs'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t";
        // line 13
        echo ($context["mpblogmenu"] ?? null);
        echo "
\t<div class=\"mpblog-body\">
\t\t<div class=\"container-fluid\">
\t\t\t";
        // line 16
        if (($context["error_warning"] ?? null)) {
            // line 17
            echo "\t\t\t<div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t\t</div>
\t\t\t";
        }
        // line 21
        echo "\t\t\t";
        if (($context["success"] ?? null)) {
            // line 22
            echo "\t\t\t<div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i> ";
            echo ($context["success"] ?? null);
            echo "
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t\t</div>
\t\t\t";
        }
        // line 26
        echo "\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t<div class=\"tile\">
\t\t\t\t\t\t<a href=\"";
        // line 29
        echo ($context["mpblogs"] ?? null);
        echo "\">
\t\t\t\t\t\t<div class=\"tile-heading\">";
        // line 30
        echo ($context["text_total_blogs"] ?? null);
        echo " </div>
\t\t\t\t\t\t<div class=\"tile-body\">
\t\t\t\t\t\t\t<h2> <i class=\"fa fa-file-text-o\"></i> ";
        // line 32
        echo ($context["total_blogs"] ?? null);
        echo "</h2>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"tile-footer\">";
        // line 34
        echo ($context["text_view_blogs"] ?? null);
        echo "</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t<div class=\"tile\">
\t\t\t\t\t\t<a href=\"";
        // line 40
        echo ($context["mpcategory"] ?? null);
        echo "\">
\t\t\t\t\t\t<div class=\"tile-heading\">";
        // line 41
        echo ($context["text_total_categories"] ?? null);
        echo " </div>
\t\t\t\t\t\t<div class=\"tile-body\">
\t\t\t\t\t\t\t<h2> <i class=\"fa fa-list\"></i> ";
        // line 43
        echo ($context["total_categories"] ?? null);
        echo "</h2>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"tile-footer\">";
        // line 45
        echo ($context["text_view_categories"] ?? null);
        echo "</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t<div class=\"tile\">
\t\t\t\t\t\t<a href=\"";
        // line 51
        echo ($context["mpcomment"] ?? null);
        echo "\">
\t\t\t\t\t\t<div class=\"tile-heading\">";
        // line 52
        echo ($context["text_total_comments"] ?? null);
        echo " </div>
\t\t\t\t\t\t<div class=\"tile-body\">
\t\t\t\t\t\t\t<h2> <i class=\"fa fa-comments\"></i> ";
        // line 54
        echo ($context["total_comments"] ?? null);
        echo "</h2>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"tile-footer\">";
        // line 56
        echo ($context["text_view_comments"] ?? null);
        echo "</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t<div class=\"tile\">
\t\t\t\t\t\t<a href=\"";
        // line 62
        echo ($context["mprating"] ?? null);
        echo "\">
\t\t\t\t\t\t<div class=\"tile-heading\">";
        // line 63
        echo ($context["text_total_ratings"] ?? null);
        echo " </div>
\t\t\t\t\t\t<div class=\"tile-body\">
\t\t\t\t\t\t\t<h2> <i class=\"fa fa-star\"></i> ";
        // line 65
        echo ($context["total_ratings"] ?? null);
        echo "</h2>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"tile-footer\">";
        // line 67
        echo ($context["text_view_ratings"] ?? null);
        echo "</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-12\">
\t\t\t\t\t<div class=\"panel panel-default\">
\t\t\t\t\t  <div class=\"panel-heading\">
\t\t\t\t\t    <h3 class=\"panel-title\"><i class=\"fa fa-pencil-square-o\"></i> ";
        // line 76
        echo ($context["text_latest_blogs"] ?? null);
        echo "</h3>
\t\t\t\t\t  </div>
\t\t\t\t\t  <div class=\"table-responsive\">
\t\t\t\t\t\t<table class=\"table\">
\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>";
        // line 82
        echo ($context["column_image"] ?? null);
        echo "</td>
\t\t\t\t\t\t\t\t\t<td>";
        // line 83
        echo ($context["column_blog"] ?? null);
        echo "</td>
\t\t\t\t\t\t\t\t\t<td>";
        // line 84
        echo ($context["column_published"] ?? null);
        echo "</td>
\t\t\t\t\t\t\t\t\t<td>";
        // line 85
        echo ($context["column_date_available"] ?? null);
        echo "</td>
\t\t\t\t\t\t\t\t\t<td class=\"text-right\">";
        // line 86
        echo ($context["column_action"] ?? null);
        echo "</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t";
        // line 90
        if (($context["latest_blogs"] ?? null)) {
            echo " 
\t\t\t\t\t\t\t\t";
            // line 91
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["latest_blogs"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["latest_blog"]) {
                // line 92
                echo "\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td><img src=\"";
                // line 93
                echo twig_get_attribute($this->env, $this->source, $context["latest_blog"], "thumb", [], "any", false, false, false, 93);
                echo "\" alt=\"";
                echo twig_get_attribute($this->env, $this->source, $context["latest_blog"], "name", [], "any", false, false, false, 93);
                echo "\" /></td>
\t\t\t\t\t\t\t\t\t<td><a href=\"";
                // line 94
                echo twig_get_attribute($this->env, $this->source, $context["latest_blog"], "href", [], "any", false, false, false, 94);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["latest_blog"], "name", [], "any", false, false, false, 94);
                echo "</a></td>
\t\t\t\t\t\t\t\t\t<td>";
                // line 95
                echo twig_get_attribute($this->env, $this->source, $context["latest_blog"], "status", [], "any", false, false, false, 95);
                echo "</td>
\t\t\t\t\t\t\t\t\t<td>";
                // line 96
                echo twig_get_attribute($this->env, $this->source, $context["latest_blog"], "date_available", [], "any", false, false, false, 96);
                echo "</td>
\t\t\t\t\t\t\t\t\t<td class=\"text-right\"><a href=\"";
                // line 97
                echo twig_get_attribute($this->env, $this->source, $context["latest_blog"], "href", [], "any", false, false, false, 97);
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo ($context["text_edit"] ?? null);
                echo "\" class=\"btn btn-info\" data-original-title=\"";
                echo ($context["text_edit"] ?? null);
                echo "\"><i class=\"fa fa-eye\"></i></a></td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['latest_blog'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 100
            echo "\t\t\t\t\t\t\t    ";
        } else {
            // line 101
            echo "\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td colspan=\"6\" align=\"center\">";
            // line 102
            echo ($context["text_no_blogs"] ?? null);
            echo "</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t";
        }
        // line 105
        echo "\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t</table>
\t\t\t\t\t  </div>\t\t\t\t\t  \t\t
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"buttons text-right\">
\t\t\t\t\t\t<a class=\"btn btn-primary\" href=\"";
        // line 110
        echo ($context["all_blogs"] ?? null);
        echo "\">";
        echo ($context["button_viewall"] ?? null);
        echo "</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-12\">
\t\t\t\t\t<div class=\"panel panel-default\">
\t\t\t\t\t  <div class=\"panel-heading\">
\t\t\t\t\t    <h3 class=\"panel-title\"><i class=\"fa fa-comments\"></i> ";
        // line 116
        echo ($context["text_latest_comments"] ?? null);
        echo "</h3>
\t\t\t\t\t  </div>
\t\t\t\t\t  <div class=\"table-responsive\">
\t\t\t\t\t    <table class=\"table\">
\t\t\t\t\t      <thead>
\t\t\t\t\t        <tr>
\t\t\t\t\t          <td>";
        // line 122
        echo ($context["column_blog"] ?? null);
        echo "</td>
\t\t\t\t\t          <td>";
        // line 123
        echo ($context["column_commentby"] ?? null);
        echo "</td>
\t\t\t\t\t          <td>";
        // line 124
        echo ($context["column_comment"] ?? null);
        echo "</td>
\t\t\t\t\t          <td>";
        // line 125
        echo ($context["column_date_added"] ?? null);
        echo "</td>

\t\t\t\t\t          <td class=\"text-right\">";
        // line 127
        echo ($context["column_action"] ?? null);
        echo "</td>
\t\t\t\t\t        </tr>
\t\t\t\t\t      </thead>
\t\t\t\t\t      <tbody>
\t\t\t\t\t          \t";
        // line 131
        if (($context["latest_comments"] ?? null)) {
            echo " 
\t\t\t\t\t\t\t\t";
            // line 132
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["latest_comments"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["latest_comment"]) {
                // line 133
                echo "\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>";
                // line 134
                echo twig_get_attribute($this->env, $this->source, $context["latest_comment"], "name", [], "any", false, false, false, 134);
                echo "</td>
\t\t\t\t\t\t\t\t\t<td><a href=\"";
                // line 135
                echo twig_get_attribute($this->env, $this->source, $context["latest_comment"], "href", [], "any", false, false, false, 135);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["latest_comment"], "author", [], "any", false, false, false, 135);
                echo "</a></td>
\t\t\t\t\t\t\t\t\t<td>";
                // line 136
                echo twig_get_attribute($this->env, $this->source, $context["latest_comment"], "text", [], "any", false, false, false, 136);
                echo "</td>
\t\t\t\t\t\t\t\t\t<td>";
                // line 137
                echo twig_get_attribute($this->env, $this->source, $context["latest_comment"], "date_added", [], "any", false, false, false, 137);
                echo "</td>
\t\t\t\t\t\t\t\t\t<td class=\"text-right\"><a href=\"";
                // line 138
                echo twig_get_attribute($this->env, $this->source, $context["latest_comment"], "href", [], "any", false, false, false, 138);
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo ($context["text_edit"] ?? null);
                echo "\" class=\"btn btn-info\" data-original-title=\"";
                echo ($context["text_edit"] ?? null);
                echo "\"><i class=\"fa fa-eye\"></i></a></td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['latest_comment'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 141
            echo "\t\t\t\t\t\t\t    ";
        } else {
            // line 142
            echo "\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td colspan=\"6\">";
            // line 143
            echo ($context["text_no_comments"] ?? null);
            echo "</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t";
        }
        // line 146
        echo "\t\t\t\t\t      </tbody>
\t\t\t\t\t    </table>
\t\t\t\t\t  </div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"buttons text-right\">
\t\t\t\t\t\t<a class=\"btn btn-primary\" href=\"";
        // line 151
        echo ($context["all_comments"] ?? null);
        echo "\">";
        echo ($context["button_viewall"] ?? null);
        echo "</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div> 
\t\t</div>
\t</div>
</div>
";
        // line 158
        echo ($context["footer"] ?? null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "mpblog/mpblogdashboard.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  409 => 158,  397 => 151,  390 => 146,  384 => 143,  381 => 142,  378 => 141,  365 => 138,  361 => 137,  357 => 136,  351 => 135,  347 => 134,  344 => 133,  340 => 132,  336 => 131,  329 => 127,  324 => 125,  320 => 124,  316 => 123,  312 => 122,  303 => 116,  292 => 110,  285 => 105,  279 => 102,  276 => 101,  273 => 100,  260 => 97,  256 => 96,  252 => 95,  246 => 94,  240 => 93,  237 => 92,  233 => 91,  229 => 90,  222 => 86,  218 => 85,  214 => 84,  210 => 83,  206 => 82,  197 => 76,  185 => 67,  180 => 65,  175 => 63,  171 => 62,  162 => 56,  157 => 54,  152 => 52,  148 => 51,  139 => 45,  134 => 43,  129 => 41,  125 => 40,  116 => 34,  111 => 32,  106 => 30,  102 => 29,  97 => 26,  89 => 22,  86 => 21,  78 => 17,  76 => 16,  70 => 13,  65 => 10,  54 => 8,  50 => 7,  45 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "mpblog/mpblogdashboard.twig", "");
    }
}
