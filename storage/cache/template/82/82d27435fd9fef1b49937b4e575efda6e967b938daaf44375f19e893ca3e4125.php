<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/module/isenselabs_seo/sd_product_data_breadcrumbs.twig */
class __TwigTemplate_929f7795e443ed44c7837e785ff36a043c015b79d7651dfd71a06fb0226a61c1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["product_data"] ?? null)) {
            // line 2
            echo "<script type=\"application/ld+json\">
{
    \"@context\": \"http://schema.org\",
    \"@type\": \"Product\",
    ";
            // line 6
            if (($context["image"] ?? null)) {
                echo "\"image\": \"";
                echo ($context["image"] ?? null);
                echo "\",";
            }
            echo "    
    \"name\": \"";
            // line 7
            echo ($context["name"] ?? null);
            echo "\",
    \"model\": \"";
            // line 8
            echo ($context["model"] ?? null);
            echo "\",
    \"sku\": \"";
            // line 9
            echo ($context["sku"] ?? null);
            echo "\",
    \"gtin8\": \"";
            // line 10
            echo ($context["ean"] ?? null);
            echo "\",
    \"mpn\": \"";
            // line 11
            echo ($context["mpn"] ?? null);
            echo "\",
    \"description\": \"";
            // line 12
            echo ($context["description"] ?? null);
            echo "\",
    \"brand\": {
        \"@type\": \"Thing\",
        \"name\": \"";
            // line 15
            echo ($context["manufacturer"] ?? null);
            echo "\"
    },
    \"offers\": {
        \"@type\": \"Offer\",
        \"url\": \"";
            // line 19
            echo ($context["url"] ?? null);
            echo "\",
        \"priceCurrency\": \"";
            // line 20
            echo ($context["currency_code"] ?? null);
            echo "\",
        \"price\": \"";
            // line 21
            echo ($context["price"] ?? null);
            echo "\",
        \"priceValidUntil\": \"";
            // line 22
            echo ($context["price_valid_until"] ?? null);
            echo "\",
    ";
            // line 23
            if ((($context["quantity"] ?? null) > 0)) {
                // line 24
                echo "        \"availability\": \"http://schema.org/InStock\"
    ";
            } else {
                // line 26
                echo "        \"availability\": \"http://schema.org/OutOfStock\"
    ";
            }
            // line 28
            echo "    }
    ";
            // line 29
            if ((($context["review_count"] ?? null) > 0)) {
                echo ",
    \"aggregateRating\": {
        \"@type\": \"aggregateRating\",
        \"ratingValue\": \"";
                // line 32
                echo ($context["rating"] ?? null);
                echo "\",
        \"reviewCount\": \"";
                // line 33
                echo ($context["review_count"] ?? null);
                echo "\",
        \"bestRating\": \"5\",
        \"worstRating\" : \"1\"
    },
    \"review\":{
      \"@type\": \"Review\",
      \"reviewRating\": {
        \"@type\": \"Rating\",
        \"ratingValue\": \"";
                // line 41
                echo ($context["review_rating"] ?? null);
                echo "\",
        \"bestRating\": \"5\"
      },
    \"author\": {
        \"@type\": \"Person\",
        \"name\": \"";
                // line 46
                echo ($context["review_author"] ?? null);
                echo "\"
      }
    }
    ";
            }
            // line 50
            echo "}
</script>
";
        }
        // line 53
        echo "
";
        // line 54
        if ((($context["product_breadcrumbs"] ?? null) && ($context["breadcrumbs"] ?? null))) {
            // line 55
            echo "<script type=\"application/ld+json\">
{
  \"@context\": \"http://schema.org\",
  \"@type\": \"BreadcrumbList\",
  \"itemListElement\": [
  
  ";
            // line 61
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["index"] => $context["breadcrumb"]) {
                // line 62
                echo "  {
    \"@type\": \"ListItem\",
    \"position\": ";
                // line 64
                echo ($context["index"] + 1);
                echo ",
    \"item\": {
      \"@id\": \"";
                // line 66
                echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 66);
                echo "\",
      \"name\": \"";
                // line 67
                echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "name", [], "any", false, false, false, 67);
                echo "\"
    }
  }   
  ";
                // line 70
                if (twig_get_attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 70)) {
                    // line 71
                    echo "  ]
  ";
                } else {
                    // line 73
                    echo "  ,
  ";
                }
                // line 75
                echo "  ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['index'], $context['breadcrumb'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 76
            echo "}
</script>
";
        }
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/isenselabs_seo/sd_product_data_breadcrumbs.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  229 => 76,  215 => 75,  211 => 73,  207 => 71,  205 => 70,  199 => 67,  195 => 66,  190 => 64,  186 => 62,  169 => 61,  161 => 55,  159 => 54,  156 => 53,  151 => 50,  144 => 46,  136 => 41,  125 => 33,  121 => 32,  115 => 29,  112 => 28,  108 => 26,  104 => 24,  102 => 23,  98 => 22,  94 => 21,  90 => 20,  86 => 19,  79 => 15,  73 => 12,  69 => 11,  65 => 10,  61 => 9,  57 => 8,  53 => 7,  45 => 6,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/extension/module/isenselabs_seo/sd_product_data_breadcrumbs.twig", "");
    }
}
