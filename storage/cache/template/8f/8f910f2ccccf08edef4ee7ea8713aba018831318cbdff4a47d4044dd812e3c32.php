<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/common/menu.twig */
class __TwigTemplate_f85d52f35218428b7514398097713a390c69ff7d6dbf0dc590c462a480c85bde extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["categories"] ?? null)) {
            // line 2
            echo "<nav class=\"l-nav\" role=\"navigation\" aria-label=\"メインメニュー\">
  <ul class=\"m-nav-list js-nav-list\">
    <li class=\"js-nav-list-content\">
      <a class=\"m-nav js-nav\" data-nav=\"products\" href=\"";
            // line 5
            echo ($context["home_href"] ?? null);
            echo "\" style=\"text-transform: uppercase;\">
        <span class=\"m-nav-txt\">";
            // line 6
            echo ($context["text_home"] ?? null);
            echo "</span>
      </a>
    </li>
    <li class=\"js-nav-list-content\">
      <a class=\"m-nav js-nav\" href=\"";
            // line 10
            echo ($context["information_href"] ?? null);
            echo "\" style=\"text-transform: uppercase;\">
        <span class=\"m-nav-txt\">";
            // line 11
            echo ($context["text_information"] ?? null);
            echo "</span>
      </a>
    </li>
    ";
            // line 14
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 15
                echo "    ";
                if (twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 15)) {
                    // line 16
                    echo "    <li class=\"js-nav-list-content\">
      <label class=\"m-nav-btn js-nav\" data-nav=\"cm-campaign\" tabindex=\"0\">
        <a href=\"";
                    // line 18
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 18);
                    echo "\" class=\"m-nav-txt\" style=\"color:black;text-decoration: none; text-transform: uppercase;\">";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 18);
                    echo "</a>
        <button class=\"js-nav-btn\">
          <img src=\"/catalog/view/theme/default/stylesheet/img/icons/plus.svg\" class=\"m-nav-btn-icon js-nav-btn-icon\" alt=\"開く\">
        </button>
      </label>
      ";
                    // line 23
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_array_batch(twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 23), (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 23)) / twig_round(twig_get_attribute($this->env, $this->source, $context["category"], "column", [], "any", false, false, false, 23), 1, "ceil"))));
                    foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                        // line 24
                        echo "      <div id=\"cm-subMenu\" class=\"l-nav-container js-nav-container\">
        <div class=\"l-nav-inner js-nav-inner\" style=\"padding:0px 30px 0px\">
          <div class=\"l-nav-cm-grid\">
            <div class=\"l-nav-item-2\">
              <ul class=\"m-nav-btn-list\">
                ";
                        // line 29
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($context["children"]);
                        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                            // line 30
                            echo "                <li data-u-col=\"4\" data-u-col-md=\"12\">
                  <a href=\"";
                            // line 31
                            echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 31);
                            echo "\" class=\"m-nav-link-btn\">
                    <span class=\"m-nav-link-btn-txt\">";
                            // line 32
                            echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 32);
                            echo "</span>
                  </a>
                </li>
                ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 36
                        echo "              </ul>
            </div>
            <div class=\"l-nav-item-3\" style=\"justify-self: center;\">
              <a href=\"";
                        // line 39
                        echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 39);
                        echo "\" class=\"m-nav-link-txt see-all\">";
                        echo ($context["text_all"] ?? null);
                        echo "
                ";
                        // line 40
                        echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 40);
                        echo "</a>
            </div>
          </div>
        </div>
      </div>
      ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 46
                    echo "    </li>
    ";
                } else {
                    // line 48
                    echo "    <li class=\"js-nav-list-content\">
      <a href=\"";
                    // line 49
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 49);
                    echo "\" class=\"m-nav js-nav\" data-nav=\"products\" style=\"text-transform: uppercase;\">
        <span class=\"m-nav-txt\">";
                    // line 50
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 50);
                    echo "</span>
      </a>
    </li>
    ";
                }
                // line 54
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 55
            echo "    <li class=\"js-nav-list-content\">
      <a class=\"m-nav js-nav\" href=\"";
            // line 56
            echo ($context["contact_href"] ?? null);
            echo "\" style=\"text-transform: uppercase;\">
        <span class=\"m-nav-txt\">";
            // line 57
            echo ($context["text_contact"] ?? null);
            echo "</span>
      </a>
    </li>
  </ul>
</nav>
";
        }
        // line 63
        echo "
";
    }

    public function getTemplateName()
    {
        return "default/template/common/menu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  181 => 63,  172 => 57,  168 => 56,  165 => 55,  159 => 54,  152 => 50,  148 => 49,  145 => 48,  141 => 46,  129 => 40,  123 => 39,  118 => 36,  108 => 32,  104 => 31,  101 => 30,  97 => 29,  90 => 24,  86 => 23,  76 => 18,  72 => 16,  69 => 15,  65 => 14,  59 => 11,  55 => 10,  48 => 6,  44 => 5,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/common/menu.twig", "");
    }
}
