<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/module/featured.twig */
class __TwigTemplate_0a2997655a02a35a5231c57edfb21d37db2065ae0f630c58226b929aa99785ba extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<style>
  .product-name {
    display: -webkit-box;
    max-height: 3.2rem;
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: normal;
    -webkit-line-clamp: 1;
    line-height: 2.5rem;
  }
</style>
<div class=\"inner\" style=\"margin-top:10%\">
  <div class=\"section\">
    <div class=\"section-container\">
      <div class=\"section-inner\">
        <div class=\"product\">
          <div class=\"heading-container\">
            <div class=\"heading\">
              <label id=\"product\" class=\"heading\" style=\"text-align: center; color: black; font-size: 45px\" data-delay=\"80\">";
        // line 20
        echo ($context["heading_title_product"] ?? null);
        echo "</label>
            </div>
          </div>
          <ul class=\"product-list\" style=\"justify-content: center;\">
            ";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 25
            echo "            <li id=\"product";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 25);
            echo "\">
              <a class=\"product-link\" href=\"";
            // line 26
            echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 26);
            echo "\">
                <div class=\"product-img\">
                  <img alt=\"";
            // line 28
            echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 28);
            echo "\" src=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb", [], "any", false, false, false, 28);
            echo "\" data-size=\"lg\">
                </div>
                <div class=\"product-body\">
                  <p class=\"product-name\">";
            // line 31
            echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 31);
            echo "</p>
                </div>
              </a>
            </li>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/featured.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 36,  87 => 31,  79 => 28,  74 => 26,  69 => 25,  65 => 24,  58 => 20,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/extension/module/featured.twig", "");
    }
}
