<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/common/success.twig */
class __TwigTemplate_fb20def1c40dce9ab02ba69ee3abe0259fb78fbc33e1bc67a9b7611522a15f6c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div style=\"background-color: #DBF9FB;\">
  <div class=\"l-breadcrumb\" style=\"margin-bottom: 30px;\"> 
    <div class=\"l-breadcrumb-container\"> 
      <div class=\"l-breadcrumb-inner\"> 
        <nav role=\"navigation\" aria-label=\"現在位置\"> 
          <ol class=\"m-breadcrumb\" itemscope=\"itemscope\" itemtype=\"http://schema.org/BreadcrumbList\"> 
            ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 9
            echo "            <li itemprop=\"itemListElement\" itemscope=\"itemscope\" itemtype=\"http://schema.org/ListItem\"><a style=\"text-transform: uppercase;\" href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 9);
            echo "\" class=\"m-breadcrumb-item\" itemprop=\"item\"><span style=\"color: #fff; font-size: 18px;\" itemprop=\"name\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 9);
            echo "</span></a>
              <meta itemprop=\"position\" content=\"1\" style=\"color: #fff; font-size: 18px;\"> </li> 
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "          </ol> 
        </nav> 
      </div> 
    </div> 
  </div>
  <div id=\"common-success\" class=\"container\" style=\"padding-bottom: 15px;\">
    <div class=\"row\">";
        // line 18
        echo ($context["column_left"] ?? null);
        echo "
      ";
        // line 19
        if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
            // line 20
            echo "      ";
            $context["class"] = "col-sm-6";
            // line 21
            echo "      ";
        } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 22
            echo "      ";
            $context["class"] = "col-sm-9";
            // line 23
            echo "      ";
        } else {
            // line 24
            echo "      ";
            $context["class"] = "col-sm-12";
            // line 25
            echo "      ";
        }
        // line 26
        echo "      <div id=\"content\" class=\"";
        echo ($context["class"] ?? null);
        echo "\" style=\"line-height: 1.7;\">
        <h1>";
        // line 27
        echo ($context["heading_title"] ?? null);
        echo "</h1>
        ";
        // line 28
        echo ($context["text_success"] ?? null);
        echo "
        <div class=\"buttons\">
          <div class=\"pull-right\"><a href=\"";
        // line 30
        echo ($context["continue"] ?? null);
        echo "\" class=\"btn btn-pink\" style=\"font-size: 17px; margin-bottom: 20px;\">";
        echo ($context["button_continue"] ?? null);
        echo "</a></div>
        </div>
        ";
        // line 32
        echo ($context["content_bottom"] ?? null);
        echo "</div>
      ";
        // line 33
        echo ($context["column_right"] ?? null);
        echo "</div>
  </div>
</div>
";
        // line 36
        echo ($context["footer"] ?? null);
        echo "
<script>
var check_cart = \$('#content h1').text().trim()
var check_mail = \$('#content p').text().trim()
if(check_cart == \"Thành công !!!\" || check_cart == \"Success !!!\" || check_cart == \"注文完了 !!!\" || check_cart == \"CONTACT US\" || check_cart == \"お問合せ\" ) {
\$('#common-success').css(\"height\", \"510px\")
}
if (check_mail == \"Yêu cầu của bạn đã được gửi tới chủ cửa hàng!\") {
\$('#common-success').css(\"height\", \"510px\")
}
</script>";
    }

    public function getTemplateName()
    {
        return "default/template/common/success.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 36,  120 => 33,  116 => 32,  109 => 30,  104 => 28,  100 => 27,  95 => 26,  92 => 25,  89 => 24,  86 => 23,  83 => 22,  80 => 21,  77 => 20,  75 => 19,  71 => 18,  63 => 12,  51 => 9,  47 => 8,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/common/success.twig", "");
    }
}
