<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/mpblog/blogs_list.twig */
class __TwigTemplate_f13f6a1c2be60d548b1b06b1c8180374d1e6c0bab8f57b5f1d9d962485818399 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div style=\"background-color: #DBF9FB\">
  <div class=\"l-breadcrumb\" style=\"margin-bottom: 30px;\"> 
    <div class=\"l-breadcrumb-container\"> 
      <div class=\"l-breadcrumb-inner\"> 
        <nav role=\"navigation\" aria-label=\"現在位置\"> 
          <ol class=\"m-breadcrumb\" itemscope=\"itemscope\" itemtype=\"http://schema.org/BreadcrumbList\"> 
            ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 9
            echo "            <li itemprop=\"itemListElement\" itemscope=\"itemscope\" itemtype=\"http://schema.org/ListItem\"><a style=\"text-transform: uppercase;\" href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 9);
            echo "\" class=\"m-breadcrumb-item\" itemprop=\"item\"><span style=\"color: #fff; font-size: 18px;\" itemprop=\"name\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 9);
            echo "</span></a>
              <meta itemprop=\"position\" content=\"1\" style=\"color: #fff; font-size: 18px;\"> </li> 
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "          </ol> 
        </nav> 
      </div> 
    </div> 
  </div>
  <div id=\"container\" class=\"container j-container mp-blog ";
        // line 17
        echo ($context["journal_class"] ?? null);
        echo "\">
    <div class=\"row blog_list\">";
        // line 18
        echo ($context["column_left"] ?? null);
        echo "
      ";
        // line 19
        if ((($context["theme_name"] ?? null) == "journal2")) {
            // line 20
            echo "      ";
            echo ($context["column_right"] ?? null);
            echo "
      ";
        }
        // line 22
        echo "      ";
        if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
            // line 23
            echo "      ";
            $context["class"] = "col-sm-6";
            // line 24
            echo "      ";
        } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 25
            echo "      ";
            $context["class"] = "col-sm-8";
            // line 26
            echo "      ";
        } else {
            // line 27
            echo "      ";
            $context["class"] = "col-sm-12";
            // line 28
            echo "      ";
        }
        // line 29
        echo "      <div id=\"content\" class=\"";
        echo ($context["class"] ?? null);
        echo "\">";
        echo ($context["content_top"] ?? null);
        echo "
        ";
        // line 30
        if (($context["mpblogposts"] ?? null)) {
            // line 31
            echo "        <div class=\"xl-100\">
          ";
            // line 32
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["mpblogposts"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["mpblogpost"]) {
                // line 33
                echo "          <div class=\"mpblogpost-layout mpblogpost-list\">
            <div class=\"row\">
              <div class=\"col-sm-4 xl-33 sm-100\">
                <div class=\"image\">
                  ";
                // line 37
                if (twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "showImage", [], "any", false, false, false, 37)) {
                    // line 38
                    echo "                  <a href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "href", [], "any", false, false, false, 38);
                    echo "\"><img src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "thumb", [], "any", false, false, false, 38);
                    echo "\" alt=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "name", [], "any", false, false, false, 38);
                    echo "\" title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "name", [], "any", false, false, false, 38);
                    echo "\" class=\"img-responsive\" /></a>
                  ";
                } else {
                    // line 40
                    echo "                  <div class=\"video-container\"><iframe width=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "width", [], "any", false, false, false, 40);
                    echo "\" height=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "height", [], "any", false, false, false, 40);
                    echo "\" src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "iframeVideo", [], "any", false, false, false, 40);
                    echo "\" frameborder=\"0\" allowfullscreen></iframe></div>
                  ";
                }
                // line 42
                echo "                  <ul class=\"list-inline dar hidden-xs\">
                    ";
                // line 43
                if (($context["show_author"] ?? null)) {
                    // line 44
                    echo "                    <li title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "author", [], "any", false, false, false, 44);
                    echo "\">
                      <i class=\"fa fa-user\"></i>                    
                      ";
                    // line 46
                    if (twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "authorurl", [], "any", false, false, false, 46)) {
                        // line 47
                        echo "                      <a href=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "authorurl", [], "any", false, false, false, 47);
                        echo "\">
                        ";
                        // line 48
                        echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "author", [], "any", false, false, false, 48);
                        echo "
                      </a>
                      ";
                    } else {
                        // line 51
                        echo "                      ";
                        echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "author", [], "any", false, false, false, 51);
                        echo "
                      ";
                    }
                    // line 53
                    echo "                    </li>
                    ";
                }
                // line 55
                echo "                  </ul>  
                  ";
                // line 56
                if (($context["show_viewed"] ?? null)) {
                    // line 57
                    echo "                  <div class=\"view\" title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "viewed", [], "any", false, false, false, 57);
                    echo "\">
                    <i class=\"fa fa-eye\"></i>
                    ";
                    // line 59
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "viewed", [], "any", false, false, false, 59);
                    echo "
                  </div>
                  ";
                }
                // line 62
                echo "                </div>
              </div>
              <div class=\"col-sm-8 xl-66 sm-100\">
                <div class=\"caption\">
                  <h4><a href=\"";
                // line 66
                echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "href", [], "any", false, false, false, 66);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "name", [], "any", false, false, false, 66);
                echo "</a></h4>
                  <ul class=\"list-inline dar mpblogpost-id-";
                // line 67
                echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "mpblogpost_id", [], "any", false, false, false, 67);
                echo "\">
                    ";
                // line 68
                if (($context["show_date"] ?? null)) {
                    // line 69
                    echo "                    <li title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "date_available", [], "any", false, false, false, 69);
                    echo "\">
                      <i class=\"fa fa-calendar\"></i>
                      ";
                    // line 71
                    if (twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "date_availableurl", [], "any", false, false, false, 71)) {
                        // line 72
                        echo "                      <a href=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "date_availableurl", [], "any", false, false, false, 72);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "date_available", [], "any", false, false, false, 72);
                        echo "</a>
                      ";
                    } else {
                        // line 74
                        echo "                      ";
                        echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "date_available", [], "any", false, false, false, 74);
                        echo "
                      ";
                    }
                    // line 76
                    echo "                    </li>
                    ";
                }
                // line 78
                echo "                    ";
                if (($context["show_rating"] ?? null)) {
                    // line 79
                    echo "                    <li title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "rating", [], "any", false, false, false, 79);
                    echo "\">
                      <div class=\"rating small-rating\">
                        <div class=\"rating-icons-container text-center\">
                          <div class=\"rating-icons-wrap\">
                            ";
                    // line 83
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(range(1, 5));
                    foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                        // line 84
                        echo "                            ";
                        if ((twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "rating", [], "any", false, false, false, 84) < $context["i"])) {
                            // line 85
                            echo "                            ";
                            $context["rdecimal"] = "EMPTY_STAR";
                            // line 86
                            echo "                            ";
                            if ((twig_round(twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "rating", [], "any", false, false, false, 86), 1, "ceil") == $context["i"])) {
                                // line 87
                                echo "                            ";
                                $context["rates"] = twig_split_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "rating", [], "any", false, false, false, 87), ".");
                                // line 88
                                echo "                            ";
                                if ((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["rates"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[1] ?? null) : null)) {
                                    // line 89
                                    echo "                            ";
                                    if ((("0" . twig_round((($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["rates"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[1] ?? null) : null), 1, "floor")) < 0.6)) {
                                        // line 90
                                        echo "                            ";
                                        $context["rdecimal"] = "HALF_STAR";
                                        // line 91
                                        echo "                            ";
                                    }
                                    // line 92
                                    echo "                            ";
                                    if ((("0" . twig_round((($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["rates"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b[1] ?? null) : null), 1, "floor")) > 0.6)) {
                                        // line 93
                                        echo "                            ";
                                        $context["rdecimal"] = "FULL_STAR";
                                        // line 94
                                        echo "                            ";
                                    }
                                    // line 95
                                    echo "                            ";
                                }
                                // line 96
                                echo "                            ";
                            }
                            // line 97
                            echo "                            ";
                            if ((($context["rdecimal"] ?? null) == "HALF_STAR")) {
                                // line 98
                                echo "                            <div class=\"rating-icons half\">
                              <span class=\"red\"></span>  
                              <span class=\"grey\"></span>  
                              <i class=\"fa fa-star\"></i>
                            </div>
                            ";
                            }
                            // line 103
                            echo " ";
                            if ((($context["rdecimal"] ?? null) == "FULL_STAR")) {
                                // line 104
                                echo "                            <div class=\"rating-icons full\">
                              <span class=\"red\"></span>  
                              <span class=\"grey\"></span>  
                              <i class=\"fa fa-star\"></i>
                            </div>
                            ";
                            }
                            // line 109
                            echo " ";
                            if ((($context["rdecimal"] ?? null) == "EMPTY_STAR")) {
                                // line 110
                                echo "                            <div class=\"rating-icons\">
                              <span class=\"red\"></span>  
                              <span class=\"grey\"></span>  
                              <i class=\"fa fa-star\"></i>
                            </div>
                            ";
                            }
                            // line 115
                            echo " ";
                        } else {
                            // line 116
                            echo "                            <div class=\"rating-icons full\">
                              <span class=\"red\"></span>  
                              <span class=\"grey\"></span>  
                              <i class=\"fa fa-star\"></i>
                            </div>
                            ";
                        }
                        // line 122
                        echo "                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 123
                    echo "                          </div>
                        </div>
                      </div>
                    </li>
                    ";
                }
                // line 128
                echo "                  </ul>
                  ";
                // line 129
                if (($context["show_sdescription"] ?? null)) {
                    echo "<p class=\"desc\">";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "sdescription1", [], "any", false, false, false, 129);
                    echo " ";
                    if ((twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "sdescription1", [], "any", false, false, false, 129) && ($context["show_readmore"] ?? null))) {
                        echo " <a href=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "href", [], "any", false, false, false, 129);
                        echo "\">  ";
                        echo ($context["text_readmore"] ?? null);
                        echo " </a> ";
                    }
                    echo "</p>";
                }
                // line 130
                echo "                  <ul class=\"list-inline dar\">
                    ";
                // line 131
                if (($context["show_wishlist"] ?? null)) {
                    // line 132
                    echo "                    <li title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "wishlist", [], "any", false, false, false, 132);
                    echo "\">
                      <span class=\"mpbloglike ";
                    // line 133
                    if (twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "isLikeByMe", [], "any", false, false, false, 133)) {
                        echo " liked ";
                    }
                    echo "\" data-id=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "mpblogpost_id", [], "any", false, false, false, 133);
                    echo "\">
                        <i class=\"fa fa-heart ";
                    // line 134
                    if ( !twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "isLikeByMe", [], "any", false, false, false, 134)) {
                        echo " fa-heart-o ";
                    }
                    echo "\"></i>
                        <span>";
                    // line 135
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "wishlist", [], "any", false, false, false, 135);
                    echo "</span>
                      </span>
                    </li>
                    ";
                }
                // line 139
                echo "                    ";
                if (($context["show_comments"] ?? null)) {
                    // line 140
                    echo "                    <li title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "comments", [], "any", false, false, false, 140);
                    echo "\">
                      <i class=\"fa ";
                    // line 141
                    if (twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "comments", [], "any", false, false, false, 141)) {
                        echo " fa-comments ";
                    } else {
                        echo " fa-comments-o ";
                    }
                    echo "\"></i>
                      ";
                    // line 142
                    echo ((twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "comments", [], "any", false, false, false, 142) . " ") . ($context["text_comment"] ?? null));
                    echo "
                    </li>
                    ";
                }
                // line 145
                echo "                  </ul>
                  ";
                // line 146
                if ((($context["show_tag"] ?? null) && twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "tag", [], "any", false, false, false, 146))) {
                    // line 147
                    echo "                  <ul class=\"list-inline blog-tags\">
                    <li>
                      <i class=\"fa fa-tags\"></i>
                      ";
                    // line 150
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["mpblogpost"], "tag", [], "any", false, false, false, 150));
                    foreach ($context['_seq'] as $context["_key"] => $context["rtag"]) {
                        // line 151
                        echo "                      ";
                        if (twig_get_attribute($this->env, $this->source, $context["rtag"], "href", [], "any", false, false, false, 151)) {
                            // line 152
                            echo "                      <a href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["rtag"], "href", [], "any", false, false, false, 152);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["rtag"], "tag", [], "any", false, false, false, 152);
                            echo "</a>
                      ";
                        } else {
                            // line 154
                            echo "                      ";
                            echo twig_get_attribute($this->env, $this->source, $context["rtag"], "tag", [], "any", false, false, false, 154);
                            echo "
                      ";
                        }
                        // line 156
                        echo "                      ,
                      ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rtag'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 158
                    echo "                    </li>
                  </ul>
                  ";
                }
                // line 161
                echo "                </div>
              </div>
            </div>  
          </div>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mpblogpost'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 166
            echo "        </div>
        <div class=\"row\">
          <div class=\"col-sm-6 text-left\">";
            // line 168
            echo ($context["pagination"] ?? null);
            echo "</div>
          <div class=\"col-sm-6 text-right\">";
            // line 169
            echo ($context["results"] ?? null);
            echo "</div>
        </div>

        ";
        } else {
            // line 173
            echo "        <div class=\"row\">
          <div class=\"col-sm-12\">
            <p>";
            // line 175
            echo ($context["text_empty"] ?? null);
            echo "</p>
            <div class=\"buttons text-right\">
              <div><a href=\"";
            // line 177
            echo ($context["continue"] ?? null);
            echo "\" class=\"btn btn-pink\" style=\"border-radius:20px; font-size: 15px\">";
            echo ($context["button_continue"] ?? null);
            echo "</a></div>
            </div>
          </div>
        </div>
        ";
        }
        // line 182
        echo "        ";
        echo ($context["content_bottom"] ?? null);
        echo "</div>
      ";
        // line 183
        if ((($context["theme_name"] ?? null) != "journal2")) {
            // line 184
            echo "      ";
            echo ($context["column_right"] ?? null);
            echo "
      ";
        }
        // line 185
        echo "</div>
  </div>
</div>
";
        // line 188
        echo ($context["footer"] ?? null);
        echo "
<script>
  var num_blog = \$('.mpblogpost-list').length
  if(num_blog < 2) {
  \t\$('.blog_list').css(\"height\", \"515px\")
  }
</script>";
    }

    public function getTemplateName()
    {
        return "default/template/mpblog/blogs_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  534 => 188,  529 => 185,  523 => 184,  521 => 183,  516 => 182,  506 => 177,  501 => 175,  497 => 173,  490 => 169,  486 => 168,  482 => 166,  472 => 161,  467 => 158,  460 => 156,  454 => 154,  446 => 152,  443 => 151,  439 => 150,  434 => 147,  432 => 146,  429 => 145,  423 => 142,  415 => 141,  410 => 140,  407 => 139,  400 => 135,  394 => 134,  386 => 133,  381 => 132,  379 => 131,  376 => 130,  362 => 129,  359 => 128,  352 => 123,  346 => 122,  338 => 116,  335 => 115,  327 => 110,  324 => 109,  316 => 104,  313 => 103,  305 => 98,  302 => 97,  299 => 96,  296 => 95,  293 => 94,  290 => 93,  287 => 92,  284 => 91,  281 => 90,  278 => 89,  275 => 88,  272 => 87,  269 => 86,  266 => 85,  263 => 84,  259 => 83,  251 => 79,  248 => 78,  244 => 76,  238 => 74,  230 => 72,  228 => 71,  222 => 69,  220 => 68,  216 => 67,  210 => 66,  204 => 62,  198 => 59,  192 => 57,  190 => 56,  187 => 55,  183 => 53,  177 => 51,  171 => 48,  166 => 47,  164 => 46,  158 => 44,  156 => 43,  153 => 42,  143 => 40,  131 => 38,  129 => 37,  123 => 33,  119 => 32,  116 => 31,  114 => 30,  107 => 29,  104 => 28,  101 => 27,  98 => 26,  95 => 25,  92 => 24,  89 => 23,  86 => 22,  80 => 20,  78 => 19,  74 => 18,  70 => 17,  63 => 12,  51 => 9,  47 => 8,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/mpblog/blogs_list.twig", "");
    }
}
