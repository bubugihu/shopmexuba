<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/module/mpbloglatest_grid.twig */
class __TwigTemplate_a6b30bda9fd67e95ab52a5b5b8643d4d1641a4c59c33ec87364b4b33d0a88f95 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div>
  ";
        // line 10
        echo "</div>
<div class=\"inner\" data-color=\"beige\">
  <div class=\"section\">
    <div class=\"section-container\">
      <div class=\"section-inner\">
        <div class=\"quality\">
          <h2 class=\"quality-heading2\"> <img class=\"quality-logo  enter\" alt=\"明日をもっとおいしく 株式会社 明治\" src=\"https://nucos.jp/image/catalog/logo/Logo_nucos_black.png\" style=\"transition-delay: 0ms;\">
            <span class=\"enter\" data-delay=\"20\" style=\"transition-delay: 20ms;\">";
        // line 17
        echo ($context["text_title_blog"] ?? null);
        echo "</span>
          </h2>
          <ul class=\"quality-content-list\" data-layout=\"grid\">
            <li class=\"quality-content  enter\" data-color=\"green\" style=\"transition-delay: 0ms;\">
              <a class=\"quality-content-link\" href=\"";
        // line 21
        echo twig_get_attribute($this->env, $this->source, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["mpblogposts"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[0] ?? null) : null), "href", [], "any", false, false, false, 21);
        echo "\">
                <div class=\"quality-content-img\">
                  <img src=\"";
        // line 23
        echo twig_get_attribute($this->env, $this->source, (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["mpblogposts"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[0] ?? null) : null), "image_order", [], "any", false, false, false, 23);
        echo "\" alt=\"";
        echo twig_get_attribute($this->env, $this->source, (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["mpblogposts"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b[0] ?? null) : null), "name", [], "any", false, false, false, 23);
        echo "\" style=\"border: 1px solid #ffa697;border-radius: 10px 10px 0 0;\">
                </div>
                <div class=\"quality-content-body\">
                  <p class=\"quality-content-ttl\">";
        // line 26
        echo twig_get_attribute($this->env, $this->source, (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = ($context["mpblogposts"] ?? null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002[0] ?? null) : null), "name", [], "any", false, false, false, 26);
        echo "</p>
                  <p class=\"quality-content-txt\">";
        // line 27
        echo twig_get_attribute($this->env, $this->source, (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = ($context["mpblogposts"] ?? null)) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4[0] ?? null) : null), "sdescription", [], "any", false, false, false, 27);
        echo "</p>
                </div>
              </a>
            </li>
            <li class=\"quality-content  enter\" data-color=\"red\" style=\"transition-delay: 0ms;\">
              <a class=\"quality-content-link\" href=\"";
        // line 32
        echo twig_get_attribute($this->env, $this->source, (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = ($context["mpblogposts"] ?? null)) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666[1] ?? null) : null), "href", [], "any", false, false, false, 32);
        echo "\">
                <div class=\"quality-content-img\">
                  <img src=\"";
        // line 34
        echo twig_get_attribute($this->env, $this->source, (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = ($context["mpblogposts"] ?? null)) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e[1] ?? null) : null), "image_order", [], "any", false, false, false, 34);
        echo "\" alt=\"";
        echo twig_get_attribute($this->env, $this->source, (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = ($context["mpblogposts"] ?? null)) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52[1] ?? null) : null), "name", [], "any", false, false, false, 34);
        echo "\" style=\"border: 1px solid #ffa697;border-radius: 10px 10px 0 0;\">
                </div>
                <div class=\"quality-content-body\">
                  <p class=\"quality-content-ttl\">";
        // line 37
        echo twig_get_attribute($this->env, $this->source, (($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = ($context["mpblogposts"] ?? null)) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136[1] ?? null) : null), "name", [], "any", false, false, false, 37);
        echo "</p>
                  <p class=\"quality-content-txt\">";
        // line 38
        echo twig_get_attribute($this->env, $this->source, (($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = ($context["mpblogposts"] ?? null)) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386[1] ?? null) : null), "sdescription", [], "any", false, false, false, 38);
        echo "</p>
                </div>
              </a>
            </li>
            <li class=\"quality-content  enter\" data-color=\"pink\" style=\"transition-delay: 0ms;\">
              <a class=\"quality-content-link\" href=\"";
        // line 43
        echo twig_get_attribute($this->env, $this->source, (($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = ($context["mpblogposts"] ?? null)) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9[2] ?? null) : null), "href", [], "any", false, false, false, 43);
        echo "\">
                <div class=\"quality-content-img\">
                  <img src=\"";
        // line 45
        echo twig_get_attribute($this->env, $this->source, (($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = ($context["mpblogposts"] ?? null)) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae[2] ?? null) : null), "image_order", [], "any", false, false, false, 45);
        echo "\" alt=\"";
        echo twig_get_attribute($this->env, $this->source, (($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = ($context["mpblogposts"] ?? null)) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f[2] ?? null) : null), "name", [], "any", false, false, false, 45);
        echo "\" style=\"border: 1px solid #AAAAAA;border-radius: 10px 10px 0 0;\">
                </div>
                <div class=\"quality-content-body\">
                  <p class=\"quality-content-ttl\">";
        // line 48
        echo twig_get_attribute($this->env, $this->source, (($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = ($context["mpblogposts"] ?? null)) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40[2] ?? null) : null), "name", [], "any", false, false, false, 48);
        echo "</p>
                  <p class=\"quality-content-txt\">";
        // line 49
        echo twig_get_attribute($this->env, $this->source, (($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f = ($context["mpblogposts"] ?? null)) && is_array($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f) || $__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f instanceof ArrayAccess ? ($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f[2] ?? null) : null), "sdescription", [], "any", false, false, false, 49);
        echo "</p>
                </div>
              </a>
            </li>
            <li class=\"quality-content  enter\" data-color=\"orange\" style=\"transition-delay: 170ms;\">
              <a class=\"quality-content-link\" href=\"";
        // line 54
        echo twig_get_attribute($this->env, $this->source, (($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 = ($context["mpblogposts"] ?? null)) && is_array($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760) || $__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 instanceof ArrayAccess ? ($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760[3] ?? null) : null), "href", [], "any", false, false, false, 54);
        echo "\">
                <div class=\"quality-content-img\">
                  <img src=\"";
        // line 56
        echo twig_get_attribute($this->env, $this->source, (($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce = ($context["mpblogposts"] ?? null)) && is_array($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce) || $__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce instanceof ArrayAccess ? ($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce[3] ?? null) : null), "image_order", [], "any", false, false, false, 56);
        echo "\" alt=\"";
        echo twig_get_attribute($this->env, $this->source, (($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b = ($context["mpblogposts"] ?? null)) && is_array($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b) || $__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b instanceof ArrayAccess ? ($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b[3] ?? null) : null), "name", [], "any", false, false, false, 56);
        echo "\" style=\"border: 1px solid #AAAAAA;border-radius: 10px 10px 0 0;\">
                </div>
                <div class=\"quality-content-body\">
                  <p class=\"quality-content-ttl\">";
        // line 59
        echo twig_get_attribute($this->env, $this->source, (($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c = ($context["mpblogposts"] ?? null)) && is_array($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c) || $__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c instanceof ArrayAccess ? ($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c[3] ?? null) : null), "name", [], "any", false, false, false, 59);
        echo "</p>
                  <p class=\"quality-content-txt\">";
        // line 60
        echo twig_get_attribute($this->env, $this->source, (($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 = ($context["mpblogposts"] ?? null)) && is_array($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972) || $__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 instanceof ArrayAccess ? ($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972[3] ?? null) : null), "sdescription", [], "any", false, false, false, 60);
        echo "</p>
                </div>
              </a>
            </li>
            <li class=\"quality-content  enter\" data-color=\"gray\" style=\"transition-delay: 170ms;\">
              <a class=\"quality-content-link\" href=\"";
        // line 65
        echo twig_get_attribute($this->env, $this->source, (($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 = ($context["mpblogposts"] ?? null)) && is_array($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216) || $__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 instanceof ArrayAccess ? ($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216[4] ?? null) : null), "href", [], "any", false, false, false, 65);
        echo "\">
                <div class=\"quality-content-img\">
                  <img src=\"";
        // line 67
        echo twig_get_attribute($this->env, $this->source, (($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 = ($context["mpblogposts"] ?? null)) && is_array($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0) || $__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 instanceof ArrayAccess ? ($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0[4] ?? null) : null), "image_order", [], "any", false, false, false, 67);
        echo "\" alt=\"";
        echo twig_get_attribute($this->env, $this->source, (($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c = ($context["mpblogposts"] ?? null)) && is_array($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c) || $__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c instanceof ArrayAccess ? ($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c[4] ?? null) : null), "name", [], "any", false, false, false, 67);
        echo "\" style=\"border: 1px solid #AAAAAA;border-radius: 10px 10px 0 0;\">
                </div>
                <div class=\"quality-content-body\">
                  <p class=\"quality-content-ttl\">";
        // line 70
        echo twig_get_attribute($this->env, $this->source, (($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f = ($context["mpblogposts"] ?? null)) && is_array($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f) || $__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f instanceof ArrayAccess ? ($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f[4] ?? null) : null), "name", [], "any", false, false, false, 70);
        echo "</p>
                  <p class=\"quality-content-txt\">";
        // line 71
        echo twig_get_attribute($this->env, $this->source, (($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc = ($context["mpblogposts"] ?? null)) && is_array($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc) || $__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc instanceof ArrayAccess ? ($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc[4] ?? null) : null), "sdescription", [], "any", false, false, false, 71);
        echo "</p>
                </div>
              </a>
            </li>
            <li class=\"quality-content enter\" data-color=\"blue\" style=\"transition-delay: 170ms;\">
              <a class=\"quality-content-link\" href=\"";
        // line 76
        echo twig_get_attribute($this->env, $this->source, (($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 = ($context["mpblogposts"] ?? null)) && is_array($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55) || $__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 instanceof ArrayAccess ? ($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55[5] ?? null) : null), "href", [], "any", false, false, false, 76);
        echo "\">
                <div class=\"quality-content-img\">
                  <img src=\"";
        // line 78
        echo twig_get_attribute($this->env, $this->source, (($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba = ($context["mpblogposts"] ?? null)) && is_array($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba) || $__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba instanceof ArrayAccess ? ($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba[5] ?? null) : null), "image_order", [], "any", false, false, false, 78);
        echo "\" alt=\"";
        echo twig_get_attribute($this->env, $this->source, (($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 = ($context["mpblogposts"] ?? null)) && is_array($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78) || $__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 instanceof ArrayAccess ? ($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78[5] ?? null) : null), "name", [], "any", false, false, false, 78);
        echo "\" style=\"border: 1px solid #AAAAAA;border-radius: 10px 10px 0 0;\">
                </div>
                <div class=\"quality-content-body\">
                  <p class=\"quality-content-ttl\">";
        // line 81
        echo twig_get_attribute($this->env, $this->source, (($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de = ($context["mpblogposts"] ?? null)) && is_array($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de) || $__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de instanceof ArrayAccess ? ($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de[5] ?? null) : null), "name", [], "any", false, false, false, 81);
        echo "</p>
                  <p class=\"quality-content-txt\">";
        // line 82
        echo twig_get_attribute($this->env, $this->source, (($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 = ($context["mpblogposts"] ?? null)) && is_array($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828) || $__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 instanceof ArrayAccess ? ($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828[5] ?? null) : null), "sdescription", [], "any", false, false, false, 82);
        echo "</p>
                </div>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!--<div class=\"section\">
            <div class=\"section-container\">
              <div class=\"section-inner\">
                <div class=\"cm-gallery\">
                  <div class=\"heading-container\">
                    <div class=\"heading\">
                      <h2 class=\"heading2\" data-delay=\"80\">CMギャラリー</h2>
                    </div>
                    <div class=\"heading-btn\">
                      <a href=\"/cm/\" class=\"m-txtLink-strong\"><strong>一覧へ</strong></a></div>
                  </div>
                  <div class=\"cm-gallery-content\">
                    <svg class=\"cm-gallery-hane\" width=\"142\" height=\"14\" focusable=\"false\">
                      <path d=\"M141.87 13.79C62.27 11.81 13 .94 13 .94 3.38-1.51.57.93 0 7.01v6.79h141.87z\"></path>
                    </svg>
                    <div class=\"cm-gallery-body\">
                      <p class=\"cm-gallery-ttl\">ミルクチョコレート</p>
                      <a href=\"https://www.meiji.co.jp/products/brand/mchoco/\" class=\"cm-gallery-link\">「チョコレート鉄道の旅」篇 30秒</a>
                      <a href=\"https://www.meiji.co.jp/products/brand/mchoco/\" class=\"cm-gallery-btn\"><span class=\"cm-gallery-btn-txt\">ブランドサイトへ</span></a>
                    </div>
                    <div class=\"cm-gallery-image\" style=\"position: relative;\"><img src=\"/assets/js/spacer.png\" alt=\"\" aria-hidden=\"true\" oncontextmenu=\"return false;\" onselectstart=\"return false;\" onmousedown=\"return false;\" style=\"position: absolute; left: 0; top: 0; width: 100%; height: 100%; border: 0; z-index: 1\">
                      <img src=\"catalog/view/theme/default/stylesheet/cm/img/mchoco_rail_journey_30s.jpg\" class=\"js-click-cancel  enter\" alt=\"写真：「チョコレート鉄道の旅」篇 30秒 のサムネイル\" style=\"transition-delay: 0ms;\">
                    </div>
                  </div>
                </div>
              </div>
            </div>-->
</div>
<div class=\"_container\" data-color=\"beige\">
  <div class=\"l-content-decoration\">
    <svg xmlns=\"http://www.w3.org/2000/svg\" class=\"m-content-decoration-lg\" width=\"1600\" height=\"80\" viewBox=\"0 0 1600 80\" focusable=\"false\"> 
      <path class=\"st0\" d=\"M1600,46.81L1600,46.81c-134.1-35.88-251.25,3.45-363,10.14c-133,7.96-249,5.95-249,5.95c-139,2.01-241-17.1-241-17.1C454-5.48,320,0.19,320,0.19C180.82,0.19,49.69,31.73,0,45.22V80h1600V46.81z\"></path> 
    </svg>
    <svg xmlns=\"http://www.w3.org/2000/svg\" class=\"m-content-decoration-sm\" width=\"750\" height=\"70\" viewBox=\"0 0 750 70\" focusable=\"false\"> 
      <path d=\"M750,45.75c-11.74,3.84-44.82,13.16-70.68,17.34c-68.65,11.09-172.13,2.6-172.13,2.6C409,59,311.35,34.18,311.35,34.18C151-7,13.33,0.63,13.33,0.63C8.93,0.71,4.48,0.93,0,1.26V80h750V45.75z\"></path> 
    </svg>
  </div>
</div>
</div>";
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/mpbloglatest_grid.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  198 => 82,  194 => 81,  186 => 78,  181 => 76,  173 => 71,  169 => 70,  161 => 67,  156 => 65,  148 => 60,  144 => 59,  136 => 56,  131 => 54,  123 => 49,  119 => 48,  111 => 45,  106 => 43,  98 => 38,  94 => 37,  86 => 34,  81 => 32,  73 => 27,  69 => 26,  61 => 23,  56 => 21,  49 => 17,  40 => 10,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/extension/module/mpbloglatest_grid.twig", "");
    }
}
