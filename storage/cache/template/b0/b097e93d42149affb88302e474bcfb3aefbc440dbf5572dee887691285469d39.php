<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/module/mpblogcategory.twig */
class __TwigTemplate_9aa6e2411996cd2f40c81f8f29d43dfb67498039653ee86a8085d5bc695e1940 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"mp-list-group\">
<h1>";
        // line 2
        echo ($context["heading_title_category"] ?? null);
        echo "</h1>
  ";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["mpblogcategories"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["mpblogcategory"]) {
            // line 4
            echo "  ";
            if ((twig_get_attribute($this->env, $this->source, $context["mpblogcategory"], "mpblogcategory_id", [], "any", false, false, false, 4) == ($context["mpblogcategory_id"] ?? null))) {
                // line 5
                echo "  <a href=\"";
                echo twig_get_attribute($this->env, $this->source, $context["mpblogcategory"], "href", [], "any", false, false, false, 5);
                echo "\" class=\"list-group-item active\"><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i> ";
                echo twig_get_attribute($this->env, $this->source, $context["mpblogcategory"], "name", [], "any", false, false, false, 5);
                echo "</a>
  ";
                // line 6
                if (twig_get_attribute($this->env, $this->source, $context["mpblogcategory"], "children", [], "any", false, false, false, 6)) {
                    // line 7
                    echo "  ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["mpblogcategory"], "children", [], "any", false, false, false, 7));
                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                        // line 8
                        echo "  ";
                        if ((twig_get_attribute($this->env, $this->source, $context["child"], "mpblogcategory_id", [], "any", false, false, false, 8) == ($context["child_id"] ?? null))) {
                            // line 9
                            echo "  <a href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 9);
                            echo "\" class=\"list-group-item active\">&nbsp;&nbsp;&nbsp;- ";
                            echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 9);
                            echo "</a>
  ";
                        } else {
                            // line 11
                            echo "  <a href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 11);
                            echo "\" class=\"list-group-item\">&nbsp;&nbsp;&nbsp;- ";
                            echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 11);
                            echo "</a>
  ";
                        }
                        // line 13
                        echo "  ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 14
                    echo "  ";
                }
                // line 15
                echo "  ";
            } else {
                // line 16
                echo "  <a href=\"";
                echo twig_get_attribute($this->env, $this->source, $context["mpblogcategory"], "href", [], "any", false, false, false, 16);
                echo "\" class=\"list-group-item\"><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i> ";
                echo twig_get_attribute($this->env, $this->source, $context["mpblogcategory"], "name", [], "any", false, false, false, 16);
                echo "</a>
  ";
            }
            // line 18
            echo "  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mpblogcategory'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/mpblogcategory.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 19,  104 => 18,  96 => 16,  93 => 15,  90 => 14,  84 => 13,  76 => 11,  68 => 9,  65 => 8,  60 => 7,  58 => 6,  51 => 5,  48 => 4,  44 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/extension/module/mpblogcategory.twig", "");
    }
}
