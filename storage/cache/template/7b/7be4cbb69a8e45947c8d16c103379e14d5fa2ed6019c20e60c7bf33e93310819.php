<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/checkout/checkout.twig */
class __TwigTemplate_e943bc70d49d43129f47152126de228ee1d051917f707b265a1c98ba12cdd4f7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div style=\"background-color: #DBF9FB\">
  <div class=\"l-breadcrumb\" style=\"margin-bottom: 20px;\"> 
    <div class=\"l-breadcrumb-container\"> 
      <div class=\"l-breadcrumb-inner\"> 
        <nav role=\"navigation\" aria-label=\"現在位置\"> 
          <ol class=\"m-breadcrumb\" itemscope=\"itemscope\" itemtype=\"http://schema.org/BreadcrumbList\"> 
            ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 9
            echo "            <li itemprop=\"itemListElement\" itemscope=\"itemscope\" itemtype=\"http://schema.org/ListItem\"><a style=\"text-transform: uppercase;\" href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 9);
            echo "\" class=\"m-breadcrumb-item\" itemprop=\"item\"><span style=\"color: #fff; font-size: 18px;\" itemprop=\"name\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 9);
            echo "</span></a>
              <meta itemprop=\"position\" content=\"1\" style=\"color: #fff; font-size: 18px;\"> </li> 
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "          </ol> 
        </nav> 
      </div> 
    </div> 
  </div>
  <div id=\"checkout-checkout\" class=\"container\" style=\"padding-bottom: 117px; padding-top: 42px;\">
    ";
        // line 18
        if (($context["error_warning"] ?? null)) {
            // line 19
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 23
        echo "    <div class=\"row\">";
        echo ($context["column_left"] ?? null);
        echo "
      ";
        // line 24
        if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
            // line 25
            echo "      ";
            $context["class"] = "col-sm-6";
            // line 26
            echo "      ";
        } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 27
            echo "      ";
            $context["class"] = "col-sm-9";
            // line 28
            echo "      ";
        } else {
            // line 29
            echo "      ";
            $context["class"] = "col-sm-12";
            // line 30
            echo "      ";
        }
        // line 31
        echo "      <div id=\"content\" class=\"";
        echo ($context["class"] ?? null);
        echo "\">";
        echo ($context["content_top"] ?? null);
        echo "
        <h1>";
        // line 32
        echo ($context["heading_title"] ?? null);
        echo "</h1>
        <div class=\"panel-group\" id=\"accordion\">
          <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
              <h4 class=\"panel-title\">";
        // line 36
        echo ($context["text_checkout_option"] ?? null);
        echo "</h4>
            </div>
            <div class=\"panel-collapse collapse\" id=\"collapse-checkout-option\">
              <div class=\"panel-body\"></div>
            </div>
          </div>
          ";
        // line 42
        if (( !($context["logged"] ?? null) && (($context["account"] ?? null) != "guest"))) {
            // line 43
            echo "          <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
              <h4 class=\"panel-title\">";
            // line 45
            echo ($context["text_checkout_account"] ?? null);
            echo "</h4>
            </div>
            <div class=\"panel-collapse collapse\" id=\"collapse-payment-address\">
              <div class=\"panel-body\"></div>
            </div>
          </div>
          ";
        } else {
            // line 52
            echo "          <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
              <h4 class=\"panel-title\">";
            // line 54
            echo ($context["text_checkout_payment_address"] ?? null);
            echo "</h4>
            </div>
            <div class=\"panel-collapse collapse\" id=\"collapse-payment-address\">
              <div class=\"panel-body\"></div>
            </div>
          </div>
          ";
        }
        // line 61
        echo "          ";
        if (($context["shipping_required"] ?? null)) {
            // line 62
            echo "          <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
              <h4 class=\"panel-title\">";
            // line 64
            echo ($context["text_checkout_shipping_address"] ?? null);
            echo "</h4>
            </div>
            <div class=\"panel-collapse collapse\" id=\"collapse-shipping-address\">
              <div class=\"panel-body\"></div>
            </div>
          </div>
          <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
              <h4 class=\"panel-title\">";
            // line 72
            echo ($context["text_checkout_shipping_method"] ?? null);
            echo "</h4>
            </div>
            <div class=\"panel-collapse collapse\" id=\"collapse-shipping-method\">
              <div class=\"panel-body\"></div>
            </div>
          </div>
          ";
        }
        // line 79
        echo "          <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
              <h4 class=\"panel-title\">";
        // line 81
        echo ($context["text_checkout_payment_method"] ?? null);
        echo "</h4>
            </div>
            <div class=\"panel-collapse collapse\" id=\"collapse-payment-method\">
              <div class=\"panel-body\"></div>
            </div>
          </div>
          <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
              <h4 class=\"panel-title\">";
        // line 89
        echo ($context["text_checkout_confirm"] ?? null);
        echo "</h4>
            </div>
            <div class=\"panel-collapse collapse\" id=\"collapse-checkout-confirm\">
              <div class=\"panel-body\"></div>
            </div>
          </div>
        </div>
        ";
        // line 96
        echo ($context["content_bottom"] ?? null);
        echo "</div>
      ";
        // line 97
        echo ($context["column_right"] ?? null);
        echo "</div>
  </div>
</div>
<script type=\"text/javascript\"><!--
\$(document).on('change', 'input[name=\\'account\\']', function() {
 if (\$('#collapse-payment-address').parent().find('.panel-heading .panel-title > *').is('a')) {
  if (this.value == 'register') {
   \$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href=\"#collapse-payment-address\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
        // line 104
        echo ($context["text_checkout_account"] ?? null);
        echo " <i class=\"fa fa-caret-down\"></i></a>');
  } else {
   \$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href=\"#collapse-payment-address\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
        // line 106
        echo ($context["text_checkout_payment_address"] ?? null);
        echo " <i class=\"fa fa-caret-down\"></i></a>');
  }
 } else {
  if (this.value == 'register') {
   \$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('";
        // line 110
        echo ($context["text_checkout_account"] ?? null);
        echo "');
  } else {
   \$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('";
        // line 112
        echo ($context["text_checkout_payment_address"] ?? null);
        echo "');
  }
 }
});

";
        // line 117
        if ( !($context["logged"] ?? null)) {
            // line 118
            echo "\$(document).ready(function() {
    \$.ajax({
        url: 'index.php?route=checkout/login',
        dataType: 'html',
        success: function(html) {
           \$('#collapse-checkout-option .panel-body').html(html);

   \$('#collapse-checkout-option').parent().find('.panel-heading .panel-title').html('<a href=\"#collapse-checkout-option\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 125
            echo ($context["text_checkout_option"] ?? null);
            echo " <i class=\"fa fa-caret-down\"></i></a>');

   \$('a[href=\\'#collapse-checkout-option\\']').trigger('click');
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
    });
});
";
        } else {
            // line 135
            echo "\$(document).ready(function() {
    \$.ajax({
        url: 'index.php?route=checkout/payment_address',
        dataType: 'html',
        success: function(html) {
            \$('#collapse-payment-address .panel-body').html(html);

   \$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href=\"#collapse-payment-address\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 142
            echo ($context["text_checkout_payment_address"] ?? null);
            echo " <i class=\"fa fa-caret-down\"></i></a>');

   \$('a[href=\\'#collapse-payment-address\\']').trigger('click');
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
    });
});
";
        }
        // line 152
        echo "
// Checkout
\$(document).delegate('#button-account', 'click', function() {
    \$.ajax({
        url: 'index.php?route=checkout/' + \$('input[name=\\'account\\']:checked').val(),
        dataType: 'html',
        beforeSend: function() {
         \$('#button-account').button('loading');
  },
        complete: function() {
   \$('#button-account').button('reset');
        },
        success: function(html) {
            \$('.alert-dismissible, .text-danger').remove();
   \$('.form-group').removeClass('has-error');

            \$('#collapse-payment-address .panel-body').html(html);

   if (\$('input[name=\\'account\\']:checked').val() == 'register') {
    \$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href=\"#collapse-payment-address\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
        // line 171
        echo ($context["text_checkout_account"] ?? null);
        echo " <i class=\"fa fa-caret-down\"></i></a>');
   } else {
    \$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href=\"#collapse-payment-address\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
        // line 173
        echo ($context["text_checkout_payment_address"] ?? null);
        echo " <i class=\"fa fa-caret-down\"></i></a>');
   }

   \$('a[href=\\'#collapse-payment-address\\']').trigger('click');
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
    });
});

// Login
\$(document).delegate('#button-login', 'click', function() {
    \$.ajax({
        url: 'index.php?route=checkout/login/save',
        type: 'post',
        data: \$('#collapse-checkout-option :input'),
        dataType: 'json',
        beforeSend: function() {
         \$('#button-login').button('loading');
  },
        complete: function() {
            \$('#button-login').button('reset');
        },
        success: function(json) {
            \$('.alert-dismissible, .text-danger').remove();
            \$('.form-group').removeClass('has-error');

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                \$('#collapse-checkout-option .panel-body').prepend('<div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');

    // Highlight any found errors
    \$('input[name=\\'email\\']').parent().addClass('has-error');
    \$('input[name=\\'password\\']').parent().addClass('has-error');
     }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
    });
});

// Register
\$(document).delegate('#button-register', 'click', function() {
    \$.ajax({
        url: 'index.php?route=checkout/register/save',
        type: 'post',
        data: \$('#collapse-payment-address input[type=\\'text\\'], #collapse-payment-address input[type=\\'date\\'], #collapse-payment-address input[type=\\'datetime-local\\'], #collapse-payment-address input[type=\\'time\\'], #collapse-payment-address input[type=\\'password\\'], #collapse-payment-address input[type=\\'hidden\\'], #collapse-payment-address input[type=\\'checkbox\\']:checked, #collapse-payment-address input[type=\\'radio\\']:checked, #collapse-payment-address textarea, #collapse-payment-address select'),
        dataType: 'json',
        beforeSend: function() {
   \$('#button-register').button('loading');
  },
        success: function(json) {
            \$('.alert-dismissible, .text-danger').remove();
            \$('.form-group').removeClass('has-error');

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                \$('#button-register').button('reset');

                if (json['error']['warning']) {
                    \$('#collapse-payment-address .panel-body').prepend('<div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                }

    for (i in json['error']) {
     var element = \$('#input-payment-' + i.replace('_', '-'));

     if (\$(element).parent().hasClass('input-group')) {
      \$(element).parent().after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
     } else {
      \$(element).after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
     }
    }

    // Highlight any found errors
    \$('.text-danger').parent().addClass('has-error');
            } else {
                ";
        // line 253
        if (($context["shipping_required"] ?? null)) {
            // line 254
            echo "                var shipping_address = \$('#payment-address input[name=\\'shipping_address\\']:checked').prop('value');

                if (shipping_address) {
                    \$.ajax({
                        url: 'index.php?route=checkout/shipping_method',
                        dataType: 'html',
                        success: function(html) {
       // Add the shipping address
                            \$.ajax({
                                url: 'index.php?route=checkout/shipping_address',
                                dataType: 'html',
                                success: function(html) {
                                    \$('#collapse-shipping-address .panel-body').html(html);

         \$('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href=\"#collapse-shipping-address\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 268
            echo ($context["text_checkout_shipping_address"] ?? null);
            echo " <i class=\"fa fa-caret-down\"></i></a>');
                                },
                                error: function(xhr, ajaxOptions, thrownError) {
                                    alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                                }
                            });

       \$('#collapse-shipping-method .panel-body').html(html);

       \$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href=\"#collapse-shipping-method\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 277
            echo ($context["text_checkout_shipping_method"] ?? null);
            echo " <i class=\"fa fa-caret-down\"></i></a>');

          \$('a[href=\\'#collapse-shipping-method\\']').trigger('click');

       \$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('";
            // line 281
            echo ($context["text_checkout_shipping_method"] ?? null);
            echo "');
       \$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('";
            // line 282
            echo ($context["text_checkout_payment_method"] ?? null);
            echo "');
       \$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('";
            // line 283
            echo ($context["text_checkout_confirm"] ?? null);
            echo "');
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                        }
                    });
                } else {
                    \$.ajax({
                        url: 'index.php?route=checkout/shipping_address',
                        dataType: 'html',
                        success: function(html) {
                            \$('#collapse-shipping-address .panel-body').html(html);

       \$('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href=\"#collapse-shipping-address\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 296
            echo ($context["text_checkout_shipping_address"] ?? null);
            echo " <i class=\"fa fa-caret-down\"></i></a>');

       \$('a[href=\\'#collapse-shipping-address\\']').trigger('click');

       \$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('";
            // line 300
            echo ($context["text_checkout_shipping_method"] ?? null);
            echo "');
       \$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('";
            // line 301
            echo ($context["text_checkout_payment_method"] ?? null);
            echo "');
       \$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('";
            // line 302
            echo ($context["text_checkout_confirm"] ?? null);
            echo "');
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                        }
                    });
                }
                ";
        } else {
            // line 310
            echo "                \$.ajax({
                    url: 'index.php?route=checkout/payment_method',
                    dataType: 'html',
                    success: function(html) {
                        \$('#collapse-payment-method .panel-body').html(html);

      \$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href=\"#collapse-payment-method\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 316
            echo ($context["text_checkout_payment_method"] ?? null);
            echo " <i class=\"fa fa-caret-down\"></i></a>');

      \$('a[href=\\'#collapse-payment-method\\']').trigger('click');

      \$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('";
            // line 320
            echo ($context["text_checkout_confirm"] ?? null);
            echo "');
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                    }
                });
                ";
        }
        // line 327
        echo "
                \$.ajax({
                    url: 'index.php?route=checkout/payment_address',
                    dataType: 'html',
                    complete: function() {
                        \$('#button-register').button('reset');
                    },
                    success: function(html) {
                        \$('#collapse-payment-address .panel-body').html(html);

      \$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href=\"#collapse-payment-address\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
        // line 337
        echo ($context["text_checkout_payment_address"] ?? null);
        echo " <i class=\"fa fa-caret-down\"></i></a>');
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                    }
                });
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
    });
});

// Payment Address
\$(document).delegate('#button-payment-address', 'click', function() {
    \$.ajax({
        url: 'index.php?route=checkout/payment_address/save',
        type: 'post',
        data: \$('#collapse-payment-address input[type=\\'text\\'], #collapse-payment-address input[type=\\'date\\'], #collapse-payment-address input[type=\\'datetime-local\\'], #collapse-payment-address input[type=\\'time\\'], #collapse-payment-address input[type=\\'password\\'], #collapse-payment-address input[type=\\'checkbox\\']:checked, #collapse-payment-address input[type=\\'radio\\']:checked, #collapse-payment-address input[type=\\'hidden\\'], #collapse-payment-address textarea, #collapse-payment-address select'),
        dataType: 'json',
        beforeSend: function() {
         \$('#button-payment-address').button('loading');
  },
        complete: function() {
   \$('#button-payment-address').button('reset');
        },
        success: function(json) {
            \$('.alert-dismissible, .text-danger').remove();
   \$('.form-group').removeClass('has-error');

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                if (json['error']['warning']) {
                    \$('#collapse-payment-address .panel-body').prepend('<div class=\"alert alert-warning alert-dismissible\">' + json['error']['warning'] + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                }

    for (i in json['error']) {
     var element = \$('#input-payment-' + i.replace('_', '-'));

     if (\$(element).parent().hasClass('input-group')) {
      \$(element).parent().after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
     } else {
      \$(element).after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
     }
    }

    // Highlight any found errors
    \$('.text-danger').parent().parent().addClass('has-error');
            } else {
                ";
        // line 388
        if (($context["shipping_required"] ?? null)) {
            // line 389
            echo "                \$.ajax({
                    url: 'index.php?route=checkout/shipping_address',
                    dataType: 'html',
                    success: function(html) {
                        \$('#collapse-shipping-address .panel-body').html(html);

      \$('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href=\"#collapse-shipping-address\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 395
            echo ($context["text_checkout_shipping_address"] ?? null);
            echo " <i class=\"fa fa-caret-down\"></i></a>');

      \$('a[href=\\'#collapse-shipping-address\\']').trigger('click');

      \$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('";
            // line 399
            echo ($context["text_checkout_shipping_method"] ?? null);
            echo "');
      \$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('";
            // line 400
            echo ($context["text_checkout_payment_method"] ?? null);
            echo "');
      \$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('";
            // line 401
            echo ($context["text_checkout_confirm"] ?? null);
            echo "');
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                    }
                }).done(function() {
     \$.ajax({
      url: 'index.php?route=checkout/payment_address',
      dataType: 'html',
      success: function(html) {
       \$('#collapse-payment-address .panel-body').html(html);
      },
      error: function(xhr, ajaxOptions, thrownError) {
       alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
      }
     });
    });
                ";
        } else {
            // line 419
            echo "                \$.ajax({
                    url: 'index.php?route=checkout/payment_method',
                    dataType: 'html',
                    success: function(html) {
                        \$('#collapse-payment-method .panel-body').html(html);

      \$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href=\"#collapse-payment-method\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 425
            echo ($context["text_checkout_payment_method"] ?? null);
            echo " <i class=\"fa fa-caret-down\"></i></a>');

      \$('a[href=\\'#collapse-payment-method\\']').trigger('click');

      \$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('";
            // line 429
            echo ($context["text_checkout_confirm"] ?? null);
            echo "');
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                    }
                }).done(function() {
     \$.ajax({
      url: 'index.php?route=checkout/payment_address',
      dataType: 'html',
      success: function(html) {
       \$('#collapse-payment-address .panel-body').html(html);
      },
      error: function(xhr, ajaxOptions, thrownError) {
       alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
      }
     });\t\t\t\t
    });
                ";
        }
        // line 447
        echo "            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
    });
});

// Shipping Address
\$(document).delegate('#button-shipping-address', 'click', function() {
    \$.ajax({
        url: 'index.php?route=checkout/shipping_address/save',
        type: 'post',
        data: \$('#collapse-shipping-address input[type=\\'text\\'], #collapse-shipping-address input[type=\\'date\\'], #collapse-shipping-address input[type=\\'datetime-local\\'], #collapse-shipping-address input[type=\\'time\\'], #collapse-shipping-address input[type=\\'password\\'], #collapse-shipping-address input[type=\\'checkbox\\']:checked, #collapse-shipping-address input[type=\\'radio\\']:checked, #collapse-shipping-address textarea, #collapse-shipping-address select'),
        dataType: 'json',
        beforeSend: function() {
   \$('#button-shipping-address').button('loading');
     },
        success: function(json) {
            \$('.alert-dismissible, .text-danger').remove();
   \$('.form-group').removeClass('has-error');

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                \$('#button-shipping-address').button('reset');

                if (json['error']['warning']) {
                    \$('#collapse-shipping-address .panel-body').prepend('<div class=\"alert alert-warning alert-dismissible\">' + json['error']['warning'] + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                }

    for (i in json['error']) {
     var element = \$('#input-shipping-' + i.replace('_', '-'));

     if (\$(element).parent().hasClass('input-group')) {
      \$(element).parent().after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
     } else {
      \$(element).after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
     }
    }

    // Highlight any found errors
    \$('.text-danger').parent().parent().addClass('has-error');
            } else {
                \$.ajax({
                    url: 'index.php?route=checkout/shipping_method',
                    dataType: 'html',
                    complete: function() {
                        \$('#button-shipping-address').button('reset');
                    },
                    success: function(html) {
                        \$('#collapse-shipping-method .panel-body').html(html);

      \$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href=\"#collapse-shipping-method\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
        // line 500
        echo ($context["text_checkout_shipping_method"] ?? null);
        echo " <i class=\"fa fa-caret-down\"></i></a>');

      \$('a[href=\\'#collapse-shipping-method\\']').trigger('click');

      \$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('";
        // line 504
        echo ($context["text_checkout_payment_method"] ?? null);
        echo "');
      \$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('";
        // line 505
        echo ($context["text_checkout_confirm"] ?? null);
        echo "');

                        \$.ajax({
                            url: 'index.php?route=checkout/shipping_address',
                            dataType: 'html',
                            success: function(html) {
                                \$('#collapse-shipping-address .panel-body').html(html);
                            },
                            error: function(xhr, ajaxOptions, thrownError) {
                                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                            }
                        });
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                    }
                }).done(function() {
     \$.ajax({
      url: 'index.php?route=checkout/payment_address',
      dataType: 'html',
      success: function(html) {
       \$('#collapse-payment-address .panel-body').html(html);
      },
      error: function(xhr, ajaxOptions, thrownError) {
       alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
      }
     });
    });
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
    });
});

// Guest
\$(document).delegate('#button-guest', 'click', function() {
    \$.ajax({
        url: 'index.php?route=checkout/guest/save',
        type: 'post',
        data: \$('#collapse-payment-address input[type=\\'text\\'], #collapse-payment-address input[type=\\'date\\'], #collapse-payment-address input[type=\\'datetime-local\\'], #collapse-payment-address input[type=\\'time\\'], #collapse-payment-address input[type=\\'checkbox\\']:checked, #collapse-payment-address input[type=\\'radio\\']:checked, #collapse-payment-address input[type=\\'hidden\\'], #collapse-payment-address textarea, #collapse-payment-address select'),
        dataType: 'json',
        beforeSend: function() {
         \$('#button-guest').button('loading');
     },
        success: function(json) {
            \$('.alert-dismissible, .text-danger').remove();
   \$('.form-group').removeClass('has-error');

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                \$('#button-guest').button('reset');

                if (json['error']['warning']) {
                    \$('#collapse-payment-address .panel-body').prepend('<div class=\"alert alert-warning alert-dismissible\">' + json['error']['warning'] + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                }

    for (i in json['error']) {
     var element = \$('#input-payment-' + i.replace('_', '-'));

     if (\$(element).parent().hasClass('input-group')) {
      \$(element).parent().after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
     } else {
      \$(element).after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
     }
    }

    // Highlight any found errors
    \$('.text-danger').parent().addClass('has-error');
            } else {
                ";
        // line 577
        if (($context["shipping_required"] ?? null)) {
            // line 578
            echo "                var shipping_address = \$('#collapse-payment-address input[name=\\'shipping_address\\']:checked').prop('value');

                if (shipping_address) {
                    \$.ajax({
                        url: 'index.php?route=checkout/shipping_method',
                        dataType: 'html',
                        complete: function() {
                            \$('#button-guest').button('reset');
                        },
                        success: function(html) {
       // Add the shipping address
                            \$.ajax({
                                url: 'index.php?route=checkout/guest_shipping',
                                dataType: 'html',
                                success: function(html) {
                                    \$('#collapse-shipping-address .panel-body').html(html);

         \$('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href=\"#collapse-shipping-address\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 595
            echo ($context["text_checkout_shipping_address"] ?? null);
            echo " <i class=\"fa fa-caret-down\"></i></a>');
                                },
                                error: function(xhr, ajaxOptions, thrownError) {
                                    alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                                }
                            });

          \$('#collapse-shipping-method .panel-body').html(html);

       \$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href=\"#collapse-shipping-method\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 604
            echo ($context["text_checkout_shipping_method"] ?? null);
            echo " <i class=\"fa fa-caret-down\"></i></a>');

       \$('a[href=\\'#collapse-shipping-method\\']').trigger('click');

       \$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('";
            // line 608
            echo ($context["text_checkout_payment_method"] ?? null);
            echo "');
       \$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('";
            // line 609
            echo ($context["text_checkout_confirm"] ?? null);
            echo "');
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                        }
                    });
                } else {
                    \$.ajax({
                        url: 'index.php?route=checkout/guest_shipping',
                        dataType: 'html',
                        complete: function() {
                            \$('#button-guest').button('reset');
                        },
                        success: function(html) {
                            \$('#collapse-shipping-address .panel-body').html(html);

       \$('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href=\"#collapse-shipping-address\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 625
            echo ($context["text_checkout_shipping_address"] ?? null);
            echo " <i class=\"fa fa-caret-down\"></i></a>');

       \$('a[href=\\'#collapse-shipping-address\\']').trigger('click');

       \$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('";
            // line 629
            echo ($context["text_checkout_shipping_method"] ?? null);
            echo "');
       \$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('";
            // line 630
            echo ($context["text_checkout_payment_method"] ?? null);
            echo "');
       \$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('";
            // line 631
            echo ($context["text_checkout_confirm"] ?? null);
            echo "');
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                        }
                    });
                }
                ";
        } else {
            // line 639
            echo "                \$.ajax({
                    url: 'index.php?route=checkout/payment_method',
                    dataType: 'html',
                    complete: function() {
                        \$('#button-guest').button('reset');
                    },
                    success: function(html) {
                        \$('#collapse-payment-method .panel-body').html(html);

      \$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href=\"#collapse-payment-method\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 648
            echo ($context["text_checkout_payment_method"] ?? null);
            echo " <i class=\"fa fa-caret-down\"></i></a>');

      \$('a[href=\\'#collapse-payment-method\\']').trigger('click');

      \$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('";
            // line 652
            echo ($context["text_checkout_confirm"] ?? null);
            echo "');
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                    }
                });
                ";
        }
        // line 659
        echo "            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
    });
});

// Guest Shipping
\$(document).delegate('#button-guest-shipping', 'click', function() {
    \$.ajax({
        url: 'index.php?route=checkout/guest_shipping/save',
        type: 'post',
        data: \$('#collapse-shipping-address input[type=\\'text\\'], #collapse-shipping-address input[type=\\'date\\'], #collapse-shipping-address input[type=\\'datetime-local\\'], #collapse-shipping-address input[type=\\'time\\'], #collapse-shipping-address input[type=\\'password\\'], #collapse-shipping-address input[type=\\'checkbox\\']:checked, #collapse-shipping-address input[type=\\'radio\\']:checked, #collapse-shipping-address textarea, #collapse-shipping-address select'),
        dataType: 'json',
        beforeSend: function() {
         \$('#button-guest-shipping').button('loading');
  },
        success: function(json) {
            \$('.alert-dismissible, .text-danger').remove();
   \$('.form-group').removeClass('has-error');

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                \$('#button-guest-shipping').button('reset');

                if (json['error']['warning']) {
                    \$('#collapse-shipping-address .panel-body').prepend('<div class=\"alert alert-danger alert-dismissible\">' + json['error']['warning'] + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                }

    for (i in json['error']) {
     var element = \$('#input-shipping-' + i.replace('_', '-'));

     if (\$(element).parent().hasClass('input-group')) {
      \$(element).parent().after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
     } else {
      \$(element).after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
     }
    }

    // Highlight any found errors
    \$('.text-danger').parent().addClass('has-error');
            } else {
                \$.ajax({
                    url: 'index.php?route=checkout/shipping_method',
                    dataType: 'html',
                    complete: function() {
                        \$('#button-guest-shipping').button('reset');
                    },
                    success: function(html) {
                        \$('#collapse-shipping-method .panel-body').html(html);

      \$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href=\"#collapse-shipping-method\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
        // line 712
        echo ($context["text_checkout_shipping_method"] ?? null);
        echo " <i class=\"fa fa-caret-down\"></i>');

      \$('a[href=\\'#collapse-shipping-method\\']').trigger('click');

      \$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('";
        // line 716
        echo ($context["text_checkout_payment_method"] ?? null);
        echo "');
      \$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('";
        // line 717
        echo ($context["text_checkout_confirm"] ?? null);
        echo "');
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                    }
                });
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
    });
});

\$(document).delegate('#button-shipping-method', 'click', function() {
    \$.ajax({
        url: 'index.php?route=checkout/shipping_method/save',
        type: 'post',
        data: \$('#collapse-shipping-method input[type=\\'radio\\']:checked, #collapse-shipping-method textarea'),
        dataType: 'json',
        beforeSend: function() {
         \$('#button-shipping-method').button('loading');
  },
        success: function(json) {
            \$('.alert-dismissible, .text-danger').remove();

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                \$('#button-shipping-method').button('reset');

                if (json['error']['warning']) {
                    \$('#collapse-shipping-method .panel-body').prepend('<div class=\"alert alert-danger alert-dismissible\">' + json['error']['warning'] + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                }
            } else {
                \$.ajax({
                    url: 'index.php?route=checkout/payment_method',
                    dataType: 'html',
                    complete: function() {
                        \$('#button-shipping-method').button('reset');
                    },
                    success: function(html) {
                        \$('#collapse-payment-method .panel-body').html(html);

      \$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href=\"#collapse-payment-method\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
        // line 761
        echo ($context["text_checkout_payment_method"] ?? null);
        echo " <i class=\"fa fa-caret-down\"></i></a>');

      \$('a[href=\\'#collapse-payment-method\\']').trigger('click');

      \$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('";
        // line 765
        echo ($context["text_checkout_confirm"] ?? null);
        echo "');
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                    }
                });
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
    });
});

\$(document).delegate('#button-payment-method', 'click', function() {
    \$.ajax({
        url: 'index.php?route=checkout/payment_method/save',
        type: 'post',
        data: \$('#collapse-payment-method input[type=\\'radio\\']:checked, #collapse-payment-method input[type=\\'checkbox\\']:checked, #collapse-payment-method textarea'),
        dataType: 'json',
        beforeSend: function() {
          \$('#button-payment-method').button('loading');
  },
        success: function(json) {
            \$('.alert-dismissible, .text-danger').remove();

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                \$('#button-payment-method').button('reset');

                if (json['error']['warning']) {
                    \$('#collapse-payment-method .panel-body').prepend('<div class=\"alert alert-danger alert-dismissible\">' + json['error']['warning'] + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                }
            } else {
                \$.ajax({
                    url: 'index.php?route=checkout/confirm',
                    dataType: 'html',
                    complete: function() {
                        \$('#button-payment-method').button('reset');
                    },
                    success: function(html) {
                        \$('#collapse-checkout-confirm .panel-body').html(html);

      \$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<a href=\"#collapse-checkout-confirm\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
        // line 809
        echo ($context["text_checkout_confirm"] ?? null);
        echo " <i class=\"fa fa-caret-down\"></i></a>');

      \$('a[href=\\'#collapse-checkout-confirm\\']').trigger('click');
     },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                    }
                });
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
    });
});
//--></script>
";
        // line 825
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "default/template/checkout/checkout.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1110 => 825,  1091 => 809,  1044 => 765,  1037 => 761,  990 => 717,  986 => 716,  979 => 712,  924 => 659,  914 => 652,  907 => 648,  896 => 639,  885 => 631,  881 => 630,  877 => 629,  870 => 625,  851 => 609,  847 => 608,  840 => 604,  828 => 595,  809 => 578,  807 => 577,  732 => 505,  728 => 504,  721 => 500,  666 => 447,  645 => 429,  638 => 425,  630 => 419,  609 => 401,  605 => 400,  601 => 399,  594 => 395,  586 => 389,  584 => 388,  530 => 337,  518 => 327,  508 => 320,  501 => 316,  493 => 310,  482 => 302,  478 => 301,  474 => 300,  467 => 296,  451 => 283,  447 => 282,  443 => 281,  436 => 277,  424 => 268,  408 => 254,  406 => 253,  323 => 173,  318 => 171,  297 => 152,  284 => 142,  275 => 135,  262 => 125,  253 => 118,  251 => 117,  243 => 112,  238 => 110,  231 => 106,  226 => 104,  216 => 97,  212 => 96,  202 => 89,  191 => 81,  187 => 79,  177 => 72,  166 => 64,  162 => 62,  159 => 61,  149 => 54,  145 => 52,  135 => 45,  131 => 43,  129 => 42,  120 => 36,  113 => 32,  106 => 31,  103 => 30,  100 => 29,  97 => 28,  94 => 27,  91 => 26,  88 => 25,  86 => 24,  81 => 23,  73 => 19,  71 => 18,  63 => 12,  51 => 9,  47 => 8,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/checkout/checkout.twig", "");
    }
}
