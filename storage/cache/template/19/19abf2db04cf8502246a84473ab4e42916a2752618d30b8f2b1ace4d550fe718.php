<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/module/isenselabs_seo/sd_category_data_breadcrumbs.twig */
class __TwigTemplate_9404dcd4d782d1f4f10afc5b2a490662ae90fe8e10da9aab811fd09e9a98d1a4 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((($context["category_breadcrumbs"] ?? null) && ($context["breadcrumbs"] ?? null))) {
            // line 2
            echo "<script type=\"application/ld+json\">
{
  \"@context\": \"http://schema.org\",
  \"@type\": \"BreadcrumbList\",
  \"itemListElement\": [
  
  ";
            // line 8
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["index"] => $context["breadcrumb"]) {
                // line 9
                echo "  {
    \"@type\": \"ListItem\",
    \"position\": ";
                // line 11
                echo ($context["index"] + 1);
                echo ",
    \"item\": {
      \"@id\": \"";
                // line 13
                echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 13);
                echo "\",
      \"name\": \"";
                // line 14
                echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "name", [], "any", false, false, false, 14);
                echo "\"
    }
  }   
  ";
                // line 17
                if (twig_get_attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 17)) {
                    // line 18
                    echo "  ]
  ";
                } else {
                    // line 20
                    echo "  ,
  ";
                }
                // line 22
                echo "  ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['index'], $context['breadcrumb'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "}
</script>
";
        }
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/isenselabs_seo/sd_category_data_breadcrumbs.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 23,  93 => 22,  89 => 20,  85 => 18,  83 => 17,  77 => 14,  73 => 13,  68 => 11,  64 => 9,  47 => 8,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/extension/module/isenselabs_seo/sd_category_data_breadcrumbs.twig", "");
    }
}
