<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/module/mpblogsearch.twig */
class __TwigTemplate_8ecf538a13d20a1202efbf774ef4ed3eeb4d729f0221b65eb7ae1e02fae6310d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div id=\"blogsearch\">
\t<h1>";
        // line 2
        echo ($context["text_search"] ?? null);
        echo "</h1>\t
\t<div  class=\"input-group\">
\t  <input type=\"text\" name=\"blogsearch\" value=\"";
        // line 4
        echo ($context["search"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["text_search"] ?? null);
        echo "\" class=\"form-control input-md\" data-url=\"";
        echo ($context["url"] ?? null);
        echo "\" />
\t  <span class=\"input-group-btn\">
\t    <button type=\"button\" class=\"btn-search-blog btn-md\" style=\"border-bottom-left-radius: 0; border-top-left-radius: 0;height: 37px;\"><i class=\"fa fa-search\"></i></button>
\t  </span>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/mpblogsearch.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 4,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/extension/module/mpblogsearch.twig", "");
    }
}
