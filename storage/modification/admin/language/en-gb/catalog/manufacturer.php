<?php
// Heading
$_['heading_title']     = 'Manufacturers';

// Text
$_['text_success']      = 'Success: You have modified manufacturers!';
$_['text_list']         = 'Manufacturer List';
$_['text_add']          = 'Add Manufacturer';
$_['text_edit']         = 'Edit Manufacturer';
$_['text_default']      = 'Default';
$_['text_percent']      = 'Percentage';
$_['text_amount']       = 'Fixed Amount';
$_['text_keyword']      = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';

// Column
$_['column_name']       = 'Manufacturer Name';
$_['column_sort_order'] = 'Sort Order';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']        = 'Manufacturer Name';
$_['entry_store']       = 'Stores';
$_['entry_keyword']     = 'Keyword';
$_['entry_image']       = 'Image';
$_['entry_sort_order']  = 'Sort Order';
$_['entry_type']        = 'Type';

				// additional fields
				$_['entry_description']      			= 'Description';
				$_['entry_meta_title']       			= 'Meta Tag Title';
				$_['entry_meta_keyword']     			= 'Meta Tag Keywords';
				$_['entry_meta_description'] 			= 'Meta Tag Description';
				$_['entry_image_custom_title']  		= 'Image Custom Title';
				$_['entry_image_custom_alt']    		= 'Image Custom Alt';
				$_['text_focus_keyphrase']    			= 'Focus Keyphrase';
				$_['entry_tags']    		  			= 'Tags';
			

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify manufacturers!';
$_['error_name']        = 'Manufacturer Name must be between 1 and 64 characters!';
$_['error_keyword']     = 'SEO URL already in use!';
$_['error_unique']      = 'SEO URL must be unique!';
$_['error_product']     = 'Warning: This manufacturer cannot be deleted as it is currently assigned to %s products!';