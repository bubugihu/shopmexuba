<?php
class ControllerCommonMenu extends Controller {
	public function index() {
		$this->load->language('common/menu');
        $data['text_home']=$this->language->get('text_home');
		// Menu
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);

					$children_data[] = array(
						'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}

				// Level 1
				$data['categories'][] = array(
					'name'     => $category['name'],
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
		}
        
        //blog
		$this->load->language('mpblog/blogmenu');
		$mblog_link = $this->url->link('mpblog/blogs','',true);
		$text_mblog = $this->language->get('text_mblog');
		$this->load->model('mpblog/mpblogcategory');
		$mblog_cate = $this->model_mpblog_mpblogcategory->getMpBlogCategories(0);
		$mblog_children = array();
		foreach($mblog_cate as $cate)
		{
			$mblog_children[] = array(
				'name'  => $cate['name'],
				'href'  => $this->url->link('mpblog/blogcategory', 'mpcpath=' . $cate['mpblogcategory_id'])
			);
		}
		//$data['text_home']=$this->language->get('text_home');
		// echo '<pre>' , var_dump($mblog_children) , '</pre>';die;
		$mpblogmenu = array(
			'name'     => $text_mblog,
			'children' => $mblog_children,
			'column'   => 1,
			'href'     => $mblog_link
		);
		if($this->config->get('mpblog_status')) {
		array_push($data['categories'], $mpblogmenu);
		}
		//end blog
		$data['home_href'] = $this->url->link('common/home');
		$data['information_href'] = $this->url->link('information/information','information_id='.'4');
		$data['contact_href'] = $this->url->link('information/contact');

			/* mps */
			$this->load->language('mpblog/blogmenu');
			$mblog_link = $this->url->link('mpblog/blogs','',true);
			$text_mblog = $this->language->get('text_mblog');
			if($this->config->get('mpblog_home_category')) {
			$this->load->model('mpblog/mpblogcategory');
			$mpblog_catinfo = $this->model_mpblog_mpblogcategory->getMpBlogCategory($this->config->get('mpblog_home_category'));
			if($mpblog_catinfo) {
				$mblog_link = $this->url->link('mpblog/blogcategory','mpcpath='.$mpblog_catinfo['mpblogcategory_id'] ,true);
				$text_mblog = $mpblog_catinfo['name'];
			}
			}
			$mpblogmenu = array(
				'name'     => $text_mblog,
				'children' => array(),
				'column'   => 1,
				'href'     => $mblog_link
			);
			if($this->config->get('mpblog_status')) {
			
			}
			/* mpe */
			
		return $this->load->view('common/menu', $data);
		
		
	}
}
