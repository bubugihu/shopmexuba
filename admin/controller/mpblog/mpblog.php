<?php
class ControllerMpBlogMpBlog extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('mpblog/mpblog');

		if(isset($this->request->get['store_id'])) {
			$data['store_id'] = $this->request->get['store_id'];
		}else{
			$data['store_id'] = 0;
		}

		$this->document->addStyle("view/javascript/mpblog/bootstrap-switch/css/bootstrap3/bootstrap-switch.min.css");
		$this->document->addScript("view/javascript/mpblog/bootstrap-switch/js/bootstrap-switch.min.js");


		$this->document->setTitle($this->language->get('heading_title'));

		
		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_setting_setting->editSetting('mpblog', $this->request->post, $data['store_id']);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('mpblog/mpblog', 'user_token=' . $this->session->data['user_token'].'&store_id='. $data['store_id'], true));
		}

		$data['text_mpblog_category'] = $this->language->get('text_mpblog_category');
		$data['text_mpblog_post'] = $this->language->get('text_mpblog_post');
		$data['text_mpblog_comment'] = $this->language->get('text_mpblog_comment');
		$data['text_mpblog_review'] = $this->language->get('text_mpblog_review');
		$data['text_mpblog'] = $this->language->get('text_mpblog');

		$data['mpblogcategory'] = $this->url->link('mpblog/mpblogcategory', 'user_token=' . $this->session->data['user_token'], true);
		$data['mpblogpost'] = $this->url->link('mpblog/mpblogpost', 'user_token=' . $this->session->data['user_token'], true);
		$data['mpblogcomment'] = $this->url->link('mpblog/mpblogcomment', 'user_token=' . $this->session->data['user_token'], true);
		$data['mpblogreview'] = $this->url->link('mpblog/mpblogreview', 'user_token=' . $this->session->data['user_token'], true);
		$data['mpblog'] = $this->url->link('mpblog/mpblog', 'user_token=' . $this->session->data['user_token'], true);

		$mpblog = array();
			
		if ($this->user->hasPermission('access', 'mpblog/mpblogcategory')) {
			$mpblog[] = array(
				'name'	   => $this->language->get('text_mpblog_category'),
				'href'     => $this->url->link('mpblog/mpblogcategory', 'user_token=' . $this->session->data['user_token'], true),
				'children' => array()		
			);	
		}	
		if ($this->user->hasPermission('access', 'mpblog/mpblogpost')) {
			$mpblog[] = array(
				'name'	   => $this->language->get('text_mpblog_post'),
				'href'     => $this->url->link('mpblog/mpblogpost', 'user_token=' . $this->session->data['user_token'], true),
				'children' => array()		
			);	
		}
		if ($this->user->hasPermission('access', 'mpblog/mpblogcomment')) {
			$mpblog[] = array(
				'name'	   => $this->language->get('text_mpblog_comment'),
				'href'     => $this->url->link('mpblog/mpblogcomment', 'user_token=' . $this->session->data['user_token'], true),
				'children' => array()		
			);	
		}
		if ($this->user->hasPermission('access', 'mpblog/mpblogreview')) {
			$mpblog[] = array(
				'name'	   => $this->language->get('text_mpblog_review'),
				'href'     => $this->url->link('mpblog/mpblogreview', 'user_token=' . $this->session->data['user_token'], true),
				'children' => array()		
			);	
		}
		if ($this->user->hasPermission('access', 'mpblog/mpblog')) {
			$mpblog[] = array(
				'name'	   => $this->language->get('text_mpblog'),
				'href'     => $this->url->link('mpblog/mpblog', 'user_token=' . $this->session->data['user_token'], true),
				'children' => array()		
			);	
		}
		
		if ($mpblog) {
			$data['menus'][] = array(
				'id'       => 'menu-mpblog',
				'icon'	   => 'fa-tags', 
				'name'	   => $this->language->get('text_mpblog'),
				'href'     => '',
				'children' => $mpblog
			);		
		}

		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_top'] = $this->language->get('text_top');
		$data['text_bottom'] = $this->language->get('text_bottom');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_store'] = $this->language->get('text_store');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_grid'] = $this->language->get('text_grid');
		$data['text_list'] = $this->language->get('text_list');


		$data['text_column'] = $this->language->get('text_column');
		$data['text_column_title'] = $this->language->get('text_column_title');
		$data['text_column_defination'] = $this->language->get('text_column_defination');

		$data['text_img_size_title'] = $this->language->get('text_img_size_title');
		$data['text_img_size_defination'] = $this->language->get('text_img_size_defination');
		$data['text_cimg_size_title'] = $this->language->get('text_cimg_size_title');
		$data['text_cimg_size_defination'] = $this->language->get('text_cimg_size_defination');

		$data['text_socialmedia_title'] = $this->language->get('text_socialmedia_title');
		$data['text_socialmedia_defination'] = $this->language->get('text_socialmedia_defination');

		$data['text_comment_title'] = $this->language->get('text_comment_title');
		$data['text_comment_defination'] = $this->language->get('text_comment_defination');

		$data['text_edit'] = $this->language->get('text_edit');	
	
		$data['text_category_home'] = $this->language->get('text_category_home');
		$data['text_category_post_count'] = $this->language->get('text_category_post_count');
		$data['text_category_show_image'] = $this->language->get('text_category_show_image');
		$data['text_category_show_description'] = $this->language->get('text_category_show_description');
		$data['text_category_page_limit'] = $this->language->get('text_category_page_limit');
		$data['text_category_design'] = $this->language->get('text_category_design');

		$data['text_blog_show_image'] = $this->language->get('text_blog_show_image');
		$data['text_blog_show_image_popup'] = $this->language->get('text_blog_show_image_popup');
		$data['text_blog_show_description'] = $this->language->get('text_blog_show_description');
		$data['text_blog_show_sdescription'] = $this->language->get('text_blog_show_sdescription');
		$data['text_blog_sdescription_length'] = $this->language->get('text_blog_sdescription_length');
		$data['text_blog_page_limit'] = $this->language->get('text_blog_page_limit');
		$data['text_blog_show_author'] = $this->language->get('text_blog_show_author');
		$data['text_blog_show_date'] = $this->language->get('text_blog_show_date');
		$data['text_blog_date_format'] = $this->language->get('text_blog_date_format');
		$data['text_blog_show_comment'] = $this->language->get('text_blog_show_comment');
		$data['text_blog_use_comment'] = $this->language->get('text_blog_use_comment');
		$data['text_blog_allow_comment'] = $this->language->get('text_blog_allow_comment');
		$data['text_blog_approve_comment'] = $this->language->get('text_blog_approve_comment');
		$data['text_blog_captcha_comment'] = $this->language->get('text_blog_captcha_comment');
		$data['text_blog_show_rating'] = $this->language->get('text_blog_show_rating');
		$data['text_blog_allow_rating'] = $this->language->get('text_blog_allow_rating');
		$data['text_blog_approve_rating'] = $this->language->get('text_blog_approve_rating');
		$data['text_blog_guest_rating'] = $this->language->get('text_blog_guest_rating');
		$data['text_blog_show_readmore'] = $this->language->get('text_blog_show_readmore');
		$data['text_blog_show_viewcount'] = $this->language->get('text_blog_show_viewcount');
		$data['text_blog_show_sharethis'] = $this->language->get('text_blog_show_sharethis');
		$data['text_blog_show_nextprev'] = $this->language->get('text_blog_show_nextprev');
		$data['text_blog_show_nextprev_title'] = $this->language->get('text_blog_show_nextprev_title');
		$data['text_blog_show_tags'] = $this->language->get('text_blog_show_tags');
		$data['text_blog_view'] = $this->language->get('text_blog_view');
		$data['text_category_view'] = $this->language->get('text_category_view');
		$data['text_blog_show_viewsocial'] = $this->language->get('text_blog_show_viewsocial');
		$data['text_blog_show_sociallocation'] = $this->language->get('text_blog_show_sociallocation');
		$data['text_blog_show_viewwishlist'] = $this->language->get('text_blog_show_viewwishlist');
		$data['text_blog_design'] = $this->language->get('text_blog_design');


		$data['text_comment_default'] = $this->language->get('text_comment_default');
		$data['text_comment_default_guest'] = $this->language->get('text_comment_default_guest');

		$data['text_comment_facebook'] = $this->language->get('text_comment_facebook');
		$data['text_facebook_appid'] = $this->language->get('text_facebook_appid');
		$data['text_facebook_nocomment'] = $this->language->get('text_facebook_nocomment');
		$data['text_facebook_color'] = $this->language->get('text_facebook_color');
		$data['text_facebook_colorlight'] = $this->language->get('text_facebook_colorlight');
		$data['text_facebook_colordark'] = $this->language->get('text_facebook_colordark');
		$data['text_facebook_order'] = $this->language->get('text_facebook_order');
		$data['text_facebook_ordersocial'] = $this->language->get('text_facebook_ordersocial');
		$data['text_facebook_orderreverse_time'] = $this->language->get('text_facebook_orderreverse_time');
		$data['text_facebook_ordertime'] = $this->language->get('text_facebook_ordertime');
		$data['text_facebook_width'] = $this->language->get('text_facebook_width');

		$data['text_comment_google'] = $this->language->get('text_comment_google');


		$data['text_comment_disqus'] = $this->language->get('text_comment_disqus');
		$data['text_comment_disqus_code'] = $this->language->get('text_comment_disqus_code');
		$data['text_comment_disqus_count'] = $this->language->get('text_comment_disqus_count');


		$data['legend_blogpage'] = $this->language->get('legend_blogpage');
		$data['legend_bloglist'] = $this->language->get('legend_bloglist');
		$data['legend_design'] = $this->language->get('legend_design');
		$data['legend_image_sizes'] = $this->language->get('legend_image_sizes');
		$data['legend_comment'] = $this->language->get('legend_comment');
		
		$data['legend_msg_unsubscribe'] = $this->language->get('legend_msg_unsubscribe');
		$data['legend_msg_confirm'] = $this->language->get('legend_msg_confirm');
		$data['legend_msg_invalid'] = $this->language->get('legend_msg_invalid');


		$data['info_subscribe_approval'] = $this->language->get('info_subscribe_approval');
		$data['info_subscribe_pending'] = $this->language->get('info_subscribe_pending');
		$data['info_subscribe_pending'] = $this->language->get('info_subscribe_pending');
		$data['info_subscribe_confirm'] = $this->language->get('info_subscribe_confirm');
		$data['info_subscribe_confirm_page'] = $this->language->get('info_subscribe_confirm_page');
		$data['info_unsubscribe'] = $this->language->get('info_unsubscribe');
		$data['info_unsubscribe_page'] = $this->language->get('info_unsubscribe_page');
		$data['info_invalidurl'] = $this->language->get('info_invalidurl');

		$data['info_email_blogadd'] = $this->language->get('info_email_blogadd');
		$data['info_email_blogedit'] = $this->language->get('info_email_blogedit');
		

		$data['info_title_rssfeed'] = $this->language->get('info_title_rssfeed');
		$data['info_text_rssfeed'] = $this->language->get('info_text_rssfeed');
		
		$data['text_rssfeed_title'] = $this->language->get('text_rssfeed_title');
		$data['text_rssfeed_description'] = $this->language->get('text_rssfeed_description');
		$data['text_rssfeed_format'] = $this->language->get('text_rssfeed_format');
		
		$data['text_rssfeed_limit'] = $this->language->get('text_rssfeed_limit');
		$data['text_rssfeed_web_master'] = $this->language->get('text_rssfeed_web_master');
		$data['text_rssfeed_copy_write'] = $this->language->get('text_rssfeed_copy_write');
		
		$data['text_autoapprove'] = $this->language->get('text_autoapprove');
		$data['text_adminapprove'] = $this->language->get('text_adminapprove');
		$data['text_confirmapprove'] = $this->language->get('text_confirmapprove');
		

		$data['entry_social_icon'] = $this->language->get('entry_social_icon');
		$data['entry_social_href'] = $this->language->get('entry_social_href');
		$data['entry_social_name'] = $this->language->get('entry_social_name');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['placeholder_social_icon'] = $this->language->get('placeholder_social_icon');
		$data['placeholder_social_href'] = $this->language->get('placeholder_social_href');
		$data['placeholder_social_name'] = $this->language->get('placeholder_social_name');
		$data['placeholder_sort_order'] = $this->language->get('placeholder_sort_order');
		$data['entry_support'] = $this->language->get('entry_support');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['entry_width'] = $this->language->get('entry_width');
		$data['entry_height'] = $this->language->get('entry_height');

		$data['entry_image_category'] = $this->language->get('entry_image_category');
		$data['entry_image_category_thumb'] = $this->language->get('entry_image_category_thumb');


		$data['entry_image_post_thumb'] = $this->language->get('entry_image_post_thumb');
		$data['entry_image_post_popup'] = $this->language->get('entry_image_post_popup');
		$data['entry_image_post'] = $this->language->get('entry_image_post');
		$data['entry_image_post_additional'] = $this->language->get('entry_image_post_additional');
		$data['entry_image_post_related'] = $this->language->get('entry_image_post_related');
		
		$data['entry_subscribeapprove'] = $this->language->get('entry_subscribeapprove');
		$data['entry_subscribeadminmail'] = $this->language->get('entry_subscribeadminmail');
		$data['entry_mail_subject'] = $this->language->get('entry_mail_subject');
		$data['entry_mail_message'] = $this->language->get('entry_mail_message');
		$data['entry_mail_send'] = $this->language->get('entry_mail_send');
		$data['entry_page_title'] = $this->language->get('entry_page_title');
		$data['entry_page_content'] = $this->language->get('entry_page_content');
		
		$data['tab_setting'] = $this->language->get('tab_setting');
		$data['tab_mpblogcategory'] = $this->language->get('tab_mpblogcategory');
		$data['tab_mpblogpost'] = $this->language->get('tab_mpblogpost');
		$data['tab_mpblogmodule'] = $this->language->get('tab_mpblogmodule');
		
		$data['tab_emails'] = $this->language->get('tab_emails');
		$data['tab_email_blogadd'] = $this->language->get('tab_email_blogadd');
		$data['tab_email_blogedit'] = $this->language->get('tab_email_blogedit');

		$data['tab_subscribers'] = $this->language->get('tab_subscribers');
		$data['tab_email_subscribeadmin'] = $this->language->get('tab_email_subscribeadmin');
		$data['tab_email_subscribeapproval'] = $this->language->get('tab_email_subscribeapproval');
		$data['tab_email_subscribepending'] = $this->language->get('tab_email_subscribepending');
		$data['tab_email_subscribeconfirm'] = $this->language->get('tab_email_subscribeconfirm');
		$data['tab_email_unsubscribe'] = $this->language->get('tab_email_unsubscribe');
		$data['tab_email_invalidurl'] = $this->language->get('tab_email_invalidurl');
		
		$data['tab_mpblogcomments'] = $this->language->get('tab_mpblogcomments');

		$data['tab_mpblogcomments_default'] = $this->language->get('tab_mpblogcomments_default');
		$data['tab_mpblogcomments_facebook'] = $this->language->get('tab_mpblogcomments_facebook');
		$data['tab_mpblogcomments_google'] = $this->language->get('tab_mpblogcomments_google');
		$data['tab_mpblogcomments_disqus'] = $this->language->get('tab_mpblogcomments_disqus');

		$data['tab_mpblogrss'] = $this->language->get('tab_mpblogrss');
		$data['tab_mpblogdoc'] = $this->language->get('tab_mpblogdoc');
		$data['tab_mpsupport'] = $this->language->get('tab_mpsupport');
		$data['tab_mpblogs_listing'] = $this->language->get('tab_mpblogs_listing');
		$data['tab_mpblogs_view'] = $this->language->get('tab_mpblogs_view');

		$data['help_category_home'] = $this->language->get('help_category_home');
		$data['help_blog_captcha_comment'] = $this->language->get('help_blog_captcha_comment');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_support'] = $this->language->get('button_support');
		$data['button_design_add'] = $this->language->get('button_design_add');
		$data['button_social_add'] = $this->language->get('button_social_add');
		$data['button_design_remove'] = $this->language->get('button_design_remove');
		$data['button_remove'] = $this->language->get('button_remove');
		
		$data['button_shortcodes'] = $this->language->get('button_shortcodes');

		$data['sh_code'] = $this->language->get('sh_code');
		$data['sh_name'] = $this->language->get('sh_name');
		$data['sh_logo'] = $this->language->get('sh_logo');
		$data['sh_storename'] = $this->language->get('sh_storename');
		$data['sh_storelink'] = $this->language->get('sh_storelink');
		$data['sh_email'] = $this->language->get('sh_email');
		$data['sh_confirmlink'] = $this->language->get('sh_confirmlink');
		$data['sh_confirmcode'] = $this->language->get('sh_confirmcode');

		$data['sh_blog_name'] = $this->language->get('sh_blog_name');
		$data['sh_blog_thumb'] = $this->language->get('sh_blog_thumb');
		$data['sh_blog_description'] = $this->language->get('sh_blog_description');
		$data['sh_blog_shortdescription'] = $this->language->get('sh_blog_shortdescription');
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->error['subscribemail'])) {
			$data['error_subscribemail'] = $this->error['subscribemail'];
		} else {
			$data['error_subscribemail'] = array();
		}

		if (isset($this->error['emails'])) {
			$data['error_emails'] = $this->error['emails'];
		} else {
			$data['error_emails'] = array();
		}
		
		if (isset($this->error['image_post_thumb'])) {
			$data['error_image_post_thumb'] = $this->error['image_post_thumb'];
		} else {
			$data['error_image_post_thumb'] = '';
		}
		
		if (isset($this->error['image_post_popup'])) {
			$data['error_image_post_popup'] = $this->error['image_post_popup'];
		} else {
			$data['error_image_post_popup'] = '';
		}

		if (isset($this->error['image_post'])) {
			$data['error_image_post'] = $this->error['image_post'];
		} else {
			$data['error_image_post'] = '';
		}

		if (isset($this->error['image_post_additional'])) {
			$data['error_image_post_additional'] = $this->error['image_post_additional'];
		} else {
			$data['error_image_post_additional'] = '';
		}

		if (isset($this->error['image_post_related'])) {
			$data['error_image_post_related'] = $this->error['image_post_related'];
		} else {
			$data['error_image_post_related'] = '';
		}

		// blog category
		if (isset($this->error['image_category'])) {
			$data['error_image_category'] = $this->error['image_category'];
		} else {
			$data['error_image_category'] = '';
		}

		
		if (isset($this->error['image_category_thumb'])) {
			$data['error_image_category_thumb'] = $this->error['image_category_thumb'];
		} else {
			$data['error_image_category_thumb'] = '';
		}
		

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);


		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('mpblog/mpblog', 'user_token=' . $this->session->data['user_token'], true)
		);

		if(isset($data['store_id'])) {
			$data['action'] = $this->url->link('mpblog/mpblog', 'user_token=' . $this->session->data['user_token'].'&store_id='. $data['store_id'], true);
		} else{
			$data['action'] = $this->url->link('mpblog/mpblog', 'user_token=' . $this->session->data['user_token'], true);
		}


		$data['user_token'] = $this->session->data['user_token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		$module_info = $this->model_setting_setting->getSetting('mpblog', $data['store_id']);



		if (!isset($module_info['mpblog_category_post_count']))   {
			$data['configuration'] = $this->language->get('text_configuration');
		} else if((isset($module_info['mpblog_category_post_count']) && is_null($module_info['mpblog_category_post_count']) )) {
			$data['configuration'] = $this->language->get('text_configuration');
		} else {
			$data['configuration'] = '';
		}

		// setting
		if (isset($this->request->post['mpblog_status'])) {
			$data['mpblog_status'] = $this->request->post['mpblog_status'];
		} else if(isset($module_info['mpblog_status'])) {
			$data['mpblog_status'] = $module_info['mpblog_status'];
		} else {
			$data['mpblog_status'] = 0;
		}



		// category
		if (isset($this->request->post['mpblog_home_category'])) {
			$data['mpblog_home_category'] = $this->request->post['mpblog_home_category'];
		} else if(isset($module_info['mpblog_home_category'])) {
			$data['mpblog_home_category'] = $module_info['mpblog_home_category'];
		} else {
			$data['mpblog_home_category'] = 0;
		}

		$this->load->model('mpblog/mpblogcategory');

		$categories = $this->model_mpblog_mpblogcategory->getMpBlogCategories();
		$data['mpblog_categories'] = array();
		foreach ($categories as $key => $category) {
			$data['mpblog_categories'][] = array(
				'mpblogcategory_id' => $category['mpblogcategory_id'],
				'name' => $category['name'],
			);
		}

		if (isset($this->request->post['mpblog_category_post_count'])) {
			$data['mpblog_category_post_count'] = $this->request->post['mpblog_category_post_count'];
		} else if(isset($module_info['mpblog_category_post_count'])) {
			$data['mpblog_category_post_count'] = $module_info['mpblog_category_post_count'];
		}  else {
			$data['mpblog_category_post_count'] = 0;
		}

		if (isset($this->request->post['mpblog_category_image'])) {
			$data['mpblog_category_image'] = $this->request->post['mpblog_category_image'];
		} else if(isset($module_info['mpblog_category_image'])) {
			$data['mpblog_category_image'] = $module_info['mpblog_category_image'];
		}  else {
			$data['mpblog_category_image'] = 1;
		}

		if (isset($this->request->post['mpblog_category_description'])) {
			$data['mpblog_category_description'] = $this->request->post['mpblog_category_description'];
		} elseif(isset($module_info['mpblog_category_description'])) {
			$data['mpblog_category_description'] = $module_info['mpblog_category_description'];
		} else {
			$data['mpblog_category_description'] = 1;
		}

		if (isset($this->request->post['mpblog_category_page_limit'])) {
			$data['mpblog_category_page_limit'] = $this->request->post['mpblog_category_page_limit'];
		} elseif(isset($module_info['mpblog_category_page_limit'])) {
			$data['mpblog_category_page_limit'] = $module_info['mpblog_category_page_limit'];
		} else {
			$data['mpblog_category_page_limit'] = 15;
		}

		if (isset($this->request->post['mpblog_category_design'])) {
			$data['mpblog_category_designs'] = $this->request->post['mpblog_category_design'];
		} elseif(isset($module_info['mpblog_category_design'])) {
			$data['mpblog_category_designs'] = $module_info['mpblog_category_design'];
		} else {
			$data['mpblog_category_designs'] = array( 0 => 3);
		}

		if (isset($this->request->post['mpblog_image_category_width'])) {
	      $data['mpblog_image_category_width'] = $this->request->post['mpblog_image_category_width'];
	    } elseif (isset($module_info['mpblog_image_category_width'])) {
	      $data['mpblog_image_category_width'] = $module_info['mpblog_image_category_width'];
		} else {
	      $data['mpblog_image_category_width'] = 405;
	    }
	    
	    if (isset($this->request->post['mpblog_image_category_height'])) {
	      $data['mpblog_image_category_height'] = $this->request->post['mpblog_image_category_height'];
	    } elseif (isset($module_info['mpblog_image_category_height'])) {
	      $data['mpblog_image_category_height'] = $module_info['mpblog_image_category_height'];
		} else {
	      $data['mpblog_image_category_height'] = 251;
	    }

	    if (isset($this->request->post['mpblog_image_category_thumb_width'])) {
	      $data['mpblog_image_category_thumb_width'] = $this->request->post['mpblog_image_category_thumb_width'];
	    } elseif(isset($module_info['mpblog_image_category_thumb_width'])) {
	      $data['mpblog_image_category_thumb_width'] = $module_info['mpblog_image_category_thumb_width'];
		} else {
	      $data['mpblog_image_category_thumb_width'] = 405;
	    }

	    if (isset($this->request->post['mpblog_image_category_thumb_height'])) {
	      $data['mpblog_image_category_thumb_height'] = $this->request->post['mpblog_image_category_thumb_height'];
	    } elseif(isset($module_info['mpblog_image_category_thumb_height'])) {
	      $data['mpblog_image_category_thumb_height'] = $module_info['mpblog_image_category_thumb_height'];
		} else {
	      $data['mpblog_image_category_thumb_height'] = 251;
	    }


	    // blog
		if (isset($this->request->post['mpblog_blog_image'])) {
			$data['mpblog_blog_image'] = $this->request->post['mpblog_blog_image'];
		} elseif(isset($module_info['mpblog_blog_image'])) {
			$data['mpblog_blog_image'] = $module_info['mpblog_blog_image'];
		} else {
			$data['mpblog_blog_image'] = 1;
		}

		if (isset($this->request->post['mpblog_blog_image_popup'])) {
			$data['mpblog_blog_image_popup'] = $this->request->post['mpblog_blog_image_popup'];
		} elseif(isset($module_info['mpblog_blog_image_popup'])) {
			$data['mpblog_blog_image_popup'] = $module_info['mpblog_blog_image_popup'];
		} else {
			$data['mpblog_blog_image_popup'] = 1;
		}

		if (isset($this->request->post['mpblog_blog_description'])) {
			$data['mpblog_blog_description'] = $this->request->post['mpblog_blog_description'];
		} elseif(isset($module_info['mpblog_blog_description'])) {
			$data['mpblog_blog_description'] = $module_info['mpblog_blog_description'];
		} else {
			$data['mpblog_blog_description'] = 1;
		}

		if (isset($this->request->post['mpblog_blog_sdescription'])) {
			$data['mpblog_blog_sdescription'] = $this->request->post['mpblog_blog_sdescription'];
		} elseif(isset($module_info['mpblog_blog_sdescription'])) {
			$data['mpblog_blog_sdescription'] = $module_info['mpblog_blog_sdescription'];
		} else {
			$data['mpblog_blog_sdescription'] = 1;
		}

		if (isset($this->request->post['mpblog_blog_sdescription_length'])) {
			$data['mpblog_blog_sdescription_length'] = $this->request->post['mpblog_blog_sdescription_length'];
		} elseif(isset($module_info['mpblog_blog_sdescription_length'])) {
			$data['mpblog_blog_sdescription_length'] = $module_info['mpblog_blog_sdescription_length'];
		} else {
			$data['mpblog_blog_sdescription_length'] = 250;
		}

		if (isset($this->request->post['mpblog_blog_page_limit'])) {
			$data['mpblog_blog_page_limit'] = $this->request->post['mpblog_blog_page_limit'];
		} elseif(isset($module_info['mpblog_blog_page_limit'])) {
			$data['mpblog_blog_page_limit'] = $module_info['mpblog_blog_page_limit'];
		} else {
			$data['mpblog_blog_page_limit'] = 15;
		}

		if (isset($this->request->post['mpblog_blog_author'])) {
			$data['mpblog_blog_author'] = $this->request->post['mpblog_blog_author'];
		} elseif(isset($module_info['mpblog_blog_author'])) {
			$data['mpblog_blog_author'] = $module_info['mpblog_blog_author'];
		} else {
			$data['mpblog_blog_author'] = 1;
		}

		if (isset($this->request->post['mpblog_blog_date'])) {
			$data['mpblog_blog_date'] = $this->request->post['mpblog_blog_date'];
		} elseif(isset($module_info['mpblog_blog_date'])) {
			$data['mpblog_blog_date'] = $module_info['mpblog_blog_date'];
		} else {
			$data['mpblog_blog_date'] = 1;
		}

		if (isset($this->request->post['mpblog_blog_date_format'])) {
			$data['mpblog_blog_date_format'] = $this->request->post['mpblog_blog_date_format'];
		} elseif(isset($module_info['mpblog_blog_date_format'])) {
			$data['mpblog_blog_date_format'] = $module_info['mpblog_blog_date_format'];
		} else {
			$data['mpblog_blog_date_format'] = $this->language->get('date_format_short');
		}

		if (isset($this->request->post['mpblog_blog_show_comment'])) {
			$data['mpblog_blog_show_comment'] = $this->request->post['mpblog_blog_show_comment'];
		} elseif(isset($module_info['mpblog_blog_show_comment'])) {
			$data['mpblog_blog_show_comment'] = $module_info['mpblog_blog_show_comment'];
		} else {
			$data['mpblog_blog_show_comment'] = 1;
		}

		if (isset($this->request->post['mpblog_blog_use_comment'])) {
			$data['mpblog_blog_use_comment'] = $this->request->post['mpblog_blog_use_comment'];
		} elseif(isset($module_info['mpblog_blog_use_comment'])) {
			$data['mpblog_blog_use_comment'] = $module_info['mpblog_blog_use_comment'];
		} else {
			$data['mpblog_blog_use_comment'] = 1;
		}

		if (isset($this->request->post['mpblog_blog_allow_comment'])) {
			$data['mpblog_blog_allow_comment'] = $this->request->post['mpblog_blog_allow_comment'];
		} elseif(isset($module_info['mpblog_blog_allow_comment'])) {
			$data['mpblog_blog_allow_comment'] = $module_info['mpblog_blog_allow_comment'];
		} else {
			$data['mpblog_blog_allow_comment'] = 1;
		}

		if (isset($this->request->post['mpblog_blog_approve_comment'])) {
			$data['mpblog_blog_approve_comment'] = $this->request->post['mpblog_blog_approve_comment'];
		} elseif(isset($module_info['mpblog_blog_approve_comment'])) {
			$data['mpblog_blog_approve_comment'] = $module_info['mpblog_blog_approve_comment'];
		} else {
			$data['mpblog_blog_approve_comment'] = 1;
		}

		if (isset($this->request->post['mpblog_blog_captcha_comment'])) {
			$data['mpblog_blog_captcha_comment'] = $this->request->post['mpblog_blog_captcha_comment'];
		} elseif(isset($module_info['mpblog_blog_captcha_comment'])) {
			$data['mpblog_blog_captcha_comment'] = $module_info['mpblog_blog_captcha_comment'];
		} else {
			$data['mpblog_blog_captcha_comment'] = 1;
		}

		if (isset($this->request->post['mpblog_blog_show_rating'])) {
			$data['mpblog_blog_show_rating'] = $this->request->post['mpblog_blog_show_rating'];
		} elseif(isset($module_info['mpblog_blog_show_rating'])) {
			$data['mpblog_blog_show_rating'] = $module_info['mpblog_blog_show_rating'];
		} else {
			$data['mpblog_blog_show_rating'] = 1;
		}

		if (isset($this->request->post['mpblog_blog_allow_rating'])) {
			$data['mpblog_blog_allow_rating'] = $this->request->post['mpblog_blog_allow_rating'];
		} elseif(isset($module_info['mpblog_blog_allow_rating'])) {
			$data['mpblog_blog_allow_rating'] = $module_info['mpblog_blog_allow_rating'];
		} else {
			$data['mpblog_blog_allow_rating'] = 1;
		}

		if (isset($this->request->post['mpblog_blog_approve_rating'])) {
			$data['mpblog_blog_approve_rating'] = $this->request->post['mpblog_blog_approve_rating'];
		} elseif(isset($module_info['mpblog_blog_approve_rating'])) {
			$data['mpblog_blog_approve_rating'] = $module_info['mpblog_blog_approve_rating'];
		} else {
			$data['mpblog_blog_approve_rating'] = 1;
		}

		if (isset($this->request->post['mpblog_blog_guest_rating'])) {
			$data['mpblog_blog_guest_rating'] = $this->request->post['mpblog_blog_guest_rating'];
		} elseif(isset($module_info['mpblog_blog_guest_rating'])) {
			$data['mpblog_blog_guest_rating'] = $module_info['mpblog_blog_guest_rating'];
		} else {
			$data['mpblog_blog_guest_rating'] = 1;
		}

		if (isset($this->request->post['mpblog_blog_show_readmore'])) {
			$data['mpblog_blog_show_readmore'] = $this->request->post['mpblog_blog_show_readmore'];
		} elseif(isset($module_info['mpblog_blog_show_readmore'])) {
			$data['mpblog_blog_show_readmore'] = $module_info['mpblog_blog_show_readmore'];
		} else {
			$data['mpblog_blog_show_readmore'] = 1;
		}

		if (isset($this->request->post['mpblog_blog_show_tags'])) {
			$data['mpblog_blog_show_tags'] = $this->request->post['mpblog_blog_show_tags'];
		} elseif(isset($module_info['mpblog_blog_show_tags'])) {
			$data['mpblog_blog_show_tags'] = $module_info['mpblog_blog_show_tags'];
		} else {
			$data['mpblog_blog_show_tags'] = 0;
		}

		if (isset($this->request->post['mpblog_blog_viewcount'])) {
			$data['mpblog_blog_viewcount'] = $this->request->post['mpblog_blog_viewcount'];
		} elseif(isset($module_info['mpblog_blog_viewcount'])) {
			$data['mpblog_blog_viewcount'] = $module_info['mpblog_blog_viewcount'];
		} else {
			$data['mpblog_blog_viewcount'] = 1;
		}

		if (isset($this->request->post['mpblog_blog_sharethis'])) {
			$data['mpblog_blog_sharethis'] = $this->request->post['mpblog_blog_sharethis'];
		} elseif(isset($module_info['mpblog_blog_sharethis'])) {
			$data['mpblog_blog_sharethis'] = $module_info['mpblog_blog_sharethis'];
		} else {
			$data['mpblog_blog_sharethis'] = 1;
		}

		if (isset($this->request->post['mpblog_blog_nextprev'])) {
			$data['mpblog_blog_nextprev'] = $this->request->post['mpblog_blog_nextprev'];
		} elseif(isset($module_info['mpblog_blog_nextprev'])) {
			$data['mpblog_blog_nextprev'] = $module_info['mpblog_blog_nextprev'];
		} else {
			$data['mpblog_blog_nextprev'] = 1;
		}

		if (isset($this->request->post['mpblog_blog_nextprev_title'])) {
			$data['mpblog_blog_nextprev_title'] = $this->request->post['mpblog_blog_nextprev_title'];
		} elseif(isset($module_info['mpblog_blog_nextprev_title'])) {
			$data['mpblog_blog_nextprev_title'] = $module_info['mpblog_blog_nextprev_title'];
		} else {
			$data['mpblog_blog_nextprev_title'] = 1;
		}

		if (isset($this->request->post['mpblog_blog_view'])) {
			$data['mpblog_blog_view'] = $this->request->post['mpblog_blog_view'];
		} elseif(isset($module_info['mpblog_blog_view'])) {
			$data['mpblog_blog_view'] = $module_info['mpblog_blog_view'];
		} else {
			$data['mpblog_blog_view'] = 'GRID';
		}

		if (isset($this->request->post['mpblog_category_view'])) {
			$data['mpblog_category_view'] = $this->request->post['mpblog_category_view'];
		} elseif(isset($module_info['mpblog_category_view'])) {
			$data['mpblog_category_view'] = $module_info['mpblog_category_view'];
		} else {
			$data['mpblog_category_view'] = 'GRID';
		}

		if (isset($this->request->post['mpblog_blog_sociallocation'])) {
			$data['mpblog_blog_sociallocation'] = $this->request->post['mpblog_blog_sociallocation'];
		} elseif(isset($module_info['mpblog_blog_sociallocation'])) {
			$data['mpblog_blog_sociallocation'] = (array)$module_info['mpblog_blog_sociallocation'];
		} else {
			$data['mpblog_blog_sociallocation'] = array('TOP','BOTTOM');
		}

		if (isset($this->request->post['mpblog_blog_viewsocial'])) {
			$data['mpblog_blog_viewsocial'] = $this->request->post['mpblog_blog_viewsocial'];
		} elseif(isset($module_info['mpblog_blog_viewsocial'])) {
			$data['mpblog_blog_viewsocial'] = $module_info['mpblog_blog_viewsocial'];
		} else {
			$data['mpblog_blog_viewsocial'] = 1;
		}

		if (isset($this->request->post['mpblog_blog_viewwishlist'])) {
			$data['mpblog_blog_viewwishlist'] = $this->request->post['mpblog_blog_viewwishlist'];
		} elseif(isset($module_info['mpblog_blog_viewwishlist'])) {
			$data['mpblog_blog_viewwishlist'] = $module_info['mpblog_blog_viewwishlist'];
		} else {
			$data['mpblog_blog_viewwishlist'] = 1;
		}

		if (isset($this->request->post['mpblog_blog_design'])) {
			$data['mpblog_blog_designs'] = $this->request->post['mpblog_blog_design'];
		} elseif(isset($module_info['mpblog_blog_design'])) {
			$data['mpblog_blog_designs'] = $module_info['mpblog_blog_design'];
		} else {
			$data['mpblog_blog_designs'] = array( 0 => 3);
		}

		if (isset($this->request->post['mpblog_image_post_thumb_width'])) {
			$data['mpblog_image_post_thumb_width'] = $this->request->post['mpblog_image_post_thumb_width'];
		} elseif(isset($module_info['mpblog_image_post_thumb_width'])) {
			$data['mpblog_image_post_thumb_width'] = $module_info['mpblog_image_post_thumb_width'];
		} else {
			$data['mpblog_image_post_thumb_width'] = 408;
		}

		if (isset($this->request->post['mpblog_image_post_thumb_height'])) {
			$data['mpblog_image_post_thumb_height'] = $this->request->post['mpblog_image_post_thumb_height'];
		} elseif(isset($module_info['mpblog_image_post_thumb_height'])) {
			$data['mpblog_image_post_thumb_height'] = $module_info['mpblog_image_post_thumb_height'];
		} else {
			$data['mpblog_image_post_thumb_height'] = 251;
		}

		if (isset($this->request->post['mpblog_image_post_popup_width'])) {
			$data['mpblog_image_post_popup_width'] = $this->request->post['mpblog_image_post_popup_width'];
		} elseif (isset($module_info['mpblog_image_post_popup_width'])) {
			$data['mpblog_image_post_popup_width'] = $module_info['mpblog_image_post_popup_width'];
		} else {
			$data['mpblog_image_post_popup_width'] = 1140;
		}
		
		if (isset($this->request->post['mpblog_image_post_popup_height'])) {
			$data['mpblog_image_post_popup_height'] = $this->request->post['mpblog_image_post_popup_height'];
		} elseif (isset($module_info['mpblog_image_post_popup_height'])) {
			$data['mpblog_image_post_popup_height'] = $module_info['mpblog_image_post_popup_height'];
		} else {
			$data['mpblog_image_post_popup_height'] = 700;
		}
		
		if (isset($this->request->post['mpblog_image_post_width'])) {
			$data['mpblog_image_post_width'] = $this->request->post['mpblog_image_post_width'];
		} elseif (isset($module_info['mpblog_image_post_width'])) {
			$data['mpblog_image_post_width'] = $module_info['mpblog_image_post_width'];
		} else {
			$data['mpblog_image_post_width'] = 1140;
		}
		
		if (isset($this->request->post['mpblog_image_post_height'])) {
			$data['mpblog_image_post_height'] = $this->request->post['mpblog_image_post_height'];
		} elseif (isset($module_info['mpblog_image_post_height'])) {
			$data['mpblog_image_post_height'] = $module_info['mpblog_image_post_height'];
		} else {
			$data['mpblog_image_post_height'] = 700;
		}
		
		if (isset($this->request->post['mpblog_image_post_additional_width'])) {
			$data['mpblog_image_post_additional_width'] = $this->request->post['mpblog_image_post_additional_width'];
		} elseif (isset($module_info['mpblog_image_post_additional_width'])) {
			$data['mpblog_image_post_additional_width'] = $module_info['mpblog_image_post_additional_width'];
		} else {
			$data['mpblog_image_post_additional_width'] = 220;
		}
		
		if (isset($this->request->post['mpblog_image_post_additional_height'])) {
			$data['mpblog_image_post_additional_height'] = $this->request->post['mpblog_image_post_additional_height'];
		} elseif (isset($module_info['mpblog_image_post_additional_height'])) {
			$data['mpblog_image_post_additional_height'] = $module_info['mpblog_image_post_additional_height'];
		} else {
			$data['mpblog_image_post_additional_height'] = 135;
		}
		
		if (isset($this->request->post['mpblog_image_post_related_width'])) {
			$data['mpblog_image_post_related_width'] = $this->request->post['mpblog_image_post_related_width'];
		} elseif (isset($module_info['mpblog_image_post_related_width'])) {
			$data['mpblog_image_post_related_width'] = $module_info['mpblog_image_post_related_width'];
		} else {
			$data['mpblog_image_post_related_width'] = 408;
		}
		
		if (isset($this->request->post['mpblog_image_post_related_height'])) {
			$data['mpblog_image_post_related_height'] = $this->request->post['mpblog_image_post_related_height'];
		} elseif (isset($module_info['mpblog_image_post_related_height'])) {
			$data['mpblog_image_post_related_height'] = $module_info['mpblog_image_post_related_height'];
		} else {
			$data['mpblog_image_post_related_height'] = 251;
		}

		
		// module subscribers
		if (isset($this->request->post['mpblog_subscribeapprove'])) {
			$data['mpblog_subscribeapprove'] = $this->request->post['mpblog_subscribeapprove'];
		} elseif(isset($module_info['mpblog_subscribeapprove'])) {
			$data['mpblog_subscribeapprove'] = $module_info['mpblog_subscribeapprove'];
		} else {
			$data['mpblog_subscribeapprove'] = 'AUTO';
		}
		if (isset($this->request->post['mpblog_subscribeadminmail_status'])) {
			$data['mpblog_subscribeadminmail_status'] = $this->request->post['mpblog_subscribeadminmail_status'];
		} elseif(isset($module_info['mpblog_subscribeadminmail_status'])) {
			$data['mpblog_subscribeadminmail_status'] = $module_info['mpblog_subscribeadminmail_status'];
		} else {
			$data['mpblog_subscribeadminmail_status'] = 0;
		}
		if (isset($this->request->post['mpblog_subscribeapproval_status'])) {
			$data['mpblog_subscribeapproval_status'] = $this->request->post['mpblog_subscribeapproval_status'];
		} elseif(isset($module_info['mpblog_subscribeapproval_status'])) {
			$data['mpblog_subscribeapproval_status'] = $module_info['mpblog_subscribeapproval_status'];
		} else {
			$data['mpblog_subscribeapproval_status'] = 0;
		}
		if (isset($this->request->post['mpblog_subscribepending_status'])) {
			$data['mpblog_subscribepending_status'] = $this->request->post['mpblog_subscribepending_status'];
		} elseif(isset($module_info['mpblog_subscribepending_status'])) {
			$data['mpblog_subscribepending_status'] = $module_info['mpblog_subscribepending_status'];
		} else {
			$data['mpblog_subscribepending_status'] = 0;
		}
		if (isset($this->request->post['mpblog_subscribeconfirm_status'])) {
			$data['mpblog_subscribeconfirm_status'] = $this->request->post['mpblog_subscribeconfirm_status'];
		} elseif(isset($module_info['mpblog_subscribeconfirm_status'])) {
			$data['mpblog_subscribeconfirm_status'] = $module_info['mpblog_subscribeconfirm_status'];
		} else {
			$data['mpblog_subscribeconfirm_status'] = 0;
		}
		if (isset($this->request->post['mpblog_unsubscribe_status'])) {
			$data['mpblog_unsubscribe_status'] = $this->request->post['mpblog_unsubscribe_status'];
		} elseif(isset($module_info['mpblog_unsubscribe_status'])) {
			$data['mpblog_unsubscribe_status'] = $module_info['mpblog_unsubscribe_status'];
		} else {
			$data['mpblog_unsubscribe_status'] = 0;
		}
		if (isset($this->request->post['mpblog_subscribeadminmail'])) {
			$data['mpblog_subscribeadminmail'] = $this->request->post['mpblog_subscribeadminmail'];
		} elseif(isset($module_info['mpblog_subscribeadminmail'])) {
			$data['mpblog_subscribeadminmail'] = $module_info['mpblog_subscribeadminmail'];
		} else {
			$data['mpblog_subscribeadminmail'] = $this->config->get('config_email');
		}

		if (isset($this->request->post['mpblog_subscribemail'])) {
			$data['mpblog_subscribemail'] = $this->request->post['mpblog_subscribemail'];
		} elseif(isset($module_info['mpblog_subscribemail'])) {
			$data['mpblog_subscribemail'] = $module_info['mpblog_subscribemail'];
		} else {
			$data['mpblog_subscribemail'] = array();
		}

		if (isset($this->request->post['mpblog_blogadd_status'])) {
			$data['mpblog_blogadd_status'] = $this->request->post['mpblog_blogadd_status'];
		} elseif(isset($module_info['mpblog_blogadd_status'])) {
			$data['mpblog_blogadd_status'] = $module_info['mpblog_blogadd_status'];
		} else {
			$data['mpblog_blogadd_status'] = 0;
		}

		if (isset($this->request->post['mpblog_blogedit_status'])) {
			$data['mpblog_blogedit_status'] = $this->request->post['mpblog_blogedit_status'];
		} elseif(isset($module_info['mpblog_blogedit_status'])) {
			$data['mpblog_blogedit_status'] = $module_info['mpblog_blogedit_status'];
		} else {
			$data['mpblog_blogedit_status'] = 0;
		}


		if (isset($this->request->post['mpblog_emails'])) {
			$data['mpblog_emails'] = $this->request->post['mpblog_emails'];
		} elseif(isset($module_info['mpblog_emails'])) {
			$data['mpblog_emails'] = $module_info['mpblog_emails'];
		} else {
			$data['mpblog_emails'] = array();
		}
		
		// module social media

		if (isset($this->request->post['mpblog_social'])) {
			$data['mpblog_socials'] = $this->request->post['mpblog_social'];
		} elseif(isset($module_info['mpblog_social'])) {
			$data['mpblog_socials'] = $module_info['mpblog_social'];
		} else {
			$data['mpblog_socials'] = array();
		}

		// comments

		// default
		if (isset($this->request->post['mpblog_comments_default'])) {
			$data['mpblog_comments_default'] = $this->request->post['mpblog_comments_default'];
		} elseif(isset($module_info['mpblog_comments_default'])) {
			$data['mpblog_comments_default'] = $module_info['mpblog_comments_default'];
		} else {
			$data['mpblog_comments_default'] = 1;
		}

		if (isset($this->request->post['mpblog_comments_default_guest'])) {
			$data['mpblog_comments_default_guest'] = $this->request->post['mpblog_comments_default_guest'];
		} elseif(isset($module_info['mpblog_comments_default_guest'])) {
			$data['mpblog_comments_default_guest'] = $module_info['mpblog_comments_default_guest'];
		} else {
			$data['mpblog_comments_default_guest'] = 0;
		}


		// facebook

		if (isset($this->request->post['mpblog_comments_facebook'])) {
			$data['mpblog_comments_facebook'] = $this->request->post['mpblog_comments_facebook'];
		} elseif(isset($module_info['mpblog_comments_facebook'])) {
			$data['mpblog_comments_facebook'] = $module_info['mpblog_comments_facebook'];
		} else {
			$data['mpblog_comments_facebook'] = 0;
		}
		
		if (isset($this->request->post['mpblog_facebook_appid'])) {
			$data['mpblog_facebook_appid'] = $this->request->post['mpblog_facebook_appid'];
		} elseif(isset($module_info['mpblog_facebook_appid'])) {
			$data['mpblog_facebook_appid'] = $module_info['mpblog_facebook_appid'];
		} else {
			$data['mpblog_facebook_appid'] = '';
		}

		if (isset($this->request->post['mpblog_facebook_nocomment'])) {
			$data['mpblog_facebook_nocomment'] = $this->request->post['mpblog_facebook_nocomment'];
		} elseif(isset($module_info['mpblog_facebook_nocomment'])) {
			$data['mpblog_facebook_nocomment'] = $module_info['mpblog_facebook_nocomment'];
		} else {
			$data['mpblog_facebook_nocomment'] = 10;
		}

		if (isset($this->request->post['mpblog_facebook_color'])) {
			$data['mpblog_facebook_color'] = $this->request->post['mpblog_facebook_color'];
		} elseif(isset($module_info['mpblog_facebook_color'])) {
			$data['mpblog_facebook_color'] = $module_info['mpblog_facebook_color'];
		} else {
			$data['mpblog_facebook_color'] = 'light'; // light, dark
		}

		if (isset($this->request->post['mpblog_facebook_order'])) {
			$data['mpblog_facebook_order'] = $this->request->post['mpblog_facebook_order'];
		} elseif(isset($module_info['mpblog_facebook_order'])) {
			$data['mpblog_facebook_order'] = $module_info['mpblog_facebook_order'];
		} else {
			$data['mpblog_facebook_order'] = 'time'; //social, reverse_time, time
		}

		if (isset($this->request->post['mpblog_facebook_width'])) {
			$data['mpblog_facebook_width'] = $this->request->post['mpblog_facebook_width'];
		} elseif(isset($module_info['mpblog_facebook_width'])) {
			$data['mpblog_facebook_width'] = $module_info['mpblog_facebook_width'];
		} else {
			$data['mpblog_facebook_width'] = '500'; // min 320
		}

		// google

		if (isset($this->request->post['mpblog_comments_google'])) {
			$data['mpblog_comments_google'] = $this->request->post['mpblog_comments_google'];
		} elseif(isset($module_info['mpblog_comments_google'])) {
			$data['mpblog_comments_google'] = $module_info['mpblog_comments_google'];
		} else {
			$data['mpblog_comments_google'] = 0;
		}

		// disqus

		if (isset($this->request->post['mpblog_comments_disqus'])) {
			$data['mpblog_comments_disqus'] = $this->request->post['mpblog_comments_disqus'];
		} elseif(isset($module_info['mpblog_comments_disqus'])) {
			$data['mpblog_comments_disqus'] = $module_info['mpblog_comments_disqus'];
		} else {
			$data['mpblog_comments_disqus'] = 0;
		}

		if (isset($this->request->post['mpblog_comment_disqus_code'])) {
			$data['mpblog_comment_disqus_code'] = $this->request->post['mpblog_comment_disqus_code'];
		} elseif(isset($module_info['mpblog_comment_disqus_code'])) {
			$data['mpblog_comment_disqus_code'] = $module_info['mpblog_comment_disqus_code'];
		} else {
			$data['mpblog_comment_disqus_code'] = '';
		}

		if (isset($this->request->post['mpblog_comment_disqus_count'])) {
			$data['mpblog_comment_disqus_count'] = $this->request->post['mpblog_comment_disqus_count'];
		} elseif(isset($module_info['mpblog_comment_disqus_count'])) {
			$data['mpblog_comment_disqus_count'] = $module_info['mpblog_comment_disqus_count'];
		} else {
			$data['mpblog_comment_disqus_count'] = '';
		}

		// rss feed

		if (isset($this->request->post['mpblog_rssfeed_title'])) {
			$data['mpblog_rssfeed_title'] = $this->request->post['mpblog_rssfeed_title'];
		} elseif(isset($module_info['mpblog_rssfeed_title'])) {
			$data['mpblog_rssfeed_title'] = $module_info['mpblog_rssfeed_title'];
		} else {
			$data['mpblog_rssfeed_title'] =  $this->config->get('config_name') . ' Blog Feed';
		}
		
		if (isset($this->request->post['mpblog_rssfeed_description'])) {
			$data['mpblog_rssfeed_description'] = $this->request->post['mpblog_rssfeed_description'];
		} elseif(isset($module_info['mpblog_rssfeed_description'])) {
			$data['mpblog_rssfeed_description'] = $module_info['mpblog_rssfeed_description'];
		} else {
			$data['mpblog_rssfeed_description'] =  $this->config->get('config_name') . ' Blog Feed Description';
		}

		if (isset($this->request->post['mpblog_rssfeed_format'])) {
			$data['mpblog_rssfeed_format'] = $this->request->post['mpblog_rssfeed_format'];
		} elseif(isset($module_info['mpblog_rssfeed_format'])) {
			$data['mpblog_rssfeed_format'] = $module_info['mpblog_rssfeed_format'];
		} else {
			$data['mpblog_rssfeed_format'] =  'Atom';
		}

		if (isset($this->request->post['mpblog_rssfeed_limit'])) {
			$data['mpblog_rssfeed_limit'] = $this->request->post['mpblog_rssfeed_limit'];
		} elseif(isset($module_info['mpblog_rssfeed_limit'])) {
			$data['mpblog_rssfeed_limit'] = $module_info['mpblog_rssfeed_limit'];
		} else {
			$data['mpblog_rssfeed_limit'] =  50;
		}

		if (isset($this->request->post['mpblog_rssfeed_web_master'])) {
			$data['mpblog_rssfeed_web_master'] = $this->request->post['mpblog_rssfeed_web_master'];
		} elseif(isset($module_info['mpblog_rssfeed_web_master'])) {
			$data['mpblog_rssfeed_web_master'] = $module_info['mpblog_rssfeed_web_master'];
		} else {
			$data['mpblog_rssfeed_web_master'] =  'support@modulepoints.com (Module Points)';
		}
		
		if (isset($this->request->post['mpblog_rssfeed_copy_write'])) {
			$data['mpblog_rssfeed_copy_write'] = $this->request->post['mpblog_rssfeed_copy_write'];
		} elseif(isset($module_info['mpblog_rssfeed_copy_write'])) {
			$data['mpblog_rssfeed_copy_write'] = $module_info['mpblog_rssfeed_copy_write'];
		} else {
			$data['mpblog_rssfeed_copy_write'] =  '(&copy;) Module Points | M-Blog';
		}


		$this->load->model('setting/store');
		$data['stores'] = $this->model_setting_store->getStores();

		$store_info = $this->model_setting_store->getStore($data['store_id']);
		if($store_info) {
			$data['store_name'] = $store_info['name'];
		}else{
			$data['store_name'] = $this->language->get('text_default');
		}

		$data['user_token'] = $this->session->data['user_token'];

		$data['mpblogmenu'] = $this->load->controller('mpblog/mpblogmenu');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('mpblog/mpblog', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'mpblog/mpblog')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		

		if($this->request->post['mpblog_subscribeadminmail_status']) {
			foreach ($this->request->post['mpblog_subscribemail']['admin'] as $language_id => $value) {

				if ((utf8_strlen($value['subject']) < 3) || (utf8_strlen($value['subject']) > 255)) {
					$this->error['subscribemail']['admin'][$language_id]['subject'] = $this->language->get('error_subject');
				}

				if ((utf8_strlen($value['message']) < 10)) {
					$this->error['subscribemail']['admin'][$language_id]['message'] = $this->language->get('error_message');
				}
			}
		}

		if($this->request->post['mpblog_subscribeapproval_status']) {
			foreach ($this->request->post['mpblog_subscribemail']['approval'] as $language_id => $value) {
				if ((utf8_strlen($value['subject']) < 3) || (utf8_strlen($value['subject']) > 255)) {
					$this->error['subscribemail']['approval'][$language_id]['subject'] = $this->language->get('error_subject');
				}

				if ((utf8_strlen($value['message']) < 10)) {
					$this->error['subscribemail']['approval'][$language_id]['message'] = $this->language->get('error_message');
				}
			}
		}

		if($this->request->post['mpblog_subscribepending_status']) {
			foreach ($this->request->post['mpblog_subscribemail']['pending'] as $language_id => $value) {
				if ((utf8_strlen($value['subject']) < 3) || (utf8_strlen($value['subject']) > 255)) {
					$this->error['subscribemail']['pending'][$language_id]['subject'] = $this->language->get('error_subject');
				}

				if ((utf8_strlen($value['message']) < 10)) {
					$this->error['subscribemail']['pending'][$language_id]['message'] = $this->language->get('error_message');
				}
			}
		}

		if($this->request->post['mpblog_subscribeconfirm_status']) {
			foreach ($this->request->post['mpblog_subscribemail']['confirm'] as $language_id => $value) {
				if ((utf8_strlen($value['subject']) < 3) || (utf8_strlen($value['subject']) > 255)) {
					$this->error['subscribemail']['confirm'][$language_id]['subject'] = $this->language->get('error_subject');
				}

				if ((utf8_strlen($value['message']) < 10)) {
					$this->error['subscribemail']['confirm'][$language_id]['message'] = $this->language->get('error_message');
				}

				if ((utf8_strlen($value['title']) < 3) || (utf8_strlen($value['title']) > 255)) {
					$this->error['subscribemail']['confirm'][$language_id]['title'] = $this->language->get('error_pagetitle');
				}

				if ((utf8_strlen($value['content']) < 10)) {
					$this->error['subscribemail']['confirm'][$language_id]['content'] = $this->language->get('error_pagecontent');
				}
			}
		}

		if($this->request->post['mpblog_unsubscribe_status']) {
			foreach ($this->request->post['mpblog_subscribemail']['unsubscribe'] as $language_id => $value) {
				if ((utf8_strlen($value['subject']) < 3) || (utf8_strlen($value['subject']) > 255)) {
					$this->error['subscribemail']['unsubscribe'][$language_id]['subject'] = $this->language->get('error_subject');
				}

				if ((utf8_strlen($value['message']) < 10)) {
					$this->error['subscribemail']['unsubscribe'][$language_id]['message'] = $this->language->get('error_message');
				}

				if ((utf8_strlen($value['title']) < 3) || (utf8_strlen($value['title']) > 255)) {
					$this->error['subscribemail']['unsubscribe'][$language_id]['title'] = $this->language->get('error_pagetitle');
				}

				if ((utf8_strlen($value['content']) < 10)) {
					$this->error['subscribemail']['unsubscribe'][$language_id]['content'] = $this->language->get('error_pagecontent');
				}
			}
		}

		if($this->request->post['mpblog_subscribeconfirm_status'] || $this->request->post['mpblog_unsubscribe_status']) {

			foreach ($this->request->post['mpblog_subscribemail']['invalid'] as $language_id => $value) {
				
				if ((utf8_strlen($value['title']) < 3) || (utf8_strlen($value['title']) > 255)) {
					$this->error['subscribemail']['invalid'][$language_id]['title'] = $this->language->get('error_pagetitle');
				}

				if ((utf8_strlen($value['content']) < 10)) {
					$this->error['subscribemail']['invalid'][$language_id]['content'] = $this->language->get('error_pagecontent');
				}
			}
		}

		// Blog Add/Edit Emails
		if($this->request->post['mpblog_blogadd_status']) {
			foreach ($this->request->post['mpblog_emails']['blogadd'] as $language_id => $value) {
				if ((utf8_strlen($value['subject']) < 3) || (utf8_strlen($value['subject']) > 255)) {
					$this->error['emails']['blogadd'][$language_id]['subject'] = $this->language->get('error_subject');
				}

				if ((utf8_strlen($value['message']) < 10)) {
					$this->error['emails']['blogadd'][$language_id]['message'] = $this->language->get('error_message');
				}
			}
		}

		if($this->request->post['mpblog_blogedit_status']) {
			foreach ($this->request->post['mpblog_emails']['blogedit'] as $language_id => $value) {
				if ((utf8_strlen($value['subject']) < 3) || (utf8_strlen($value['subject']) > 255)) {
					$this->error['emails']['blogedit'][$language_id]['subject'] = $this->language->get('error_subject');
				}

				if ((utf8_strlen($value['message']) < 10)) {
					$this->error['emails']['blogedit'][$language_id]['message'] = $this->language->get('error_message');
				}
			}
		}
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	public function dashboard(){
		$data['user_token'] = $this->session->data['user_token'];


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('mpblog/dashboard', $data));
 	
	}
}