<?php
class ControllerMpBlogMpBlogMenu extends Controller {
	public function index() {

		$this->load->language('mpblog/mpblogmenu');

		$this->document->addStyle('view/stylesheet/modulepoints/mpblog.css');

		$this->document->addScript('view/javascript/mpblog/mpblog.js');

		// Menu
		$data['menus'][] = array(
			'id'       => 'menu-dashboard',
			'icon'	   => 'fa-dashboard',
			'name'	   => $this->language->get('text_mpblogdashboard'),
			'href'     => $this->url->link('mpblog/mpblogdashboard', 'user_token=' . $this->session->data['user_token'], true),
			'children' => array()
		);

		$data['menus'][] = array(
			'id'       => 'menu-mpblogcategory',
			'icon'	   => 'fa-list-ul',
			'name'	   => $this->language->get('text_mpblog_category'),
			'href'     => $this->url->link('mpblog/mpblogcategory', 'user_token=' . $this->session->data['user_token'], true),
			'children' => array()
		);

		$data['menus'][] = array(
			'id'       => 'menu-mpblogpost',
			'icon'	   => 'fa-pencil-square-o',
			'name'	   => $this->language->get('text_mpblog_post'),
			'href'     => $this->url->link('mpblog/mpblogpost', 'user_token=' . $this->session->data['user_token'], true),
			'children' => array()
		);

		$data['menus'][] = array(
			'id'       => 'menu-mpblogcomment',
			'icon'	   => 'fa-comments',
			'name'	   => $this->language->get('text_mpblog_comment'),
			'href'     => $this->url->link('mpblog/mpblogcomment', 'user_token=' . $this->session->data['user_token'], true),
			'children' => array()
		);

		$data['menus'][] = array(
			'id'       => 'menu-mpblograting',
			'icon'	   => 'fa-star',
			'name'	   => $this->language->get('text_mpblog_rating'),
			'href'     => $this->url->link('mpblog/mpblograting', 'user_token=' . $this->session->data['user_token'], true),
			'children' => array()
		);
		
		$data['menus'][] = array(
			'id'       => 'menu-mpblogsubscribers',
			'icon'	   => 'fa-users',
			'name'	   => $this->language->get('text_mpblog_subscriber'),
			'href'     => $this->url->link('mpblog/mpblogsubscriber', 'user_token=' . $this->session->data['user_token'], true),
			'children' => array()
		);
		
		$data['menus'][] = array(
			'id'       => 'menu-mpblogsetting',
			'icon'	   => 'fa-wrench',
			'name'	   => $this->language->get('text_mpblogsetting'),
			'href'     => $this->url->link('mpblog/mpblog', 'user_token=' . $this->session->data['user_token'], true),
			'children' => array()
		);


		$data['text_mpblog'] = $this->language->get('text_mpblog');

		return $this->load->view('mpblog/mpblogmenu', $data);
	}
}