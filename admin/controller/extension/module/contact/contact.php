<?php
class ControllerExtensionModuleContactContact extends Controller { 
	private $error = array();

	public function index() {
		$this->load->language('catalog/contact');

		$this->document->setTitle($this->language->get('heading_title'));
		 
		$this->load->model('extension/contact/contact');

		$this->getList();
	}

	public function insert() {
		$this->load->language('catalog/contact');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('extension/contact/contact');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_extension_contact_contact->addEvent($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->response->redirect($this->url->link('extension/module/contact/contact', 'user_token=' . $this->session->data['user_token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->load->language('catalog/contact');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('extension/contact/contact');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_extension_contact_contact->editEvent($this->request->get['contact_id'], $this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->response->redirect($this->url->link('extension/module/contact/contact', 'user_token=' . $this->session->data['user_token'] . $url, 'SSL'));
		}

		$this->getForm();
	}
 
	public function delete() {
		$this->load->language('catalog/contact');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('extension/contact/contact');
		
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $contact_id) {
				$this->model_extension_contact_contact->deleteContact($contact_id);
			}
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->response->redirect($this->url->link('extension/module/contact/contact', 'user_token=' . $this->session->data['user_token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	private function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'id.title';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], 'SSL'),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/module/contact/contact', 'user_token=' . $this->session->data['user_token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);
							
		$data['insert'] = $this->url->link('extension/module/contact/contact/insert', 'user_token=' . $this->session->data['user_token'] . $url, 'SSL');
		$data['delete'] = $this->url->link('extension/module/contact/contact/delete', 'user_token=' . $this->session->data['user_token'] . $url, 'SSL');	
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['button_rebuild'] = $this->language->get('button_rebuild');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['contacts'] = array();

		$datacontact = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		
		$contact_total = $this->model_extension_contact_contact->getTotalContacts();
	
		$results = $this->model_extension_contact_contact->getContacts($datacontact);
		
		
    	foreach ($results as $result) {
			$action = array();
						
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('extension/module/contact/contact/sendEmail', 'user_token=' . $this->session->data['user_token'] . '&contact_id=' . $result['contact_id'] . $url, 'SSL')
			);
						
			$data['contacts'][] = array(
				'selected'       		=> isset($this->request->post['selected']) && in_array($result['contact_id'], $this->request->post['selected']),
				'contact_id' 			=> $result['contact_id'],
				'status'          		=> $result['status'],
				'contact_date'     		=> $result['contact_date'],
				'reply'					=> $result['reply'],
				// 'phone'     			=> $result['phone'],
				'email'     			=> $result['email'],
				// 'title'     			=> $result['title'],
				'feedback'     			=> $result['feedback'],
				'fullname'     			=> $result['fullname'],
				'view'          		=> $this->url->link('extension/module/contact/contact/update', 'user_token=' . $this->session->data['user_token'] . '&contact_id=' . $result['contact_id'] . $url, true),
				'edit'          		=> $this->url->link('extension/module/contact/contact/update', 'user_token=' . $this->session->data['user_token'] . '&contact_id=' . $result['contact_id'] . $url, true),
				'delete'          		=> $this->url->link('extension/module/contact/contact/delete', 'user_token=' . $this->session->data['user_token'] . '&contact_id=' . $result['contact_id'] . $url, true),
				'action'         		=> $this->url->link('extension/module/contact/contact/sendEmail', 'user_token=' . $this->session->data['user_token'])
			);
		}	
		// echo '<pre>' , var_dump($data['contacts']) , '</pre>';die;
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_no_results'] = $this->language->get('text_no_results');

		// $data['column_title'] = $this->language->get('column_title');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');		
		
		$data['button_insert'] = $this->language->get('button_insert');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['subscribers'] = $this->language->get('subscribers');
		$data['subscribers_href'] = $this->url->link('extension/module/contact/contact/export', 'user_token=' . $this->session->data['user_token'], 'SSL');
		
 
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->get['success'])) {
			$data['success'] = $this->request->get['success'];
		}
		// echo '<pre>' , var_dump($data['success']) , '</pre>';die;

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['sort_title'] = $this->url->link('extension/module/contact/contact', 'user_token=' . $this->session->data['user_token'] . '&sort=id.title' . $url, 'SSL');
		$data['sort_sort_order'] = $this->url->link('extension/module/contact/contact', 'user_token=' . $this->session->data['user_token'] . '&sort=i.sort_order' . $url, 'SSL');
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $contact_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('extension/module/contact/contact', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', 'SSL');
			
		$data['pagination'] = $pagination->render();
		$data['results'] = sprintf($this->language->get('text_pagination'), ($contact_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($contact_total - $this->config->get('config_limit_admin'))) ? $contact_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $contact_total, ceil($contact_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		// echo '<pre>' , var_dump($data['success']) , '</pre>';die;
		$this->response->setOutput($this->load->view('extension/contact/contact_list', $data));
					
	}

	private function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$this->document->addScript('view/javascript/bootstrap/js/bootstrap-colorpicker.min.js');
		$this->document->addStyle('view/javascript/bootstrap/css/bootstrap-colorpicker.min.css');

		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
    	$data['text_disabled'] = $this->language->get('text_disabled');
		
		// $data['entry_title'] = $this->language->get('entry_title');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');
		$data['entry_date'] = $this->language->get('entry_date');
		$data['entry_allow_subscribe'] = $this->language->get('entry_allow_subscribe');
		$data['allow_no_subscribers'] = $this->language->get('allow_no_subscribers');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['text_image_manager'] = $this->language->get('text_image_manager');
		$data['button_send'] = $this->language->get('button_send');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['entry_products'] = $this->language->get('entry_products');
		
		$data['text_browse'] = $this->language->get('text_browse');
		$data['text_clear'] = $this->language->get('text_clear');
		
		$data['tab_general'] = $this->language->get('tab_general');
    	$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');
		
		$data['user_token'] = $this->session->data['user_token'];

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

 		// if (isset($this->error['title'])) {
		// 	$data['error_title'] = $this->error['title'];
		// } else {
		// 	$data['error_title'] = array();
		// }
		
	 	if (isset($this->error['description'])) {
			$data['error_description'] = $this->error['description'];
		} else {
			$data['error_description'] = array();
		}
		
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL')
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/module/contact/contact', 'user_token=' . $this->session->data['user_token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);
							
		// if (!isset($this->request->get['contact_id'])) {
		// 	$data['action'] = $this->url->link('extension/module/contact/contact/insert', 'user_token=' . $this->session->data['user_token'] . $url, 'SSL');
		// } else {
		// 	$data['action'] = $this->url->link('extension/module/contact/contact/update', 'user_token=' . $this->session->data['user_token'] . '&contact_id=' . $this->request->get['contact_id'] . $url, 'SSL');
		// }
		
		$data['cancel'] = $this->url->link('extension/module/contact/contact', 'user_token=' . $this->session->data['user_token'] . $url, 'SSL');

		if (isset($this->request->get['contact_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$contact_info = $this->model_extension_contact_contact->getContact($this->request->get['contact_id']);
		}
		
		$this->load->model('localisation/language');
		
		$data['languages'] = $this->model_localisation_language->getLanguages();
		$this->load->model('setting/store');
		$data['stores'] = $this->model_setting_store->getStores();
		// if (isset($this->request->post['contact_description'])) {
		// 	$data['contact_description'] = $this->request->post['contact_description'];
		// } elseif (isset($this->request->get['contact_id'])) {
		// 	$data['contact_description'] = $this->model_extension_contact_contact->getEventDescriptions($this->request->get['contact_id']);
		// } else {
		// 	$data['contact_description'] = array();
		// }
		// echo '<pre>' , var_dump($contact_info) , '</pre>';die;


		$data['contact_id'] = $contact_info['contact_id'];
		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (isset($contact_info)) {
			$data['status'] = $contact_info['status'];
		} else {
			$data['status'] = 1;
		}
		
		if (isset($this->request->post['contact_date'])) {
			$data['contact_date'] = $this->request->post['contact_date'];
		} elseif (isset($contact_info)) {
			$data['contact_date'] = $contact_info['contact_date'];
		} else {
			$data['contact_date'] = date('Y-m-d');
		}

		// if (isset($this->request->post['title'])) {
		// 	$data['title'] = $this->request->post['title'];
		// } elseif (isset($contact_info)) {
		// 	$data['title'] = $contact_info['title'];
		// } else {
		// 	$data['title'] = 'No title';
		// }

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} elseif (isset($contact_info)) {
			$data['email'] = $contact_info['email'];
		} else {
			$data['email'] = 'No email';
		}

		if (isset($this->request->post['feedback'])) {
			$data['feedback'] = $this->request->post['feedback'];
		} elseif (isset($contact_info)) {
			$data['feedback'] = $contact_info['feedback'];
		} else {
			$data['feedback'] = 'No feedback';
		}

		// if (isset($this->request->post['phone'])) {
		// 	$data['phone'] = $this->request->post['phone'];
		// } elseif (isset($contact_info)) {
		// 	$data['phone'] = $contact_info['phone'];
		// } else {
		// 	$data['phone'] = 'No phone';
		// }

		if (isset($this->request->post['fullname'])) {
			$data['fullname'] = $this->request->post['fullname'];
		} elseif (isset($contact_info)) {
			$data['fullname'] = $contact_info['fullname'];
		} else {
			$data['fullname'] = 'No fullname';
		}

		if (isset($this->request->post['reply'])) {
			$data['reply'] = $this->request->post['reply'];
		} elseif (isset($contact_info)) {
			$data['reply'] = $contact_info['reply'];
		} else {
			$data['reply'] = 'No reply';
		}
		// if (isset($this->request->post['allow_subscribe'])) {
		// 	$data['allow_subscribe'] = $this->request->post['allow_subscribe'];
		// } elseif (isset($contact_info)) {
		// 	$data['allow_subscribe'] = $contact_info['allow_subscribe'];
		// } else {
		// 	$data['allow_subscribe'] = '';
		// }
		// if (isset($this->request->post['image'])) {
		// 	$data['image'] = $this->request->post['image'];
		// } elseif (isset($contact_info)) {
		// 	$data['image'] = $contact_info['image'];
		// } else {
		// 	$data['image'] = '';
		// }
		// $this->load->model('tool/image');

		// if (isset($contact_info) && $contact_info['image'] && file_exists(DIR_IMAGE . $contact_info['image'])) {
		// 	$data['preview'] = $this->model_tool_image->resize($contact_info['image'], 100, 100);
		// } else {
		// 	$data['preview'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		// }
		
		// $data['no_image'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		
		
		// $this->load->model('setting/store');
		
		// $data['stores'] = $this->model_setting_store->getStores();
		
		// if (isset($this->request->post['contact_store'])) {
		// 	$data['contact_store'] = $this->request->post['contact_store'];
		// } elseif (isset($contact_info)) {
		// 	$data['contact_store'] = $this->model_extension_contact_contact->getEventStores($this->request->get['contact_id']);
		// } else {
		// 	$data['contact_store'] = array(0);
		// }		
		
		// if (isset($this->request->post['keyword'])) {
		// 	$data['keyword'] = $this->request->post['keyword'];
		// } elseif (isset($contact_info)) {
		// 	$data['keyword'] = $contact_info['keyword'];
		// } else {
		// 	$data['keyword'] = '';
		// }
		
		// if (isset($this->request->post['sort_order'])) {
		// 	$data['sort_order'] = $this->request->post['sort_order'];
		// } elseif (isset($contact_info)) {
		// 	$data['sort_order'] = $contact_info['sort_order'];
		// } else {
		// 	$data['sort_order'] = '';
		// }
		
		// if (isset($this->request->post['contact_layout'])) {
		// 	$data['contact_layout'] = $this->request->post['contact_layout'];
		// } elseif (isset($contact_info)) {
		// 	$data['contact_layout'] = $this->model_extension_contact_contact->getEventLayouts($this->request->get['contact_id']);
		// } else {
		// 	$data['contact_layout'] = array();
		// }
		
		
		// if (isset($this->request->post['no_of_sub'])) {
		// 	$data['no_of_sub'] = $this->request->post['no_of_sub'];
		// } elseif (isset($contact_info)) {
		// 	$data['no_of_sub'] = $contact_info['no_of_sub'];
		// } else {
		// 	$data['no_of_sub'] = '';
		// }

		// if (isset($this->request->post['color'])) {
		// 	$data['color'] = $this->request->post['color'];
		// } elseif (isset($contact_info)) {
		// 	$data['color'] = $contact_info['color'];
		// } else {
		// 	$data['color'] = '';
		// }

		// if (isset($this->request->post['reminder_numdays'])) {
		// 	$data['reminder_numdays'] = $this->request->post['reminder_numdays'];
		// } elseif (isset($contact_info)) {
		// 	$data['reminder_numdays'] = $contact_info['reminder_numdays'];
		// } else {
		// 	$data['reminder_numdays'] = '';
		// }

		// if (isset($this->request->post['customer_group'])) {
		// 	$data['customer_group'] = $this->request->post['customer_group'];
		// } elseif (isset($contact_info)) {
		// 	$data['customer_group'] = explode(',', $contact_info['customer_group']);
		// } else {
		// 	$data['customer_group'] = array();
		// }

		// $this->load->model('design/layout');
		
		// $data['layouts'] = $this->model_design_layout->getLayouts();
		
		
		// $data['product_related'] = array();
		// if (isset($this->request->post['product_related'])) {
		// 	$products = $this->request->post['product_related'];
		// } elseif (isset($contact_info) && $contact_info['product_related']!='') {		
		// 	$products = unserialize($contact_info['product_related']);
		// } else {
		// 	$products = array();
		// }
		// $this->load->model('catalog/product');
		// foreach ($products as $product_id) {
		// 	$related_info = $this->model_catalog_product->getProduct($product_id);
			
		// 	if ($related_info) {
		// 		$data['product_related'][] = array(
		// 			'product_id' => $related_info['product_id'],
		// 			'name'       => $related_info['name']
		// 		);
		// 	}
		// }

		// $this->load->model('customer/customer_group');
		// $data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups();
		$data['action'] = $this->url->link('extension/module/contact/contact/sendEmail', 'user_token=' . $this->session->data['user_token']);
		// echo '<pre>' , var_dump($data['action']) , '</pre>';die;
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('extension/contact/contact_form', $data));
	}

	private function validateForm() {
		// if (!$this->user->hasPermission('modify', 'extension/module/contact/contact')) {
		// 	$this->error['warning'] = $this->language->get('error_permission');
		// }

		// foreach ($this->request->post['contact_description'] as $language_id => $value) {
		// 	if ((strlen(utf8_decode($value['title'])) < 3) || (strlen(utf8_decode($value['title'])) > 64)) {
		// 		$this->error['title'][$language_id] = $this->language->get('error_title');
		// 	}
		
		// 	if (strlen(utf8_decode($value['description'])) < 3) {
		// 		$this->error['description'][$language_id] = $this->language->get('error_description');
		// 	}
		// }
		
		// if ($this->error && !isset($this->error['warning'])) {
		// 	$this->error['warning'] = $this->language->get('error_warning');
		// }
			
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	private function validateDelete() {
		if (!$this->user->hasPermission('modify', 'extension/module/contact/contact')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	
	public function export() {
		$sql = "select fname,lname,phone,email,comments,dtadd from ".DB_PREFIX."contact_subscribers where contact_id='".$this->request->get['contact_id']."'";
		$query = $this->db->query($sql);
		$results = $query->rows;
		$out = "First Name, Last Name, Phone, Email, Comments, Date \n";
		foreach ($results as $result) {
			$out .='"'.$result['fname'].'",';
			$out .='"'.$result['lname'].'",';
			$out .='"'.$result['phone'].'",';
			$out .='"'.$result['email'].'",';
			$out .='"'.$result['comments'].'",';
			$out .='"'.$result['dtadd'].'",';
			$out .="\n";
		}
				
		header("Content-type: text/x-csv");
		//header("Content-type: text/csv");
		//header("Content-type: application/csv");
		header("Content-Disposition: attachment; filename=subscribers.csv");
		echo $out;
		die();
	}	

	/**
	 * Send notification to subscribers,.
	 */
	public function send_notification() 
	{
		$this->load->model('extension/contact/contact');
		$this->load->model('setting/store');
		$sql = "select * from ".DB_PREFIX."contact_subscribers where notification_sent = 0";
		$query = $this->db->query($sql);
		$results = $query->rows;
		$today = date('Y-m-d');
		foreach ($results as $k => $v) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "contacts i LEFT JOIN " . DB_PREFIX . "contact_description id ON (i.contact_id = id.contact_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' AND i.contact_id = " . $v['contact_id']);
			$contact = $query->row;
			if (!$contact['reminder_numdays']) {
				continue;
			}
			$reminder_date = date('Y-m-d', strtotime($contact['contact_date'] . "-" . $contact['reminder_numdays'] . " days"));
			$store_info = $this->model_setting_store->getStore($this->config->get('config_store_id'));
			if ($store_info) {
				$store_name = $store_info['name'];
			} else {
				$store_name = $this->config->get('config_name');
			}
			if ($today == $reminder_date) {
				$subject = "Reminder for " . $contact['title'];
				$message = 'Hello ' . $v['fname'] . '<br><br>'; 
				$message.= 'You asked us to send you a reminder for the following contact.'; 
				$message.= '<br><br>';
				$message.= '<h2>' . $contact['title'] . '</h2>';
				$message.= date('W, F d, Y', strtotime($contact['contact_date']));
				$message.= '<br><br>You can view the details here: ' . $this->url->link('information/contact', 'contact_id=' . $v['contact_id'], 'SSL');
				$message.= '<br><br>Best Regards,<br>';
				$message.= $store_name;
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
				$mail->smtp_username = $this->config->get('config_mail_smtp_username');
				$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
				$mail->smtp_port = $this->config->get('config_mail_smtp_port');
				$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

				$mail->setTo($v['email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender(html_entity_decode($store_name, ENT_QUOTES, 'UTF-8'));
				$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
				$mail->setHtml($message);
				$mail->send();
				$this->db->query("UPDATE ".DB_PREFIX."contact_subscribers 
					SET notification_sent = 1
					WHERE id = " . $v['id']);
			}
		}
	}

	public function sendEmail()
	{
		$this->load->language('catalog/contact');

		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && isset($_POST['fullnameshop'])  ) 
		{	
			$contact_id = $_POST['contact_id'];
			$shop = $_POST['fullnameshop'];
			// $title = $_POST['titleshop'];
			$feedback = $_POST['feedbackshop'];
			$fullname = $_POST['fullname'];
			$email = $_POST['email'];
			$this->load->model('setting/store');

			$store_info = $this->model_setting_store->getStore($this->request->post['store_id']);

			if ($store_info) {
				$store_name = $store_info['name'];
			} else {
				$store_name = $this->config->get('config_name');
			}
			
			// $contact_info = $this->model_extension_contact_contact->getContact($this->request->get['contact_id']);
			// $contact_info['reply'] = $feedback;
			// echo '<pre>' , var_dump($feedback) , '</pre>';die;
			$this->load->model('extension/contact/contact');
			$this->model_extension_contact_contact->updateContact($contact_id, $this->request->post);
			// echo '<pre>' , var_dump("ok") , '</pre>';die;
			$this->load->model('setting/setting');
			$setting = $this->model_setting_setting->getSetting('config', $this->request->post['store_id']);
			$store_email = isset($setting['config_email']) ? $setting['config_email'] : $this->config->get('config_email');


			$message  = '<html dir="ltr" lang="en">' . "\n";
			$message .= '  <head>' . "\n";
			$message .= '    <title>' . $shop . '</title>' . "\n";
			$message .= '    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . "\n";
			$message .= '    <meta http-equiv="Content-Type" content="image/jpeg; charset=UTF-8">' . "\n";
			$message .= '  </head>' . "\n";
			$message .= '  <body>' . $shop ." xin chào ". $fullname  .
			'  <p>Chúng tôi cảm ơn phản hồi của bạn .</p>'  .
			// '  <p>Rất vui vì được bạn phản hồi .</p>'  .
			html_entity_decode($feedback, ENT_QUOTES, 'UTF-8')  ;
			$message .= '<p>Rất vui vì được bạn phản hồi .</p>'. '</body>' . "\n" . '</html>' . "\n";
			

			$mail = new Mail($this->config->get('config_mail_engine'));
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
			$mail->SMTPSecure = 'tls';///////////////////////////////
			$mail->setTo($email);
			$mail->setFrom($store_email);
			$mail->setSender(html_entity_decode($store_name, ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(html_entity_decode($store_name, ENT_QUOTES, 'UTF-8'));
			$mail->setHtml($message);
			$mail->send();

			// $data['success'] = 'OK';
			$url = '';
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/module/contact/contact', 'user_token=' . $this->session->data['user_token']  . $url, 'SSL'));
		}
	}
}
?>