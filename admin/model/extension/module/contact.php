<?php
class ModelExtensionModuleContact extends Model {
	
	public function createDatabaseTables() {
		$sql  = "
				CREATE TABLE IF NOT EXISTS `".DB_PREFIX."contact` (
				  `contact_id` int(11) NOT NULL auto_increment,
				  `reply` text default NULL,
				  `sort_order` int(3) NOT NULL default '0',
				  `status` int(1) NOT NULL default '0',
				  `contact_date` date default NULL,
				  `fullname` varchar(255) default NULL,
				  `feedback` varchar(255) default NULL,
				  `email` varchar(255) default NULL,
				  PRIMARY KEY  (`contact_id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;";
		$this->db->query($sql);
	}

	public function dropDatabaseTables() {
		$sql = "DROP TABLE IF EXISTS `".DB_PREFIX."contact`;";
		$this->db->query($sql);
	}
}
?>