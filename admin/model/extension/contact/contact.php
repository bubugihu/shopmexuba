<?php
class ModelExtensionContactContact extends Model {
	public function addContact($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "contact SET sort_order = '" . (int)$this->request->post['sort_order'] . "', status = '" . (int)$data['status'] . "', contact_date = '" . $this->request->post['contact_date'] . "', allow_subscribe = '" . $this->request->post['allow_subscribe'] . "', no_of_sub = '" . $this->request->post['no_of_sub'] . "', customer_group = '" . @implode(',', $this->request->post['customer_group']) . "', 
			color = '" . $this->request->post['color'] . "'");


		$contact_id = $this->db->getLastId(); 
		
		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "contact SET image = '" . $this->db->escape($data['image']) . "' WHERE contact_id = '" . (int)$contact_id . "'");
		}
		if (isset($data['product_related']) && is_array($data['product_related'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "contact SET product_related = '" . serialize($data['product_related']). "' WHERE contact_id = '" . (int)$contact_id . "'");
		}
		
			
		foreach ($data['contact_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "contact_description SET contact_id = '" . (int)$contact_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}
		
		if (isset($data['contact_store'])) {
			foreach ($data['contact_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "contact_to_store SET contact_id = '" . (int)$contact_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		if (isset($data['contact_layout'])) {
			foreach ($data['contact_layout'] as $store_id => $layout) {
				if ($layout) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "contact_to_layout SET contact_id = '" . (int)$contact_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout['layout_id'] . "'");
				}
			}
		}
				
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET query = 'contact_id=" . (int)$contact_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
		
		$this->cache->delete('contact');
	}
	public function updateContact($contact_id, $data)
	{	
		// echo '<pre>' , var_dump($data) , '</pre>';die;
		// echo '<pre>' , var_dump("UPDATE " . DB_PREFIX . "contact SET reply ='". $data['feedbackshop']."'" .' WHERE contact_id = ' . (int)$contact_id) , '</pre>';die;
		$this->db->query("UPDATE " . DB_PREFIX . "contact SET reply ='". $data['feedbackshop']."'" .' WHERE contact_id = ' . (int)$contact_id);
	}
	public function editContact($contact_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "contact SET reply ='". $data['feedbackshop']  ."' ,sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "',contact_date = '" . $data['contact_date'] . "', allow_subscribe = '" . $data['allow_subscribe'] . "', no_of_sub = '" . $data['no_of_sub'] . "', customer_group = '" . @implode(',', $this->request->post['customer_group']) . "', color = '" . $this->request->post['color'] . "' WHERE contact_id = '" . (int)$contact_id . "'");
		
		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "contact SET image = '" . $this->db->escape($data['image']) . "' WHERE contact_id = '" . (int)$contact_id . "'");
		}
		
		if (isset($data['product_related']) && is_array($data['product_related'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "contact SET product_related = '" . serialize($data['product_related']). "' WHERE contact_id = '" . (int)$contact_id . "'");
		}else{
			$this->db->query("UPDATE " . DB_PREFIX . "contact SET product_related = '' WHERE contact_id = '" . (int)$contact_id . "'");
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "contact_description WHERE contact_id = '" . (int)$contact_id . "'");
					
		foreach ($data['contact_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "contact_description SET contact_id = '" . (int)$contact_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "contact_to_store WHERE contact_id = '" . (int)$contact_id . "'");
		
		if (isset($data['contact_store'])) {
			foreach ($data['contact_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "contact_to_store SET contact_id = '" . (int)$contact_id . "', store_id = '" . (int)$store_id . "'");
			}
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "contact_to_layout WHERE contact_id = '" . (int)$contact_id . "'");

		if (isset($data['contact_layout'])) {
			foreach ($data['contact_layout'] as $store_id => $layout) {
				if ($layout['layout_id']) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "contact_to_layout SET contact_id = '" . (int)$contact_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout['layout_id'] . "'");
				}
			}
		}
				
		$this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'contact_id=" . (int)$contact_id. "'");
		
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET query = 'contact_id=" . (int)$contact_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
		
		$this->cache->delete('contact');
	}
	
	public function deleteContact($contact_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "contact WHERE contact_id = '" . (int)$contact_id . "'");
		

		$this->cache->delete('contact');
	}	

	public function getContact($contact_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "seo_url WHERE query = 'contact_id=" . (int)$contact_id . "') AS keyword FROM " . DB_PREFIX . "contact WHERE contact_id = '" . (int)$contact_id . "'");
		
		return $query->row;
	}
		
	public function getContacts($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "contact  WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "'";
		
			$sort_data = array(
				'status',
				
				'contact_id'
			);		
		
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY id.title";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
		// if (isset($data['start']) || isset($data['limit'])) {
		// 		// if ($data['start'] < 0) {
		// 		// 	$data['start'] = 0;
		// 		// }		

		// 		// if ($data['limit'] < 1) {
		// 		// 	$data['limit'] = 20;
		// 		// }	
			
		// 		$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		// 	}	
			
		// 	$query = $this->db->query($sql);
			
		// 	return $query->rows;
		// } else {
			

			$contact_data = $this->cache->get('contact.' . $this->config->get('config_language_id'));
			
			// if (!$contact_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "contact ");
				
				$contact_data = $query->rows;
				// echo '<pre>' , var_dump('hello') , '</pre>';die;
			
			// }	
	
			return $contact_data;			
		}
	}
	
	public function getContactDescriptions($contact_id) {
		$contact_description_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "contact_description WHERE contact_id = '" . (int)$contact_id . "'");

		foreach ($query->rows as $result) {
			$contact_description_data[$result['language_id']] = array(
				'title'       => $result['title'],
				'description' => $result['description']
			);
		}
		
		return $contact_description_data;
	}
	
	public function getContactStores($contact_id) {
		$contact_store_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "contact_to_store WHERE contact_id = '" . (int)$contact_id . "'");

		foreach ($query->rows as $result) {
			$contact_store_data[] = $result['store_id'];
		}
		
		return $contact_store_data;
	}

	public function getContactLayouts($contact_id) {
		$contact_layout_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "contact_to_layout WHERE contact_id = '" . (int)$contact_id . "'");
		
		foreach ($query->rows as $result) {
			$contact_layout_data[$result['store_id']] = $result['layout_id'];
		}
		
		return $contact_layout_data;
	}
		
	public function getTotalContacts() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "contact");
		
		return $query->row['total'];
	}	
	
	public function getTotalContactsByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "contact_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}	
}
?>