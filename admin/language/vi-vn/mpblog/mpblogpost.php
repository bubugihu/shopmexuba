<?php
// Tiêu đề
$_['heading_title'] = 'Blog';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi blog!';
$_['text_list'] = 'Danh sách blog';
$_['text_add'] = 'Thêm Blog';
$_['text_edit'] = 'Chỉnh sửa Blog';
$_['text_plus'] = '+';
$_['text_minus'] = '-';
$_['text_default'] = 'Mặc định';
$_['text_keyword'] = 'Không sử dụng dấu cách, thay vào đó hãy thay dấu cách bằng - và đảm bảo rằng URL SEO là duy nhất trên toàn cầu.';

$_['text_post_artical'] = 'Bài báo';
$_['text_post_video'] = 'Video';
$_['text_post_images'] = 'Hình ảnh / Hình ảnh';


// Cột
$_['column_name'] = 'Tên Blog';
$_['column_totalcomment'] = 'Tổng số nhận xét';
$_['column_author'] = 'Tác giả';
$_['column_image'] = 'Hình ảnh';
$_['column_status'] = 'Đã xuất bản';
$_['column_action'] = 'Hành động';

// Mục nhập
$_['entry_name'] = 'Tên Blog';
$_['entry_sdescription'] = 'Mô tả ngắn gọn';
$_['entry_description'] = 'Mô tả';
$_['entry_meta_title'] = 'Tiêu đề thẻ meta';
$_['entry_meta_keyword'] = 'Từ khóa thẻ meta';
$_['entry_meta_description'] = 'Mô tả thẻ meta';
$_['entry_keyword'] = 'URL SEO';
$_['entry_author'] = 'Tác giả';
$_['entry_store'] = 'Cửa hàng';
$_['entry_image'] = 'Hình ảnh';
$_['entry_additional_image'] = 'Hình ảnh bổ sung';
$_['entry_video'] = 'URL video (Youtube)';
$_['entry_status'] = 'Đã xuất bản';
$_['entry_posttype'] = 'Loại bài đăng';
$_['entry_date_available'] = 'Ngày đăng';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';
$_['entry_mpblogcategory'] = 'Danh mục blog';
$_['entry_related']          = 'Blog liên quan';
$_['entry_relatedcategory']          = 'Danh mục Liên quan';
$_['entry_relatedproduct']          = 'Sản phẩm Liên quan';
$_['entry_inity'] = 'Blog liên quan';
$_['entry_osystemcategory'] = 'Danh mục Liên quan';
$_['entry_osystemproduct'] = 'Sản phẩm Liên quan';
$_['entry_tag'] = 'Thẻ Blog';
//add
$_['entry_image_order']      = 'Thứ tự ảnh <br/>1,2 Hình lớn<br/> 3,4,5,6 Hình nhỏ';                    
//end add
// Help
$_['help_keyword'] = 'Không sử dụng dấu cách, thay vào đó hãy thay dấu cách bằng - và đảm bảo rằng URL SEO là duy nhất trên toàn cầu.';
$_['help_author'] = '(Tự động điền)';
$_['help_mpblogcategory'] = '(Tự động hoàn thành)';
$_['trợ giúp_ có liên quan'] = '(Tự động điền)';
$_['help_tag'] = 'Phân cách bằng dấu phẩy';
$_['help_video'] = 'Tùy chọn, chỉ được sử dụng nếu loại bài đăng là video';
$_['help_posttype'] = 'Chọn loại blog';
$_['help_osystemproduct'] = '(Tự động hoàn thành)';
$_['help_osystemcategory'] = '(Tự động hoàn thành)';

// Lỗi
$_['error_warning'] = 'Cảnh báo: Vui lòng kiểm tra kỹ biểu mẫu để tìm lỗi!';
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi blog!';
$_['error_name'] = 'Tên Blog phải lớn hơn 3 và ít hơn 255 ký tự!';
$_['error_meta_title'] = 'Meta Title phải lớn hơn 3 và ít hơn 255 ký tự!';
$_['error_author'] = 'Tác giả Blog phải lớn hơn 1 và ít hơn 64 ký tự!';
$_['error_keyword'] = 'URL SEO đã được sử dụng!';
$_['error_unique'] = 'URL SEO phải là duy nhất!';