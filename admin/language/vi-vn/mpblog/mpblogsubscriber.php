<?php
// Phần mở đầu
$_['heading_title'] = 'Người đăng ký blog';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi người đăng ký blog!';
$_['text_list'] = 'Danh sách người đăng ký blog';
$_['text_add'] = 'Thêm người đăng ký blog';
$_['text_edit'] = 'Chỉnh sửa người đăng ký blog';
$_['text_missing'] = 'Thiếu';
$_['text_guest'] = 'Khách';

// Cột
$_['column_mpblogpost'] = 'Blog';

$_['column_email'] = 'E-mail';
$_['column_customer'] = 'Người đăng ký';
$_['column_store'] = 'Cửa hàng';
$_['column_language'] = 'Ngôn ngữ';
$_['column_status'] = 'Trạng thái';
$_['column_date_added'] = 'Ngày Thêm';
$_['column_date_modified'] = 'Ngày sửa đổi';
$_['column_action'] = 'Hành động';

// Mục nhập
$_['entry_email'] = 'E-mail';
$_['entry_customer'] = 'Người đăng ký';
$_['entry_sendmail'] = 'Gửi E-mail Phê duyệt';
$_['entry_status'] = 'Trạng thái';
$_['entry_date_added'] = 'Ngày thêm';
$_['entry_date_modified'] = 'Ngày sửa đổi';

// Cứu giúp
$_['help_mpblogpost'] = '(Tự động điền)';
$_['help_sendmail'] = 'Gửi E-mail Phê duyệt đến người đăng ký. Phương pháp hay nhất để gửi email phê duyệt khi trạng thái của chúng bị vô hiệu hóa ';

// Lỗi
$_['error_warning'] = 'Cảnh báo: Vui lòng kiểm tra kỹ biểu mẫu để tìm lỗi!';
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi người đăng ký blog!';
$_['error_email'] = 'Địa chỉ E-mail dường như không hợp lệ!';
$_['error_exists'] = 'Cảnh báo: Địa chỉ Email đã được đăng ký!';