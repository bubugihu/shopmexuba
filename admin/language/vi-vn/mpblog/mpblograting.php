<?php
// Phần mở đầu
$_['heading_title'] = 'Xếp hạng blog';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi xếp hạng blog!';
$_['text_list'] = 'Danh sách Xếp hạng Blog';
$_['text_add'] = 'Thêm Xếp hạng Blog';
$_['text_edit'] = 'Chỉnh sửa Xếp hạng Blog';

// Cột
$_['column_mpblogpost'] = 'Blog';

$_['column_rating'] = 'Xếp hạng';
$_['column_status'] = 'Trạng thái';
$_['column_date_added'] = 'Ngày Thêm';
$_['column_action'] = 'Hành động';

// Mục nhập
$_['entry_mpblogpost'] = 'Blog';
$_['entry_rating'] = 'Xếp hạng';
$_['entry_status'] = 'Trạng thái';
$_['entry_date_added'] = 'Ngày thêm';

// Cứu giúp
$_['help_mpblogpost'] = '(Tự động điền)';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi xếp hạng blog!';
$_['error_mpblogpost'] = 'Cần có blog!';
$_['error_rating'] = 'Yêu cầu xếp hạng Xếp hạng Blog!';