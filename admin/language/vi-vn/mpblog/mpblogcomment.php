<?php
// Phần mở đầu
$_['heading_title'] = 'Nhận xét trên blog';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi nhận xét trên blog!';
$_['text_list'] = 'Danh sách bình luận trên blog';
$_['text_add'] = 'Thêm bình luận trên blog';
$_['text_edit'] = 'Chỉnh sửa bình luận trên blog';

// Cột
$_['column_mpblogpost'] = 'Blog';
$_['column_author'] = 'Tác giả';
$_['column_status'] = 'Xuất bản';
$_['column_date_added'] = 'Ngày Thêm';
$_['column_action'] = 'Hành động';

// Mục nhập
$_['entry_mpblogpost'] = 'Blog';
$_['entry_author'] = 'Tác giả';
$_['entry_status'] = 'Xuất bản';
$_['entry_text'] = 'Văn bản';
$_['entry_date_added'] = 'Ngày thêm';

// Cứu giúp
$_['help_mpblogpost'] = '(Tự động điền)';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi các bình luận trên blog!';
$_['error_mpblogpost'] = 'Cần có blog!';
$_['error_author'] = 'Tác giả phải có từ 3 đến 64 ký tự!';
$_['error_text'] = 'Văn bản Nhận xét Blog phải có ít nhất 1 ký tự!';
$_['error_rating'] = 'Yêu cầu xếp hạng Nhận xét trên Blog!';