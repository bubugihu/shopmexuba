<?php
// Phần mở đầu
$_['heading_title'] = 'Cài đặt Blog';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã cập nhật cài đặt blog!';
$_['text_configuration'] = 'Cấu hình: Bạn cần nhấn nút Lưu một lần để cấu hình blog vào hệ thống của bạn. Đây là một lần duy nhất. Tất cả các yêu cầu được điền bằng các giá trị mặc định. Bạn có thể thay đổi chúng theo yêu cầu của bạn. ';

$_['text_edit'] = 'Chỉnh sửa Cài đặt Blog';

$_['text_store'] = 'Cửa hàng';
$_['text_column'] = '- Cột';
$_['text_top'] = 'Lên trên';
$_['text_bottom'] = 'Dưới cùng';
$_['text_grid'] = 'Lưới';
$_['text_list'] = 'Danh sách';
$_['text_mpblog'] = 'Blog';
$_['text_mpblog_category'] = 'Chuyên mục Blog';
$_['text_mpblog_post'] = 'Bài đăng trên blog';
$_['text_mpblog_comment'] = 'Bình luận trên blog';
$_['text_mpblog_review'] = 'Xếp hạng blog';

// cách hoạt động của cột
$_['text_column_title'] = 'Cách hoạt động của cột blog trên mỗi hàng?';
$_['text_column_defination'] = 'Chọn Số lượng Blog để Hiển thị Mỗi Hàng. Ví dụ 1 Blog mỗi hàng. 3 Blog mỗi hàng. 6 Blog mỗi hàng. Vẫn chưa rõ ràng ?. Hãy tạo các bài viết khác nhau: <br> 1 Blog mỗi hàng <br> 3 Blog mỗi hàng <br> 2 Blog mỗi hàng <br> 6 Blog mỗi hàng. ';

$_['text_img_size_title'] = 'Kích thước hình ảnh blog';
$_['text_img_size_defination'] = 'Cung cấp Kích thước Kích thước Hình ảnh Blog Chiều rộng X Chiều cao. Ví dụ: 200 X 120 ';
$_['text_cimg_size_title'] = 'Kích cỡ ảnh danh mục';
$_['text_cimg_size_defination'] = 'Cho Category Blog Kích thước Hình ảnh Kích thước Chiều rộng X Chiều cao. Ví dụ: 200 X 120 ';

$_['text_socialmedia_title'] = 'Liên kết mạng xã hội';
$_['text_socialmedia_defination'] = 'Thêm các liên kết truyền thông xã hội được hiển thị trong mỗi blog. Chọn Nếu Bạn muốn Hiển thị Thông tin Truyền thông Xã hội trên Blog trên cùng hoặc dưới cùng. Thêm phương tiện truyền thông xã hội theo lựa chọn của bạn. Bạn cũng có thể chia sẻ blog cụ thể bằng cách sử dụng plugin Sharethis. ';

$_['text_comment_title'] = 'Hệ thống bình luận trên blog';
$_['text_comment_defination'] = 'Thêm Hệ thống Nhận xét vào Blog. Chọn Loại Hệ thống Nhận xét cho Blog của Bạn. Bạn có nhiều lựa chọn về hệ thống bình luận để phù hợp nhất với nhu cầu của bạn. ';


$_['text_category_home'] = 'Trình đơn Trang chủ Blog';
$_['text_category_post_count'] = 'Hiển thị số lượng bài đăng danh mục';
$_['text_category_show_image'] = 'Hiển thị hình ảnh chính của danh mục';
$_['text_category_show_description'] = 'Hiển thị mô tả danh mục';
$_['text_category_page_limit'] = 'Giới hạn trang danh mục';
$_['text_category_design'] = 'Blog mỗi hàng';

$_['text_blog_show_image'] = 'Hiển thị hình ảnh chính của blog';
$_['text_blog_show_image_popup'] = 'Hiển thị hình ảnh chính của blog trong cửa sổ bật lên';
$_['text_blog_show_description'] = 'Hiển thị mô tả';
$_['text_blog_show_sdescription'] = 'Hiển thị mô tả ngắn gọn';
$_['text_blog_sdescription_length'] = 'Độ dài mô tả ngắn gọn';
$_['text_blog_page_limit'] = 'Giới hạn trang';
$_['text_blog_show_author'] = 'Hiển thị tác giả';
$_['text_blog_show_date'] = 'Hiển thị ngày xuất bản';
$_['text_blog_date_format'] = 'Định dạng ngày tháng';
$_['text_blog_show_comment'] = 'Hiển thị nhận xét';
$_['text_blog_use_comment'] = 'Cho phép nhận xét trên blog';
$_['text_blog_allow_comment'] = 'Cho phép nhận xét';
$_['text_blog_approve_comment'] = 'Phê duyệt nhận xét';
$_['text_blog_captcha_comment'] = 'Captcha khi nhận xét';
$_['text_blog_show_rating'] = 'Hiển thị Xếp hạng';
$_['text_blog_allow_rating'] = 'Cho phép xếp hạng';
$_['text_blog_approve_rating'] = 'Tự động phê duyệt xếp hạng';
$_['text_blog_guest_rating'] = 'Cho phép xếp hạng của khách';
$_['text_blog_show_readmore'] = 'Hiển thị liên kết Readmore';
$_['text_blog_show_viewcount'] = 'Hiển thị Tổng lượt xem Blog';
$_['text_blog_show_sharethis'] = 'Hiển thị các nút chia sẻ trên mạng xã hội';
$_['text_blog_show_nextprev'] = 'Hiển thị blog trước tiếp theo';
$_['text_blog_show_nextprev_title'] = 'Hiển thị tiêu đề blog trước tiếp theo';
$_['text_blog_view'] = 'Chế độ xem danh sách blog';
$_['text_category_view'] = 'Xem blog trên trang chuyên mục';
$_['text_blog_show_tags'] = 'Hiển thị các thẻ blog';
$_['text_blog_show_viewsocial'] = 'Hiển thị các biểu tượng trên mạng xã hội';
$_['text_blog_show_sociallocation'] = 'Vị trí trên mạng xã hội';
$_['text_blog_show_viewwishlist'] = 'Hiển thị Blog Yêu thích (Dấu trang)';
$_['text_blog_design'] = 'Cột blog trên mỗi hàng';

$_['text_comment_default'] = 'Sử dụng Nhận xét Mặc định';
$_['text_comment_default_guest'] = 'Cho phép khách nhận xét';

$_['text_comment_facebook'] = 'Sử dụng Bình luận trên Facebook';
$_['text_facebook_appid'] = 'ID ỨNG DỤNG Facebook';
$_['text_facebook_nocomment'] = 'Hiển thị số lượng bình luận';
$_['text_facebook_color'] = 'Phối màu';
$_['text_facebook_colorlight'] = 'Ánh sáng';
$_['text_facebook_colordark'] = 'Tối';
$_['text_facebook_order'] = 'Thứ tự nhận xét';
$_['text_facebook_ordersocial'] = 'Xã hội';
$_['text_facebook_orderreverse_time'] = 'Ngược thời gian';
$_['text_facebook_ordertime'] = 'Thời gian';
$_['text_facebook_width'] = 'Chiều rộng';

$_['text_comment_google'] = 'Sử dụng Nhận xét của Google';

$_['text_comment_disqus'] = 'Sử dụng Nhận xét Disqus';
$_['text_comment_disqus_code'] = 'Mã chung Disqus';
$_['text_comment_disqus_count'] = 'Số lượng bình luận hiển thị Disqus';


$_['text_rssfeed_title'] = 'Tiêu đề nguồn cấp dữ liệu';
$_['text_rssfeed_description'] = 'Mô tả nguồn cấp dữ liệu';
$_['text_rssfeed_format'] = 'Định dạng nguồn cấp dữ liệu mặc định';

$_['text_rssfeed_limit'] = 'Nguồn cấp dữ liệu trên mỗi trang';
$_['text_rssfeed_web_master'] = 'Trang chủ nguồn cấp dữ liệu web';
$_['text_rssfeed_copy_write'] = 'Ghi bản sao nguồn cấp dữ liệu';
$_['text_autoapprove'] = 'Tự động phê duyệt';
$_['text_adminapprove'] = 'Quản trị viên phê duyệt thủ công';
$_['text_confirmapprove'] = 'Phê duyệt bằng xác nhận qua email';

// Huyền thoại

$_['legend_blogpage'] = 'Trang xem blog';
$_['legend_bloglist'] = 'Trang danh sách blog';
$_['legend_design'] = 'Blog mỗi hàng';
$_['legend_image_sizes'] = 'Kích thước hình ảnh';
$_['legend_comment'] = 'Nhận xét';

$_['legend_msg_unsubscribe'] = 'Hủy đăng ký thông báo hoàn chỉnh';
$_['legend_msg_confirm'] = 'Thông báo hoàn tất xác nhận';
$_['legend_msg_invalid'] = 'Thông báo về URL không hợp lệ';

// Thông tin

$_['info_title_rssfeed'] = 'Nguồn cấp RSS';
$_['info_text_rssfeed'] = 'RSS (Phân phối thực sự đơn giản) Để cung cấp hoặc đăng ký nguồn cấp dữ liệu của một trang web, blog. Nhiều trang web liên quan đến tin tức, nhật ký web và nhà xuất bản trực tuyến khác cung cấp nội dung của họ dưới dạng nguồn cấp dữ liệu Rss cho bất kỳ ai muốn nó. Định cấu hình cài đặt nguồn cấp dữ liệu Rss. ';

$_['info_subscribe_approval'] = 'Thành viên đăng ký Nhận email phê duyệt. Đầu tiên khi bật tự động phê duyệt, Quản trị viên thứ hai phê duyệt người đăng ký, Thứ ba khi xác nhận được thực hiện bằng liên kết xác minh. ';
$_['info_subscribe_pend'] = 'Thành viên đăng ký Nhận e-mail đang chờ xử lý. Khi người đăng ký sẽ được quản trị viên phê duyệt. ';
$_['info_subscribe_confirm'] = 'Thành viên đăng ký Nhận email xác nhận. Khi người đăng ký cần xác minh E-mail bằng liên kết xác minh. ';
$_['info_subscribe_confirm_page'] = 'Đặt tiêu đề và thông điệp cho Trang khi url xác minh Đăng ký được xác minh và đăng ký thành công.';
$_['info_unsubscribe'] = 'Thành viên đăng ký Nhận email hủy đăng ký. Khi họ cố gắng Hủy đăng ký. ';
$_['info_unsubscribe_page'] = 'Đặt tiêu đề và thông báo cho Trang khi url xác minh Hủy đăng ký được xác minh và hủy đăng ký thành công.';
$_['info_invalidurl'] = 'Đặt tiêu đề trang và thông báo khi url xác minh không hợp lệ.';
$_['info_email_blogadd'] = 'Gửi email cho người đăng ký blog mới do quản trị viên thêm vào. Tất cả người đăng ký đều nhận được email. ';
$_['info_email_blogedit'] = 'Gửi email cho người đăng ký để quản trị viên chỉnh sửa / cập nhật blog. Tất cả người đăng ký đều nhận được email. ';

// Mục nhập
$_['entry_support'] = 'Hỗ trợ';
$_['entry_status'] = 'Trạng thái';
$_['entry_social_icon'] = 'Lớp biểu tượng';
$_['entry_social_href'] = 'Liên kết mạng xã hội';
$_['entry_social_name'] = 'Tên';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';
$_['placeholder_social_icon'] = 'fa fa-facebook-official';
$_['placeholder_social_href'] = 'https://www.facebook.com/';
$_['placeholder_social_name'] = 'Facebook';
$_['placeholder_sort_order'] = '5';

$_['entry_width'] = 'Chiều rộng';
$_['entry_height'] = 'Chiều cao';

$_['entry_image_category'] = 'Kích thước hình ảnh danh mục blog';
$_['entry_image_category_thumb'] = 'Kích thước hình ảnh tiểu mục blog';

$_['entry_image_post'] = 'Kích thước hình ảnh chính của blog';
$_['entry_image_post_thumb'] = 'Kích thước từng hình ảnh trong danh sách blog';
$_['entry_image_post_popup'] = 'Kích thước cửa sổ bật lên hình ảnh blog';
$_['entry_image_post_additional'] = 'Kích thước hình ảnh bổ sung cho blog';
$_['entry_image_post_osystem'] = 'Kích thước Hình ảnh Liên quan đến Blog';

$_['entry_subscripeapprove'] = 'Phê duyệt thành viên đăng ký';
$_['entry_subscripeadminmail'] = 'Email quản trị viên';
$_['entry_mail_subject'] = 'Chủ đề Email';
$_['entry_mail_message'] = 'Email Messsage';
$_['entry_mail_send'] = 'Gửi Email';
$_['entry_page_title'] = 'Tiêu đề trang';
$_['entry_page_content'] = 'Tin nhắn trang';

// Cái nút
$_['button_support'] = 'Gửi Email đến Hỗ trợ';
$_['button_design_add'] = 'Thêm hàng';
$_['button_social_add'] = 'Thêm mạng xã hội';
$_['button_design_remove'] = 'Xóa hàng';

$_['button_shortcodes'] = 'Mã ngắn';



// Mã ngắn
$_['sh_code'] = 'Mã ngắn';
$_['sh_name'] = 'Tên';
$_['sh_logo'] = 'Biểu trưng';
$_['sh_storename'] = 'Tên cửa hàng';
$_['sh_storelink'] = 'Liên kết cửa hàng';
$_['sh_email'] = 'E-mail';
$_['sh_confirmlink'] = 'Liên kết xác nhận';
$_['sh_confirmcode'] = 'Mã xác nhận';

$_['sh_blog_name'] = 'Tên Blog';
$_['sh_blog_thumb'] = 'Ngón tay cái của blog';
$_['sh_blog_description'] = 'Mô tả Blog';
$_['sh_blog_shortdescription'] = 'Mô tả ngắn về blog';

// Chuyển hướng
$_['tab_setting'] = 'Cài đặt';
$_['tab_mpblogcategory'] = 'Danh mục blog';
$_['tab_mpblogpost'] = 'Trang blog';
$_['tab_mpblogmodule'] = 'Truyền thông xã hội';

$_['tab_emails'] = 'E-mail';
$_['tab_email_blogadd'] = 'Blog Thêm E-mail';
$_['tab_email_blogedit'] = 'Blog Chỉnh sửa E-mail';

$_['tab_subscriber'] = 'Người đăng ký';
$_['tab_email_subscripeadmin'] = 'Email quản trị viên';
$_['tab_email_subscripeapproval'] = 'Email phê duyệt';
$_['tab_email_subscribecting'] = 'E-mail đang chờ phê duyệt';
$_['tab_email_subscripeconfirm'] = 'Email xác nhận người đăng ký';
$_['tab_email_unsubscribe'] = 'Bỏ đăng ký E-mail';
$_['tab_email_invalidurl'] = 'URL xác minh không hợp lệ';

$_['tab_mpblogcomments'] = 'Nhận xét';

$_['tab_mpblogcomments_default'] = 'Nhận xét mặc định';
$_['tab_mpblogcomments_facebook'] = 'Nhận xét trên Facebook';
$_['tab_mpblogcomments_google'] = 'Nhận xét trên Google+';
$_['tab_mpblogcomments_disqus'] = 'Disqus Comments';

$_['tab_mpblogrss'] = 'Nguồn cấp RSS';
$_['tab_mpblogdoc'] = 'Tài liệu';
$_['tab_mpsupport'] = 'Hỗ trợ';
$_['tab_mpblogs_view'] = 'Trang Xem Toàn bộ Blog';
$_['tab_mpblogs_listing'] = 'Trang danh sách blog';

// Cứu giúp
$_['help_category_home'] = 'Đặt Danh mục Trang chủ để Hiển thị Trong Trang chủ Blog. Đây là điểm truy cập vào blog bằng danh mục blog. ';
$_['help_blog_captcha_comment'] = 'Hiển thị Captcha trên Phần Bình luận Blog. Bạn cần bật Captcha từ phần mở rộng và cài đặt từ phần phụ trợ. Nếu Cài đặt Captcha không được bật thì Captcha sẽ không hoạt động. ';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi xếp hạng blog!';

$_['error_warning'] = 'Cảnh báo: Vui lòng kiểm tra kỹ biểu mẫu để tìm lỗi!';
$_['error_subject'] = 'Tiêu đề email phải lớn hơn 3 và ít hơn 255 ký tự!';
$_['error_message'] = 'Thư điện tử phải lớn hơn 10 ký tự!';
$_['error_pagetitle'] = 'Tiêu đề trang phải lớn hơn 3 và ít hơn 255 ký tự!';
$_['error_pagecontent'] = 'Thông báo Trang phải lớn hơn 10 ký tự!';