<?php
// Phần mở đầu
$_['heading_title'] = 'Danh mục blog';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi danh mục blog!';
$_['text_list'] = 'Danh sách Chuyên mục Blog';
$_['text_add'] = 'Thêm danh mục blog';
$_['text_edit'] = 'Chỉnh sửa danh mục blog';
$_['text_default'] = 'Mặc định';
$_['text_keyword'] = 'Không sử dụng dấu cách, thay vào đó hãy thay dấu cách bằng - và đảm bảo rằng URL SEO là duy nhất trên toàn cầu.';

// Cột
$_['column_name'] = 'Tên Chuyên mục Blog';
$_['column_sort_order'] = 'Sắp xếp Thứ tự';
$_['column_status'] = 'Trạng thái';
$_['column_action'] = 'Hành động';

// Mục nhập
$_['entry_name'] = 'Tên chuyên mục blog';
$_['entry_description'] = 'Mô tả';
$_['entry_meta_title'] = 'Tiêu đề thẻ meta';
$_['entry_meta_keyword'] = 'Từ khóa thẻ meta';
$_['entry_meta_description'] = 'Mô tả thẻ meta';
$_['entry_keyword'] = 'URL SEO';
$_['entry_parent'] = 'Danh mục cha';
$_['entry_store'] = 'Cửa hàng';
$_['entry_image'] = 'Hình ảnh';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';
$_['entry_status'] = 'Trạng thái';
$_['entry_layout'] = 'Ghi đè bố cục';

// Cứu giúp
$_['help_filter'] = '(Tự động hoàn thành)';
$_['help_keyword'] = 'Không sử dụng dấu cách, thay vào đó hãy thay dấu cách bằng - và đảm bảo rằng URL SEO là duy nhất trên toàn cầu.';


// Lỗi
$_['error_warning'] = 'Cảnh báo: Vui lòng kiểm tra kỹ biểu mẫu để tìm lỗi!';
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi các danh mục blog!';
$_['error_name'] = 'Tên Chuyên mục Blog phải từ 2 đến 255 ký tự!';
$_['error_meta_title'] = 'Tiêu đề meta phải lớn hơn 3 và ít hơn 255 ký tự!';
$_['error_keyword'] = 'URL SEO đã được sử dụng!';
$_['error_unique'] = 'URL SEO phải là duy nhất!';
$_['error_parent'] = 'Danh mục blog chính mà bạn đã chọn là con của blog hiện tại!';
