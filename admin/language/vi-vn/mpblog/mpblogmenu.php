<?php
// Text
$_['text_mpblog'] = 'M-Blog';
$_['text_mpblogsetting'] = 'Cài đặt';
$_['text_mpblogdashboard'] = 'Trang tổng quan';
$_['text_mpblog_category'] = 'Danh mục';
$_['text_mpblog_post'] = 'Bài đăng';
$_['text_mpblog_comment'] = 'Nhận xét';
$_['text_mpblog_rating'] = 'Xếp hạng';
$_['text_mpblog_subscriber'] = 'Người đăng ký';
