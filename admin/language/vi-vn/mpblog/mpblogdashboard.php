<?php
// Phần mở đầu
$_['heading_title'] = 'Trang tổng quan M-Blog';

// Bản văn
$_['text_total_blogs'] = 'Tổng số blog';
$_['text_total_categories'] = 'Tổng số danh mục';
$_['text_total_comments'] = 'Tổng số nhận xét';
$_['text_total_ratings'] = 'Tổng xếp hạng';
$_['text_edit'] = 'Xem';

$_['text_view_blogs'] = 'Xem blog';
$_['text_view_categories'] = 'Xem danh mục';
$_['text_view_comments'] = 'Xem bình luận';
$_['text_view_ratings'] = 'Xem Xếp hạng';

$_['text_latest_blogs'] = 'Blog mới nhất';
$_['text_latest_comments'] = 'Nhận xét mới nhất';
$_['text_top_rated_blogs'] = 'Các blog được xếp hạng cao nhất';
$_['text_top_viewed_blogs'] = 'Các blog được xem nhiều nhất';
$_['text_no_blogs'] = 'Chưa có blog nào. ';
$_['text_no_comments'] = 'Chưa có bình luận nào. ';

// Cột
$_['column_image'] = 'Hình ảnh';
$_['column_blog'] = 'Tên Blog';
$_['column_date_available'] = 'Ngày thêm';
$_['column_author'] = 'Tác giả';
$_['column_published'] = 'Đã xuất bản';
$_['column_action'] = 'Hành động';
$_['column_commentby'] = 'Nhận xét bởi';
$_['column_date_added'] = 'Ngày Thêm';
$_['column_comment'] = 'Bình luận';

// Cái nút
$_['button_viewall'] = 'Xem tất cả';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi xếp hạng blog!';