<?php
// Heading
$_['heading_title']      = 'Chủ đề phiếu quà tặng';

// Text
$_['text_success']       = 'Thành công: Bạn đã sửa đổi các chủ đề phiếu quà tặng!';
$_['text_list']         = 'Danh sách mẫu phiếu quà tặng';
$_['text_add']          = 'Thêm mẫu phiếu quà tặng';
$_['text_edit']         = 'Chỉnh sửa mẫu phiếu quà tặng';

// Column
$_['column_name']        = 'Tên phiếu quà tặng';
$_['column_action']      = 'Thao tác';
// Entry
$_['entry_name']         = 'Tên phiếu quà tặng:';
$_['entry_description']  = 'Mô tả phiếu quà tặng:';
$_['entry_image']        = 'Hình ảnh:';

// Error
$_['error_permission']  = 'Cảnh báo: Bạn không có quyền sửa đổi chủ đề phiếu quà tặng!';
$_['error_name']        = 'Tên phiếu quà tặng phải có từ 3 đến 32 ký tự!';
$_['error_image']       = 'Hình được yêu cầu!';
$_['error_voucher']     = 'Cảnh báo: Chủ đề phiếu quà tặng này không thể xóa được vì nó đang được sửa dung bởi %s phiếu quà tặng!';