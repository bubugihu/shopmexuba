<?php
// Heading
$_['heading_title']                        = 'Đơn hàng định kỳ';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi thanh toán định kỳ!';
$_['text_list'] = 'Danh sách đơn hàng định kỳ';
$_['text_filter'] = 'Bộ lọc';
$_['text_recurring_detail'] = 'Chi tiết Định kỳ';
$_['text_order_detail'] = 'Chi tiết Đơn hàng';
$_['text_product_detail'] = 'Chi tiết Sản phẩm';
$_['text_transaction'] = 'Giao dịch';
$_['text_order_recurring_id'] = 'ID đơn đặt hàng định kỳ';
$_['text_reference'] = 'Tham chiếu Thanh toán';
$_['text_recurring_name'] = 'thanh toán định kỳ';
$_['text_recurring_description'] = 'Mô tả';
$_['text_recurring_status'] = 'Trạng thái định kỳ';
$_['text_payment_method'] = 'Phương thức thanh toán';
$_['text_order_id'] = 'ID đơn hàng';
$_['text_customer'] = 'Khách hàng';
$_['text_email'] = 'Email';
$_['text_date_added'] = 'Ngày thêm';
$_['text_order_status'] = 'Trạng thái Đơn hàng';
$_['text_type'] = 'Loại';
$_['text_action'] = 'Hành động';
$_['text_product'] = 'Sản phẩm';
$_['text_quantity'] = 'Số lượng';
$_['text_amount'] = 'Số tiền';
$_['text_cancel_payment'] = 'Hủy thanh toán';
$_['text_status_1'] = 'Hoạt động';
$_['text_status_2'] = 'Không hoạt động';
$_['text_status_3'] = 'Đã hủy';
$_['text_status_4'] = 'Bị tạm ngưng';
$_['text_status_5'] = 'Hết hạn';
$_['text_status_6'] = 'Đang chờ xử lý';

$_['text_transactions'] = 'Giao dịch';
$_['text_cancel_confirm'] = 'Không thể hoàn tác việc hủy bỏ hồ sơ! Bạn có chắc chắn muốn làm điều này? ';
$_['text_transaction_date_added'] = 'Ngày thêm';
$_['text_transaction_payment'] = 'Thanh toán';
$_['text_transaction_outinent_payment'] = 'Chưa thanh toán';
$_['text_transaction_skipped'] = 'Thanh toán bị bỏ qua';
$_['text_transaction_failed'] = 'Thanh toán không thành công';
$_['text_transaction_cancelt'] = 'Đã hủy';
$_['text_transaction_suspended'] = 'Bị tạm ngưng';
$_['text_transaction_suspended_failed'] = 'Bị đình chỉ do thanh toán không thành công';
$_['text_transaction_outinent_failed'] = 'Chưa thanh toán được';
$_['text_transaction_expired'] = 'Hết hạn';
$_['text_cancelt'] = 'Thanh toán định kỳ đã bị hủy bỏ';

// Cột
$_['column_order_recurring_id'] = 'ID định kỳ';
$_['column_order_id'] = 'ID Đôn hàng';
$_['column_reference'] = 'Tham chiếu Thanh toán';
$_['column_customer'] = 'Khách hàng';
$_['column_date_added'] = 'Ngày Thêm';
$_['column_status'] = 'Tình trạng';
$_['column_amount'] = 'Số tiền';
$_['column_type'] = 'Loại';
$_['column_action'] = 'Thao tác';

// Mục nhập
$_['entry_order_recurring_id'] = 'ID định kỳ';
$_['entry_order_id'] = 'ID đơn hàng';
$_['entry_reference'] = 'Tham chiếu Thanh toán';
$_['entry_customer'] = 'Khách hàng';
$_['entry_date_added'] = 'Ngày thêm';
$_['entry_status'] = 'Trạng thái';
$_['entry_type'] = 'Loại';
$_['entry_action'] = 'Hành động';
$_['entry_email'] = 'Email';
$_['entry_description'] = 'Mô tả của thanh toán định kỳ';
$_['entry_product'] = 'Sản phẩm';
$_['entry_quantity'] = 'Số lượng';
$_['entry_amount'] = 'Số tiền';
$_['entry_recurring'] = 'Thanh toán định kỳ';
$_['entry_payment_method'] = 'Phương thức Thanh toán';
$_['entry_cancel_payment'] = 'Hủy thanh toán';

// Lỗi
$_['error_not_cancelt'] = 'Lỗi:% s';
$_['error_not_found'] = 'Không thể hủy tiểu sử ký kết';