<?php
// Tiêu đề
$_['heading_title'] = 'Liên hệ';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi Liên hệ!';
$_['text_default'] = 'Mặc định';
$_['text_image_manager'] = 'Trình quản lý hình ảnh';

// Cột
$_['column_id'] = 'ID';
$_['column_title'] = 'Tiêu đề';
$_['column_fullname'] = 'Tên đầy đủ';
$_['column_email'] = 'Email';
$_['column_date'] = 'Ngày';
$_['column_action'] = 'Hành động';

// Mục nhập
$_['entry_title'] = 'Tiêu đề:';
$_['entry_description'] = 'FeedBack:';
//thêm vào
$_['entry_fullname'] = 'Họ và Tên:';
$_['entry_phone'] = 'Điện thoại:';
$_['entry_email'] = 'Email:';
// kết thúc thêm
$_['entry_store'] = 'Cửa hàng:';
$_['entry_keyword'] = 'Từ khoá SEO:';
$_['entry_status'] = 'Trạng thái:';
$_['entry_sort_order'] = 'Sắp xếp Thứ tự: <br/> <span class = "help"> Đặt thành -1 để ẩn khỏi danh sách </span>';
$_['entry_layout'] = 'Ghi đè bố cục:';
$_['entry_date'] = 'Ngày liên hệ:';
$_['entry_allow_subscribe'] = 'Cho phép đăng ký ?:';
$_['thuê bao'] = 'Người đăng ký';
$_['allow_no_subscriber'] = 'Cho phép Không có người đăng ký nào';
$_['entry_image'] = 'Hình ảnh';
$_['entry_products'] = 'Đính kèm sản phẩm';

$_['text_browse'] = 'Duyệt qua';
$_['text_clear'] = 'Xóa';

// Lỗi
$_['error_warning'] = 'Cảnh báo: Vui lòng kiểm tra kỹ biểu mẫu để tìm lỗi!';
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi Liên hệ!';
$_['error_title'] = 'Tiêu đề liên hệ phải từ 3 đến 64 ký tự!';
$_['error_description'] = 'Mô tả phải có từ 3 ký tự!';
$_['error_account'] = 'Cảnh báo: Không thể xóa trang Liên hệ này vì nó hiện đang được gán cho điều khoản tài khoản cửa hàng!';
$_['error_checkout'] = 'Cảnh báo: Không thể xóa trang Liên hệ này vì nó hiện đang được chỉ định làm điều khoản thanh toán tại cửa hàng!';
$_['error_affiliate'] = 'Cảnh báo: Không thể xóa trang Liên hệ này vì nó hiện đang được gán là điều khoản liên kết của cửa hàng!';
$_['error_store'] = 'Cảnh báo: Không thể xóa trang Liên hệ này vì trang này hiện đang được các cửa hàng% s sử dụng!';

//hình thức
$_['tab_general'] = 'Phản hồi';
$_['text_home'] = 'Danh mục';
$_['tab_data'] = 'Trả lời';
$_['entry_shop'] = 'Nucos Japan';
?>