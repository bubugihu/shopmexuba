<?php
// Heading
$_['heading_title']     = 'Báo cáo trực tuyến';

// Text
$_['text_extension']    = 'Tiện ích mở rộng';
$_['text_success']      = 'Thành công: Bạn đã sửa đổi báo cáo khách hàng trực tuyến!';
$_['text_list']         = 'Dnh sách trực tuyến';
$_['text_filter']       = 'Chọn bộ lọc';
$_['text_guest']        = 'Khách';

// Column
$_['column_ip']         = 'Địa chỉ IP';
$_['column_customer']   = 'Khách hàng';
$_['column_url']        = 'Trang web truy cập cuối cùng';
$_['column_referer']    = 'Liên kết';
$_['column_date_added'] = 'Lần truy cập cuối cùng';
$_['column_action']     = 'Thao tác';

// Entry
$_['entry_ip']          = 'Địa chỉ IP';
$_['entry_customer']    = 'Khách hàng';
$_['entry_status']      = 'Tình trạng';
$_['entry_sort_order']  = 'Thứ tự';

// Error
$_['error_permission']  = 'Cảnh báo: Bạn không có quyền sửa đổi báo cáo trực tuyến của khách hàng!';