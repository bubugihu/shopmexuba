<?php
// Heading
$_['heading_title']		 				= 'Nền tảng thương mại PayPal';

// Text
$_['text_paypal']		 				= '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Commerce Platform" title="Nền tảng thương mại PayPal" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_extensions'] = 'Phần mở rộng';
$_['text_edit'] = 'Chỉnh sửa PayPal';
$_['text_general'] = 'Chung';
$_['text_order_status'] = 'Trạng thái Đơn hàng';
$_['text_checkout_express'] = 'Thanh toán';
$_['text_checkout_card'] = 'Thẻ nâng cao';
$_['text_checkout_message'] = 'Thanh toán thư sau';
$_['text_production'] = 'Bán hàng';
$_['text_sandbox'] = 'Thử nghiệm';
$_['text_authorization'] = 'Authorization';
$_['text_sale'] = 'Sale';
$_['text_connect']						= 'Tài khoản người bán của bạn đã được kết nối.<br />ID khách hàng = %s<br />Khóa bí mật = %s<br />ID người bán = %s<br />Nếu bạn muốn kết nối một tài khoản khác, vui lòng ngắt kết nối.';
$_['text_currency_aud'] = 'Đô la Úc';
$_['text_currency_brl'] = 'Đồng Real của Braxin';
$_['text_currency_cad'] = 'Đô la Canada';
$_['text_currency_czk'] = 'Krone Séc';
$_['text_currency_dkk'] = 'Krone Đan Mạch';
$_['text_currency_eur'] = 'Euro';
$_['text_currency_hkd'] = 'Đô la Hồng Kông';
$_['text_currency_huf'] = 'Đồng Forint của Hungary';
$_['text_currency_inr'] = 'Đồng Rupee Ấn Độ';
$_['text_currency_ils'] = 'Đồng Shekel mới của Israel';
$_['text_currency_jpy'] = 'Yên Nhật';
$_['text_currency_myr'] = 'Ringgit Malaysia';
$_['text_currency_mxn'] = 'Peso Mexico';
$_['text_currency_twd'] = 'Đô la Đài Loan mới';
$_['text_currency_nzd'] = 'Đô la New Zealand';
$_['text_currency_nok'] = 'Krone Na Uy';
$_['text_currency_php'] = 'Đồng peso của Philippine';
$_['text_currency_pln'] = 'Đồng Zloty của Ba Lan';
$_['text_currency_gbp'] = 'Bảng Anh';
$_['text_currency_rub'] = 'Đồng rúp Nga';
$_['text_currency_sgd'] = 'Đô la Singapore';
$_['text_currency_sek'] = 'Krone Thụy Điển';
$_['text_currency_chf'] = 'Thụy Sĩ Frank';
$_['text_currency_thb'] = 'Bạt Thái Lan';
$_['text_currency_usd'] = 'Đô la Mỹ';
$_['text_completed_status'] = 'Trạng thái đã hoàn thành';
$_['text_denied_status'] = 'Trạng thái bị từ chối';
$_['text_failed_status'] = 'Trạng thái không thành công';
$_['text_pending_status'] = 'Trạng thái đang chờ xử lý';
$_['text_refunded_status'] = 'Trạng thái đã hoàn tiền';
$_['text_reversed_status'] = 'Trạng thái đảo ngược';
$_['text_voided_status'] = 'Trạng thái bị hủy bỏ';
$_['text_align_left'] = 'Căn trái';
$_['text_align_center'] = 'Căn giữa';
$_['text_align_right'] = 'Căn phải';
$_['text_small'] = 'Nhỏ';
$_['text_medium'] = 'Trung bình';
$_['text_large'] = 'Lớn';
$_['text_responsive'] = 'Phản hồi';
$_['text_gold'] = 'Vàng';
$_['text_blue'] = 'Xanh lam';
$_['text_silver'] = 'Bạc';
$_['text_white'] = 'Trắng';
$_['text_black'] = 'Đen';
$_['text_pill'] = 'Thuốc viên';
$_['text_rect'] = 'Hình thức';
$_['text_checkout'] = 'Thanh toán';
$_['text_pay'] = 'Thanh toán';
$_['text_buy_now'] = 'Mua ngay';
$_['text_pay_pal'] = 'PayPal';
$_['text_installment'] = 'Trả góp';
$_['text_text'] = 'Tin nhắn Văn bản';
$_['text_flex'] = 'Biểu ngữ linh hoạt';
$_['text_accept'] = 'Chấp nhận';
$_['text_decline'] = 'Từ chối';
$_['text_recommended'] = '(được khuyến nghị)';
$_['text_3ds_undefined'] = 'Bạn không yêu cầu 3D Secure cho người mua hoặc mạng thẻ không yêu cầu 3D Secure.';
$_['text_3ds_error'] = 'Đã xảy ra lỗi với hệ thống xác thực 3DS.';
$_['text_3ds_skipped_by_buyer'] = 'Người mua đã nhận được thử thách Bảo mật 3D nhưng đã chọn bỏ qua xác thực.';
$_['text_3ds_failure'] = 'Người mua có thể đã thất bại trong thử thách hoặc thiết bị chưa được xác minh.';
$_['text_3ds_bypassed'] = 'Bảo mật 3D đã bị bỏ qua vì hệ thống xác thực không yêu cầu thử thách.';
$_['text_3ds_attempted'] = 'Thẻ chưa được đăng ký 3D Secure vì ngân hàng phát hành thẻ không tham gia 3D Secure.';
$_['text_3ds_unavailable'] = 'Ngân hàng phát hành không thể hoàn tất xác thực.';
$_['text_3ds_card_inenough'] = 'Thẻ không đủ điều kiện để xác thực 3DS Secure.';
$_['text_confirm'] = 'Bạn có chắc không?';

// Mục nhập
$_['entry_connect'] = 'Kết nối';
$_['entry_checkout_express_status'] = 'Thanh toán';
$_['entry_checkout_card_status'] = 'Thẻ nâng cao';
$_['entry_checkout_message_status'] = 'Thanh toán thư sau';
$_['entry_environment'] = 'Môi trường';
$_['entry_debug'] = 'Ghi nhật ký gỡ lỗi';
$_['entry_transaction_method'] = 'Phương pháp Thanh toán';
$_['entry_total'] = 'Tổng số';
$_['entry_geo_zone'] = 'Vùng địa lý';
$_['entry_status'] = 'Trạng thái';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';
$_['entry_currency_code'] = 'Tiền tệ';
$_['entry_currency_value'] = 'Giá trị tiền tệ';
$_['entry_smart_button'] = 'Nút thông minh';
$_['entry_button_align'] = 'Căn chỉnh nút';
$_['entry_button_size'] = 'Kích thước nút';
$_['entry_button_color'] = 'Màu nút';
$_['entry_button_shape'] = 'Hình dạng nút';
$_['entry_button_label'] = 'Nhãn nút';
$_['entry_form_align'] = 'Căn chỉnh biểu mẫu';
$_['entry_form_size'] = 'Kích thước biểu mẫu';
$_['entry_secure_status'] = 'Trạng thái bảo mật 3D';
$_['entry_secure_scenario'] = 'Các tình huống bảo mật 3D';
$_['entry_message_align'] = 'Căn chỉnh thông báo';
$_['entry_message_size'] = 'Kích thước tin nhắn';
$_['entry_message_layout'] = 'Bố cục Thư';
$_['entry_message_text_color'] = 'Màu văn bản tin nhắn';
$_['entry_message_text_size'] = 'Kích thước văn bản tin nhắn';
$_['entry_message_flex_color'] = 'Màu biểu ngữ tin nhắn';
$_['entry_message_flex_ratio'] = 'Tỷ lệ biểu ngữ thông báo';

// Cứu giúp
$_['help_checkout_express'] = 'Nếu quốc gia của bạn không có trong danh sách khi trải nghiệm gia nhập PayPal, vui lòng <a id = "button_connect_express_checkout" href = "% s" target = "_ blank" data-paypal-onboard- complete = "onBoardCallback"> nhấp vào đây </a>. ';
$_['help_checkout_express_status'] = 'Khi kích hoạt PayPal sẽ hiển thị các Nút thông minh được cá nhân hóa có sẵn cho khách hàng của bạn dựa trên vị trí của họ.';
$_['help_checkout_card_status'] = 'PayPal xác minh xem bạn có đủ điều kiện để thanh toán bằng thẻ nâng cao hay không và sẽ hiển thị tùy chọn này ở bước thanh toán nếu có.';
$_['help_checkout_message_status'] = 'Thêm tin nhắn trả tiền sau vào trang web của bạn.';
$_['help_total'] = 'Tổng số tiền mà đơn đặt hàng phải đạt được trước khi phương thức thanh toán này có hiệu lực.';
$_['help_currency_code'] = 'Chọn đơn vị tiền tệ mặc định cho PayPal.';
$_['help_currency_value'] = 'Đặt thành 1.00000 nếu đây là đơn vị tiền tệ mặc định của bạn.';
$_['help_secure_status'] = '3D Secure cho phép bạn xác thực chủ sở hữu thẻ thông qua công ty phát hành thẻ. Nó làm giảm khả năng gian lận khi bạn sử dụng thẻ được hỗ trợ và cải thiện hiệu suất giao dịch. Xác thực 3D Secure thành công có thể chuyển trách nhiệm bồi hoàn do gian lận từ bạn - người bán - sang nhà phát hành thẻ. ';
$_['help_secure_scenario'] = 'Xác thực 3D Secure chỉ hoàn thiện nếu thẻ được đăng ký dịch vụ. Trong các tình huống xác thực 3D Secure không thành công, bạn có tùy chọn tự chịu rủi ro để hoàn tất thanh toán, có nghĩa là bạn - người bán - sẽ phải chịu trách nhiệm trong trường hợp có khoản bồi hoàn. ';

// Cái nút
$_['button_connect'] = 'Kết nối với PayPal';
$_['button_disconnect'] = 'Ngắt kết nối';
$_['button_smart_button'] = 'Cấu hình nút thông minh';

// Sự thành công
$_['success_save'] = 'Thành công: Bạn đã sửa đổi PayPal!';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi PayPal thanh toán!';
$_['error_timeout'] = 'Xin lỗi, PayPal hiện đang bận. Vui lòng thử lại sau!';