<?php
// Tiêu đề
$_['header_title'] = 'Phí giao hàng';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi phí giao hàng cố định!';
$_['text_edit'] = 'Chỉnh sửa phí giao hàng cố định';

// Mục nhập
$_['entry_cost'] = 'Chi phí (Sử dụng tiền tệ mặc định)';
$_['entry_tax_class'] = 'Thuế';
$_['entry_geo_zone'] = 'Vùng lãnh thổ';
$_['entry_status'] = 'Trạng thái';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không được phép sửa đổi phí vận chuyển cố định!';