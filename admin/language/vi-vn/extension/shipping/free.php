<?php
// Tiêu đề
$_['heading_title'] = 'Giao hàng miễn phí';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi giao hàng miễn phí!';
$_['text_edit'] = 'Chỉnh sửa Giao hàng Miễn phí';

// Mục nhập
$_['entry_total'] = 'Tổng số';
$_['entry_geo_zone'] = 'Vùng địa lý';
$_['entry_status'] = 'Trạng thái';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';

// Cứu giúp
$_['help_total'] = 'Tổng số tiền cần thiết trước khi mô-đun vận chuyển miễn phí có sẵn.';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không được phép sửa đổi giao hàng miễn phí!';