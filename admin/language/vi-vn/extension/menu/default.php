<?php
// Tiêu đề
$_['heading_title'] = 'Menu mặc định';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi nguồn cấp dữ liệu Google Base!';
$_['text_edit'] = 'Chỉnh sửa Google Base';
$_['text_import'] = 'Để tải xuống danh sách danh mục mới nhất của Google bằng <a href = "https://support.google.com/merchants/answer/160081?hl=vi" target = "_ blank" class = " alert-link "> nhấp vào đây </a> và chọn phân loại với ID số trong tệp Văn bản thuần túy (.txt). Tải lên qua nút nhập màu xanh lục. ';

// Cột
$_['column_google_category'] = 'Danh mục Google';
$_['column_category'] = 'Danh mục';
$_['column_action'] = 'Hành động';

// Mục nhập
$_['entry_google_category'] = 'Danh mục Google';
$_['entry_category'] = 'Danh mục';
$_['entry_data_feed'] = 'Url nguồn cấp dữ liệu';
$_['entry_status'] = 'Trạng thái';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi nguồn cấp dữ liệu Google Base!';
$_['error_upload'] = 'Không thể tải tệp lên!';
$_['error_filetype'] = 'Loại tệp không hợp lệ!';