<?php
// Tiêu đề
$_['header_title'] = 'iExtend Sơ đồ trang web';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi nguồn cấp dữ liệu Sơ đồ trang web iExtend!';
$_['text_edit'] = 'Chỉnh sửa iExtend Sơ đồ trang web';

// Mục nhập
$_['entry_status'] = 'Trạng thái';
$_['entry_data_feed'] = 'iExtend Sitemap Url';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi Sơ đồ trang web iExtend!';