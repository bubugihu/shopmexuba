<?php
// Phần mở đầu
$_['header_title'] = 'Ba lô SEO';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi mô-đun!';
$_['text_edit'] = 'Chỉnh sửa mô-đun';
$_['text_module'] = 'Mô-đun';
$_['text_complete'] = 'Hoàn thành';
$_['text_error'] = 'Lỗi';

// Mục nhập
$_['entry_status'] = 'Trạng thái';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi mô-đun!';
$_['error_uneosystem'] = 'Đã xảy ra lỗi không mong muốn. Vui lòng thử lại sau hoặc liên hệ với nhà phát triển mô-đun với ID lỗi sau: ';

// Sự thành công
$_['success_enable_seo_urls'] = 'Tuyệt vời! Cài đặt để sử dụng URL SEO hiện đã được bật! ';
$_['success_migration_text'] = 'Tuyệt vời! Các URL SEO đã được di chuyển thành công! Làm mới trang để xem kết quả mới :) ';
$_['success_saved_settings'] = 'Cài đặt được cập nhật thành công!';

// Cấp phép
$_['text_your_license'] = 'Giấy phép của bạn';
$_['text_please_enter_the_code'] = 'Vui lòng nhập mã giấy phép mua sản phẩm của bạn:';
$_['text_activate_license'] = 'Kích hoạt Giấy phép';
$_['text_not_having_a_license'] = 'Bạn không có mã? Lấy nó từ đây. ';
$_['text_license_holder'] = 'Chủ Giấy phép';
$_['text_registered_domains'] = 'Tên miền đã đăng ký';
$_['text_expires_on'] = 'Giấy phép Hết hạn vào';
$_['text_valid_license'] = 'GIẤY PHÉP HỢP LỆ';
$_['text_manage'] = 'quản lý';
$_['text_get_support'] = 'Nhận hỗ trợ';
$_['text_community'] = 'Cộng đồng';
$_['text_ask_our_community'] = 'Chúng tôi có một cộng đồng lớn. Bạn có thể tự do hỏi nó về vấn đề của bạn trên diễn đàn. ';
$_['text_browse_forums'] = 'Duyệt qua các diễn đàn';
$_['text_tickets'] = 'Vé';
$_['text_open_a_ticket'] = 'Bạn muốn giao lưu 1-1 với những người công nghệ của chúng tôi? Sau đó, mở một phiếu hỗ trợ. ';
$_['text_open_ticket_for_real'] = 'Mở vé';
$_['text_pre_sale'] = 'Bán trước';
$_['text_pre_sale_text'] = 'Bạn có một ý tưởng tuyệt vời cho cửa hàng web của mình? Nhóm các nhà phát triển của chúng tôi có thể biến nó thành hiện thực. ';
$_['text_bump_the_sales'] = 'Tăng doanh số bán hàng';

// Sửa nội dung
$_['fix_config_seo_url'] = 'Bật SEO URL';
$_['fix_seo_images_rename'] = 'Sử dụng Công cụ Đổi tên Hình ảnh ít nhất một lần';
$_['fix_product_seo_urls'] = 'Thêm URL SEO cho sản phẩm của bạn';
$_['fix_category_seo_urls'] = 'Thêm URL SEO cho danh mục của bạn';
$_['fix_information_seo_urls'] = 'Thêm URL SEO cho các trang thông tin của bạn';
$_['fix_manooter_seo_urls'] = 'Thêm URL SEO cho các trang nhà sản xuất của bạn';
$_['fix_product_seo_titles'] = 'Thêm Meta Titles cho sản phẩm của bạn';
$_['fix_category_seo_titles'] = 'Thêm Meta Titles cho danh mục của bạn';
$_['fix_information_seo_titles'] = 'Thêm Meta Titles cho các trang thông tin của bạn';
$_['fix_product_seo_descriptions'] = 'Thêm mô tả meta cho sản phẩm của bạn';
$_['fix_category_seo_descriptions'] = 'Thêm mô tả meta cho danh mục của bạn';
$_['fix_information_seo_descriptions'] = 'Thêm mô tả meta cho thông tin của bạn';
$_['fix_product_seo_keywords'] = 'Thêm Từ khoá Meta cho sản phẩm của bạn';
$_['fix_category_seo_keywords'] = 'Thêm Từ khoá Meta cho danh mục của bạn';
$_['fix_information_seo_keywords'] = 'Thêm Từ khoá Meta cho các trang thông tin của bạn';
$_['fix_richsnippets_product_data'] = 'Thêm các đoạn mã chi tiết cho dữ liệu sản phẩm';
$_['fix_richsnippets_product_breadcrumbs'] = 'Thêm Rich Snippets cho Product Breadcrumbs';
$_['fix_richsnippets_company_info'] = 'Thêm các đoạn mã phong phú cho thông tin cửa hàng';
$_['fix_htaccess_exists'] = 'Đảm bảo rằng .htaccess tồn tại và nó được định cấu hình chính xác';
$_['fix_robots_exists'] = 'Đảm bảo rằng robots.txt tồn tại và nó được định cấu hình chính xác';

// Trình trợ giúp hình ảnh
$_['image_zoom_helper'] = 'Bấm để xem ảnh ở kích thước đầy đủ ...';

// Nhãn
$_['tab_home'] = 'Trang tổng quan';
$_['tab_urls_linking'] = 'URL & Liên kết';
$_['tab_content'] = 'Nội dung';
$_['tab_social_seo'] = 'SEO trên mạng xã hội';
$_['tab_structured_data'] = 'Dữ liệu có cấu trúc';
$_['tab_image_names'] = 'Tên hình ảnh';
$_['tab_page_crawler'] = 'Trình thu thập thông tin trang';
$_['tab_seo_analysis'] = 'Phân tích Công cụ Tìm kiếm';
$_['tab_file_editor'] = 'Trình chỉnh sửa tập tin';
$_['tab_what_is_seo'] = 'Tài liệu';
$_['tab_support'] = 'Hỗ trợ';
$_['tab_advanced_editor'] = 'Trình chỉnh sửa nâng cao';
$_['tab_feed'] = 'Nguồn cấp sơ đồ trang web';

$_['tab_basic_seo'] = 'SEO cơ bản';
$_['tab_advanced_seo'] = 'SEO nâng cao';
$_['text_seo_score'] = 'Điểm SEO';
$_['text_seo_score_for'] = 'Điểm SEO của bạn';
$_['button_test_again'] = 'Kiểm tra lại';
$_['text_passed'] = 'Đạt:';
$_['text_to_improve'] = 'Cải thiện:';
$_['text_errors'] = 'Lỗi:';
$_['text_what_can_be_improved'] = 'Các bước để cải thiện';
$_['text_fix_now'] = 'Khắc phục ngay!';
$_['text_running_tests'] = 'Đang chạy thử nghiệm, vui lòng đợi ...';
$_['text_loading_please_wait'] = 'Đang tải, vui lòng đợi ...';
$_['text_performing_operation'] = 'Thực hiện một thao tác ...';
$_['text_uneosystem_error_id'] = 'Lỗi không mong muốn! Vui lòng liên hệ với các nhà phát triển của mô-đun. ID lỗi: ';
$_['text_image_renamer_tool_heading'] = 'Công cụ đổi tên hình ảnh sản phẩm';
$_['text_image_last_usage'] = 'Lần sử dụng cuối cùng vào:';
$_['text_osystem'] = 'Không bao giờ';
$_['text_image_last_results'] = 'Kết quả từ lần sử dụng gần đây nhất:';
$_['text_not_used'] = 'Bạn chưa sử dụng tính năng này.';
$_['text_images_rename'] = 'Hình ảnh được đổi tên:';
$_['text_images_already_rename'] = 'Đã được đổi tên:';
$_['text_product_images_ids'] = 'Sản phẩm không có hình ảnh (ID sản phẩm):';
$_['text_errors'] = 'Lỗi:';
$_['text_no_errors'] = 'Không có lỗi';
$_['text_yes_image'] = 'CÓ!';
$_['text_none'] = 'Không có';
$_['text_generate_image_names'] = 'Tạo tên hình ảnh mới!';
$_['text_generate_additional_image_names'] = 'Tạo tên hình ảnh mới cho các hình ảnh bổ sung';
$_['text_advanced_settings'] = 'Cài đặt nâng cao';
$_['text_image_string'] = 'Chuỗi hình ảnh:';
$_['text_short_codes'] = 'Các mã ngắn có sẵn:';
$_['text_save_changes'] = 'Lưu thay đổi!';
$_['text_products'] = 'Sản phẩm';
$_['text_categories'] = 'Danh mục';
$_['text_manosystem Enterprises'] = 'Nhà sản xuất';
$_['text_information_pages'] = 'Trang Thông tin';
$_['text_total_items'] = 'Tổng số mục';
$_['text_seo_urls'] = 'URL SEO';
$_['text_missing_urls'] = 'Thiếu URL';
$_['text_action'] = 'Hành động';
$_['text_start_fresh'] = 'Bắt ​​đầu làm mới';
$_['text_generate'] = 'Tạo';
$_['text_fix_missing_urls_count'] = 'Số lượng sửa chữa';
$_['text_product_url_string'] = 'Chuỗi URL sản phẩm:';
$_['text_category_url_string'] = 'Chuỗi URL danh mục:';
$_['text_manooter_url_string'] = 'Chuỗi URL của nhà sản xuất:';
$_['text_information_url_string'] = 'Chuỗi URL thông tin:';
$_['text_random_shortcode'] = '* [ngẫu nhiên] - Tạo một chuỗi ký tự ngẫu nhiên để tránh các URL trùng lặp<br />* [lang] - Thêm mã ngôn ngữ trong URL SEO. Ví dụ: nếu trang của bạn bằng tiếng Anh, URL sẽ có "en" trong slug của nó';
$_['text_meta_titles'] = 'Tiêu đề Meta';
$_['text_missing_meta_titles'] = 'Thiếu tiêu đề';
$_['text_product_title_string'] = 'Chuỗi tiêu đề sản phẩm:';
$_['text_category_title_string'] = 'Chuỗi tiêu đề danh mục:';
$_['text_information_title_string'] = 'Chuỗi tiêu đề thông tin:';
$_['text_tag_word_limit'] = 'Chỉ định bao nhiêu từ từ văn bản mô tả sẽ được sử dụng.';
$_['text_meta_keywords'] = 'Từ khoá Meta';
$_['text_missing_keywords'] = 'Thiếu Từ khoá';
$_['text_meta_descriptions'] = 'Mô tả meta';
$_['text_missing_descriptions'] = 'Thiếu mô tả';
$_['text_product_keywords_string'] = 'Chuỗi Từ khoá Sản phẩm:';
$_['text_category_keywords_string'] = 'Chuỗi Từ khoá Danh mục:';
$_['text_information_keywords_string'] = 'Chuỗi Từ khoá Thông tin:';
$_['text_product_description_string'] = 'Chuỗi mô tả sản phẩm:';
$_['text_category_description_string'] = 'Chuỗi mô tả danh mục:';
$_['text_information_description_string'] = 'Chuỗi mô tả thông tin:';
$_['text_description_tag_word_limit'] = '[sự miêu tả] Giới hạn từ khóa: ';
$_['text_add_new_autolink'] = 'Thêm liên kết mới';
$_['text_keyword'] = 'Từ khóa';
$_['text_link'] = 'Liên kết';
$_['text_add_custom_url'] = 'Thêm URL tùy chỉnh mới';
$_['text_route'] = 'Tuyến đường';
$_['text_custom_url'] = 'URL tùy chỉnh';
$_['text_settings'] = 'Cài đặt';
$_['text_enable_product_data'] = 'Cho phép Dữ liệu Sản phẩm';
$_['text_enable_product_breadcrumbs'] = 'Kích hoạt Đường dẫn cho Sản phẩm';
$_['text_enable_category_breadcrumbs'] = 'Bật đường dẫn cho danh mục';
$_['text_yes'] = 'Có';
$_['text_no'] = 'Không';
$_['text_additonal_information'] = 'Thông tin bổ sung:';
$_['text_additonal_information_helper'] = 'Các tùy chọn bao gồm thêm các đoạn mã theo sau:';
$_['text_product_data'] = 'Dữ liệu sản phẩm';
$_['text_product_data_helper'] = '- Tên, mô tả, hình ảnh, kiểu máy, giá (đặc biệt), nhà sản xuất, xếp hạng, phiếu bầu, đơn vị tiền tệ giá, tình trạng còn hàng';
$_['text_product_breadcrumbs'] = 'Đường dẫn sản phẩm';
$_['text_product_breadcrumbs_helper'] = '- Dữ liệu đường dẫn đầy đủ cho các sản phẩm trong trang sản phẩm, bao gồm cả đường dẫn danh mục mà sản phẩm đã cho được thêm vào';
$_['text_enable_fb_og'] = 'Kích hoạt Facebook Open Graph';
$_['text_app_id'] = 'ID ứng dụng';
$_['text_enable_twitter_card'] = 'Bật thẻ Twitter';
$_['text_username'] = 'Tên người dùng';
$_['text_enable_google_publisher'] = 'Bật nhà xuất bản của Google';
$_['text_google_plus_id'] = 'ID Google+';
$_['text_enable'] = 'Bật';
$_['text_disable'] = 'Tắt';
$_['text_migrate_seo_urls'] = 'Di chuyển URL SEO';
$_['text_old_seo_urls_detected'] = 'Đã phát hiện các URL SEO cũ!';
$_['text_old_seo_urls_helper'] = 'Chúng tôi nhận thấy rằng bạn đã có URL SEO cho một số trang của mình. Thật không may, chúng sẽ không hoạt động với logic mới, được triển khai bởi SEO Backpack. Nếu bạn muốn giữ chúng, bạn có thể nhấp vào nút "Di chuyển URL SEO" ở cuối thông báo này. Ngoài ra, nếu bạn muốn bắt đầu lại, chỉ cần tạo các URL SEO mới bằng cách sử dụng các chức năng của mô-đun và thông báo này sẽ biến mất. ';

$_['text_advanced_seo'] = 'SEO nâng cao';
$_['text_custom_url_redirects'] = 'Chuyển hướng URL tùy chỉnh';
$_['text_auto_links'] = 'Liên kết tự động';
$_['text_rich_snippets'] = 'Các đoạn mã chi tiết';
$_['text_social_links'] = 'Liên kết xã hội';
$_['text_titles'] = 'Tiêu đề';
$_['text_descriptions'] = 'Mô tả';
$_['text_keywords'] = 'Từ khóa';
$_['text_seo_links'] = 'Liên kết SEO';
$_['text_image_titles'] = 'Tiêu đề hình ảnh';
$_['text_delete'] = 'Xóa';
$_['text_delete_confirmation'] = 'Bạn có chắc chắn muốn xóa URL tùy chỉnh đã chọn không? Điều này không thể được hoàn tác.';
$_['text_delete_a_confirmation'] = 'Bạn có chắc chắn muốn xóa tự động liên kết đã chọn không? Điều này không thể được hoàn tác.';
$_['text_opencart_route'] = 'Lộ trình OpenCart:';
$_['text_cancel'] = 'Hủy bỏ';
$_['text_submit'] = 'Gửi';
$_['text_url_address'] = 'Địa chỉ URL:';
$_['text_fields_filled_in_helper'] = '* Đảm bảo rằng cả hai trường đều được điền.';
$_['text_all_fields_filled_in_helper'] = '* Đảm bảo rằng tất cả các trường đều được điền.';
$_['text_confirm'] = 'Xác nhận';
$_['text_edit_autolink'] = 'Chỉnh sửa liên kết tự động';
$_['text_keyword_field'] = 'Từ khóa:';
$_['text_edit_customurl'] = 'Chỉnh sửa URL tùy chỉnh';
$_['text_seo_score_last_checked'] = 'Kiểm tra lần cuối:';
$_['text_what_is_already_improved'] = 'Đã thực hiện xong các bước';
$_['text_success_btn'] = 'Thành công!';
$_['text_show_more'] = 'Hiển thị thêm ...';
$_['text_show_less'] = 'Hiển thị bớt ...';
$_['text_no_custom_urls'] = 'Này, bạn chưa có URL tùy chỉnh nào! Kiểm tra cách tạo một trong cột bên phải. ';
$_['text_no_auto_links'] = 'Này, bạn chưa có một liên kết tự động nào! Kiểm tra cách tạo một trong cột bên phải. ';
$_['text_confirm_action'] = 'Hộp thoại Xác nhận';
$_['text_confirm_image_rename'] = 'Hành động này sẽ cố gắng đổi tên tên tệp hình ảnh của sản phẩm của bạn. Đảm bảo đọc thông tin trợ giúp ở phía bên phải của trang. <br /><br />Bạn có muốn tiếp tục?';

$_['text_delete_selected_items'] = 'Xóa các mục đã chọn';
$_['text_show'] = 'Hiển thị';
$_['text_cyrillic_url'] = 'Tắt tính năng chuyển ngữ của URL Kirin';
$_['text_redirect_to_seo_links'] = 'Luôn chuyển hướng đến các URL thân thiện với SEO';
$_['text_product_heading_tags'] = 'Thẻ tiêu đề sản phẩm';
$_['text_h1_heading_tags'] = 'Thẻ H1';
$_['text_h2_heading_tags'] = 'Thẻ H2';
$_['text_missing_heading_tags'] = 'Thiếu thẻ tiêu đề';
$_['text_h1_heading_tags_string'] = 'Chuỗi thẻ H1';
$_['text_h2_heading_tags_string'] = 'Chuỗi thẻ H2';
$_['text_default_lang_prefix'] = 'Bao gồm tiền tố ngôn ngữ mặc định';
$_['text_redirect_active_lang_prefix'] = 'Chuyển hướng tiền tố ngôn ngữ hoạt động';
$_['text_subfolder_prefixes_alias'] = 'Bí danh tiền tố ngôn ngữ duy nhất';

// Cho ăn
$_['text_sitemap_feed'] = 'Nguồn cấp sơ đồ trang web';
$_['text_sitemap_feed_setting'] = 'Cài đặt';
$_['text_feed_product_limit'] = 'Giới hạn sản phẩm hiển thị';
$_['text_feed_category_product'] = 'Hiển thị sản phẩm cho từng danh mục';
$_['text_feed_manooter_product'] = 'Hiển thị sản phẩm của từng nhà sản xuất';
$_['text_feed_url'] = 'Url nguồn cấp dữ liệu';

$_['text_sitemap_feed_helper'] = '<p>Nguồn cấp dữ liệu sơ đồ trang web cho trình thu thập thông tin biết rằng các url trong nguồn cấp dữ liệu sơ đồ trang web là tìm kiếm chất lượng tốt và các trang đích được cập nhật, xứng đáng được lập chỉ mục hoặc cập nhật chỉ mục tìm kiếm.</p>';
$_['text_feed_product_limit_helper'] = '<p>Giới hạn sản phẩm được hiển thị trong <b>đơn hàng cập nhật mới nhất</b>. Insert <code>-1</code> để hiển thị tất cả các sản phẩm.</p>';
$_['text_feed_category_product_helper'] = '<p>Liệt kê các url sản phẩm cho mỗi cài đặt giới hạn cho từng danh mục.</p>';
$_['text_feed_manufacturer_product_helper'] = '<p>Liệt kê các url sản phẩm cho mỗi cài đặt giới hạn cho từng nhà sản xuất.</p>';


// FAQ
$_['text_general'] = 'Chung';
$_['text_general_helper'] = 'Với tư cách là chủ cửa hàng OpenCart, một lượng khách hàng hợp lý đến với bạn thông qua Google, Bing hoặc một số công cụ tìm kiếm khác, vì vậy điều quan trọng là phải có một trang web thân thiện với công cụ tìm kiếm. Đó là lý do tại sao bạn cần liên tục cải thiện nội dung các trang trên trang web của mình. Điều đó bao gồm sử dụng một số kỹ thuật SEO. ';
$_['text_what_is_seo'] = 'SEO là gì?';
$_['text_what_is_seo_text'] = 'SEO là từ viết tắt của "Tối ưu hóa công cụ tìm kiếm". Nó là một kỹ thuật giúp các công cụ tìm kiếm hiểu rõ hơn về các trang web. Mục đích của việc tối ưu hóa là giao tiếp với Google, Bing và các tìm kiếm khác theo cách mà họ có thể giới thiệu trang web của bạn cho những khách hàng khác. Điều này sẽ giúp tăng lượng khách truy cập và tăng doanh thu. <br /> <br />
SEO kết hợp hai nhóm yếu tố chính cần thiết để các công cụ tìm kiếm hiểu rõ hơn về trang web của bạn. Đây là tối ưu hóa On-page và tối ưu hóa Off -page. Tối ưu hóa ngoài trang, như tên cho thấy, không được kết nối trực tiếp với nội dung HTML thực tế của trang web của bạn và nó không hoàn toàn nằm trong tầm kiểm soát của bạn. Mặt khác, tối ưu hóa trên trang đề cập đến văn bản và nội dung của các trang web của bạn và hoàn toàn phụ thuộc vào nhà phát triển của trang web. ';
$_['text_need_seo'] = 'Tôi có thực sự cần SEO không?';
$_['text_need_seo_text'] = 'SEO có thể rất hữu ích. Không quan trọng nếu bạn đã có một cửa hàng thành công hay chỉ là một vài lần bán hàng. Thực hiện đúng, tối ưu hóa có thể tăng lượt truy cập trang web của bạn. Càng nhiều lượt ghé thăm cửa hàng của bạn, bạn càng có nhiều đơn đặt hàng. Nó đơn giản như vậy.';
$_['text_seo_damage'] = 'SEO có thể làm hỏng cửa hàng của tôi không?';
$_['text_seo_damage_text'] = 'Tối ưu hóa SEO có thể khiến mọi thứ tồi tệ hơn nếu không được thực hiện tốt. May mắn thay, tất cả các tính năng trong mô-đun của chúng tôi đều thân thiện với công cụ tìm kiếm và sẽ xếp hạng bạn cao hơn trong kết quả tìm kiếm nếu bạn thiết lập chúng đúng cách. ';

$_['text_basic_seo'] = 'SEO cơ bản';
$_['text_basic_seo_helper'] = 'Dữ liệu meta rất quan trọng và đây là lý do bạn nên tập trung vào đó.';
$_['text_meta_tag_title'] = 'Tiêu đề thẻ meta';
$_['text_meta_tag_description'] = 'Mô tả thẻ meta';
$_['text_meta_tag_keywords'] = 'Từ khoá Thẻ meta';
$_['text_in_conclustion'] = 'Kết luận';
$_['text_in_conclustion_helper'] = 'Tối ưu hóa công cụ tìm kiếm thường là một nhiệm vụ thực sự khó khăn, nhưng ngày nay nó quan trọng hơn bao giờ hết. Đôi khi có công cụ phù hợp để thực hiện tối ưu hóa On-page của bạn có thể là con bài chiến thắng trong tay bạn. Sản phẩm của chúng tôi cung cấp cho bạn cách đơn giản nhưng hiệu quả để biến trang web của bạn trở thành lựa chọn đầu tiên của công cụ tìm kiếm. ';
$_['text_more_about_seo'] = 'Bạn có thể tìm hiểu thêm về SEO từ các bài viết dưới đây:';
$_['text_already_improved_great_result'] = 'Xin chúc mừng! Không tìm thấy vấn đề. ';
$_['text_seo_score_metering'] = 'Kết quả này có nghĩa là gì?';
$_['text_htaccess'] = 'Trình soạn thảo htaccess';
$_['text_empty_file'] = 'Có vẻ như yêu cầu trống nên mô-đun sẽ không ghi đè lên tệp. Nếu bạn cho rằng đây là sự nhầm lẫn, vui lòng liên hệ với nhà phát triển của mô-đun và cho họ biết ID lỗi sau: ERR_HTEMPT ';
$_['text_empty_file_robots'] = 'Có vẻ như yêu cầu trống nên mô-đun sẽ không ghi đè lên tệp. Nếu bạn cho rằng đây là lỗi, vui lòng liên hệ với nhà phát triển của mô-đun và cho họ biết ID lỗi sau: ERR_RBEMPT ';
$_['text_robots'] = 'Trình chỉnh sửa robots.txt';
$_['text_text_editor'] = 'Trình soạn thảo văn bản';
$_['text_enable_hreflang'] = 'Bật thẻ "hreflang"';
$_['text_products'] = 'Sản phẩm:';
$_['text_categories'] = 'Danh mục:';
$_['text_manosystem Enterprises'] = 'Nhà sản xuất:';
$_['text_informations'] = 'Trang thông tin:';

$_['fix_hreflang_products'] = 'Bật thẻ "hreflang" cho sản phẩm';
$_['fix_hreflang_categories'] = 'Bật thẻ "hreflang" cho các danh mục';
$_['fix_hreflang_manosystemkers'] = 'Bật thẻ "hreflang" cho các nhà sản xuất';
$_['fix_hreflang_informations'] = 'Bật thẻ "hreflang" cho các trang thông tin';

$_['text_editor_products'] = 'Sản phẩm';
$_['text_editor_categories'] = 'Danh mục';
$_['text_editor_information_pages'] = 'Trang Thông tin';
$_['text_editor_manosystem'] = 'Nhà sản xuất';

$_['text_editor_name'] = 'Tên';
$_['text_editor_meta_title'] = 'Tiêu đề Meta';
$_['text_editor_meta_description'] = 'Mô tả Meta';
$_['text_editor_meta_keyword'] = 'Từ khoá Meta';
$_['text_editor_seo_keyword'] = 'Từ khoá SEO';

$_['text_no_results'] = 'Hiện tại không có kết quả :/';
$_['text_empty_field_alert'] = 'Bạn không được để trống trường này!';
    
// Trình thu thập thông tin
$_['text_enter_a_web_page_url'] = 'Chọn từ trên cùng hoặc viết tùy chỉnh';
$_['text_seo_crawler'] = 'Thông tin chi tiết về trình thu thập thông tin';
$_['tab_seo_crawler'] = 'Trình thu thập thông tin SEO';
$_['text_analyzing_pls_wait'] = 'Đang phân tích, vui lòng đợi ...';
$_['text_crawler_error_heading'] = 'Chúng tôi đã gặp lỗi!';
$_['text_crawler_url_error'] = 'Trường URL trống hoặc văn bản đã nhập không phải là URL. Vui lòng thử lại.';
$_['text_heading_titles'] = 'Tiêu đề đầu trang';
$_['text_long_small_headings'] = 'Các tiêu đề quá dài hoặc quá ngắn';
$_['text_long_small_headings_helper'] = 'Các thẻ tiêu đề phải có từ 15 đến 65 ký tự.';
$_['text_heading'] = 'Tiêu đề';
$_['text_text'] = 'Văn bản';
$_['text_missing_h1'] = 'Trang không có thẻ H1!';
$_['text_many_h1_tags'] = 'Bạn có nhiều hơn một thẻ H1!';
$_['text_long_small_h1_tag'] = 'Bạn chỉ có một thẻ H1, nhưng nó quá ngắn / dài';
$_['text_h1_tag'] = 'Bạn chỉ có một thẻ H1 và nó trông rất hoàn hảo!';
$_['text_h1_label'] = 'Tiêu đề H1 của bạn:';
$_['text_h1_missing'] = 'Điều quan trọng là đảm bảo mọi trang đều có thẻ H1. Tuy nhiên, chỉ sử dụng nhiều hơn một trên mỗi trang nếu bạn đang sử dụng HTML5. Nếu bạn muốn có nhiều hơn một tiêu đề trong trang của mình, hãy sử dụng nhiều thẻ H2 - H6 và tuân theo thứ bậc của chúng. ';
$_['text_title_tag'] = 'Thẻ tiêu đề';
$_['text_description_tag'] = 'Thẻ mô tả';
$_['text_title_tag_helper'] = 'Độ dài tiêu đề tối ưu là khoảng 55, bởi vì Google thường hiển thị 50-60 ký tự đầu tiên của thẻ tiêu đề trong trang kết quả của công cụ tìm kiếm (SERP). Tuy nhiên, hãy đảm bảo rằng trang của bạn không quá nhỏ và cố gắng dài ít nhất 10 ký tự. ';
$_['text_long_small_title_tag'] = 'Thẻ tiêu đề của bạn quá ngắn / dài';
$_['text_title_perfect_tag'] = 'Thẻ tiêu đề của bạn trông rất đẹp!';
$_['text_meta_description_tag_helper'] = 'Mô tả meta có thể dài bất kỳ, nhưng các công cụ tìm kiếm thường cắt bớt các đoạn dài hơn 160 ký tự. Tốt nhất là giữ các mô tả meta từ 150 đến 160 ký tự. ';
$_['text_long_small_meta_description_tag'] = 'Thẻ mô tả của bạn quá ngắn / dài';
$_['text_meta_description_perfect_tag'] = 'Thẻ mô tả của bạn trông rất đẹp!';
$_['text_character_length'] = 'Độ dài ký tự:';
$_['text_character_text'] = 'Văn bản:';
$_['text_no_og_tags'] = 'Hiện tại bạn không có thẻ Open Graph';
$_['text_og_tags_success'] = 'Tuyệt vời! Bạn có thẻ Open Graph trên trang của mình! ';
$_['text_og_tags_helper'] = 'Thẻ Open Graph được sử dụng để tích hợp giữa Facebook và trang web của bạn. Các thẻ trở thành các đối tượng "đồ thị" phong phú với chức năng tương tự như các đối tượng Facebook khác. Họ có thể hiển thị thêm thông tin (tên, mô tả, hình ảnh hoặc giá cả) trong các bài đăng, trỏ đến cửa hàng của bạn. Thẻ Open Graph cũng được Twitter, Google+ và LinkedIn công nhận. ';
$_['text_og_tag'] = 'Mở đồ thị';
$_['text_og_tags_listing'] = 'Bạn đã thêm các thẻ sau vào trang:';
$_['text_tag'] = 'Thẻ';
$_['text_content'] = 'Nội dung';
$_['text_analyze'] = 'Phân tích';
$_['text_twitter_tags'] = 'Thẻ Twitter';
$_['text_no_twitter_tags'] = 'Hiện tại bạn không có thẻ Twitter Card';
$_['text_twitter_tags_success'] = 'Tuyệt vời! Bạn có thẻ Twitter Card trên trang của mình! ';
$_['text_twitter_tags_helper'] = 'Thẻ Twitter là biến thể của thẻ Open Graph. Chúng được tạo riêng cho Twitter bằng cách hiển thị thông tin bổ sung trên các tweet dưới dạng bài đăng mô tả, trỏ đến cửa hàng của bạn. ';
$_['text_canonical_tag'] = 'URL chuẩn';
$_['text_canonical_missing_tag'] = 'Bạn không có thẻ URL chuẩn!';
$_['text_canonical_tag_success'] = 'Tuyệt vời! Bạn đã đặt một thẻ chuẩn! ';
$_['text_canonical_url'] = 'URL chuẩn:';
$_['text_canonical_tested_url'] = 'URL được phân tích:';
$_['text_canonical_helper'] = 'Phần tử liên kết chuẩn là một phần tử HTML giúp quản trị viên web ngăn chặn các vấn đề nội dung trùng lặp bằng cách chỉ định phiên bản "chuẩn" hoặc "ưu tiên" của trang web như một phần của tối ưu hóa công cụ tìm kiếm.';
$_['text_server_speed'] = 'Thời gian phản hồi của máy chủ';
$_['text__seconds'] = 'giây';
$_['text_ttfb_time'] = 'TTFB:';
$_['text_site_loading_time'] = 'Tải trang web:';
$_['text_ttfb_helper'] = '<strong> Thời gian đến Byte đầu tiên (TTFB) </strong> là một phép đo được sử dụng như một chỉ báo về khả năng đáp ứng của máy chủ web hoặc tài nguyên mạng khác. TTFB đo khoảng thời gian từ người dùng hoặc ứng dụng khách thực hiện yêu cầu HTTP đến byte đầu tiên của trang được trình duyệt của ứng dụng khách nhận. Theo Google PageSpeed ​​Insights, <strong> thời gian phản hồi của máy chủ phải dưới 200 mili giây (0,2 giây) </strong> ';
$_['text_site_load_helper'] = '<strong> Tải trang </strong> cho bạn biết cần bao nhiêu thời gian để tải toàn bộ trang.';
$_['text_server_response_time_bad'] = 'Thời gian phản hồi máy chủ của bạn rất chậm!';
$_['text_server_response_time_medium'] = 'Thời gian phản hồi cho trang đó là hơn 200 mili giây nhưng không quá 1 giây';
$_['text_server_response_time_good'] = 'Tuyệt vời! Thời gian phản hồi máy chủ của bạn dưới 200ms ';
$_['text_ssl_helper'] = 'SSL (Lớp cổng bảo mật) là công nghệ bảo mật tiêu chuẩn để thiết lập liên kết được mã hóa giữa máy chủ web và trình duyệt. Liên kết này đảm bảo rằng tất cả dữ liệu được truyền giữa máy chủ web và trình duyệt vẫn riêng tư và không thể tách rời. ';
$_['text_ssl_helper2'] = 'Trở lại năm 2014, Google đã thông báo rằng họ bao gồm HTTPS làm tín hiệu xếp hạng, có nghĩa là mọi trang web có chứng chỉ SSL hợp lệ sẽ được xếp hạng cao hơn những trang không có chứng chỉ SSL.';
$_['text_https_ssl'] = 'HTTPS (SSL)';
$_['text_https_is_supported'] = 'Trang web của bạn có chứng chỉ SSL!';
$_['text_https_is_not_supported'] = 'Trang của bạn không có chứng chỉ SSL.';
$_['text_url_not_accessible'] = 'Không thể truy cập URL, vui lòng thử lại sau hoặc bằng URL khác.';
$_['text_home_page'] = 'Trang chủ';
$_['text_product_page'] = 'Trang Sản phẩm';
$_['text_category_page'] = 'Trang chuyên mục';
$_['text_information_page'] = 'Trang thông tin';
$_['text_enable_company_info'] = 'Bật thông tin cửa hàng';
$_['text_crawler'] = 'Trình thu thập thông tin';
$_['text_visited_url'] = 'URL đã truy cập';
$_['text_date_time'] = 'Đã truy cập vào';
$_['text_no_crawler_results'] = 'Hiện tại không có bản ghi nào ở đây. Chờ một lúc để mô-đun thu thập dữ liệu. ';
$_['text_date_start'] = 'Ngày bắt đầu';
$_['text_date_end'] = 'Ngày kết thúc';
$_['text_url'] = 'URL';
$_['text_filter'] = 'Bộ lọc';
$_['text_loading_data'] = 'Đang tải dữ liệu, vui lòng đợi ...';
$_['text_crawler_actions'] = 'Thao tác có sẵn';
$_['text_clear_analysis_list'] = 'Xóa danh sách';
$_['text_confirm_clear_analysis'] = 'Thao tác này sẽ xóa các kết quả thu thập được trong quá trình phân tích. <br /> <br /> Bạn có muốn tiếp tục không?';
$_['tab_seo_links_title'] = 'Nâng cao';
$_['tab_seo_meta_titles_title'] = 'Nâng cao';
$_['tab_seo_meta_descriptions_title'] = 'Nâng cao';
$_['tab_seo_meta_keywords_title'] = 'Nâng cao';
$_['tab_seo_product_heading_tags_title'] = 'Nâng cao';

// Helpers
$_['tab_seo_meta_titles_helper_title'] = 'Meta Titles';
$_['tab_seo_meta_titles_helper'] = '
<p>Tiêu đề Meta là tiêu đề chính của trang và nó xuất hiện trong kết quả của Google (SERP) khi thực hiện tìm kiếm.</p>
<p><a title="'.$_['image_zoom_helper'].'" href="view/image/isenselabs_seo/meta_title_preview_1.png" data-toggle="lightbox"><img class="img img-responsive img-thumbnail img-small" src="view/image/isenselabs_seo/meta_title_preview_1.png" /></a></p>
<br />
<p><h4>Hoạt động có sẵn:</h4></p>
<p><strong>Tạo ra</strong> - Chỉ tạo tiêu đề meta cho các mục không có chúng.</p>
<p><strong>Bắt đầu</strong> - Loại bỏ tất cả các tiêu đề meta hiện tại và tạo các tiêu đề mới.</p>
<p>Meta Title của một trang web được hiểu là một mô tả chính xác và ngắn gọn về nội dung của một trang. Dưới đây, bạn có thể thấy Meta Title trông như thế nào trong trình duyệt:</p>
<div class="helper">
    <p><a title="'.$_['image_zoom_helper'].'" href="view/image/isenselabs_seo/meta_title_preview_2.png" data-toggle="lightbox"><img class="img img-responsive img-thumbnail img-small" src="view/image/isenselabs_seo/meta_title_preview_2.png" /></a></p>
</div>
';

$_['tab_seo_product_heading_tags_helper_title'] = 'Heading Tags';
$_['tab_seo_product_heading_tags_helper'] = '
<p>Thẻ H1 và H2 được sử dụng trong các trang sản phẩm. Thẻ H1 được sử dụng cho tên của sản phẩm. Thẻ H2 được sử dụng như một tiêu đề về sự hạn chế của sản phẩm .</p>
<p><a title="'.$_['image_zoom_helper'].'" href="view/image/isenselabs_seo/heading_tags.png" data-toggle="lightbox"><img class="img img-responsive img-thumbnail img-small" src="view/image/isenselabs_seo/heading_tags.png" /></a></p>
<br />
<p><a title="'.$_['image_zoom_helper'].'" href="view/image/isenselabs_seo/heading_tags_preview.png" data-toggle="lightbox"><img class="img img-responsive img-thumbnail img-small" src="view/image/isenselabs_seo/heading_tags_preview.png" /></a></p>
<br />
<p><h4>Hoạt động có sẵn:</h4></p>
<p><strong>Tạo ra</strong> - Chỉ tạo thẻ tiêu đề cho các mục không có thẻ.</p>
<p><strong>Bắt đầu</strong> - Loại bỏ tất cả các tiêu đề meta hiện tại và tạo các tiêu đề mới.</p>
';

$_['tab_seo_meta_descriptions_helper_title'] = 'Meta Descriptions';
$_['tab_seo_meta_descriptions_helper'] = '
<p>Meta Description là một thuộc tính HTML mô tả ngắn gọn nội dung của các trang web. Chúng được sử dụng trên kết quả tìm kiếm (SERPs) để hiển thị các đoạn trích cho một trang nhất định.
</p>
<p>Tầm quan trọng của Mô tả Meta đối với xếp hạng của bạn là thứ yếu so với Tiêu đề Meta. Tuy nhiên, chúng hiển thị thông tin có giá trị cho người dùng xem trước khi họ mở một trang.</p>
<br />
<p><h4>Hoạt động có sẵn:</h4></p>
<p><strong>Tạo ra</strong> - Chỉ tạo mô tả meta cho các mục không có chúng.</p>
<p><strong>Bắt đầu</strong> - Loại bỏ tất cả các mô tả meta hiện tại và tạo các mô tả mới.</p>
<div class="helper">
    <p>Tại đây, bạn có thể xem ví dụ về thẻ mô tả meta trong SERP:</p>
    <p><a title="'.$_['image_zoom_helper'].'" href="view/image/isenselabs_seo/meta_description_preview.png" data-toggle="lightbox"><img class="img img-responsive img-thumbnail img-small" src="view/image/isenselabs_seo/meta_description_preview.png" /></a></p>
</div>
';

$_['tab_seo_meta_keywords_helper_title'] = 'Meta Keywords';
$_['tab_seo_meta_keywords_helper'] = '
<p>Từ khóa Meta là một thuộc tính HTML được sử dụng để chỉ định một tập hợp các từ khóa có liên quan đến một trang nhất định.
</p>
<p>Mặc dù các Từ khoá Meta không còn là một yếu tố quan trọng đối với Google, nhưng chúng có một đóng góp nhỏ trong việc xếp hạng trong các công cụ tìm kiếm khác như Bing và Yahoo!</p>
<br />
<p><h4>Hoạt động có sẵn:</h4></p>
<p><strong>Tạo ra</strong> - Chỉ tạo từ khóa meta cho các mục không có từ khóa đó.</p>
<p><strong>Bắt đầu</strong> - Loại bỏ tất cả các từ khóa meta hiện tại và tạo các từ khóa mới.</p>
';

$_['tab_seo_links_helper_title'] = 'SEO Links';
$_['tab_seo_links_helper'] = '
<p>Liên kết là một trong những yếu tố quan trọng nhất để xếp hạng tốt trong Google và các công cụ tìm kiếm khác.</p>
<br />
<p><h4>Hoạt động có sẵn:</h4></p>
<p><strong>Tạo ra</strong> - Chỉ tạo các liên kết SEO cho các mục không có chúng.</p>
<p><strong>Bắt đầu</strong> - Loại bỏ tất cả các liên kết SEO hiện tại và tạo các liên kết mới.</p>
<p><strong>Sửa số lượng</strong> - Loại bỏ tất cả các liên kết SEO không thuộc (các) mục nào. Nút xuất hiện khi tổng số URL Thiếu bị trừ vì vẫn còn URL trong cơ sở dữ liệu khi (các) mục bị xóa.</p>
<br />
<p>OpenCart hỗ trợ các URL thân thiện với SEO, tuy nhiên chúng không được bật theo mặc định.</p>
<p>Để bật URL SEO, hãy truy cập <strong>Hệ thống> Cài đặt> Máy chủ</strong>. Lựa chọn <strong>Có</strong> trên trường <strong>sử dụng URL SEO</strong> và <strong>Lưu</strong>.</p>
<div class="helper">
    <p><a title="'.$_['image_zoom_helper'].'" href="view/image/isenselabs_seo/enable_seo_preview.png" data-toggle="lightbox"><img class="img img-responsive img-thumbnail img-small" src="view/image/isenselabs_seo/enable_seo_preview.png" /></a></p>
</div>
<p>Đây là sự khác biệt giữa một liên kết thân thiện với SEO và một liên kết bình thường:</p>
<div class="helper">
    <p><a title="'.$_['image_zoom_helper'].'" href="view/image/isenselabs_seo/enable_seo_preview_2.jpg" data-toggle="lightbox"><img class="img img-responsive img-thumbnail img-small" src="view/image/isenselabs_seo/enable_seo_preview_2.jpg" /></a></p>
</div>
';

$_['tab_image_titles_helper_title'] = 'Kiến thức cơ bản cần biết';
$_['tab_image_titles_helper'] = '
<p><h4>Sao lưu được khuyến khích!</h4></p>
<p>Đảm bảo sao lưu cơ sở dữ liệu và thư mục hình ảnh của bạn trước khi sử dụng tính năng này.</p>
<br />
<p><h4>Tên hình ảnh được tạo ra như thế nào?</h4></p>
<p>Chúng tôi sử dụng các từ từ tên sản phẩm và số kiểu máy để tạo tên hình ảnh tương đối. Tuy nhiên, nếu bạn muốn thay đổi các thông số, bạn có thể thực hiện điều đó từ "Tùy chọn nâng cao" bên dưới nút lớn màu xanh lam.
<br /><br />
<b>Hình ảnh bổ sung</b>
<br />
Các hình ảnh bổ sung sử dụng cùng tên với hình ảnh sản phẩm chính, theo sau là một số.
<br />
Ví dụ: nếu hình ảnh sản phẩm chính là "sanpham.jpg", các hình ảnh bổ sung sẽ là:
<ul>
    <li>sanpham-1.jpg</li>
    <li>sanpham-2.jpg</li>
    <li>sanpham-3.jpg</li>
    ...
</ul>
</p>
<br />
<p><h4>"Lỗi" có nghĩa là gì trong báo cáo?</h4></p>
<p>Đã xảy ra lỗi trong quá trình đổi tên. Các vấn đề về quyền trong máy chủ là nguyên nhân phổ biến nhất, ngăn tập lệnh đổi tên hình ảnh.
<br /><br />
Để giải quyết vấn đề này, bạn phải đảm bảo rằng người dùng PHP trên máy chủ có quyền ĐỌC / VIẾT chính xác cho thư mục hình ảnh trong cửa hàng của bạn. Nếu bạn không chắc chắn về điều đó, hãy liên hệ với nhà cung cấp dịch vụ lưu trữ của bạn.
</p>
<br />
<p><h4>Xóa bộ nhớ cache hình ảnh OpenCart của bạn!</h4></p>
<p>Đôi khi OpenCart có thể tiếp tục lưu vào bộ đệm các tên tệp cũ. Để giải quyết điều đó, bạn phải xóa hệ thống thư mục / lưu trữ / bộ nhớ cache. Ngoài ra, nếu bạn đang sử dụng các dịch vụ CDN, chẳng hạn như CloudFlare, hãy đảm bảo xóa cả hình ảnh / bộ nhớ cache để các hình ảnh mới có thể được lưu vào bộ nhớ cache.</p>';

$_['tab_custom_urls_helper_title'] = 'URL tùy chỉnh là gì?';
$_['tab_custom_urls_helper'] = '
<p>Chuyển hướng URL cho phép bạn tạo các URL tùy chỉnh thân thiện với SEO cho tất cả các trang trong cửa hàng của bạn.</p>
<br />
<p><h4>Cách thiết lập URL tùy chỉnh?</h4></p>
<p>Lấy tuyến hiện tại của trang, đặt nó trong Tuyến, sau đó định cấu hình URL tùy chỉnh cho nó.</p>
<p>Ví dụ: "www.yoursite.com/index.php?route=account/login".</p>
<p>Đi theo <strong>account/login</strong> định tuyến và định cấu hình chuyển hướng được gọi là "<strong>login</strong>".</p>
<p>URL SEO mới sẽ là www.yoursite.com/<strong>login</strong>.</p>';

$_['tab_autolinks_helper_title'] = 'Liên kết tự động là gì?';
$_['tab_autolinks_helper'] = '
<p>Tự động liên kết cho phép bạn chèn liên kết nội bộ giữa tất cả các trang trong cửa hàng của bạn.</p>
<p>Bạn có thể áp dụng các liên kết trong mô tả sản phẩm và danh mục, có thể dẫn đến sản phẩm, trang thông tin, bài đăng trên blog, v.v. khi nói về các mặt hàng khác trong cửa hàng của bạn.</p>
<p>Việc tham chiếu chéo các trang trong cửa hàng của bạn giữ cho trình thu thập thông tin của công cụ tìm kiếm ở trong trang web của bạn lâu hơn và nhiều trang của bạn được lập chỉ mục.</p>
<div class="helper">
    <p>Ở đây bạn có thể xem một ví dụ về kết quả cuối cùng:</p>
    <p><a title="'.$_['image_zoom_helper'].'" href="view/image/isenselabs_seo/autolinks_preview.png" data-toggle="lightbox"><img class="img img-responsive img-thumbnail img-small" src="view/image/isenselabs_seo/autolinks_preview.png" /></a></p>
</div>';

$_['tab_social_links_helper_title'] = 'Liên kết xã hội là gì?';
$_['tab_social_links_helper'] = '
<p><h4>Mở thẻ đồ thị</h4></p>
<p>Thẻ Open Graph được sử dụng để tích hợp giữa Facebook và trang web của bạn. Các thẻ trở thành các đối tượng "đồ thị" phong phú với chức năng tương tự như các đối tượng Facebook khác. Họ có thể hiển thị thêm thông tin (tên, mô tả, hình ảnh hoặc giá cả) trong các bài đăng, trỏ đến cửa hàng của bạn. Thẻ Open Graph cũng được Twitter, Google+ và LinkedIn công nhận.</p>
<p>Trong hình ảnh bên dưới, bạn có thể thấy cách dữ liệu được chia sẻ có thể được tùy chỉnh bằng cách sử dụng các thẻ OG:</p>
<p><a title="'.$_['image_zoom_helper'].'" href="view/image/isenselabs_seo/socialinks_preview.png" data-toggle="lightbox"><img class="img img-responsive img-thumbnail img-small" src="view/image/isenselabs_seo/socialinks_preview.png" /></a></p>
<br />
<p><h4>Thẻ Twitter</h4></p>
<p>Thẻ Twitter là một biến thể của hình thức trên. Chúng được tạo riêng cho Twitter bằng cách hiển thị thông tin bổ sung trên các tweet dưới dạng bài đăng mô tả, trỏ đến cửa hàng của bạn.</p>
<p>Các trường <strong>ID ứng dụng</strong> (trong cài đặt Facebook) và <strong>Tên người dùng</strong> (trong cài đặt Twitter) là tùy chọn.</p>
<br />
<p><h4>Ngôn ngữ Annotations, a.k.a. "hreflang" Tags</h4></p>
<p>Nhiều trang web phục vụ người dùng từ khắp nơi trên thế giới với nội dung được dịch hoặc nhắm mục tiêu đến người dùng bằng một ngôn ngữ nhất định. Google sử dụng <code>rel="alternate" hreflang="x"</code> để phân phát ngôn ngữ hoặc URL khu vực chính xác trong trang kết quả Tìm kiếm.<p>
<p>Từ các tùy chọn ở phía bên trái, bạn có thể bật "hreflang" thẻ cho sản phẩm, danh mục, nhà sản xuất và các trang thông tin.</p>
<p><strong>Note:</strong> Tùy chọn này dành cho các cửa hàng có nhiều hơn một ngôn ngữ và nó sẽ chỉ hoạt động nếu bạn đã kích hoạt nhiều ngôn ngữ cho trang web của mình.</p>';

$_['tab_rich_snippets_helper_title'] = 'Rich Snippets';
$_['tab_rich_snippets_helper'] = '
<p>Các tùy chọn thêm các đoạn mã sau:</p>
<ul>
<li><strong>Dữ liệu sản phẩm</strong> - Tên, mô tả, hình ảnh, kiểu máy, giá (đặc biệt), nhà sản xuất, xếp hạng, phiếu bầu, đơn vị tiền tệ của giá, tính khả dụng.</li>
<li><strong>Đường dẫn sản phẩm</strong> - Dữ liệu đường dẫn đầy đủ cho các sản phẩm trong trang sản phẩm, bao gồm cả đường dẫn danh mục mà sản phẩm thuộc về.</li>
<li><strong>Danh mục breadcrumbs</strong> - Dữ liệu đường dẫn đầy đủ cho danh mục hiện đang truy cập, bao gồm đường dẫn đầy đủ (nếu danh mục con được mở).</li>
<li><strong>Thông tin cửa hàng</strong> - Thông tin liên quan đến tên cửa hàng, url, logo, hành động tìm kiếm và thông tin liên hệ trên mọi trang.</li>
</ul>
<br />
<p><h4>Đoạn mã chi tiết là gì?</h4></p>
<p>Đoạn mã chi tiết là một cách tuyệt vời để khiến nội dung của bạn được chú ý trong kết quả tìm kiếm và nổi bật trong mắt người dùng.</p>
<br />
<p><h4>Điều gì là đặc biệt về các đoạn mã chi tiết?</h4></p>
<p>Định cấu hình đoạn mã chi tiết có nghĩa là Google và các công cụ tìm kiếm khác có thể hiển thị thông tin bổ sung, chẳng hạn như:
<ul>
    <li>Breadcrumbs</li>
    <li>Giá</li>
    <li>Xếp hạng</li>
    <li>Phiếu bầu</li>
    <li>Khả dụng</li>
</ul></p>
<p>Xem ví dụ. Tất cả các liên kết ngoài liên kết thứ ba đều có nhiều đoạn mã:</p>
<p><a title="'.$_['image_zoom_helper'].'" href="view/image/isenselabs_seo/richsnippets_preview.png" data-toggle="lightbox"><img class="img img-responsive img-thumbnail img-small" src="view/image/isenselabs_seo/richsnippets_preview.png" /></a></p>
<br />
<p><h4>Tôi nên làm gì?</h4></p>
<p>Hiện tại, bạn có thể thêm đoạn mã chi tiết cho dữ liệu sản phẩm và đường dẫn. Chỉ cần bật chúng từ cài đặt.</p>
<p>Sau đó, bạn có thể kiểm tra xem các đoạn mã có được triển khai chính xác từ <a target="_blank" href="http://www.google.com/webmasters/tools/richsnippets">Công cụ kiểm tra dữ liệu có cấu trúc</a>.</p>';

$_['text_seo_score_helper_title'] = 'Điểm SEO được tính như thế nào?'; 
$_['text_seo_score_helper'] = '
<p>Nhóm của chúng tôi đã nghiên cứu SEO trong 4 năm qua và dựa trên mô-đun SEO của chúng tôi dựa trên thông tin SEO cập nhật và có liên quan nhất từ các nguồn như Neil Patel, Moz, Barry Schwarz và Danny Sullivan để tạo ra mô-đun SEO cuối cùng.</p>
<p>"Điểm SEO" là một thuật toán, chúng tôi dựa trên một tập hợp hơn 20 yếu tố và được thiết kế để đo lường hiệu suất SEO của cửa hàng của bạn. Điểm từ 0 đến 100 điểm.
</p>
<p>"Điểm SEO" là một thuật toán mà chúng tôi dựa trên tập hợp 20+ Điểm cao hơn sẽ tốt hơn và cho biết rằng bạn đang sử dụng các kỹ thuật tốt nhất để cải thiện hiệu suất của mình trên các trang kết quả của công cụ tìm kiếm (SERPs). Một số yếu tố chúng tôi tính đến là: yếu tố và được thiết kế để đo lường hiệu suất SEO của cửa hàng của bạn. Điểm từ 0 đến 100 điểm.
<ul>
    <li>Có bao nhiêu sản phẩm, danh mục, nhà sản xuất và trang thông tin của bạn có URL thân thiện với SEO</li>
    <li>Có bao nhiêu sản phẩm, danh mục và trang thông tin của bạn có tiêu đề, mô tả và từ khóa meta</li>
    <li>Chúng tôi kiểm tra xem bạn đã bật dữ liệu có cấu trúc cho các sản phẩm và đường dẫn của mình chưa</li>
</ul>
</p>
<p>Và những người khác...</p>
<p>The "Điểm SEO "sẽ được cải thiện theo thời gian và điểm số sẽ thay đổi khi chúng tôi thêm các quy tắc mới và cải thiện phân tích của mình.</p>'; 

$_['text_htaccess_helper_title'] = 'Kiến thức cơ bản cần biết';
$_['text_htaccess_helper'] = '<p>Để sử dụng URL SEO, bạn cần đổi tên <strong>htaccess.txt</strong> để <strong>.htaccess</strong> và mô-đun Apache <strong>mod-viết lại</strong> phải được cài đặt.</p>
<p>Nếu tệp .htaccess đã được đổi tên, bạn sẽ thấy một số dữ liệu <strong>Trình soạn thảo văn bản</strong> và bỏ qua bước.</p>
<br /><p><h4>Chỉnh sửa tệp .htaccess</h4></p>
<p>Bản thân tệp không cần bất kỳ thay đổi nào. Tuy nhiên, nếu bạn muốn thực hiện chuyển hướng tùy chỉnh hoặc ghi lại bổ sung, bạn có thể sử dụng <strong>Trình soạn thảo văn bản</strong>. Chỉ cần đảm bảo có một bản sao lưu của tệp.</p>
<br /><p><h4>NGINX</h4></p>
<p>Đối với máy chủ NGINX, hãy xem <a href="https://isenselabs.com/posts/seo-urls-in-opencart-2x" target="_blank">bài viết này</a> để xem chúng được cấu hình như thế nào.</p>';

$_['text_robots_helper_title'] = 'Robots.txt';
$_['text_robots_helper'] = '<p><strong>Robots.txt</strong> là một tệp văn bản trong thư mục gốc của cửa hàng của bạn, nơi các công cụ tìm kiếm truy cập định kỳ. Bạn có thể sử dụng nó để thêm URL của các trang bạn không muốn được lập chỉ mục.</p>
<br /><p><h4>Chỉnh sửa tệp robots.txt</h4></p>
<p>Nếu tệp robots.txt của bạn trống, bạn có thể sao chép đoạn mã bên dưới, chứa các dòng cần thiết cho cửa hàng. Nó sẽ cho phép trình thu thập thông tin truy cập mọi trang trong cửa hàng của bạn, ngoại trừ bảng điều khiển quản trị, trang khách hàng, trang tìm kiếm, trang lọc và các trang khác:</p>
<code>
User-agent: *<br />
Disallow: /admin<br />
Disallow: /*route=product/search<br />
Disallow: /*route=checkout/<br />
Disallow: /*route=account/<br />
Disallow: /*sort=<br />
Disallow: /*order=<br />
Disallow: /*limit=<br />
Disallow: /*page=<br />
Disallow: /*filter_name=<br />
Disallow: /*filter_description=<br />
</code>
<br />
<p><strong>Note:</strong> Robots.txt được công cụ tìm kiếm lưu vào bộ nhớ cache và phiên bản được lưu trong bộ nhớ cache mới nhất có thể được tìm thấy trong Công cụ quản trị trang web của Google và / hoặc Công cụ quản trị trang web của Bing.</p>';

$_['text_crawler_helper_title'] = 'Giới thiệu về Trình thu thập thông tin của chúng tôi';
$_['text_crawler_helper'] = '<p>Trình thu thập thông tin được xây dựng trong nhà của chúng tôi đo lường hiệu suất SEO trên trang của một trang. Nó tìm nạp url nhiều lần để chạy một số thử nghiệm, kết quả từ đó được hiển thị trong một báo cáo hoàn chỉnh ở phía bên trái của trang. Chúng tôi đang thử nghiệm những điều sau:</p>
<ul>
    <li>Thời gian phản hồi của máy chủ</li>
    <li>HTTP (SSL) Availability</li>
    <li>Thẻ Tiêu đề </li>
    <li>Thẻ mô tả </li>
    <li>Thẻ hợp quy </li>
    <li>Mở Dữ liệu Đồ thị </li>
    <li>Dữ liệu về thẻ Twitter </li>
    <li>Tiêu đề Tiêu đề</li>
</ul>
<br />
<h4>Đây là một công cụ báo cáo</h4>
<p>Báo cáo ở đây cho bạn thấy các vấn đề tiềm ẩn với nội dung và thiết kế trên các trang. Hầu hết những điều này không thể được khắc phục chỉ với mô-đun, vì có những thay đổi, được thực hiện trực tiếp trong mẫu hoặc nội dung (tiêu đề, mô tả và các văn bản khác) trên trang nhất định. Dưới mỗi điểm của báo cáo, bạn sẽ thấy các đề xuất của chúng tôi là gì và liệu trang của bạn có đáp ứng các đề xuất đó hay không.</p>
<br />
<h4>Chúng tôi sẵn sàng đón nhận phản hồi của bạn</h4>
<p>Chúng tôi sẽ đánh giá cao bất kỳ phản hồi nào bạn muốn cung cấp cho chúng tôi về trình thu thập thông tin. Nếu bạn có đề xuất về công cụ kiểm tra hoặc các phương pháp hay nhất được đề xuất của chúng tôi, vui lòng đăng chúng <a href="https://isenselabs.com/pages/premium-services#section-contact" target="blank">ở đây</a>.</p>';

$_['text_seo_analysis_helper_title'] = 'Giới thiệu về Phân tích';
$_['text_seo_analysis_helper'] = '<p>Để các công cụ tìm kiếm lập chỉ mục trang web của bạn, họ sử dụng trình thu thập thông tin. Trình thu thập dữ liệu Web, đôi khi được gọi là con nhện, là một bot Internet duyệt qua World Wide Web một cách có hệ thống, thường cho mục đích lập chỉ mục.</p>
<p>Từ danh sách này, bạn có thể xem trình thu thập thông tin nào đã duyệt qua trang web của bạn và những trang nào đã được kiểm tra. Điều này có thể giúp bạn xác định những trang nào có thể sẽ được lập chỉ mục bởi các công cụ tìm kiếm.</p>
<p>Hiện tại, chúng tôi đang theo dõi các trình thu thập thông tin sau:
<ul>
<li><strong>Googlebot</strong> - Trình thu thập thông tin chính của Google, lập chỉ mục các trang trong trang web của bạn</li>
<li><strong>Googlebot-Image</strong> - Chịu trách nhiệm về kết quả trong các tìm kiếm Hình ảnh của Google. Nó chỉ theo dõi các hình ảnh</li>
<li><strong>Bingbot</strong> - Trình thu thập thông tin chính của Bing, lập chỉ mục các trang và hình ảnh trong trang web của bạn</li>
<li><strong>YandexBot</strong> - Trình thu thập thông tin Yandex chính, lập chỉ mục các trang trong trang web của bạn</li>
<li><strong>YandexImages</strong>- Trình thu thập dữ liệu Yandex, lập chỉ mục các hình ảnh trong trang web của bạn</li>
</ul>
</p>';

$_['text_manufacturer_title_string'] = 'Chuỗi tiêu đề của nhà sản xuất:';
$_['text_manufacturer_description_string'] = 'Chuỗi mô tả nhà sản xuất:';
$_['text_manufacturer_keywords_string'] = 'Chuỗi từ khóa của nhà sản xuất:';

$_['fix_manufacturer_seo_titles'] = 'Thêm Tiêu đề Meta cho các trang nhà sản xuất của bạn';
$_['fix_manufacturer_seo_descriptions'] = 'Thêm mô tả Meta cho nhà sản xuất của bạn';
$_['fix_manufacturer_seo_keywords'] = 'Thêm Từ khóa Meta cho các trang nhà sản xuất của bạn';

$_['error_unexpected_seo_url'] = 'URL SEO này đã tồn tại. Vui lòng thử sử dụng một chuỗi khác cho mục đó.';

$_['btn_enable_seo_analysis'] = 'Bật phân tích SEO';
$_['btn_disable_seo_analysis'] = 'Tắt phân tích SEO';

$_['text_crawler_missing_functionality'] = 'Có vẻ như trình thu thập thông tin không thể bắt đầu hoạt động trên truy vấn do chức năng bị tắt trên máy chủ của bạn. Đảm bảo rằng <strong>CURL</strong> và <strong>file_get_contents()</strong> được bật trên máy chủ của bạn.';

$_['text_404_manager'] = 'Người quản lý 404';
$_['text_detected_missing_pages'] = 'Các trang bị thiếu được phát hiện';
$_['text_404_redirects'] = '404 Chuyển hướng';

$_['text_visits'] = 'Lượt truy cập';
$_['text_first_visited'] = 'Lần đầu đến';
$_['text_last_visited'] = 'Lần cuối đến';

$_['text_remove_selected_row'] = 'Bạn có chắc chắn muốn xóa hàng đã chọn không(s)?';
$_['text_no_detected_pages'] = 'Hiện tại không có bất kỳ trang 404 nào được phát hiện trong cửa hàng của bạn.';
$_['text_no_redirects'] = 'Hiện tại không có bất kỳ chuyển hướng 404 nào trong cửa hàng của bạn.';

$_['tab_detected_pages_helper_title'] = 'Các trang bị thiếu là gì?';
$_['tab_detected_pages_helper'] = '
<p>Tại đây, bạn có thể xem những trang nào đã được khách hàng của bạn truy cập và hiện đang trả về lỗi 404.</p>
<p>404 là mã phản hồi của máy chủ cho biết rằng trang không tồn tại trên máy chủ nhất định. Nếu bạn có khách hàng thường xuyên truy cập các trang không tồn tại trên cửa hàng của mình, bạn có thể sử dụng chức năng ở đây để chuyển hướng họ đến một trang khác, có nội dung thực tế trên đó.</p>
<br />
<p><h4>Làm thế nào để thiết lập một chuyển hướng mới?</h4></p>
<p>Để thiết lập một chuyển hướng mới, bạn chỉ cần nhấp vào nút "Tạo chuyển hướng" - <i class="fa fa-external-link-square" aria-hidden="true"></i> - cho trang bị thiếu đã cho.<br /><br />Địa chỉ URL trong trường "Địa chỉ URL mới" phải được điền bằng URL mà bạn muốn sử dụng bằng cách loại bỏ tên miền. Ví dụ: nếu bạn muốn chuyển hướng đến một trang có tên <strong>"http://site.com/this-is-a-test"</strong>, trong lĩnh vực bạn phải nhập <strong>"/this-is-a-test"</strong>.</p>';

$_['text_enable_404_detection'] = 'Bật phát hiện 404';
$_['text_disable_404_detection'] = 'Tắt phát hiện 404';

$_['tab_redirects_helper_title'] = 'Chuyển hướng 404 là gì?';
$_['tab_redirects_helper'] = '
<p>Tại đây, bạn có thể chuyển hướng URL đến các trang không tồn tại (trang lỗi 404) trong cửa hàng của mình.</p>
<p>Các chuyển hướng có trạng thái là 301, điều này cũng sẽ cung cấp thông tin cho các công cụ tìm kiếm rằng đã có chuyển hướng đến một trang mới có nội dung. Điều này sẽ cải thiện thứ hạng của bạn trong SERP vì các công cụ tìm kiếm sẽ bắt đầu lập chỉ mục trang mới thay vì trang cũ.</p>
<br />
<p><h4>Làm thế nào để thiết lập một chuyển hướng mới?</h4></p>
<p>Để thiết lập một chuyển hướng mới, bạn chỉ cần nhấp vào nút "Tạo chuyển hướng" - <i class="fa fa-external-link-square" aria-hidden="true"></i> - cho trang bị thiếu đã cho.<br /><br />Địa chỉ URL trong trường "Địa chỉ URL mới" phải được điền bằng URL mà bạn muốn sử dụng bằng cách loại bỏ tên miền. Ví dụ: nếu bạn muốn chuyển hướng đến một trang có tên <strong>"http://site.com/this-is-a-test"</strong>, trong lĩnh vực bạn phải nhập <strong>"/this-is-a-test"</strong>.</p>';

$_['text_create'] = 'Đã tạo';
$_['text_modified'] = 'Đã sửa đổi';

$_['text_route_from'] = 'Đường đi từ';
$_['text_route_to'] = 'Đường đến';
$_['text_add_404_redirect'] = 'Tạo chuyển hướng';

$_['text_old_address'] = 'Địa chỉ URL cũ:';
$_['text_new_address'] = 'Địa chỉ URL mới:';
$_['text_add_new_redirect'] = 'Thêm chuyển hướng mới';

$_['text_canonicals'] = 'URL hợp quy';

$_['text_canonical_products'] = 'Bật cho Sản phẩm:';
$_['text_canonical_categories'] = 'Cho phép các danh mục:';
$_['text_canonical_manosystem Enterprises'] = 'Cho phép các nhà sản xuất:';
$_['text_canonical_information_pages'] = 'Cho phép các Trang Thông tin:';
$_['text_canonical_specials'] = 'Cho phép trang đặc biệt:';
$_['text_canonical_home_page'] = 'Bật cho Trang chủ:';

$_['tab_canonicals_helper_title'] = 'URL hợp quy';
$_['tab_canonicals_helper'] = '<p>Thuộc tính thẻ Canonical URL tương tự theo nhiều cách với chuyển hướng 301 từ góc độ SEO. Về bản chất, bạn đang nói với các engine rằng nhiều trang nên được coi là một (điều mà 301 làm), mà không thực sự chuyển hướng khách truy cập đến URL mới (thường khiến nhân viên nhà phát triển của bạn đau lòng đáng kể). <br /> <br /> Tới nói ngắn gọn, với thẻ canonical, bạn đang trỏ các công cụ tìm kiếm đến các trang gốc thay vì các trang trùng lặp, điều này sẽ dẫn đến việc xóa chúng khỏi chỉ mục. Nói cho các trang tìm kiếm biết những trang nào là trùng lặp sẽ khiến họ chuyển hướng khách hàng đến trang gốc và sẽ cải thiện thứ hạng SEO của bạn theo thời gian.</p>';

$_['text_editor_h1'] = 'Thẻ H1';
$_['text_editor_h2'] = 'Thẻ H2';

$_['text_unify_urls'] = 'Hợp nhất URL cho các sản phẩm & danh mục:';
$_['text_unify_urls_helper'] = 'Nếu bạn bật tùy chọn này, các liên kết cho sản phẩm và danh mục của bạn sẽ được thống nhất trên tất cả các trang, bất kể bạn đang ở trang chủ, trang danh mục hay bất kỳ trang nào khác. Điều này sẽ loại bỏ nội dung trùng lặp do các thay đổi nhỏ trong URL do OpenCart tạo ra. ';

$_['text_breadcrumb_products'] = 'Đường dẫn đầy đủ trong các trang sản phẩm:';
$_['text_breadcrumb_products_helper'] = 'Nếu bạn bật tùy chọn này, đường dẫn trong các trang sản phẩm sẽ bao gồm đường dẫn đầy đủ đến sản phẩm (trang chủ, danh mục chính, danh mục phụ, sản phẩm hiện tại).';

$_['text_breadcrumb_categories'] = 'Đường dẫn đầy đủ trong các trang danh mục:';
$_['text_breadcrumb_categories_helper'] = 'Nếu bạn bật tùy chọn này, đường dẫn trong các trang danh mục sẽ bao gồm đường dẫn đầy đủ đến sản phẩm (trang chủ, danh mục chính, danh mục phụ, danh mục hiện tại).';

$_['text_products_autogenerate'] = 'Tự động tạo URL cho các sản phẩm:';
$_['text_categories_autogenerate'] = 'Tự động tạo URL cho các danh mục:';
$_['text_manthers_autogenerate'] = 'Tự động tạo URL cho các nhà sản xuất:';
$_['text_informations_autogenerate'] = 'Tự động tạo URL cho các trang thông tin:';

$_['text_autogenerate'] = 'Tự động tạo URL:';
$_['text_autogenerate_helper'] = 'Nếu bạn bật tùy chọn này, khi bạn chỉnh sửa sản phẩm / danh mục / nhà sản xuất / trang thông tin và bạn không đặt slug, mô-đun của chúng tôi sẽ tự động tạo nó cho bạn. Điều này rất hữu ích nếu bạn thường xuyên thêm sản phẩm mới vào cửa hàng của mình và bạn không có thời gian để đặt URL theo cách thủ công. ';

$_['text_subfolder_prefixes'] = 'Thêm tiền tố thư mục ngôn ngữ vào URL: <span style="color:red">***</span>';
$_['text_subfolder_prefixes_helper'] = 'Nếu bạn bật điều này, SEO Backpack sẽ giới thiệu các thư mục con ảo cho các URL đa ngôn ngữ của bạn. Mã cho ngôn ngữ đã cho sẽ được sử dụng như một "thư mục". Ví dụ:<br /><br />
- http://www.yoursite.com/iphone5<br />
- http://www.yoursite.com/fr/iphone5<br />
- http://www.yoursite.com/de/iphone5<br />
- http://www.yoursite.com/bg/iphone5<br /><br /><strong>***</strong> Đây là một tính năng thử nghiệm.';

$_['text_richsnippets_category_breadcrumbs'] = 'Bật cho đường dẫn danh mục';

$_['text_cyrillic_url_helper'] = 'Nếu bạn bật tính năng này, SEO Backpack sẽ không chuyển ngữ các URL Kirin cho sản phẩm, danh mục, nhà sản xuất và các trang thông tin. Ví dụ: nếu tên sản phẩm là "Sản phẩm mẫu" thì URL sẽ được tạo cho sản phẩm đó sẽ là:<br /><br />
http://www.yoursite.com/san-pham-mau<br /><br />
Nếu tùy chọn này bị tắt thì URL được tạo sẽ là:<br /><br />
http://www.yoursite.com/test-product<br /><br />
<b>Ghi chú:</b> Bạn phải tạo các Liên kết SEO mới sau khi bật / tắt tùy chọn này ';

$_['text_default_lang_prefix_helper'] = 'Nếu bạn bật điều này, tiền tố ngôn ngữ mặc định cũng sẽ được thêm vào URL.<br /><br />
<b>Ghi chú:</b> Bạn phải kích hoạt <b>Thêm tiền tố thư mục ngôn ngữ vào URL</b> tùy chọn này để làm việc.';
$_['text_redirect_active_lang_prefix_helper'] = 'Tự động chuyển hướng đến tiền tố ngôn ngữ hoạt động. Truy cập example.com/ sẽ tự động chuyển hướng đến example.com/en/<br /><br />
<b>Ghi chú:</b> Bạn phải kích hoạt <b>Thêm tiền tố thư mục ngôn ngữ vào URL</b> và <b>Bao gồm tiền tố ngôn ngữ mặc định</b>.';

$_['text_stores'] = 'Cửa hàng';
