<?php
// Tiêu đề
$_['heading_title'] = 'Nội dung HTML';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi mô-đun Nội dung HTML!';
$_['text_edit'] = 'Chỉnh sửa Mô-đun Nội dung HTML';

// Mục nhập
$_['entry_name'] = 'Tên mô-đun';
$_['entry_title'] = 'Tiêu đề đầu trang';
$_['entry_description'] = 'Mô tả';
$_['entry_status'] = 'Trạng thái';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi mô-đun Nội dung HTML!';
$_['error_name'] = 'Tên Mô-đun phải từ 3 đến 64 ký tự!';