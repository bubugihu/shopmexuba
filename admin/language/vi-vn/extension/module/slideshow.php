<?php
// Tiêu đề
$_['heading_title'] = 'Trình chiếu';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi mô-đun trình chiếu!';
$_['text_edit'] = 'Chỉnh sửa mô-đun trình chiếu';

// Mục nhập
$_['entry_name'] = 'Tên mô-đun';
$_['entry_banner'] = 'Biểu ngữ';
$_['entry_width'] = 'Chiều rộng';
$_['entry_height'] = 'Chiều cao';
$_['entry_status'] = 'Trạng thái';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi mô-đun trình chiếu!';
$_['error_name'] = 'Tên Mô-đun phải từ 3 đến 64 ký tự!';
$_['error_width'] = 'Chiều rộng bắt buộc!';
$_['error_height'] = 'Chiều cao bắt buộc!';