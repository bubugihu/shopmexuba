<?php
// Tiêu đề
$_['heading_title'] = 'Sản phẩm nổi bật';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi mô-đun sản phẩm nổi bật!';
$_['text_edit'] = 'Chỉnh sửa Mô-đun sản phẩm nổi bật';

// Mục nhập
$_['entry_name'] = 'Tên mô-đun';
$_['entry_product'] = 'Sản phẩm';
$_['entry_limit'] = 'Giới hạn';
$_['entry_width'] = 'Chiều rộng';
$_['entry_height'] = 'Chiều cao';
$_['entry_status'] = 'Trạng thái';

// Cứu giúp
$_['help_product'] = '(Tự động điền)';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi mô-đun sản phẩm nổi bật!';
$_['error_name'] = 'Tên Mô-đun phải từ 3 đến 64 ký tự!';
$_['error_width'] = 'Chiều rộng bắt buộc!';
$_['error_height'] = 'Chiều cao bắt buộc!';