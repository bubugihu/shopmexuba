<?php
// Heading
$_['header_title'] = 'Cửa sổ bật lên của Pavo';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['entry_time_to_close'] = 'Thời gian popup tự đóng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi mô-đun';
$_['text_edit'] = 'Chỉnh sửa Mô-đun Bán hàng Flash';
$_['text_alway_show'] = 'Luôn hiện';
$_['text_alway_show_time'] = 'Hiển thị popup mỗi khi tải trang';
$_['alway_show_close'] = 'Hiển thị và Không hiển thị lại khi đóng';
// Mục nhập
$_['entry_load_module'] = 'Tải mô-đun';
$_['entry_load_module_start'] = 'Hiển thị popup';
$_['entry_name'] = 'Tên mô-đun';
$_['entry_title'] = 'Tiêu đề đầu trang';
$_['entry_description'] = 'Mô tả';
$_['entry_time_to_open'] = 'Thời gian popup xuất hiện';
$_['entry_enable_social'] = 'Cho phép đăng nhập trên mạng xã hội';
$_['entry_status'] = 'Trạng thái';
$_['entry_popup_width'] = 'Chiều rộng';
?>