<?php
// Tiêu đề
$_['heading_title'] = 'Tài khoản';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi mô-đun tài khoản!';
$_['text_edit'] = 'Chỉnh sửa mô-đun tài khoản';

// Mục nhập
$_['entry_status'] = 'Trạng thái';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi mô-đun tài khoản!';