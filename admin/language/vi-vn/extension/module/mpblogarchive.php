<?php
// Heading
$_['heading_title']    = '<span style="color: #07B8DA; font-weight: 600; ">Lịch M-Blog</span>';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi mô-đun lịch m-bog!';
$_['text_edit'] = 'Chỉnh sửa Mô-đun Lịch của M-Blog';

// Mục nhập
$_['entry_status'] = 'Trạng thái';
$_['entry_blogcount'] = 'Hiển thị số lượng blog';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi mô-đun lịch m-bog!';