<?php
// Tiêu đề
$_['heading_title'] = 'Banner';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi mô-đun banner!';
$_['text_edit'] = 'Chỉnh sửa mô-đun banner';

// Mục nhập
$_['entry_name'] = 'Tên mô-đun';
$_['entry_banner'] = 'Biểu ngữ';
$_['entry_dimension'] = 'Kích thước (W x H) và loại thay đổi kích thước';
$_['entry_width'] = 'Chiều rộng';
$_['entry_height'] = 'Chiều cao';
$_['entry_status'] = 'Trạng thái';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi mô-đun biểu ngữ!';
$_['error_name'] = 'Tên Mô-đun phải từ 3 đến 64 ký tự!';
$_['error_width'] = 'Chiều rộng bắt buộc!';
$_['error_height'] = 'Chiều cao bắt buộc!';