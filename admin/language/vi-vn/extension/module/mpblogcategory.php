<?php
// Heading
$_['heading_title']    = '<span style="color: #07B8DA; font-weight: 600; ">Danh mục M-Blog</span>';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi mô-đun danh mục m-bog!';
$_['text_edit'] = 'Chỉnh sửa Mô-đun Danh mục M-Blog';

// Mục nhập
$_['entry_status'] = 'Trạng thái';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi mô-đun danh mục m-bog!';