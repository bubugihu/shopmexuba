<?php
// Tiêu đề
$_['header_title'] = 'iExtend SEO Guru';
$_['information_title'] = 'Thông tin';
$_['information_heading_title'] = 'Thông tin iExtend SEO Guru';


// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi thông tin!';
$_['text_list'] = 'Danh sách thông tin';
$_['text_add'] = 'Thêm thông tin';
$_['text_edit'] = 'Chỉnh sửa thông tin';
$_['text_default'] = 'Mặc định';
$_['text_screen_options'] = 'Tùy chọn màn hình';
$_['text_focus_keyphrase'] = 'Cụm từ khóa tiêu điểm';
$_['text_not_started'] = 'Chưa bắt đầu';
$_['text_good'] = 'Tốt';
$_['text_acceptable'] = 'Có thể chấp nhận được';
$_['text_need_improvements'] = 'Cần cải tiến';


$_['text_keyword'] = 'Không sử dụng dấu cách, thay vào đó hãy thay dấu cách bằng - và đảm bảo rằng URL SEO là duy nhất trên toàn cầu.';

// bản văn
$_['text_generate_meta_title'] = 'Tiêu đề meta';
$_['text_generate_meta_description'] = 'Mô tả Meta';
$_['text_generate_meta_keywords'] = 'Từ khoá Meta';
$_['text_generate_image_custom_title'] = 'Tiêu đề tùy chỉnh hình ảnh';
$_['text_generate_image_custom_alt'] = 'Thay thế tùy chỉnh hình ảnh';
$_['text_generate_seo_url'] = 'Url SEO';
$_['text_warning'] = 'Không có thông tin nào được chọn, vui lòng chọn thông tin trước';

// Cột
$_['column_title'] = 'Tiêu đề Thông tin';
$_['column_status'] = 'Trạng thái';
$_['column_seo_score'] = 'Điểm SEO';
$_['column_readability_score'] = 'Điểm khả năng đọc';

$_['column_sort_order'] = 'Sắp xếp Thứ tự';
$_['column_meta_title'] = 'Tiêu đề Meta';
$_['column_meta_keyword'] = 'Từ khoá Meta';
$_['column_image_custom_title'] = 'Tiêu đề Tùy chỉnh Hình ảnh';
$_['column_image_custom_alt'] = 'Thay thế tùy chỉnh hình ảnh';

$_['column_action'] = 'Hành động';

// Mục nhập
$_['entry_title'] = 'Tiêu đề Thông tin';
$_['entry_description'] = 'Mô tả';
$_['entry_meta_title'] = 'Tiêu đề thẻ meta';
$_['entry_meta_keyword'] = 'Từ khóa thẻ meta';
$_['entry_meta_description'] = 'Mô tả thẻ meta';
$_['entry_seo'] = 'URL SEO';
$_['entry_store'] = 'Cửa hàng';
$_['entry_keyword'] = 'Từ khóa';
$_['entry_bottom'] = 'Dưới cùng';
$_['entry_status'] = 'Trạng thái';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';
$_['entry_layout'] = 'Ghi đè bố cục';

// các trường bổ sung
$_['entry_custom_h1_tag'] = 'Thẻ H1';

// Cứu giúp
$_['help_bottom'] = 'Hiển thị ở chân trang dưới cùng.';

// Lỗi
$_['error_warning'] = 'Cảnh báo: Vui lòng kiểm tra kỹ biểu mẫu để tìm lỗi!';
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi thông tin!';
$_['error_title'] = 'Tiêu đề Thông tin phải từ 1 đến 64 ký tự!';
$_['error_description'] = 'Mô tả phải có nhiều hơn 3 ký tự!';
$_['error_meta_title'] = 'Tiêu đề meta phải lớn hơn 1 và ít hơn 255 ký tự!';
$_['error_keyword'] = 'URL SEO đã được sử dụng!';
$_['error_unique'] = 'URL SEO phải là duy nhất!';
$_['error_account'] = 'Cảnh báo: Không thể xóa trang thông tin này vì nó hiện đang được gán cho điều khoản tài khoản cửa hàng!';
$_['error_checkout'] = 'Cảnh báo: Không thể xóa trang thông tin này vì nó hiện đang được chỉ định làm điều khoản thanh toán tại cửa hàng!';
$_['error_affiliate'] = 'Cảnh báo: Không thể xóa trang thông tin này vì nó hiện đang được gán là điều khoản liên kết của cửa hàng!';
$_['error_return'] = 'Cảnh báo: Không thể xóa trang thông tin này vì nó hiện đang được chỉ định làm điều khoản trả hàng của cửa hàng!';
$_['error_store'] = 'Cảnh báo: Không thể xóa trang thông tin này vì nó hiện đang được các cửa hàng% s sử dụng!';



// GÓI SEO
$_['information_seo_text'] = 'SEO thông tin';
$_['text_readability'] = 'Khả năng đọc';
$_['text_problems'] = 'Vấn đề';
$_['text_improvements'] = 'Cải tiến';
$_['text_good_results'] = 'Kết quả tốt';

// PHÂN TÍCH KHẢ NĂNG ĐỌC

// Các vấn đề
$_['subheading_problem'] = '<b> Không có tiêu đề phụ: </b> Bạn không sử dụng bất kỳ tiêu đề phụ nào, mặc dù văn bản của bạn khá dài. Hãy thử và thêm một số tiêu đề phụ. ';

$_['subheading_distribution_problem'] = '<b> Phân phối tiêu đề phụ: </b> Văn bản của bạn chứa (các) phần dài hơn 300 từ và không được phân tách bằng bất kỳ tiêu đề phụ nào. Thêm tiêu đề phụ để cải thiện khả năng đọc. ';

$_['paragraph_length_problem'] = '<b> Độ dài đoạn văn: </b> Văn bản của bạn bao gồm (các) đoạn văn chứa nhiều hơn mức tối đa được đề xuất là 150 từ. Hãy rút ngắn các đoạn văn của bạn! ';

$_['easy_reading_problem'] = '<b> Reading Ease: </b> Điểm khả năng đọc của văn bản dưới 50%, được coi là rất khó đọc. Cố gắng cải thiện nó! ';

// Cải tiến
$_['question_length_improvement'] = '<b> Độ dài câu: </b> Hơn 25% số câu chứa hơn 20 từ, Cố gắng rút ngắn câu.';

$_['easy_reading_improvement'] = '<b> Reading Ease: </b> Điểm khả năng đọc của văn bản dưới 60%, được coi là hơi khó đọc. Cố gắng cải thiện nó! ';

// Kết quả tốt

$_['easy_reading_good'] = '<b> Đọc Dễ: </b> Điểm khả năng đọc của văn bản là hơn 60%, được coi là dễ đọc. Làm tốt lắm!';

$_['paragraph_length_good'] = '<b> Độ dài đoạn văn: </b> Không đoạn văn nào quá dài. Bạn đã làm rất tốt!';

$_['transfer_words_good'] = '<b> Từ chuyển đổi: </b> Mô tả chứa các từ chuyển đổi. ';

$_['subheading_good'] = '<b> Tiêu đề phụ: </b> Bạn đang sử dụng tiêu đề phụ, rất tốt!';


$_['question_length_good'] = '<b> Độ dài câu: </b> Hầu hết các câu của bạn đều ngắn. Tuyệt quá!';
// TỪ KHÓA TẬP TRUNG
// Các vấn đề
// ------------------------- === Vấn đề OK === --------------- ------- //

$_['outbound_link_problem'] = '<b> Liên kết ra ngoài: </b> Không có liên kết đi nào xuất hiện trong trang này, hãy đảm bảo thêm một số liên kết!';

$_['no_transition_words_problem'] = '<b> Từ chuyển đổi: </b> Không có từ chuyển đổi nào xuất hiện trong trang này, hãy đảm bảo thêm một số từ!';

$_['h1_tag_problem'] = '<b> Thẻ H1: </b> Không có thẻ H1 nào được chỉ định, hãy nhớ thêm!';

$_['internal_link_problem'] = '<b> Liên kết nội bộ: </b> Không có liên kết nội bộ nào xuất hiện trong trang này, hãy đảm bảo thêm một số liên kết!';

$_['keyphrase_length_problem'] = '<b> Độ dài cụm từ khóa: </b> Không có cụm từ khóa tiêu điểm nào được đặt cho trang này. Đặt cụm từ khóa để tính điểm SEO của bạn. ';

$_['keyphrase_over_density_problem'] = '<b> Mật độ cụm từ khóa: </b> Cụm từ khóa tiêu điểm được tìm thấy nhiều hơn mức tối đa được đề xuất là 2,5% lần cho độ dài văn bản này. Đừng tối đa hóa quá mức! ';

$_['no_image_problem'] = '<b> Hình ảnh: </b> Không có hình ảnh nào xuất hiện trong trang này, hãy cân nhắc thêm một số hình ảnh nếu thích hợp.';

$_['no_description_problem'] = '<b> Không có mô tả: </b> Văn bản có 0 từ. Con số này thấp hơn nhiều so với mức tối thiểu được khuyến nghị là 300 từ. Thêm nhiều nội dung phù hợp với chủ đề. ';


$_['meta_description_length_problem'] = '<b> Độ dài mô tả meta: </b> Không có mô tả meta nào được chỉ định. Thay vào đó, các công cụ tìm kiếm sẽ hiển thị bản sao từ trang. Hãy chắc chắn để viết một! ';

$_['keyphrase_in_meta_description_problem'] = '<b> Cụm từ khóa trong mô tả meta: </b> Mô tả meta đã được chỉ định, nhưng nó không chứa cụm từ khóa. Hãy khắc phục điều đó! ';

$_['keyphrase_not_in_title_problem'] = '<b> Cụm từ khóa trong tiêu đề SEO: </b> Từ khóa trọng tâm không xuất hiện trong tiêu đề SEO.';

$_['no_meta_title_problem'] = '<b> Không có tiêu đề SEO: </b> Vui lòng tạo một tiêu đề SEO.';

$_['no_slug_problem'] = '<b> URL SEO: </b> Không có URL SEO nào được xác định.';

$_['meta_title_width_problem'] = '<b> Độ dài tiêu đề SEO: </b> Tiêu đề SEO rộng hơn giới hạn có thể xem (70 ký tự). Cố gắng làm cho nó ngắn hơn. ';

$_['keyphrase_in_slug_problem'] = '<b> URL SEO: </b> URL SEO không được chỉ định. vui lòng thêm URL SEO ';

$_['meta_description_length_over_problem'] = '<b> Meta Description Over: </b> Mô tả meta dài hơn 320 ký tự. Giảm độ dài sẽ đảm bảo toàn bộ mô tả sẽ hiển thị. ';

// Cải tiến
// ------------------------- === Cải tiến OK === --------------- ------- //

$_['keyphrase_low_density_improvement'] = '<b> Mật độ cụm từ khóa: </b> Mật độ từ khóa trong nội dung dưới 1%, thấp, hãy sử dụng cụm từ khóa nhiều lần hơn.';

$_['h1_tag_improvement'] = '<b> Thẻ H1: </b> (Các) thẻ H1 được chỉ định nhưng không chứa cụm từ khóa, hãy nhớ thêm vào!';

$_['subheading_tag_improvement'] = '<b> Thẻ Tiêu đề Phụ: </b> (Các) thẻ Tiêu đề Phụ được chỉ định nhưng không chứa cụm từ khóa, hãy nhớ thêm vào!';

$_['no_previous_keyphrase_improvement'] = '<b> Cụm từ khóa được sử dụng lại: </b> Bạn đã sử dụng từ khóa trọng tâm này trước đây. Có lẽ nên thay đổi cụm từ khóa tiêu điểm! ';

$_['keyphrase_in_slug_improvement'] = '<b> Cụm từ khóa trong slug: </b> Cụm từ khóa của bạn không xuất hiện trong slug. Thay đổi điều đó!';

$_['no_image_title_improvement'] = '<b> Tiêu đề hình ảnh: </b> Hình ảnh trên trang này thiếu thuộc tính tiêu đề.';

$_['no_image_alt_improvement'] = '<b> Hình ảnh thay thế: </b> Hình ảnh trên trang này thiếu thuộc tính alt.';

$_['image_title_improvement'] = '<b> Tiêu đề hình ảnh: </b> Hình ảnh trên trang này không có thuộc tính tiêu đề chứa cụm từ khóa của bạn. Thêm cụm từ khóa! ';

$_['image_alt_improvement'] = '<b> Hình ảnh thay thế: </b> Hình ảnh trên trang này không có thuộc tính thay thế chứa cụm từ khóa của bạn. Thêm cụm từ khóa! ';


$_['description_far_below_length_improvement'] = '<b> Độ dài văn bản: </b> Văn bản chứa dưới 200 từ. Con số này thấp hơn nhiều so với mức tối thiểu được khuyến nghị là 300 từ. Thêm nhiều nội dung hơn. ';

$_['description_slight_below_length_improvement'] = '<b> Độ dài văn bản: </b> Văn bản gồm 250 từ. Con số này thấp hơn một chút so với mức tối thiểu được khuyến nghị là 300 từ. Thêm nhiều nội dung hơn. ';

$_['description_below_length_improvement'] = '<b> Độ dài văn bản: </b> Văn bản chứa hơn 200 từ nhưng dưới mức tối thiểu được khuyến nghị là 300 từ. Thêm nhiều nội dung hơn. ';

$_['keyphrase_length_improvement'] = '<b> Độ dài cụm từ khóa: </b> Cụm từ khóa chứa nhiều hơn 4 từ. vui lòng rút ngắn cụm từ khóa tiêu điểm ';

$_['meta_description_length_over_improvement'] = '<b> Meta Description Over: </b> Mô tả meta dài hơn 200 ký tự. Giảm độ dài sẽ đảm bảo toàn bộ mô tả sẽ hiển thị. ';

$_['meta_description_length_under_improvement'] = '<b> Mô tả meta Dưới: </b> Mô tả meta dài dưới 120 ký tự. Tuy nhiên, có sẵn tối đa 320 ký tự. ';

$_['meta_title_width_improvement'] = '<b> Độ dài tiêu đề SEO: </b> Tiêu đề SEO quá ngắn. Sử dụng không gian để thêm các biến thể cụm từ khóa hoặc tạo bản sao gọi hành động hấp dẫn. ';

$_['keyphrase_in_title_improvement'] = '<b> Cụm từ khóa trong tiêu đề: </b> Kết hợp chính xác của cụm từ khóa xuất hiện trong tiêu đề SEO, nhưng không xuất hiện ở đầu. Cố gắng chuyển nó về đầu. ';


$_['keyphrase_in_intro_improvement'] = '<b> Cụm từ khóa trong phần giới thiệu: </b> Không tìm thấy cụm từ trọng tâm trong đoạn đầu tiên của mô tả. Cố gắng thêm cụm từ khóa trong đoạn giới thiệu ';

// Kết quả tốt


// ------------------------- === Tốt OK === --------------- ------- //

$_['outbound_link_good'] = '<b> Liên kết ra ngoài: </b> Trang này có các liên kết ra ngoài.';

$_['slug_good'] = '<b> URL SEO: </b> Slug được xác định. Làm tốt lắm!';

$_['h1_tag_good'] = '<b> Thẻ H1: </b> (Các) thẻ H1 được chỉ định và chứa cụm từ khóa, Tuyệt vời!';

$_['subheading_tag_good'] = '<b> Thẻ Tiêu đề phụ: </b> (Các) thẻ Tiêu đề phụ được chỉ định và chứa cụm từ khóa, Tuyệt vời!';

$_['keyphrase_in_intro_good'] = '<b> Cụm từ khóa trong phần giới thiệu: </b> Làm tốt lắm!';

$_['no_previous_keyphrase_good'] = '<b> Cụm từ khóa được sử dụng trước đây: </b> Bạn chưa sử dụng cụm từ khóa này trước đây, rất tốt.';

$_['internal_link_good'] = '<b> Liên kết nội bộ: </b> Mô tả có liên kết nội bộ.';

$_['keyphrase_in_slug_good'] = '<b> Cụm từ khóa trong slug: </b> Cụm từ khóa xuất hiện trong slug. Cái đó thật tuyệt!';

$_['keyphrase_length_good'] = '<b> Độ dài của cụm từ khóa: </b> Cụm từ khóa Tiêu điểm có độ dài tốt.';

$_['keyphrase_in_meta_description_good'] = '<b> Cụm từ khóa trong mô tả meta: </b> Mô tả meta chứa từ khóa trọng tâm.';

$_['meta_description_length_good'] = '<b> Độ dài mô tả meta: </b> Mô tả meta có độ dài tốt.';

$_['meta_title_width_good'] = '<b> Độ dài tiêu đề SEO: </b> Tiêu đề SEO có độ dài đẹp.';

$_['keyphrase_exact_in_title_good'] = '<b> Cụm từ khóa trong tiêu đề: </b> Đối sánh chính xác của cụm từ khóa xuất hiện ở đầu tiêu đề SEO. Làm tốt lắm!';

$_['keyphrase_density_good'] = '<b> Mật độ cụm từ khóa: </b> Mật độ cụm từ khóa tiêu điểm là từ 1-2,4%. Điều đó thật tuyệt!';

$_['description_length_good'] = '<b> Độ dài văn bản: </b> Văn bản chứa hơn 300 từ. Làm tốt lắm!';

$_['image_title_good'] = '<b> Tiêu đề hình ảnh: </b> Tiêu đề hình ảnh chứa cụm từ khóa. Điều đó thật tuyệt!';

$_['image_alt_good'] = '<b> Hình ảnh thay thế: </b> Hình ảnh thay thế chứa cụm từ khóa. Thật tuyệt!';

