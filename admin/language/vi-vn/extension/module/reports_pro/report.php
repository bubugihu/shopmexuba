<?php
// Tiêu đề
$_['header_title'] = 'iExtend SEO Guru';
$_['header_product'] = 'Sản phẩm';
$_['header_seo'] = 'SEO';
$_['product_heading_title'] = 'Sản phẩm iExtend SEO Guru';
$_['seo_heading_title'] = 'iExtend SEO Guru SEO';


// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi mô-đun iExtend SEO Guru!';
$_['text_edit'] = 'Chỉnh sửa Mô-đun Guru SEO của iExtend';
$_['text_edit_seo'] = 'iExtend SEO Guru SEO';

// Mục nhập
$_['entry_status'] = 'Trạng thái';
$_['entry_name'] = 'Tên hạng mục';
$_['entry_status'] = 'Trạng thái';
$_['entry_seo'] = 'SEO';
$_['entry_meta_title'] = 'Tiêu đề Meta';
$_['entry_meta_description'] = 'Mô tả Meta';
$_['entry_meta_keywords'] = 'Từ khoá Meta';



// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi mô-đun iExtend SEO Guru!';

$_['text_description'] = 'Mô tả';
$_['text_value'] = 'Giá trị';
$_['text_status'] = 'Trạng thái';
$_['text_action'] = 'Hành động';
$_['text_report'] = 'Báo cáo';
$_['text_seo'] = 'SEO';
$_['text_name'] = 'Tên';
$_['text_generate'] = 'Tạo';
$_['text_clear'] = 'Xóa';

$_['text_generate_seo'] = 'Tạo SEO';

// thông báo thành công
$_['text_meta_title_success'] = 'Thành công: Bạn đã sửa đổi Meta Titles!';
$_['text_meta_description_success'] = 'Thành công: Bạn đã sửa đổi Mô tả Meta!';
$_['text_meta_keyword_success'] = 'Thành công: Bạn đã sửa đổi Từ khoá Meta!';
$_['text_tag_success'] = 'Thành công: Bạn đã sửa đổi Thẻ!';
$_['text_seo_url_success'] = 'Thành công: Bạn đã sửa đổi URL SEO!';
$_['text_custom_image_title_success'] = 'Thành công: Bạn đã sửa đổi Tiêu đề Hình ảnh Tuỳ chỉnh!';
$_['text_custom_image_alt_success'] = 'Thành công: Bạn đã sửa đổi Hình ảnh Tuỳ chỉnh Alts!';
$_['text_image_name_rename_success'] = 'Thành công: Bạn đã sửa đổi tên hình ảnh!';


// tên trình tạo
$_['text_seo_url_generator'] = 'Trình tạo URL SEO';
$_['text_keywords_generator'] = 'Trình tạo Từ khoá Meta';
$_['text_meta_title_generator'] = 'Trình tạo tiêu đề meta';
$_['text_meta_tags_generator'] = 'Trình tạo thẻ';
$_['text_meta_description_generator'] = 'Trình tạo mô tả meta';
$_['text_custom_alts_generator'] = 'Trình tạo Alts Hình ảnh Tùy chỉnh';
$_['text_custom_titles_generator'] = 'Trình tạo tiêu đề hình ảnh tùy chỉnh';

// mô tả trình tạo
$_['text_seo_url_generator_desc'] = 'Tạo URL SEO cho Sản phẩm, Danh mục, Thông tin, Nhà sản xuất.
<ul>
<li> <b> Sản phẩm: </b> Tên Sản phẩm </li>
<li> <b> Danh mục: </b> Tên Danh mục </li>
<li> <b> Thông tin: </b> Tiêu đề Thông tin </li>
<li> <b> Nhà sản xuất: </b> Tên Nhà sản xuất </li>
</ul> ';

$_['text_keywords_generator_desc'] = 'Tạo Từ khoá Meta cho Sản phẩm, Danh mục, Thông tin, Nhà sản xuất.
<ul>
<li> <b> Sản phẩm: </b> Tên sản phẩm, Kiểu máy, Nhãn hiệu, Sku, Upc, Danh mục </li>
<li> <b> Danh mục: </b> Tên Danh mục </li>
<li> <b> Thông tin: </b> Tiêu đề Thông tin </li>
<li> <b> Nhà sản xuất: </b> Tên Nhà sản xuất </li>
</ul> ';

$_['text_meta_title_generator_desc'] = 'Tạo Tiêu đề Meta cho Sản phẩm, Danh mục, Thông tin, Nhà sản xuất.
<ul>
<li> <b> Sản phẩm: </b> Tên sản phẩm, Mẫu, Nhãn hiệu, Giá, Sku, Upc, Danh mục | Tên cửa hàng </li>
<li> <b> Danh mục: </b> Tên Danh mục | Tên cửa hàng </li>
<li> <b> Thông tin: </b> Tiêu đề Thông tin | Tên cửa hàng </li>
<li> <b> Nhà sản xuất: </b> Tên Nhà sản xuất | Tên cửa hàng </li>
</ul> ';

$_['text_meta_tags_generator_desc'] = 'Tạo thẻ cho Sản phẩm, Danh mục, Nhà sản xuất.
<ul>
<li> <b> Sản phẩm: </b> Tên sản phẩm, Kiểu máy, Nhãn hiệu, Sku, Upc, Danh mục </li>
<li> <b> Danh mục: </b> Tên Danh mục </li>
<li> <b> Nhà sản xuất: </b> Tên Nhà sản xuất </li>
</ul> ';

$_['text_meta_description_generator_desc'] = 'Tạo Meta Mô tả cho Sản phẩm, Danh mục, Thông tin, Nhà sản xuất.
<ul>
<li> <b> Sản phẩm: </b> Tên sản phẩm, kiểu máy, mô tả <small> (bắt đầu một số phần) </small> </li>
<li> <b> Danh mục: </b> Tên Danh mục, Mô tả <small> (bắt đầu một số phần) </small> </li>
<li> <b> Thông tin: </b> Tiêu đề Thông tin, Mô tả <small> (bắt đầu một số phần) </small> </li>
<li> <b> Nhà sản xuất: </b> Tên Nhà sản xuất, Mô tả <small> (bắt đầu một số phần) </small> </li>
</ul> ';

$_['text_custom_alts_generator_desc'] = 'Tạo ảnh tùy chỉnh cho Sản phẩm, Danh mục, Nhà sản xuất.
<ul>
<li> <b> Sản phẩm: </b> Tên sản phẩm, Mẫu, Nhãn hiệu, Sku, Upc, Danh mục | Tên cửa hàng </li>
<li> <b> Danh mục: </b> Tên Danh mục | Tên cửa hàng </li>
<li> <b> Nhà sản xuất: </b> Tên Nhà sản xuất | Tên cửa hàng </li>
</ul> ';

$_['text_custom_titles_generator_desc'] = 'Tạo Tiêu đề Hình ảnh Tùy chỉnh cho Sản phẩm, Danh mục, Nhà sản xuất.
<ul>
<li> <b> Sản phẩm: </b> Tên sản phẩm, Mẫu, Nhãn hiệu, Sku, Upc, Danh mục | Tên cửa hàng </li>
<li> <b> Danh mục: </b> Tên Danh mục | Tên cửa hàng </li>
<li> <b> Nhà sản xuất: </b> Tên Nhà sản xuất | Tên cửa hàng </li>
</ul> ';


$_['text_rename'] = 'Đổi tên';

$_['text_rename_images'] = 'Đổi tên hình ảnh';

// đổi tên tên
$_['text_seo_images'] = 'Tên hình ảnh SEO';

// đổi tên mô tả
$_['text_seo_images_desc'] = 'Đổi tên hình ảnh của Sản phẩm, Danh mục, Nhà sản xuất thành tên thân thiện với SEO. <br> <b> Lưu ý :</b> <small> <font color = "red"> Sản phẩm, Danh mục và Nhà sản xuất với Url SEO sẽ chỉ bị ảnh hưởng. </small> </font> ';

$_['text_clear_seo'] = 'Rõ ràng SEO';

// tên giải phóng mặt bằng
$_['text_clear_seo_url'] = 'Xóa URL SEO';
$_['text_clear_meta_keywords'] = 'Xoá Từ khoá Meta';
$_['text_clear_meta_tags'] = 'Xóa thẻ';
$_['text_clear_meta_description'] = 'Xóa Meta Description';
$_['text_clear_custom_alts'] = 'Xóa ảnh tùy chỉnh';
$_['text_clear_custom_titles'] = 'Xóa tiêu đề hình ảnh tùy chỉnh';

// mô tả giải phóng mặt bằng
$_['text_clear_seo_url_desc'] = 'Xóa các Url SEO khỏi Sản phẩm, Danh mục, Thông tin, Nhà sản xuất.';

$_['text_clear_meta_keywords_desc'] = 'Xóa các Từ khóa Meta khỏi Sản phẩm, Danh mục, Thông tin, Nhà sản xuất.';

$_['text_clear_meta_tags_desc'] = 'Xóa Thẻ khỏi Sản phẩm, Danh mục, Nhà sản xuất.';

$_['text_clear_meta_description_desc'] = 'Xóa Meta Mô tả từ Sản phẩm, Danh mục, Thông tin, Nhà sản xuất.';

$_['text_clear_custom_alts_desc'] = 'Xóa các lỗi hình ảnh tùy chỉnh khỏi Sản phẩm, Danh mục, Nhà sản xuất.';

$_['text_clear_custom_titles_desc'] = 'Xóa Tiêu đề Hình ảnh Tùy chỉnh khỏi Sản phẩm, Danh mục, Nhà sản xuất.';


// Cửa hàng
$_['text_store'] = 'Cửa hàng';
$_['text_store_meta_title'] = 'Tiêu đề Meta Store';
$_['text_store_meta_description'] = 'Mô tả Meta Cửa hàng';
$_['text_store_meta_keyword'] = 'Từ khoá Meta Cửa hàng';
$_['text_yes'] = 'Có';
$_['text_no'] = 'Không';


// Các sản phẩm
$_['text_products'] = 'Sản phẩm';
$_['text_list'] = 'Danh sách sản phẩm';
$_['text_in_stock'] = 'Còn hàng';
$_['text_out_of_stock'] = 'Hết hàng';
$_['text_total_products'] = 'Tổng sản phẩm';
$_['text_product_seo'] = 'Sản phẩm có URL SEO';
$_['text_product_no_seo'] = 'Sản phẩm không có URL SEO';
$_['text_product_active'] = 'Sản phẩm đang hoạt động';
$_['text_product_inactive'] = 'Sản phẩm Không hoạt động';
$_['text_out_of_stock_products'] = 'Sản phẩm hết hàng';
$_['text_in_stock_products'] = 'Sản phẩm có sẵn trong kho';
$_['text_products_with_image'] = 'Sản phẩm có hình ảnh';
$_['text_products_without_image'] = 'Sản phẩm không có hình ảnh';
$_['text_products_with_tags'] = 'Sản phẩm có thẻ';
$_['text_products_without_tags'] = 'Sản phẩm không có thẻ';
$_['text_products_with_meta_title'] = 'Sản phẩm có Tiêu đề Meta';
$_['text_products_without_meta_title'] = 'Sản phẩm không có Tiêu đề Meta';
$_['text_products_with_meta_description'] = 'Sản phẩm có Meta Description';
$_['text_products_without_meta_description'] = 'Sản phẩm không có Meta Description';
$_['text_products_with_meta_keywords'] = 'Sản phẩm có Từ khoá Meta';
$_['text_products_without_meta_keywords'] = 'Sản phẩm không có Từ khoá Meta';
$_['text_products_with_image_custom_title'] = 'Sản phẩm có tiêu đề tùy chỉnh hình ảnh';
$_['text_products_without_image_custom_title'] = 'Sản phẩm không có tiêu đề tùy chỉnh hình ảnh';
$_['text_products_with_image_custom_alt'] = 'Sản phẩm có chức năng thay thế tùy chỉnh hình ảnh';
$_['text_products_without_image_custom_alt'] = 'Sản phẩm không có hình ảnh thay thế tùy chỉnh';
$_['text_products_with_custom_h1_tag'] = 'Sản phẩm có thẻ H1';
$_['text_products_without_custom_h1_tag'] = 'Sản phẩm không có thẻ H1';
$_['text_products_with_custom_h2_tag'] = 'Sản phẩm có thẻ H2';
$_['text_products_without_custom_h2_tag'] = 'Sản phẩm không có thẻ H2';



// Thể loại
$_['text_categories'] = 'Danh mục';
$_['text_total_categories'] = 'Tổng số danh mục';
$_['text_category_seo'] = 'Danh mục có URL SEO';
$_['text_category_no_seo'] = 'Danh mục không có URL SEO';
$_['text_category_active'] = 'Danh mục hoạt động';
$_['text_category_inactive'] = 'Danh mục Hoạt động';
$_['text_categories_with_tags'] = 'Danh mục có thẻ';
$_['text_categories_without_tags'] = 'Danh mục không có thẻ';
$_['text_categories_with_image'] = 'Danh mục có hình ảnh';
$_['text_categories_without_image'] = 'Danh mục không có hình ảnh';
$_['text_categories_with_meta_title'] = 'Danh mục có tiêu đề meta';
$_['text_categories_without_meta_title'] = 'Danh mục không có tiêu đề meta';
$_['text_categories_with_meta_description'] = 'Danh mục có Meta Description';
$_['text_categories_without_meta_description'] = 'Danh mục không có Meta Description';
$_['text_categories_with_meta_keywords'] = 'Danh mục có Từ khoá Meta';
$_['text_categories_without_meta_keywords'] = 'Danh mục không có Từ khoá Meta';
$_['text_categories_with_image_custom_title'] = 'Danh mục có tiêu đề tùy chỉnh hình ảnh';
$_['text_categories_without_image_custom_title'] = 'Danh mục không có tiêu đề tùy chỉnh hình ảnh';
$_['text_categories_with_image_custom_alt'] = 'Danh mục có thay thế tùy chỉnh hình ảnh';
$_['text_categories_without_image_custom_alt'] = 'Danh mục không có hình ảnh thay thế tùy chỉnh';
$_['text_categories_with_custom_h1_tag'] = 'Danh mục có thẻ H1';
$_['text_categories_without_custom_h1_tag'] = 'Danh mục không có thẻ H1';


// Thông tin
$_['text_information'] = 'Thông tin';
$_['text_total_informations'] = 'Tổng số trang thông tin';
$_['text_informations_active'] = 'Trang thông tin hoạt động';
$_['text_informations_inactive'] = 'Trang thông tin không hoạt động';
$_['text_informations_seo'] = 'Thông tin về URL SEO';
$_['text_informations_no_seo'] = 'Thông tin không có URL SEO';
$_['text_informations_with_meta_title'] = 'Thông tin với Tiêu đề Meta';
$_['text_informations_without_meta_title'] = 'Thông tin không có Tiêu đề Meta';
$_['text_informations_with_meta_description'] = 'Thông tin với Meta Description';
$_['text_informations_without_meta_description'] = 'Thông tin không có Meta Description';
$_['text_informations_with_meta_keywords'] = 'Thông tin về Từ khoá Meta';
$_['text_informations_without_meta_keywords'] = 'Thông tin không có Từ khoá Meta';
$_['text_informations_with_custom_h1_tag'] = 'Thông tin với thẻ H1';
$_['text_informations_without_custom_h1_tag'] = 'Thông tin không có thẻ H1';


// Nhà chế tạo
$_['text_manosystemurer'] = 'Nhà sản xuất';
$_['text_total_manmakers'] = 'Tổng số nhà sản xuất';
$_['text_manthers_with_tags'] = 'Các nhà sản xuất có thẻ';
$_['text_manthers_without_tags'] = 'Các nhà sản xuất không có thẻ';
$_['text_manthers_with_image'] = 'Các nhà sản xuất có hình ảnh';
$_['text_manthers_without_image'] = 'Các nhà sản xuất không có hình ảnh';
$_['text_manthers_seo'] = 'Các nhà sản xuất có URL SEO';
$_['text_manthers_no_seo'] = 'Các nhà sản xuất không có URL SEO';
$_['text_manthers_with_meta_title'] = 'Các nhà sản xuất có Meta Title';
$_['text_manthers_without_meta_title'] = 'Các nhà sản xuất không có Meta Title';
$_['text_manooter_with_meta_description'] = 'Các nhà sản xuất có Meta Description';
$_['text_manthers_without_meta_description'] = 'Các nhà sản xuất không có Meta Description';
$_['text_manthers_with_meta_keywords'] = 'Các nhà sản xuất có Từ khóa Meta';
$_['text_manthers_without_meta_keywords'] = 'Các nhà sản xuất không có Từ khoá Meta';
$_['text_manthers_with_custom_h1_tag'] = 'Các nhà sản xuất có Thẻ H1';
$_['text_manthers_without_custom_h1_tag'] = 'Các nhà sản xuất không có thẻ H1';
$_['text_manthers_with_image_custom_title'] = 'Các nhà sản xuất có Tiêu đề Tùy chỉnh Hình ảnh';
$_['text_manthers_without_image_custom_title'] = 'Các nhà sản xuất không có Tiêu đề Tùy chỉnh Hình ảnh';
$_['text_manooter_with_image_custom_alt'] = 'Các nhà sản xuất có chức năng thay thế tùy chỉnh hình ảnh';
$_['text_manthers_without_image_custom_alt'] = 'Các nhà sản xuất không có chức năng thay thế tùy chỉnh hình ảnh';



// Đơn hàng
$_['text_orders'] = 'Đơn hàng';
$_['text_total_orders'] = 'Tổng số đơn hàng';
$_['text_pend_orders'] = 'Đơn đặt hàng đang chờ xử lý';
$_['text_processing_orders'] = 'Đang xử lý đơn hàng';
$_['text_shipped_orders'] = 'Đơn hàng đã giao';
$_['text_complete_orders'] = 'Hoàn tất đơn hàng';
$_['text_canceled_orders'] = 'Đơn đặt hàng đã bị hủy';
$_['text_denied_orders'] = 'Đơn đặt hàng bị từ chối';
$_['text_canceled_reversal_orders'] = 'Lệnh đảo ngược đã bị hủy';
$_['text_failed_orders'] = 'Không thành công đơn hàng';
$_['text_refunded_orders'] = 'Đơn hàng đã hoàn tiền';
$_['text_reversed_orders'] = 'Đơn đặt hàng đã đảo ngược';
$_['text_chargeback_orders'] = 'Yêu cầu bồi hoàn';
$_['text_expired_orders'] = 'Đơn hàng đã hết hạn';
$_['text_processed_orders'] = 'Đơn đặt hàng đã xử lý';
$_['text_voided_orders'] = 'Đơn đặt hàng bị hủy bỏ';