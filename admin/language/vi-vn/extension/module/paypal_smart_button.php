<?php
// Tiêu đề
$_['header_title'] = 'Nút PayPal thông minh';

// Bản văn
$_['text_extensions'] = 'Phần mở rộng';
$_['text_edit'] = 'Chỉnh sửa mô-đun nút PayPal thông minh';
$_['text_general'] = 'Chung';
$_['text_product_page'] = 'Trang Sản phẩm';
$_['text_cart_page'] = 'Trang giỏ hàng';
$_['text_insert_prepend'] = 'Chèn vào phần đầu';
$_['text_insert_append'] = 'Chèn vào phần cuối';
$_['text_insert_before'] = 'Chèn trước';
$_['text_insert_after'] = 'Chèn sau';
$_['text_align_left'] = 'Căn trái';
$_['text_align_center'] = 'Căn giữa';
$_['text_align_right'] = 'Căn phải';
$_['text_small'] = 'Nhỏ';
$_['text_medium'] = 'Trung bình';
$_['text_large'] = 'Lớn';
$_['text_responsive'] = 'Phản hồi';
$_['text_gold'] = 'Vàng';
$_['text_blue'] = 'Xanh lam';
$_['text_silver'] = 'Bạc';
$_['text_white'] = 'Trắng';
$_['text_black'] = 'Đen';
$_['text_pill'] = 'Thuốc viên';
$_['text_rect'] = 'Hình thức';
$_['text_checkout'] = 'Thanh toán';
$_['text_pay'] = 'Thanh toán';
$_['text_buy_now'] = 'Mua ngay';
$_['text_pay_pal'] = 'PayPal';
$_['text_installment'] = 'Trả góp';
$_['text_text'] = 'Tin nhắn Văn bản';
$_['text_flex'] = 'Biểu ngữ linh hoạt';
$_['text_yes'] = 'Có';
$_['text_no'] = 'Không';

// Mục nhập
$_['entry_status'] = 'Trạng thái';
$_['entry_product_page_status'] = 'Trạng thái Trang Sản phẩm';
$_['entry_cart_page_status'] = 'Trạng thái trang giỏ hàng';
$_['entry_insert_tag'] = 'Chèn thẻ';
$_['entry_insert_type'] = 'Chèn loại';
$_['entry_button_align'] = 'Căn chỉnh nút';
$_['entry_button_size'] = 'Kích thước nút';
$_['entry_button_color'] = 'Màu nút';
$_['entry_button_shape'] = 'Hình dạng nút';
$_['entry_button_label'] = 'Nhãn nút';
$_['entry_button_tagline'] = 'Dòng giới thiệu của nút';
$_['entry_message_status'] = 'Thanh toán thư sau';
$_['entry_message_align'] = 'Căn chỉnh thông báo';
$_['entry_message_size'] = 'Kích thước tin nhắn';
$_['entry_message_layout'] = 'Bố cục Thư';
$_['entry_message_text_color'] = 'Màu văn bản tin nhắn';
$_['entry_message_text_size'] = 'Kích thước văn bản tin nhắn';
$_['entry_message_flex_color'] = 'Màu biểu ngữ tin nhắn';
$_['entry_message_flex_ratio'] = 'Tỷ lệ biểu ngữ thông báo';

// Cứu giúp
$_['help_message_status'] = 'Thêm tin nhắn trả tiền sau vào trang web của bạn.';

// Sự thành công
$_['success_save'] = 'Thành công: Bạn đã sửa đổi mô-đun Nút PayPal thông minh!';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi mô-đun Nút thanh toán nhanh của PayPal!';