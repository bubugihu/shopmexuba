<?php
// Heading
$_['heading_title']    = '<span style="color: #07B8DA; font-weight: 600; ">M-Blog tất cả trong một</span>';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi m-blog tất cả trong một mô-đun!';
$_['text_edit'] = 'Chỉnh sửa M-Blog Tất cả trong một Mô-đun';

// Mục nhập
$_['entry_name'] = 'Tên mô-đun';
$_['entry_mpblogpost'] = 'Blog';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';
$_['entry_limit'] = 'Giới hạn';
$_['entry_image_size'] = 'Kích thước hình ảnh';
$_['entry_width'] = 'Chiều rộng';
$_['entry_height'] = 'Chiều cao';
$_['entry_status'] = 'Trạng thái';

// Chuyển hướng
$_['tab_featured'] = 'Nổi bật';
$_['tab_latest'] = 'Mới nhất';
$_['tab_popular'] = 'Phổ biến';
$_['tab_trending'] = 'Xu hướng';

// Cứu giúp
$_['help_mpblogpost'] = '(Tự động điền)';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi m-blog tất cả trong một mô-đun!';
$_['error_name'] = 'Tên Mô-đun phải từ 3 đến 64 ký tự!';
$_['error_image_size'] = 'Chiều rộng & Chiều cao là bắt buộc!';