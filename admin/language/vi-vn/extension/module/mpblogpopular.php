<?php
// Heading
$_['heading_title']    = '<span style="color: #07B8DA; font-weight: 600; ">M-Blog phổ biến</span>';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi mô-đun phổ biến của m-blog!';
$_['text_edit'] = 'Chỉnh sửa Mô-đun Phổ biến của M-Blog';

// Mục nhập
$_['entry_name'] = 'Tên mô-đun';
$_['entry_limit'] = 'Giới hạn';
$_['entry_image_size'] = 'Kích thước hình ảnh';
$_['entry_width'] = 'Chiều rộng';
$_['entry_height'] = 'Chiều cao';
$_['entry_status'] = 'Trạng thái';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi mô-đun phổ biến của m-blog!';
$_['error_name'] = 'Tên Mô-đun phải từ 3 đến 64 ký tự!';
$_['error_image_size'] = 'Chiều rộng & Chiều cao là bắt buộc!';