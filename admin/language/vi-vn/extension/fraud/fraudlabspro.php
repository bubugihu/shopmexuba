<?php
// Tiêu đề
$_['heading_title'] = 'FraudLabs Pro';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi Cài đặt của FraudLabs Pro!';
$_['text_edit'] = 'Cài đặt';
$_['text_signup'] = 'FraudLabs Pro là một dịch vụ phát hiện gian lận. Bạn có thể <a href="http://www.fraudlabspro.com/plan?ref=1730" target="_blank"> <u> đăng ký tại đây </u> </a> để nhận Khóa API miễn phí. ' ;
$_['text_id'] = 'ID FraudLabs Pro';
$_['text_ip_address'] = 'Địa chỉ IP';
$_['text_ip_net_speed'] = 'Tốc độ mạng IP';
$_['text_ip_isp_name'] = 'Tên IP ISP';
$_['text_ip_usage_type'] = 'Loại sử dụng IP';
$_['text_ip_domain'] = 'Tên miền IP';
$_['text_ip_time_zone'] = 'Múi giờ IP';
$_['text_ip_location'] = 'Vị trí IP';
$_['text_ip_distance'] = 'Khoảng cách IP';
$_['text_ip_latitude'] = 'IP Latitude';
$_['text_ip_longitude'] = 'Kinh độ IP';
$_['text_risk_country'] = 'Quốc gia có rủi ro cao';
$_['text_free_email'] = 'Email miễn phí';
$_['text_ship_osystem'] = 'Chuyển tiếp tàu';
$_['text_using_proxy'] = 'Sử dụng Proxy';
$_['text_bin_found'] = 'Tìm thấy BIN';
$_['text_email_blacklist'] = 'Danh sách đen email';
$_['text_credit_card_blacklist'] = 'Danh sách đen thẻ tín dụng';
$_['text_score'] = 'Điểm chuyên nghiệp của FraudLabs';
$_['text_status'] = 'Trạng thái FraudLabs Pro';
$_['text_message'] = 'Tin nhắn';
$_['text_transaction_id'] = 'ID giao dịch';
$_['text_credits'] = 'Số dư';
$_['text_error'] = 'Lỗi:';
$_['text_flp_upgrade'] = '<a href="http://www.fraudlabspro.com/plan" target="_blank"> [Nâng cấp] </a>';
$_['text_flp_merchant_area'] = 'Vui lòng đăng nhập vào <a href="http://www.fraudlabspro.com/merchant/login" target="_blank"> Khu vực người bán FraudLabs Pro </a> để biết thêm thông tin về điều này đặt hàng.';
// Mục nhập
$_['entry_status'] = 'Trạng thái';
$_['entry_key'] = 'Khóa API';
$_['entry_score'] = 'Điểm rủi ro';
$_['entry_order_status'] = 'Trạng thái Đơn hàng';
$_['entry_review_status'] = 'Trạng thái Đánh giá';
$_['entry_approve_status'] = 'Trạng thái phê duyệt';
$_['entry_reject_status'] = 'Trạng thái từ chối';
$_['entry_simulate_ip'] = 'Mô phỏng IP';

// Cứu giúp
$_['help_order_status'] = 'Các đơn hàng có điểm cao hơn điểm rủi ro mà bạn đã đặt sẽ được chỉ định trạng thái đơn hàng này.';
$_['help_review_status'] = 'Các đơn hàng được FraudLabs Pro đánh dấu là đã xem xét sẽ được chỉ định trạng thái đơn hàng này.';
$_['help_approve_status'] = 'Các đơn hàng được FraudLabs Pro đánh dấu là chấp thuận sẽ được gán trạng thái đơn hàng này.';
$_['help_reject_status'] = 'Các đơn hàng bị FraudLabs Pro đánh dấu là từ chối sẽ được chỉ định trạng thái đơn hàng này.';
$_['help_simulate_ip'] = 'Mô phỏng địa chỉ IP của khách truy cập để kiểm tra. Để trống để vô hiệu hóa nó. ';
$_['help_fraudlabspro_id'] = 'Định danh duy nhất cho một giao dịch được hệ thống FraudLabs Pro sàng lọc.';
$_['help_ip_address'] = 'Địa chỉ IP.';
$_['help_ip_net_speed'] = 'Tốc độ kết nối.';
$_['help_ip_isp_name'] = 'ISP của địa chỉ IP.';
$_['help_ip_usage_type'] = 'Loại địa chỉ IP sử dụng. Ví dụ: ISP, Thương mại, Khu dân cư. ';
$_['help_ip_domain'] = 'Tên miền của địa chỉ IP.';
$_['help_ip_time_zone'] = 'Múi giờ của địa chỉ IP.';
$_['help_ip_location'] = 'Vị trí của địa chỉ IP.';
$_['help_ip_distance'] = 'Khoảng cách từ địa chỉ IP đến Vị trí thanh toán.';
$_['help_ip_latitude'] = 'Vĩ độ của địa chỉ IP.';
$_['help_ip_longitude'] = 'Kinh độ của địa chỉ IP.';
$_['help_risk_country'] = 'Quốc gia địa chỉ IP có nằm trong danh sách quốc gia có nguy cơ cao mới nhất hay không.';
$_['help_free_email'] = 'Liệu e-mail có phải từ nhà cung cấp e-mail miễn phí hay không.';
$_['help_ship_osystem'] = 'Địa chỉ giao hàng có phải là địa chỉ giao nhận hàng hóa hay không.';
$_['help_using_proxy'] = 'Địa chỉ IP có phải từ Máy chủ proxy ẩn danh hay không.';
$_['help_bin_found'] = 'Thông tin BIN có khớp với danh sách BIN của chúng ta hay không.';
$_['help_email_blacklist'] = 'Địa chỉ email có nằm trong cơ sở dữ liệu danh sách đen của chúng tôi hay không.';
$_['help_credit_card_blacklist'] = 'Thẻ tín dụng có nằm trong cơ sở dữ liệu danh sách cấm của chúng tôi hay không.';
$_['help_score'] = 'Điểm rủi ro, 0 (rủi ro thấp) - 100 (rủi ro cao).';
$_['help_status'] = 'Trạng thái của FraudLabs Pro.';
$_['help_message'] = 'Mô tả thông báo lỗi FraudLabs Pro.';
$_['help_transaction_id'] = 'Định danh duy nhất cho một giao dịch được hệ thống FraudLabs Pro sàng lọc.';
$_['help_credits'] = 'Số dư các khoản tín dụng có sẵn sau giao dịch này.';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi cài đặt của FraudLabs Pro!';
$_['error_key'] = 'Cần có khóa API!';