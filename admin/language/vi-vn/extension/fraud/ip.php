<?php
//Tiêu đề
$_['heading_title'] = 'IP chống gian lận';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi IP Chống Gian lận!';
$_['text_edit'] = 'Chỉnh sửa IP chống gian lận';
$_['text_ip_add'] = 'Thêm địa chỉ IP';
$_['text_ip_list'] = 'Danh sách địa chỉ IP gian lận';

// Cột
$_['column_ip'] = 'IP';
$_['column_total'] = 'Tổng số tài khoản';
$_['column_date_added'] = 'Ngày Thêm';
$_['column_action'] = 'Hành động';

// Mục nhập
$_['entry_ip'] = 'IP';
$_['entry_status'] = 'Trạng thái';
$_['entry_order_status'] = 'Trạng thái Đơn hàng';

// Cứu giúp
$_['help_order_status'] = 'Khách hàng có IP bị cấm trên tài khoản của họ sẽ được gán trạng thái đơn hàng này và sẽ không được phép tự động đạt đến trạng thái hoàn chỉnh.';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi IP Chống Gian lận!';