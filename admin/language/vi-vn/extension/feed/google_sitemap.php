<?php
// Tiêu đề
$_['header_title'] = 'Sơ đồ trang web của Google';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi nguồn cấp dữ liệu Sơ đồ trang web của Google!';
$_['text_edit'] = 'Chỉnh sửa Sơ đồ trang web của Google';

// Mục nhập
$_['entry_status'] = 'Trạng thái';
$_['entry_data_feed'] = 'Url nguồn cấp dữ liệu';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi nguồn cấp dữ liệu Sơ đồ trang web của Google!';