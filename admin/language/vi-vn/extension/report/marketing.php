<?php
// Tiêu đề
$_['heading_title'] = 'Báo cáo Tiếp thị';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_edit'] = 'Chỉnh sửa Báo cáo Tiếp thị';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi báo cáo tiếp thị!';
$_['text_filter'] = 'Bộ lọc';
$_['text_all_status'] = 'Tất cả các trạng thái';

// Cột
$_['column_campaign'] = 'Tên Chiến dịch';
$_['column_code'] = 'Mã';
$_['column_clicks'] = 'Số lần nhấp';
$_['column_orders'] = 'Số lượng đơn đặt hàng ';
$_['column_total'] = 'Tổng số';

// Mục nhập
$_['entry_date_start'] = 'Ngày bắt đầu';
$_['entry_date_end'] = 'Ngày kết thúc';
$_['entry_status'] = 'Trạng thái đơn hàng';
$_['entry_status'] = 'Trạng thái';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không được phép sửa đổi báo cáo tiếp thị!';