<?php
// Tiêu đề
$_['heading_title'] = 'Báo cáo phiếu giảm giá';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_edit'] = 'Chỉnh sửa Báo cáo Phiếu thưởng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi báo cáo phiếu thưởng!';
$_['text_filter'] = 'Bộ lọc';

// Cột
$_['column_name'] = 'Tên phiếu thưởng';
$_['column_code'] = 'Mã';
$_['column_orders'] = 'Đơn hàng';
$_['column_total'] = 'Tổng số';
$_['column_action'] = 'Hành động';

// Mục nhập
$_['entry_date_start'] = 'Ngày bắt đầu';
$_['entry_date_end'] = 'Ngày kết thúc';
$_['entry_status'] = 'Trạng thái';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi báo cáo phiếu giảm giá!';