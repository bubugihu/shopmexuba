<?php
// Tiêu đề
$_['heading_title'] = 'Báo cáo bán hàng';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_edit'] = 'Chỉnh sửa Báo cáo Bán hàng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi báo cáo bán hàng!';
$_['text_filter'] = 'Bộ lọc';
$_['text_year'] = 'Năm';
$_['text_month'] = 'Tháng';
$_['text_week'] = 'Tuần';
$_['text_day'] = 'Ngày';
$_['text_all_status'] = 'Tất cả các trạng thái';

// Cột
$_['column_date_start'] = 'Ngày bắt đầu';
$_['column_date_end'] = 'Ngày kết thúc';
$_['column_orders'] = 'Số lượng đơn đặt hàng ';
$_['column_products'] = 'Số lượng sản phẩm';
$_['column_tax'] = 'Thuế';
$_['column_total'] = 'Tổng số';

// Mục nhập
$_['entry_date_start'] = 'Ngày bắt đầu';
$_['entry_date_end'] = 'Ngày kết thúc';
$_['entry_group'] = 'Nhóm theo';
$_['entry_status'] = 'Trạng thái đơn hàng';
$_['entry_status'] = 'Trạng thái';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không được phép sửa đổi báo cáo bán hàng!';