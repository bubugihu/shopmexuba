<?php
// Tiêu đề
$_['heading_title'] = 'Trả về báo cáo';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_edit'] = 'Chỉnh sửa báo cáo trả hàng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi báo cáo trả hàng!';
$_['text_filter'] = 'Bộ lọc';
$_['text_year'] = 'Năm';
$_['text_month'] = 'Tháng';
$_['text_week'] = 'Tuần';
$_['text_day'] = 'Ngày';
$_['text_all_status'] = 'Tất cả các trạng thái';

// Cột
$_['column_date_start'] = 'Ngày Bắt đầu';
$_['column_date_end'] = 'Ngày kết thúc';
$_['column_returns'] = 'Số lượng trả về ';

// Mục nhập
$_['entry_date_start'] = 'Ngày bắt đầu';
$_['entry_date_end'] = 'Ngày kết thúc';
$_['entry_group'] = 'Nhóm theo';
$_['entry_status'] = 'Trạng thái trả hàng';
$_['entry_status'] = 'Trạng thái';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi báo cáo trả hàng!';