<?php
// Tiêu đề
$_['heading_title'] = 'Báo cáo đã mua sản phẩm';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_edit'] = 'Chỉnh sửa Báo cáo đã Mua Sản phẩm';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi báo cáo đã mua sản phẩm!';
$_['text_filter'] = 'Bộ lọc';
$_['text_all_status'] = 'Tất cả các trạng thái';

// Cột
$_['column_date_start'] = 'Ngày Bắt đầu';
$_['column_date_end'] = 'Ngày kết thúc';
$_['column_name'] = 'Tên sản phẩm';
$_['column_model'] = 'Mã sản phẩm';
$_['column_quantity'] = 'Số lượng';
$_['column_total'] = 'Tổng số';

// Mục nhập
$_['entry_date_start'] = 'Ngày bắt đầu';
$_['entry_date_end'] = 'Ngày kết thúc';
$_['entry_status'] = 'Trạng thái đơn hàng';
$_['entry_status'] = 'Trạng thái';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi báo cáo sản phẩm đã mua!';