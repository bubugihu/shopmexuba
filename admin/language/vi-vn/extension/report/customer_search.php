<?php
// Tiêu đề
$_['heading_title'] = 'Báo cáo Tìm kiếm của Khách hàng';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_edit'] = 'Chỉnh sửa Báo cáo Tìm kiếm của Khách hàng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi báo cáo tìm kiếm của khách hàng!';
$_['text_filter'] = 'Bộ lọc';
$_['text_guest'] = 'Khách';
$_['text_customer']     = '<a href="%s">%s</a>';

// Cột
$_['column_keyword'] = 'Từ khóa';
$_['column_products'] = 'Sản phẩm Tìm thấy';
$_['column_category'] = 'Danh mục';
$_['column_customer'] = 'Khách hàng';
$_['column_ip'] = 'IP';
$_['column_date_added'] = 'Ngày Thêm';

// Mục nhập
$_['entry_date_start'] = 'Ngày bắt đầu';
$_['entry_date_end'] = 'Ngày kết thúc';
$_['entry_keyword'] = 'Từ khóa';
$_['entry_customer'] = 'Khách hàng';
$_['entry_ip'] = 'IP';
$_['entry_status'] = 'Trạng thái';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi báo cáo tìm kiếm của khách hàng!';