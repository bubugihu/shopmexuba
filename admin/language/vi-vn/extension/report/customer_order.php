<?php
// Tiêu đề
$_['heading_title']         = 'Báo cáo đơn đặt hàng của khách hàng';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_edit'] = 'Chỉnh sửa Báo cáo Đơn đặt hàng của Khách hàng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi báo cáo đơn đặt hàng của khách hàng!';
$_['text_filter'] = 'Bộ lọc';
$_['text_all_status'] = 'Tất cả các trạng thái';

// Cột
$_['column_customer'] = 'Tên Khách hàng';
$_['column_email'] = 'Thư điện tử';
$_['column_customer_group'] = 'Nhóm Khách hàng';
$_['column_status'] = 'Trạng thái';
$_['column_orders'] = 'Số đơn đặt hàng ';
$_['column_products'] = 'Số các sản phẩm';
$_['column_total'] = 'Tổng số';
$_['column_action'] = 'Hành động';

// Mục nhập
$_['entry_date_start'] = 'Ngày bắt đầu';
$_['entry_date_end'] = 'Ngày kết thúc';
$_['entry_customer'] = 'Khách hàng';
$_['entry_status'] = 'Trạng thái đơn hàng';
$_['entry_status'] = 'Trạng thái';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không được phép sửa đổi báo cáo đơn đặt hàng của khách hàng!';