<?php
// Tiêu đề
$_['heading_title'] = 'Báo cáo Sản phẩm đã Xem';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_edit'] = 'Chỉnh sửa Báo cáo Sản phẩm đã Xem';
$_['text_success'] = 'Thành công: Bạn đã đặt lại báo cáo sản phẩm đã xem!';

// Cột
$_['column_name'] = 'Tên sản phẩm';
$_['column_model'] = 'Mã sản phẩm';
$_['column_viewed'] = 'Đã xem';
$_['column_percent'] = 'Phần trăm';

// Mục nhập
$_['entry_status'] = 'Trạng thái';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi các sản phẩm đã xem báo cáo!';