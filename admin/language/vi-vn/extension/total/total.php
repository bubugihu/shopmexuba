<?php
// Tiêu đề
$_['heading_title'] = 'Tổng số';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi tổng cộng!';
$_['text_edit'] = 'Chỉnh sửa Tổng cộng';

// Mục nhập
$_['entry_status'] = 'Trạng thái';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi tổng tổng!';