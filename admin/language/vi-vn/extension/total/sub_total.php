<?php
// Tiêu đề
$_['heading_title'] = 'Tổng phụ';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi tổng phụ!';
$_['text_edit'] = 'Chỉnh sửa Tổng số phụ';

// Mục nhập
$_['entry_status'] = 'Trạng thái';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi tổng phụ!';