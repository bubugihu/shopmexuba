<?php
// Tiêu đề
$_['heading_title'] = 'Phiếu quà tặng';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi tổng số phiếu quà tặng!';
$_['text_edit'] = 'Chỉnh sửa Tổng số Phiếu quà tặng';

// Mục nhập
$_['entry_status'] = 'Trạng thái';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không được phép sửa đổi tổng số phiếu quà tặng!';