<?php
// Tiêu đề
$_['heading_title'] = 'Thuế';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi tổng số thuế!';
$_['text_edit'] = 'Chỉnh sửa Tổng số thuế';

// Mục nhập
$_['entry_status'] = 'Trạng thái';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không được phép sửa đổi tổng số thuế!';