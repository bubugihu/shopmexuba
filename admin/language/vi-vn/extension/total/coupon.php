<?php
// Tiêu đề
$_['heading_title'] = 'Phiếu giảm giá';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi tổng số phiếu thưởng!';
$_['text_edit'] = 'Chỉnh sửa phiếu giảm giá';

// Mục nhập
$_['entry_status'] = 'Trạng thái';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không được phép sửa đổi tổng số phiếu thưởng!';