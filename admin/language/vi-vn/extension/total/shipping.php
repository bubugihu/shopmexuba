<?php
// Tiêu đề
$_['heading_title'] = 'Vận chuyển';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi tổng số tiền vận chuyển!';
$_['text_edit'] = 'Chỉnh sửa Tổng số tiền vận chuyển';

// Mục nhập
$_['entry_estimator'] = 'Công cụ Ước tính Vận chuyển';
$_['entry_status'] = 'Trạng thái';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không được phép sửa đổi tổng số tiền vận chuyển!';