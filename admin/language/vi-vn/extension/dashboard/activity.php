<?php
// Heading
$_['heading_title']                = 'Hoạt động gần đây';

// Bản văn
$_['text_extension'] = 'Phần mở rộng';
$_['text_success'] = 'Thành công: Bạn đã sửa đổi hoạt động trên trang tổng quan!';
$_['text_edit'] = 'Chỉnh sửa Hoạt động Gần đây của Trang tổng quan';
$_['text_activity_register']       = '<a href="customer_id=%d">%s</a> đã đăng ký một tài khoản mới.';
$_['text_activity_edit']           = '<a href="customer_id=%d">%s</a> đã cập nhật chi tiết tài khoản của họ.';
$_['text_activity_password']       = '<a href="customer_id=%d">%s</a> đã cập nhật mật khẩu tài khoản của họ.';
$_['text_activity_reset']          = '<a href="customer_id=%d">%s</a> đặt lại mật khẩu tài khoản của họ.';
$_['text_activity_login']          = '<a href="customer_id=%d">%s</a> đăng nhập.';
$_['text_activity_forgotten']      = '<a href="customer_id=%d">%s</a> đã yêu cầu đặt lại mật khẩu.';
$_['text_activity_address_add']    = '<a href="customer_id=%d">%s</a> đã thêm một địa chỉ mới.';
$_['text_activity_address_edit']   = '<a href="customer_id=%d">%s</a> đã cập nhật địa chỉ của họ.';
$_['text_activity_address_delete'] = '<a href="customer_id=%d">%s</a> đã xóa một trong những địa chỉ của họ.';
$_['text_activity_return_account'] = '<a href="customer_id=%d">%s</a> gửi yêu cầu đổi trả <a href="return_id=%d">sản phẩm</a>.';
$_['text_activity_return_guest']   = '%s gửi một sản phẩm <a href="return_id=%d">trở về</a>.';
$_['text_activity_order_account']  = '<a href="customer_id=%d">%s</a> đã thêm một <a href="order_id=%d">đơn hàng mới</a>.';
$_['text_activity_order_guest']    = '%s Tạo ra một <a href="order_id=%d">đơn hàng mới</a>.';
$_['text_activity_affiliate_add']  = '<a href="customer_id=%d">%s</a> đã đăng ký một tài khoản liên kết.';
$_['text_activity_affiliate_edit'] = '<a href="customer_id=%d">%s</a> đã cập nhật chi tiết liên kết của họ.';
$_['text_activity_transaction']    = '<a href="customer_id=%d">%s</a> nhận được hoa hồng từ một <a href="order_id=%d">đặt hàng</a>.';

// Entry
$_['entry_status'] = 'Trạng thái';
$_['entry_sort_order'] = 'Sắp xếp thứ tự';
$_['entry_width'] = 'Chiều rộng';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi hoạt động của trang tổng quan!';