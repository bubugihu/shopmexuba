<?php
// Tiêu đề
$_['heading_title'] = 'Tổng đơn hàng';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi tổng!';
$_['text_list'] = 'Danh sách Tổng đơn hàng';

// Cột
$_['column_name'] = 'Tổng Đơn hàng';
$_['column_status'] = 'Trạng thái';
$_['column_sort_order'] = 'Sắp xếp Thứ tự';
$_['column_action'] = 'Hành động';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi tổng!';