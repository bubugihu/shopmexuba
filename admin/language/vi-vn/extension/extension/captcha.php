<?php
// Tiêu đề
$_['heading_title'] = 'Captchas';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi captcha!';
$_['text_list'] = 'Danh sách Captcha';

// Cột
$_['column_name'] = 'Tên captcha';
$_['column_status'] = 'Trạng thái';
$_['column_action'] = 'Hành động';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi captcha!';