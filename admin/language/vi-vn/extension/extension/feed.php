<?php
// Tiêu đề
$_['header_title'] = 'Nguồn cấp dữ liệu';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi nguồn cấp dữ liệu!';
$_['text_list'] = 'Danh sách nguồn cấp dữ liệu';

// Cột
$_['column_name'] = 'Tên Nguồn cấp Sản phẩm';
$_['column_status'] = 'Trạng thái';
$_['column_action'] = 'Hành động';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi nguồn cấp dữ liệu!';