<?php
// Tiêu đề
$_['heading_title'] = 'Menu';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi menu!';
$_['text_list'] = 'Danh sách menu';

// Cột
$_['column_name'] = 'Tên Menu';
$_['column_status'] = 'Trạng thái';
$_['column_action'] = 'Hành động';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi menu!';