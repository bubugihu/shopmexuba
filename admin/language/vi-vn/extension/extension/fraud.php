<?php
// Tiêu đề
$_['heading_title'] = 'Chống gian lận';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi tính năng chống gian lận!';
$_['text_list'] = 'Danh sách Chống Gian lận';

// Cột
$_['column_name'] = 'Tên Chống Gian lận';
$_['column_status'] = 'Trạng thái';
$_['column_action'] = 'Hành động';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không được phép sửa đổi chống gian lận!';