<?php
// Tiêu đề
$_['heading_title'] = 'Trang tổng quan';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi trang tổng quan!';
$_['text_list'] = 'Danh sách trang tổng quan';

// Cột
$_['column_name'] = 'Tên Trang tổng quan';
$_['column_width'] = 'Chiều rộng';
$_['column_status'] = 'Trạng thái';
$_['column_sort_order'] = 'Sắp xếp Thứ tự';
$_['column_action'] = 'Hành động';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi trang tổng quan!';