<?php
// Tiêu đề
$_['heading_title'] = 'Chủ đề';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi chủ đề!';
$_['text_list'] = 'Danh sách chủ đề';

// Cột
$_['column_name'] = 'Tên Chủ đề';
$_['column_status'] = 'Trạng thái';
$_['column_action'] = 'Hành động';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi chủ đề!';