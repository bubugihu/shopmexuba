<?php
// Tiêu đề
$_['heading_title'] = 'Phân tích';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi số liệu phân tích!';
$_['text_list'] = 'Danh sách phân tích';

// Cột
$_['column_name'] = 'Tên phân tích';
$_['column_status'] = 'Trạng thái';
$_['column_action'] = 'Hành động';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi phân tích!';