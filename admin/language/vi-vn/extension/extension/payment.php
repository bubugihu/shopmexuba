<?php
// Tiêu đề
$_['heading_title'] = 'Thanh toán';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi thanh toán!';
$_['text_list'] = 'Danh sách thanh toán';


// Cột
$_['column_name'] = 'Phương thức Thanh toán';
$_['column_status'] = 'Status';
$_['column_sort_order'] = 'Sắp xếp thứ tự';
$_['column_action'] = 'Hành động';

// Error
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi thanh toán tài khoản!';