<?php
// Tiêu đề
$_['heading_title'] = 'Khác';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi phần mở rộng khác!';
$_['text_list'] = 'Danh sách khác';

// Cột
$_['column_name'] = 'Khác';
$_['column_status'] = 'Trạng thái';
$_['column_action'] = 'Hành động';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi phần mở rộng khác!';