<?php
// Tiêu đề
$_['heading_title'] = 'Tiếp thị';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi cách tiếp thị!';
$_['text_list'] = 'Danh sách phân tích';

// Cột
$_['column_name'] = 'Tên Tiếp thị';
$_['column_status'] = 'Trạng thái';
$_['column_action'] = 'Hành động';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi tiếp thị!';