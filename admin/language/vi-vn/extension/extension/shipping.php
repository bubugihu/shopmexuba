<?php
// Tiêu đề
$_['heading_title'] = 'Vận chuyển';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi giao hàng!';
$_['text_list'] = 'Danh sách giao hàng';

// Cột
$_['column_name'] = 'Phương thức Vận chuyển';
$_['column_status'] = 'Trạng thái';
$_['column_sort_order'] = 'Sắp xếp Thứ tự';
$_['column_action'] = 'Hành động';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi giao hàng!';