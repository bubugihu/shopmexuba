<?php
// Tiêu đề
$_['heading_title'] = 'Báo cáo';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi báo cáo!';
$_['text_list'] = 'Danh sách báo cáo';

// Cột
$_['column_name'] = 'Tên Báo cáo';
$_['column_status'] = 'Trạng thái';
$_['column_sort_order'] = 'Sắp xếp Thứ tự';
$_['column_action'] = 'Hành động';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi báo cáo!';