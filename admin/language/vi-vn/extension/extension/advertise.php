<?php

// Tiêu đề
$_['heading_title'] = "Quảng cáo";


// Cột
$_['column_name'] = "Tên Quảng cáo";
$_['column_status'] = "Trạng thái";
$_['column_action'] = "Hành động";


// Bản văn
$_['text_success'] = "<strong> Thành công: </strong> Bạn đã sửa đổi quảng cáo!";

// Lỗi
$_['error_adblock'] = "Có vẻ như bạn đang sử dụng trình chặn quảng cáo. Để sử dụng phần Quảng cáo này, vui lòng tắt trình chặn quảng cáo cho bảng quản trị OpenCart của bạn.";