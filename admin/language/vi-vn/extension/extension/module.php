<?php
// Tiêu đề
$_['heading_title'] = 'Mô-đun';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi các mô-đun!';
$_['text_layout'] = 'Sau khi cài đặt và định cấu hình một mô-đun, bạn có thể thêm mô-đun đó vào bố cục <a href="%s" class="alert-link"> tại đây </a>!';
$_['text_add'] = 'Thêm mô-đun';
$_['text_list'] = 'Danh sách mô-đun';

// Cột
$_['column_name'] = 'Tên Mô-đun';
$_['column_status'] = 'Trạng thái';
$_['column_action'] = 'Hành động';

// Mục nhập
$_['entry_code'] = 'Mô-đun';
$_['entry_name'] = 'Tên mô-đun';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi mô-đun!';
$_['error_name'] = 'Tên Mô-đun phải từ 3 đến 64 ký tự!';
$_['error_code'] = 'Cần có phần mở rộng!';