<?php
// Tiêu đề
$_['header_title'] = 'Đơn vị tiền tệ';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi đơn vị tiền tệ!';
$_['text_list'] = 'Danh sách tiền tệ';
$_['text_add'] = 'Thêm đơn vị tiền tệ';
$_['text_edit'] = 'Chỉnh sửa đơn vị tiền tệ';
$_['text_iso']             = 'Bạn có thể tìm thấy danh sách đầy đủ các code và cài đặt tiền tệ ISO <a href="http://www.xe.com/iso4217.php" target="_blank" class="alert-link">Ở đây</a>.';

// Cột
$_['column_title'] = 'Tiêu đề Đơn vị tiền tệ';
$_['column_code'] = 'Mã';
$_['column_value'] = 'Giá trị';
$_['column_date_modified'] = 'Cập nhật lần cuối';
$_['column_action'] = 'Hành động';

// Mục nhập
$_['entry_title'] = 'Tiêu đề tiền tệ';
$_['entry_code'] = 'Mã';
$_['entry_value'] = 'Giá trị';
$_['entry_symbol_left'] = 'Bên trái ký hiệu';
$_['entry_symbol_right'] = 'Biểu tượng bên phải';
$_['entry_decimal_place'] = 'Vị trí thập phân';
$_['entry_status'] = 'Trạng thái';

// Cứu giúp
$_['help_code'] = 'Không thay đổi nếu đây là đơn vị tiền tệ mặc định của bạn.';
$_['help_value'] = 'Đặt thành 1.00000 nếu đây là đơn vị tiền tệ mặc định của bạn.';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi đơn vị tiền tệ!';
$_['error_title'] = 'Tiêu đề đơn vị tiền tệ phải từ 3 đến 32 ký tự!';
$_['error_code'] = 'Mã tiền tệ phải có 3 ký tự!';
$_['error_default']        = 'Cảnh báo: Không thể xóa đơn vị tiền tệ này vì nó hiện được chỉ định làm đơn vị tiền tệ cửa hàng mặc định! ';
$_['error_store']          = 'Cảnh báo: Không thể xóa đơn vị tiền tệ này vì nó hiện được gán cho %s cửa hàng!'; 
$_['error_order']          = 'Cảnh báo: Không thể xóa đơn vị tiền tệ này vì nó hiện được gán cho %s đơn đặt hàng!';