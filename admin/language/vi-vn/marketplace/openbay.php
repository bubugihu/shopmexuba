<?php
// Tiêu đề
$_['heading_title'] = 'OpenBay Pro';

// Nút
$_['button_retry'] = 'Thử lại';
$_['button_update'] = 'Cập nhật';
$_['button_patch'] = 'Bản vá';
$_['button_faq'] = 'Xem chủ đề Câu hỏi thường gặp';

// Chuyển hướng
$_['tab_setting'] = 'Cài đặt';
$_['tab_update'] = 'Cập nhật phần mềm';
$_['tab_developer'] = 'Nhà phát triển';

// Bản văn
$_['text_dashboard'] = 'Trang tổng quan';
$_['text_success'] = 'Thành công: Cài đặt đã được lưu';
$_['text_products'] = 'Mục';
$_['text_orders'] = 'Đơn hàng';
$_['text_manage'] = 'Quản lý';
$_['text_help'] = 'Trợ giúp';
$_['text_tutorials'] = 'Hướng dẫn';
$_['text_suggestions'] = 'Ý tưởng';
$_['text_version_latest'] = 'Bạn đang chạy phiên bản mới nhất';
$_['text_version_check'] = 'Đang kiểm tra phiên bản phần mềm';
$_['text_version_installed'] = 'Phiên bản OpenBay Pro đã cài đặt: v';
$_['text_version_current'] = 'Phiên bản của bạn là';
$_['text_version_available'] = 'mới nhất là';
$_['text_language'] = 'Ngôn ngữ phản hồi API';
$_['text_getting_messages'] = 'Nhận tin nhắn OpenBay Pro';
$_['text_complete'] = 'Hoàn thành';
$_['text_patch_complete'] = 'Bản vá đã được áp dụng';
$_['text_updated'] = 'Mô-đun đã được cập nhật (v.% s)';
$_['text_update_description'] = 'Công cụ cập nhật sẽ thực hiện các thay đổi đối với hệ thống tệp cửa hàng của bạn. Đảm bảo rằng bạn có một tệp hoàn chỉnh và sao lưu cơ sở dữ liệu trước khi cập nhật. ';
$_['text_patch_description'] = 'Nếu bạn tải lên các tệp cập nhật theo cách thủ công, bạn cần chạy bản vá để hoàn tất cập nhật';
$_['text_clear_faq'] = 'Xóa cửa sổ bật lên Câu hỏi thường gặp bị ẩn';
$_['text_clear_faq_complete'] = 'Thông báo sẽ hiển thị lại';
$_['text_install_success'] = 'Thị trường đã được cài đặt';
$_['text_uninstall_success'] = 'Thị trường đã bị xóa';
$_['text_title_messages'] = 'Tin nhắn & amp; thông báo ';
$_['text_marketplace_shipped'] = 'Trạng thái đơn hàng sẽ được cập nhật để vận chuyển trên thị trường';
$_['text_action_warning'] = 'Hành động này rất nguy hiểm nên cần được bảo vệ bằng mật khẩu.';
$_['text_check_new'] = 'Đang kiểm tra phiên bản mới hơn';
$_['text_downloading'] = 'Tải xuống các tập tin cập nhật';
$_['text_extract'] = 'Giải nén các tập tin';
$_['text_running_patch'] = 'Đang chạy các tập tin vá lỗi';
$_['text_fail_patch'] = 'Không thể giải nén các tập tin cập nhật';
$_['text_updated_ok'] = 'Cập nhật xong, phiên bản đã cài đặt bây giờ';
$_['text_check_server'] = 'Kiểm tra yêu cầu máy chủ';
$_['text_version_ok'] = 'Phần mềm đã được cập nhật, phiên bản đã cài đặt là';
$_['text_remove_files'] = 'Không cần xóa các tập tin nữa';
$_['text_confirm_backup'] = 'Đảm bảo rằng bạn có một bản sao lưu đầy đủ trước khi tiếp tục';
$_['text_software_update'] = 'Cập nhật phần mềm OpenBay Pro';
$_['text_patch_option'] = 'Thủ công vá lỗi';

// Cột
$_['column_name'] = 'Tên trình cắm';
$_['column_status'] = 'Trạng thái';
$_['column_action'] = 'Hành động';

// Mục nhập
$_['entry_patch'] = 'Bản vá cập nhật thủ công';
$_['entry_courier'] = 'Chuyển phát nhanh';
$_['entry_courier_other'] = 'Chuyển phát nhanh khác';
$_['entry_tracking'] = 'Theo dõi #';
$_['entry_empty_data'] = 'Dữ liệu lưu trữ trống?';
$_['entry_password_prompt'] = 'Vui lòng nhập mật khẩu xóa dữ liệu';
$_['entry_update'] = 'Cập nhật dễ dàng bằng 1 cú nhấp chuột';
$_['entry_beta'] = 'Sử dụng phiên bản beta';

// Lỗi
$_['error_admin'] = 'Thư mục quản trị mong đợi';
$_['error_failed'] = 'Không tải được, thử lại?';
$_['error_tracking_id_format'] = 'ID theo dõi của bạn không được chứa các ký tự> hoặc <';
$_['error_tracking_courier'] = 'Bạn phải chọn chuyển phát nhanh nếu bạn muốn thêm ID theo dõi';
$_['error_tracking_custom'] = 'Vui lòng để trống trường chuyển phát nhanh nếu bạn muốn sử dụng chuyển phát nhanh tùy chỉnh';
$_['error_permission'] = 'Bạn không được phép sửa đổi phần mở rộng OpenBay Pro';
$_['error_file_delete'] = 'Không thể xóa các tập tin này, bạn nên xóa chúng theo cách thủ công';
$_['error_mkdir'] = 'Chức năng PHP "mkdir" bị tắt, hãy liên hệ với máy chủ của bạn';
$_['error_openssl_encrypt'] = 'Hàm PHP "openssl_encrypt" không được kích hoạt. Liên hệ với nhà cung cấp dịch vụ lưu trữ của bạn. ';
$_['error_openssl_decrypt'] = 'Hàm PHP "openssl_decrypt" không được kích hoạt. Liên hệ với nhà cung cấp dịch vụ lưu trữ của bạn. ';
$_['error_fopen'] = 'Hàm PHP "fopen" không được kích hoạt. Liên hệ với nhà cung cấp dịch vụ lưu trữ của bạn. ';
$_['error_url_fopen']             		= '"allow_url_fopen" Máy chủ của bạn vô hiệu hóa chỉ thị - bạn sẽ không thể nhập hình ảnh khi nhập sản phẩm từ eBay';
$_['error_curl'] = 'Thư viện PHP "CURL" không được kích hoạt. Liên hệ với nhà cung cấp dịch vụ lưu trữ của bạn. ';
$_['error_zip'] = 'Cần tải phần mở rộng ZIP. Liên hệ với nhà cung cấp dịch vụ lưu trữ của bạn. ';
$_['error_mbstring'] = 'Thư viện PHP "chuỗi mb" không được kích hoạt. Liên hệ với nhà cung cấp dịch vụ lưu trữ của bạn. ';
$_['error_oc_version'] = 'Phiên bản OpenCart của bạn chưa được kiểm tra để hoạt động với mô-đun này. Bạn có thể gặp vấn đề. ';
// Cứu giúp
$_['help_clear_faq'] = 'Hiển thị lại tất cả các thông báo trợ giúp';
$_['help_empty_data'] = 'Điều này có thể gây ra thiệt hại nghiêm trọng, không sử dụng nó nếu bạn không biết nó có tác dụng gì!';
$_['help_easy_update'] = 'Nhấp vào cập nhật để tự động cài đặt phiên bản mới nhất của OpenBay Pro';
$_['help_patch'] = 'Nhấp để chạy tập lệnh vá';
$_['help_beta'] = 'Chú ý! Phiên bản beta là phiên bản phát triển mới nhất. Nó có thể không ổn định và có thể chứa lỗi. ';