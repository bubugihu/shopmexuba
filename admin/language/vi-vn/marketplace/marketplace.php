<?php
// Tiêu đề
$_['heading_title'] = 'Thị trường mở rộng';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi phần mở rộng!';
$_['text_list'] = 'Danh sách mở rộng';
$_['text_filter'] = 'Bộ lọc';
$_['text_search'] = 'Tìm kiếm các tiện ích mở rộng và chủ đề';
$_['text_category'] = 'Danh mục';
$_['text_all'] = 'Tất cả';
$_['text_theme'] = 'Chủ đề';
$_['text_marketplace'] = 'Thị trường';
$_['text_language'] = 'Ngôn ngữ';
$_['text_payment'] = 'Thanh toán';
$_['text_shipping'] = 'Vận chuyển';
$_['text_module'] = 'Mô-đun';
$_['text_total'] = 'Tổng đơn hàng';
$_['text_feed'] = 'Nguồn cấp dữ liệu';
$_['text_report'] = 'Báo cáo';
$_['text_other'] = 'Khác';
$_['text_free'] = 'Miễn phí';
$_['text_paid'] = 'Đã trả tiền';
$_['text_purchased'] = 'Đã mua';
$_['text_date_modified'] = 'Ngày sửa đổi';
$_['text_date_added'] = 'Ngày thêm';
$_['text_rating'] = 'Xếp hạng';
$_['text_reviews'] = 'đánh giá';
$_['text_compatibility'] = 'Tương thích';
$_['text_downloaded'] = 'Đã tải xuống';
$_['text_member_since'] = 'Thành viên kể từ:';
$_['text_price'] = 'Giá';
$_['text_partner'] = 'Được phát triển bởi Đối tác OpenCart';
$_['text_support'] = 'Hỗ trợ miễn phí 12 tháng';
$_['text_documentation'] = 'Tài liệu đi kèm';
$_['text_sales'] = 'Bán hàng';
$_['text_comment'] = 'Nhận xét';
$_['text_download'] = 'Đang tải xuống';
$_['text_install'] = 'Đang cài đặt';
$_['text_comment_add'] = 'Để lại bình luận của bạn';
$_['text_write'] = 'Viết bình luận của bạn ở đây ..';
$_['text_purchase'] = 'Vui lòng xác nhận bạn là ai!';
$_['text_pin'] = 'Vui lòng nhập số PIN 4 chữ số của bạn. Số PIN này là để bảo vệ tài khoản của bạn. ';
$_['text_secure'] = 'Không cung cấp mã PIN của bạn cho bất kỳ ai kể cả nhà phát triển. Nếu bạn yêu cầu người bán tiện ích mở rộng cài đặt tiện ích mở rộng cho bạn thì bạn nên gửi email cho họ phần mở rộng cần thiết. ';
$_['text_name'] = 'Tên tải xuống';
$_['text_progress'] = 'Tiến trình';
$_['text_available'] = 'Số lượt cài đặt sẵn có';
$_['text_action'] = 'Hành động';
// Mục nhập
$_['entry_pin'] = 'PIN';

// Chuyển hướng
$_['tab_description'] = 'Mô tả';
$_['tab_documentation'] = 'Tài liệu';
$_['tab_download'] = 'Tải xuống';
$_['tab_comment'] = 'Bình luận';

// Cái nút
$_['button_opencart'] = 'API Marketplace';
$_['button_purchase'] = 'Mua';
$_['button_view_all'] = 'Xem tất cả các tiện ích mở rộng';
$_['button_get_support'] = 'Nhận hỗ trợ';
$_['button_comment'] = 'Bình luận';
$_['button_reply'] = 'Trả lời';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi phần mở rộng!';
$_['error_opencart'] = 'Cảnh báo: Bạn phải nhập thông tin API OpenCart của mình trước khi có thể thực hiện bất kỳ giao dịch mua nào!';
$_['error_install'] = 'Quá trình cài đặt tiện ích mở rộng đang diễn ra, vui lòng đợi vài giây trước khi thử cài đặt!';
$_['error_purchase'] = 'Không thể mua tiện ích mở rộng!';
$_['error_download'] = 'Không thể tải xuống phần mở rộng!';