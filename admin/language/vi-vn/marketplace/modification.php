<?php
// Tiêu đề
$_['heading_title'] = 'Các sửa đổi';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi các sửa đổi!';
$_['text_refresh'] = 'Bất cứ khi nào bạn bật / tắt hoặc xóa sửa đổi, bạn cần nhấp vào nút làm mới để tạo lại bộ đệm sửa đổi của mình!';
$_['text_list'] = 'Danh sách sửa đổi';

// Cột
$_['column_name'] = 'Tên sửa đổi';
$_['column_author'] = 'Tác giả';
$_['column_version'] = 'Phiên bản';
$_['column_status'] = 'Trạng thái';
$_['column_date_added'] = 'Ngày Thêm';
$_['column_action'] = 'Hành động';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi các sửa đổi!';