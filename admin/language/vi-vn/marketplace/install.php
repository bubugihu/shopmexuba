<?php
// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi phần mở rộng!';
$_['text_unzip'] = 'Giải nén tập tin!';
$_['text_move'] = 'Đang sao chép tập tin!';
$_['text_xml'] = 'Đang áp dụng các sửa đổi!';
$_['text_remove'] = 'Xóa các tập tin tạm thời!';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi phần mở rộng!';
$_['error_install'] = 'Quá trình cài đặt tiện ích mở rộng đang diễn ra, vui lòng đợi vài giây trước khi thử cài đặt!';
$_['error_unzip'] = 'Không thể mở tệp zip!';
$_['error_file'] = 'Không tìm thấy tệp cài đặt!';
$_['error_directory'] = 'Không tìm thấy thư mục cài đặt!';
$_['error_code'] = 'Cần có mã duy nhất để sửa đổi XML!';
$_['error_xml'] = 'Sửa đổi %s đã được sử dụng!';
$_['error_exists'] = 'Tệp %s đã tồn tại!';
$_['error_allowed'] = 'Thư mục %s không được phép ghi vào!';