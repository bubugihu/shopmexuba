<?php
// Tiêu đề
$_['heading_title'] = 'Sự kiện';

// Bản văn
$_['text_success'] = 'Thành công: Bạn đã sửa đổi các sự kiện!';
$_['text_list'] = 'Danh sách sự kiện';
$_['text_event'] = 'Các sự kiện được tiện ích mở rộng sử dụng để ghi đè chức năng mặc định của cửa hàng của bạn. Nếu bạn gặp sự cố, bạn có thể tắt hoặc bật các sự kiện tại đây. ';
$_['text_info'] = 'Thông tin sự kiện';
$_['text_trigger'] = 'Trình kích hoạt';
$_['text_action'] = 'Hành động';

// Cột
$_['column_code'] = 'Mã sự kiện';
$_['column_status'] = 'Trạng thái';
$_['column_sort_order'] = 'Sắp xếp Thứ tự';
$_['column_action'] = 'Hành động';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi phần mở rộng!';