<?php
// Heading
$_['heading_title']    = 'Thị trường API OpenCart';

// Text
$_['text_success']     = 'Thành công: Bạn đã sửa đổi thông tin API của mình!';
$_['text_signup']      = 'Vui lòng nhập thông tin API OpenCart của bạn mà bạn có thể lấy <a href="https://www.opencart.com/index.php?route=account/store" target="_blank" class="alert-link">ở đây</a>.';

// Entry
$_['entry_username']   = 'Tên đăng nhập';
$_['entry_secret']     = 'Mã bí mật';

// Error
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi API thị trường!';
$_['error_username']   = 'Lỗi Tên đăng nhập!';
$_['error_secret']     = 'Lỗi mã bí mật!';
