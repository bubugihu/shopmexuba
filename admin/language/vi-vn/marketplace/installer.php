<?php
// Tiêu đề
$_['heading_title'] = 'Trình cài đặt tiện ích mở rộng';

// Bản văn
$_['text_progress'] = 'Tiến trình cài đặt';
$_['text_upload'] = 'Tải lên tiện ích mở rộng của bạn';
$_['text_history'] = 'Lịch sử cài đặt';
$_['text_success'] = 'Thành công: Phần mở rộng đã được cài đặt!';
$_['text_install'] = 'Đang cài đặt';

// Cột
$_['column_filename'] = 'Tên tập tin';
$_['column_date_added'] = 'Ngày Thêm';
$_['column_action'] = 'Hành động';

// Mục nhập
$_['entry_upload'] = 'Tải lên tập tin';
$_['entry_progress'] = 'Tiến trình';

// Cứu giúp
$_['help_upload']       = 'Yêu cầu tệp sửa đổi có phần mở rộng \'.ocmod.zip\'.';

// Lỗi
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi phần mở rộng!';
$_['error_install'] = 'Quá trình cài đặt tiện ích mở rộng đang diễn ra, vui lòng đợi vài giây trước khi thử cài đặt!';
$_['error_upload'] = 'Không thể tải tệp lên!';
$_['error_filetype'] = 'Loại tệp không hợp lệ!';
$_['error_file'] = 'Không tìm thấy tệp!';