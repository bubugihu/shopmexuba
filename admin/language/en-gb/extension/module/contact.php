<?php
// Heading
$_['heading_title']    = 'Contact';

$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified Contact module!';
$_['text_edit']        = 'Edit Contact Module';
$_['text_front_url']        = 'Contact URL';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Contact module!';