<?php
// Text
$_['text_mpblog']	 		 		= 'M-Blog';
$_['text_mpblogsetting']	 		= 'Settings';
$_['text_mpblogdashboard']	 		= 'Dashboard';
$_['text_mpblog_category']			= 'Categories';
$_['text_mpblog_post']	 	 		= 'Posts';
$_['text_mpblog_comment']	 		= 'Comments';
$_['text_mpblog_rating']            = 'Rating';

$_['text_mpblog_subscriber']	 		= 'Subscribers';
