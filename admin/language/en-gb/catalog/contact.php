<?php
// Heading
$_['heading_title']     = 'Contact';

// Text
$_['text_success']              = 'Success: You have modified Contact!';
$_['text_default']              = 'Default';
$_['text_image_manager']        = 'Image Manager';

// Column
$_['column_id']             = 'ID';
$_['column_title']	        = 'Title';
$_['column_fullname']	    = 'FullName';
$_['column_email']	        = 'Email';
$_['column_date']	        = 'Date';
$_['column_action']         = 'Action';

// Entry
$_['entry_title']       = 'Title:';
$_['entry_description'] = 'FeedBack:';
//add
$_['entry_fullname']    = 'Full Name:';
$_['entry_phone']       = 'Phone:';
$_['entry_email']       = 'Email:';
//end add
$_['entry_store']                   = 'Stores:';
$_['entry_keyword']                 = 'SEO Keyword:';
$_['entry_status']                  = 'Status:';
$_['entry_sort_order']              = 'Sort Order:<br/><span class="help">Set to -1 to hide from listing</span>';
$_['entry_layout']                  = 'Layout Override:';
$_['entry_date']                    = 'Contact Date:';
$_['entry_allow_subscribe']         = 'Allow subscription?:';
$_['subscribers']                   = 'Subscribers';
$_['allow_no_subscribers']          = 'Allow No of Subscribers';
$_['entry_image']                   = 'Image';
$_['entry_products']                = 'Attach Products';

$_['text_browse']                   = 'Browse';
$_['text_clear']                    = 'Clear';

// Error 
$_['error_warning']     = 'Warning: Please check the form carefully for errors!';
$_['error_permission']  = 'Warning: You do not have permission to modify Contact!';
$_['error_title']       = 'Contact Title must be between 3 and 64 characters!';
$_['error_description'] = 'Description must be between 3 characters!';
$_['error_account']     = 'Warning: This Contact page cannot be deleted as it is currently assigned as the store account terms!';
$_['error_checkout']    = 'Warning: This Contact page cannot be deleted as it is currently assigned as the store checkout terms!';
$_['error_affiliate']   = 'Warning: This Contact page cannot be deleted as it is currently assigned as the store affiliate terms!';
$_['error_store']       = 'Warning: This Contact page cannot be deleted as its currently used by %s stores!';

//form
$_['tab_general']                   = 'Feedback';
$_['text_home']                     = 'Catalog';
$_['tab_data']                      = 'Reply';
$_['entry_shop']                    = 'Nucos Việt Nam';
?>